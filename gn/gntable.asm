; **************************************************************************************************
; GN System Call Jump Table & Miscellaneous internal GN support routines.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************; -----------------------------------------------------------------------------

        Module GNTable

        include "oz.def"

        org     GN_TBL

xref    GN_ret1b

xref    GNAab
xref    GNAlp
xref    GNCl
xref    GNCls
xref    GNCme
xref    GND16
xref    GND24
xref    GNDei
xref    GNDel
xref    GNDie
xref    GNErr
xref    GNEsa
xref    GNEsp
xref    GNFab
xref    GNFcm
xref    GNFex
xref    GNFlc
xref    GNFlf
xref    GNFlo
xref    GNFlr
xref    GNFlw
xref    GNFpb
xref    GNGdn
xref    GNGdt
xref    GNGmd
xref    GNGmt
xref    GNGtm
xref    GNLab
xref    GNM16
xref    GNM24
xref    GNMsc
xref    GNNln
xref    GNOpf
xref    GNOpw
xref    GNPdn
xref    GNPdt
xref    GNPfs
xref    GNPmd
xref    GNPmt
xref    GNPrs
xref    GNPtm
xref    GNRbe
xref    GNRen
xref    GNSdo
xref    GNSip
xref    GNSkc
xref    GNSkd
xref    GNSkt
xref    GNSoe
xref    GNSop
xref    GNUab
xref    GNWbe
xref    GNWcl
xref    GNWfn
xref    GNWsm
xref    GNXdl
xref    GNXin
xref    GNXnx
xref    GNWin
xref    GNCrc
xref    GNGab
xref    GNLdm

;       ----

; GN Call table begins at $8400 (after CRC Table)

        jp      GN_ret1b
        jp      GN_ret1b                        

        defw    GNGdt                           ; $0609
        defw    GNPdt                           ; $0809
        defw    GNGtm                           ; $0A09
        defw    GNPtm                           ; $0C09

        defw    GNSdo                           ; $0E09
        defw    GNGdn                           ; $1009
        defw    GNPdn                           ; $1209
        defw    GNDie                           ; $1409
        defw    GNDei                           ; $1609
        defw    GNGmd                           ; $1809
        defw    GNGmt                           ; $1A09
        defw    GNPmd                           ; $1C09
        defw    GNPmt                           ; $1E09
        defw    GNMsc                           ; $2009

        defw    GNFlo                           ; $2209
        defw    GNFlc                           ; $2409
        defw    GNFlw                           ; $2609
        defw    GNFlr                           ; $2809
        defw    GNFlf                           ; $2A09
        defw    GNFpb                           ; $2C09
        defw    GNNln                           ; $2E09
        defw    GNCls                           ; $3009
        defw    GNSkc                           ; $3209
        defw    GNSkd                           ; $3409
        defw    GNSkt                           ; $3609
        defw    GNSip                           ; $3809
        defw    GNSop                           ; $3A09
        defw    GNSoe                           ; $3C09
        defw    GNRbe                           ; $3E09
        defw    GNWbe                           ; $4009
        defw    GNCme                           ; $4209
        defw    GNXnx                           ; $4409
        defw    GNXin                           ; $4609
        defw    GNXdl                           ; $4809
        defw    GNErr                           ; $4A09
        defw    GNEsp                           ; $4C09
        defw    GNFcm                           ; $4E09
        defw    GNFex                           ; $5009
        defw    GNOpw                           ; $5209
        defw    GNWcl                           ; $5409
        defw    GNWfn                           ; $5609
        defw    GNPrs                           ; $5809
        defw    GNPfs                           ; $5A09
        defw    GNWsm                           ; $5C09
        defw    GNEsa                           ; $5E09
        defw    GNOpf                           ; $6009
        defw    GNCl                            ; $6209
        defw    GNDel                           ; $6409
        defw    GNRen                           ; $6609
        defw    GNAab                           ; $6809
        defw    GNFab                           ; $6A09
        defw    GNLab                           ; $6C09
        defw    GNUab                           ; $6E09
        defw    GNAlp                           ; $7009

        defw    GNM16                           ; $7209
        defw    GND16                           ; $7409
        defw    GNM24                           ; $7609
        defw    GND24                           ; $7809
        defw    GNWin                           ; $7A09
        defw    GNCrc                           ; $7C09

        defw    GNGab                           ; $7E09
        defw    GNLdm                           ; $8009
