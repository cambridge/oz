; **************************************************************************************************
; GN System calls, located in segment 3
; GN_Crc implementation, Copyright (C) Gunther Strube & Garry Lancaster 2014
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************


     module GN_Crc32

     include "fileio.def"
     include "integer.def"
     include "memory.def"
     include "crc32.def"
     include "eprom.def"
     include "error.def"

     xdef GNCrc

     xref crctable                              ; gn.asm
     xref bix_s1s2, box_s1s2                    ; gnsopsoe.asm
     xref Divu24                                ; gnmath.asm
     xref Ld_A_HL                               ; gnmisc1.asm      
     xref PutOsf_BC, PutOsf_DE, PutOsf_Err      ; gnmisc3.asm
     xref GetOsf_BC, GetOsf_DE, PutOsf_BHL      ; gnmisc3.asm


; *************************************************************************************
.GNCrc
        or      a
        jr      z,CrcMemoryArea                 ; CRC_MEMA
        dec     a
        jr      z,CrcRamFile                    ; CRC_FILE
        dec     a
        jr      z,CrcRamFileByteIO              ; CRC_PARTFILEBYTE
        dec     a
        jp      z,CrcRamFilePartial             ; CRC_PARTFILEBUF
        dec     a
        jp      z,FileEprFileCrc32              ; CRC_EPFILE
        dec     a
        jp      z,FileEprFilePartCrc32          ; CRC_PARTEPFILE
.crc_bad_arguments        
        ld      a,RC_BAD
        jp      PutOsf_Err                      ; One or more bad arguments, return A = RC_BAD, Fc = 1
; *************************************************************************************

          

; *************************************************************************************
;
; Check that HL = pointer to (local) buffer is not 0
; Check that BC = size of buffer is not 0
;
.CheckBuffArgs
        ld      a,h
        or      l
        jr      z,crc_bad_arguments             ; HL must point to a local buffer
        ld      a,b
        or      c
        jr      z,crc_bad_arguments             ; size of buffer = 0!
        ret
; *************************************************************************************



; *************************************************************************************
;
; Perform CRC-32 of specified memory area and returned checksum in DEBC.
; Memory should not cross bank boundaries.
;
; In:
;    A  = 0, Iterate CRC-32 over local memory area 
;    HL = pointer to buffer (local memory),
;    BC = memory area size
;    DE = 0, Initialize CRC-32 with FFFFFFFFh
;    DE > 0, (local) pointer to initial 4-byte CRC-32 (litte endian format)
; Out:
;    DEBC = CRC-32
;
; Example: 
;    The string "The quick brown fox jumps over the lazy dog" returns 0x414FA339
;    (DE = 0x414F, BC = 0xA339)
;
; Registers changed after return:
;    ......HL/IXIY same
;    AFBCDE../.... different
;
.CrcMemoryArea
        call    CheckBuffArgs
        ret     z                               ; one of the buffer args were 0, signal Bad args & return

        call    InitCrc                         ; initialize D'E'B'C', based on DE (input)

        push    bc
        call    bix_s1s2                        ; ensure HL pointer is accessible in s0, s1 or s2
        pop     bc                              ; size of memory area to CRC-32 scan
        push    de                              ; remember old S1S2 bindings

        call    CrcIterateBuffer
        call    ReturnCrcResult                 ; D'E'B'C' -> DEBC to caller

        pop     bc                              ; restore S1 + S2 bindings
        jp      box_s1s2
; *************************************************************************************



; *************************************************************************************
;
; Perform a CRC-32 of RAM file, already opened by caller, from current file pointer until EOF.
; If the complete file is to be CRC'ed then it is vital that the current file pointer
; is at the beginning of the file (use FA_PTR / OS_FWM to reset file pointer) before
; executing this routine.
;
; This routine utilizes OS_Mv to read from file.
;
; In:
;    A = CRC_FILE, Iterate CRC-32 of open file (from current file pointer until EOF)
;    HL = pointer to file buffer (local memory),
;    BC = buffer size 
;    DE = 0, Initialize CRC-32 with FFFFFFFFh
;    DE > 0, (local) pointer to initial 4-byte CRC-32
;    IX = file handle of open file (using GN_Opf) 
;
; Out:
;    Fc = 0,
;       DEBC = CRC-32
;    Fc = 1,
;       A = RC_xxx (file & handle related errors)
;
; Registers changed after return:
;    ......HL/IXIY same
;    AFBCDE../.... different
;
.CrcRamFile         
        ld      a, FA_Eof
        oz      OS_Frm
        jp      c, PutOsf_Err                   ; bad IX file handle

        call    CheckBuffArgs
        ret     z                               ; one of the buffer args were 0, signal Bad args & return

        call    InitCrc                         ; initialise CRC registers D'E'B'C' according to input

        push    bc
        call    bix_s1s2                        ; ensure HL pointer is accessible in s0, s1 or s2
        pop     bc                              ; size of memory area to CRC-32 scan
        push    de                              ; remember old S1S2 bindings

        ex      de,hl                           ; DE points to intermediate file buffer in s0, s1 or s2
        call    scanfile
        call    z,ReturnCrcResult               ; EOF reached, get current CRC in D'E'B'C' and complement it then return as DEBC to caller                

        pop     bc                              ; restore S1 + S2 bindings
        jp      box_s1s2

.scanfile
        call    Scanbuffer
        ret     c                               ; RC_xxx error occurred
        ret     z                               ; EOF reached
        jr      scanfile
; *************************************************************************************



; *************************************************************************************
;
; Perform a partial CRC-32 of RAM file, already opened by caller, from current file 
; pointer until EOF.
; If the complete file is to be CRC'ed then it is vital that the current file pointer
; is at the beginning of the file (use FA_PTR / OS_FWM to reset file pointer) before
; executing this routine.
;
; This routine utilizes OS_Gb to read from file.
;
; In:
;    HL = max bytes to read, or -1 for rest of file
;    DEBC = Initial CRC-32 (typically FFFFFFFFh)
;    IX = file handle of open file (using GN_Opf) 
;
; Out:
;    Fc = 0,
;       DEBC = CRC-32
;    Fc = 1,
;       A = RC_xxx (file & handle related errors)
;       RC_Eof is always set when complete CRC scan is done
;
; Registers changed after return:
;    ......HL/IXIY same
;    AFBCDE../.... different
;
.CrcRamFileByteIO
        ld      a, FA_Eof
        oz      OS_Frm
        jp      c, PutOsf_Err                   ; bad IX file handle

.init_scanfileByteIO
        push    bc
        push    de
        exx
        pop     de
        pop     bc
        call    preserve_crc                    ; installed inital CRC-32 into D'E'B'C'

.scanfile_nobuf                                 ; scan RAM file, without buffer, byte-by-byte
        call    oz_os_gb                        ; read byte from file (and preserve alternate bc, de)
        jr      nc, scanfile_crcbyte
        call    PutOsf_Err                      ; store return API, EOF / file I/O error occurred...
        cp      RC_Eof
        jp      z,ReturnCrcResult               ; EOF reached, get current CRC in D'E'B'C', complement, and return to caller
        ret
.scanfile_crcbyte
        call    Crc32Byte                       ; calculate CRC-32 from A in D'E'B'C'

        ld      a,$ff
        cp      h
        jr      nz, read_max_bytes
        cp      l
        jr      nz, read_max_bytes
        jr      scanfile_nobuf                  ; HL = -1, keep reading from file until EOF
.read_max_bytes
        dec     hl
        ld      a,h
        or      l
        jr      nz,scanfile_nobuf               ; max bytes not yet reached, continue processing
.ReturnCrcPartial
        exx
        call    PutOsf_BC
        call    PutOsf_DE                       ; return partical CRC-32 (non-complemented!) result in D'E'B'C' to caller
        cp      a                               ; ensure Fc = 0, Fz = 0
        ret
; *************************************************************************************



; *************************************************************************************
;
; Perform a partial CRC-32 of RAM file, already opened by caller, from current file 
; pointer until EOF.
; If the complete file is to be CRC'ed then it is vital that the current file pointer
; is at the beginning of the file (use FA_PTR / OS_FWM to reset file pointer) before
; executing this routine.
;
; This routine utilizes OS_Mv to buffer-read from file.
;
; In:
;    A = CRC_PARTFILEBUF, Iterate CRC-32 of open file (from current file pointer until EOF)
;    HL = pointer to file buffer (local memory),
;    BC = buffer size 
;    DE = 0, Initialize CRC-32 with FFFFFFFFh
;    DE > 0, (local) pointer to initial 4-byte CRC-32
;    IX = file handle of open file (using GN_Opf) 
;
; OUT, if call successful:
;     Fc = 0
;     DEBC = partial CRC-32 result (DE = high-word, BC = low-word), to be used for next scan
; 
;     Fc = 1, A _RC_Eof
;     DEBC = final CRC-32 result (DE = high-word, BC = low-word)
;  
; OUT, if call failed:
;     Fc = 1,
;     A = RC_BAD ($04) - bad argument
;     A = RC_xxx (file & handle related errors)
;
; Registers changed after return:
;    ......HL/IXIY same
;    AFBCDE../.... different
;
.CrcRamFilePartial
        ld      a, FA_Eof
        oz      OS_Frm
        jp      c, PutOsf_Err                   ; bad IX file handle

        call    CheckBuffArgs
        ret     z                               ; one of the buffer args were 0, signal Bad args & return

        call    InitCrc                         ; initialise CRC registers D'E'B'C' according to input

        push    bc
        call    bix_s1s2                        ; ensure HL pointer is accessible in s0, s1 or s2
        pop     bc                              ; size of memory area to CRC-32 scan
        push    de                              ; remember old S1S2 bindings

        ex      de,hl                           ; DE points to intermediate file buffer in s0, s1 or s2
        call    Scanbuffer

        call    z,ReturnCrcResult               ; EOF reached, get current CRC in D'E'B'C' and complement it then return as DEBC to caller
        ld      a, RC_Eof
        call    z,PutOsf_Err                    ; indicate Fc = 1 & EOF to caller if 

        call    nz,ReturnCrcPartial             ; block has been fetched, return current partial CRC in D'E'B'C' 

        pop     bc                              ; restore S1 + S2 bindings
        jp      box_s1s2
; *************************************************************************************



; *************************************************************************************
;
; Standard Z88 File Eprom Format.
;
; Generate CRC-32 of file entry in file area.
;
; IN:
;    BHL = pointer to file entry to be CRC-32 scanned
;    DE = 0, Initialize CRC-32 with FFFFFFFFh
;    DE > 0, (local) pointer to initial 4-byte CRC-32 (litte endian format)
;
; OUT:
;   Fc = 0,
;   DEBC = CRC-32
;   Fc = 1,
;   File Entry at BHL was not found.
;   A = RC_Onf
;
; Registers changed on return:
;    ......../IXIY ........ same
;    AFBCDEHL/.. afbcdehl different
;
; -------------------------------------------------------------------------
; Design & Programming by Gunther Strube, June 2012. GN_Crc adpation 2014
; -------------------------------------------------------------------------
;
.FileEprFileCrc32
        ld      a,EP_Size
        oz      OS_Epr                          
        jp      c, PutOsf_Err                   ; File entry not recognised, exit with error...        

        ld      a,b
        exx
        ld      b,a
        ld      c,ms_s1
        rst     OZ_Mpb                          ; bind in bank at c = safe ms_s1 segment
        push    bc                              ; preserve original S1 binding for exit
        exx

        ld      a,EP_Image
        oz      OS_Epr                          ; adjust BHL pointer to first byte of file image (beyond file entry header)
        res     7,h
        set     6,h                             ; BHL pointer adjusted for reading bank in segment 1

        push    de
        call    GetOsf_DE                       ; get caller DE
        call    InitCrc                         ; initialize D'E'B'C', based on DE
        pop     de

.crc32_feentry_loop                             ; Now, CRC-32-scan the entire file entry image
        call    CheckFeFileSize                 ; File Entry size in CDE = 0?
        jr      z,return_final_crc32            ; Yes

        ld      a,(hl)
        call    Crc32Byte                       ; calculate CRC-32 result in D'E'B'C' 
        call    IncEprFptr                      ; BHL++ 
        call    DecFileSize                     ; CDE--
        jr      crc32_feentry_loop
.return_final_crc32
        pop     bc
        rst     OZ_Mpb                          ; restore caller bank binding
        jp      ReturnCrcResult                 ; complete the final CRC-32 result in DEBC to caller
; *************************************************************************************



; *************************************************************************************
;
; Standard Z88 File Eprom Format.
;
; Partly generate CRC-32 of File Entry in file area.
;
; IN:
;    Initial call:
;    BHL = pointer to File Entry
;    DE = max. size of bytes to read per partial scan
;    IX = pointer to local memory (allocated in application/popdown workspace, 12 bytes):
;    (IX + 0,3)   : Initial CRC-32 or input from result of previous partial CRC-32 scan
;    (IX + 4,6)   : Reserved for 24bit file pointer (initialized after first call)
;    (IX + 7,9)   : Reserved for size of file entry (initialized after first call)
;    (IX + 10,11) : Reserved for max. bytes to read per partial scan
;
;    Subsequent calls:
;    BHL = 0
;    IX = pointer to updated local memory of CRC-32 variables (12 bytes):
;
; OUT:
;    Fc = 0
;    BHL = 0 (always)
;    (IX + 0,3)   : partial CRC-32 result, low-byte, high-byte order
;    (IX + 4,6)   : 24bit file pointer updated for next partial scan
;    (IX + 7,9)   : Remaining file size updated for next partial scan
;    (IX + 10,11) : Updated max. bytes to read per partial scan
;
;    Fc = 1
;    BHL = 0 (always)
;    A = RC_Eof (complete file scanned)
;    (IX + 0,3)   : final CRC-32 result, low-byte, high-byte order;
;
;    Fc = 1
;    A = RC_xxx (System error occurred - file entry corrupt, not found, etc.)
;
; Registers changed on return:
;    ......../IXIY ........ same
;    AFBCDEHL/.... afbcdehl different
;
; IX variable offsets:
defc flentry_crc32 = 0                          ; (4 bytes) Partial / final CRC32 of File entry
defc flentry_fptr = 4                           ; (3 bytes) Current file pointer of File Entry (set to 0 on first call)
defc flentry_size = 7                           ; (3 bytes) Size of file entry (initialized by first call)
defc flentry_maxbytes = 10                      ; (2 bytes) Max bytes to scan per partial CRC-32 calculation
;
.FileEprFilePartCrc32
        push    ix
        exx
        pop     de
        ld      a,d
        or      e
        exx
        jp      z,crc_bad_arguments             ; IX must point to a File Entry information block, not 0!

        xor     a
        or      l
        or      h
        or      b                               ; BHL <> 0, subsequent block to be scanned?
        jr      nz, first_block


        ld      l,(ix + flentry_fptr+0)
        ld      h,(ix + flentry_fptr+1)
        ld      b,(ix + flentry_fptr+2)         ; current file pointer to file image data   
        exx
        ld      c,(ix + flentry_crc32+0)
        ld      b,(ix + flentry_crc32+1)
        ld      e,(ix + flentry_crc32+2)
        ld      d,(ix + flentry_crc32+3)        ; install partial CRC-32 from previous run
        exx     
        jr      continue_fe_scan

.first_block
        ld      (ix + flentry_maxbytes+0),e
        ld      (ix + flentry_maxbytes+1),d     ; preserve max bytes to read for subsequent calls

        ld      a,EP_Size
        oz      OS_Epr                          ; get size of File entry in CDE
        jp      c, PutOsf_Err                   ; File entry not recognised, exit with error...
        call    preserve_fe_size

        ld      a,EP_Image
        oz      OS_Epr                          ; adjust BHL pointer to first byte of file image (beyond file entry header)

        ld      de,0
        call    InitCrc                         ; first block scan, initialize D'E'B'C' to FFFFFFFFh
.continue_fe_scan

        ld      c,ms_s1
        res     7,h
        set     6,h                             ; BHL pointer adjusted for reading bank in segment 1
        ld      a,b
        rst     OZ_Mpb                          ; bind in bank at c = safe ms_s1 segment
        push    bc                              ; preserve original S1 binding for exit
        ld      b,a                             ; BHL points at first byte of file image block

        ld      e,(ix + flentry_size+0)
        ld      d,(ix + flentry_size+1)
        ld      c,(ix + flentry_size+2)         ; intialize file entry size for block loop scan

        exx
        ld      l,(ix + flentry_maxbytes+0)
        ld      h,(ix + flentry_maxbytes+1)     ; hl' = max bytes (block) to read 
        exx

.fe_scan_block
        call    CheckFeFileSize                 ; File Entry size in CDE = 0?
        jr      z,fe_partial_scan_eof           ; Yes, complete the final CRC-32 result in D'E'B'C' to caller
        exx
        ld      a,h
        or      l
        push    hl
        exx
        jr      z,fe_partial_scan_block         ; max bytes scanned of block, return partial CRC-32

        ld      a,(hl)
        call    Crc32Byte                       ; calculate CRC-32 result in D'E'B'C' 
        call    IncEprFptr                      ; BHL++ 
        call    DecFileSize                     ; CDE--
        exx
        pop     hl
        dec     hl                              ; blocksize--
        exx
        jr      fe_scan_block   
.fe_partial_scan_block
        pop     af
        exx
        jr      preserve_crc32
.fe_partial_scan_eof
        ld      a, RC_Eof
        call    PutOsf_Err                      ; indicate Fc = 1 & EOF to caller when complete file has been scanned
        call    ReturnCrcResult
.preserve_crc32        
        ld      (ix + flentry_crc32+0),c
        ld      (ix + flentry_crc32+1),b
        ld      (ix + flentry_crc32+2),e
        ld      (ix + flentry_crc32+3),d        ; update CRC-32 into runtime variable area fro caller
        exx
        call    preserve_fe_size                ; preserve updated remaining file size in (IX + flentry_size)
        call    preserve_fptr                   ; preserve updated File pointer in (IX + flentry_fptr)
        ld      b,0
        ld      h,b
        ld      l,b
        call    PutOsf_BHL                      ; always return BHL = 0 to caller
        pop     bc
        rst     oz_mpb                          ; restore original bank binding (application level)
        cp      a
        ret
; *************************************************************************************



; *************************************************************************************
;
; Load contents of file (from current file pointer) into buffer and CRC-32 iterate
; the buffer.
;
; IN:
;   DE = pointer to buffer
;   BC = size of buffer
;
; Returns:
;   Fc = 1, A = RC_xxx error (file I/O error)
;   Fz = 1, if EOF has been reached
;   D'E'B'C' contains updated CRC-32 checksum
;
; Registers changed after return:
;    ..BCDEHL/IXIY/af...... same
;    AF....../..../..bcdehl different
;
.Scanbuffer
        push    bc
        push    de
        push    bc
        ld      hl,0
        call    oz_os_mv                        ; read bytes from file (and preserve alternate bc, de)
        pop     hl
        jr      nc, crc32scan_buffer
        cp      RC_Eof
        jr      nz, scanbuff_crcerr             ; file I/O error occurred...  
.crc32scan_buffer                               ; EOF reached (probably buffer size was bigger than remaining file)...  
        cp      a
        sbc     hl,bc
        jr      z,scanbuff_crcend               ; EOF reached, no bytes read...
        ld      b,h
        ld      c,l                             ; BC=#bytes actually read
        pop     hl
        push    hl
        call    CrcIterateBuffer                ; accumulate CRC on current value in D'E'B'C'
        pop     de
        pop     bc
        set     1,a
        or      a                               ; Fz = 0, buffer fetched succesfully, not EOF
        ret                 
.scanbuff_crcerr
        pop     bc
        pop     bc
        scf
        jp      PutOsf_Err                      ; Fc = 1, Fz = 0, file I/O error occurred...
.scanbuff_crcend
        pop     af
        pop     af
        cp      a                               ; signal EOF
        ret        
; *************************************************************************************



; *************************************************************************************
; Call OS_Gb and preserve current CRC-32 in in D'E'B'C'
.oz_os_gb
        exx
        push    bc
        push    de
        exx
        oz      OS_Gb
        exx
        pop     de
        pop     bc
        exx
        ret
; *************************************************************************************



; *************************************************************************************
; Call OS_Mv and preserve current CRC-32 in in D'E'B'C'
.oz_os_mv
        exx
        push    bc
        push    de
        exx
        oz      OS_mv
        exx
        pop     de
        pop     bc
        exx
        ret
; *************************************************************************************



; *************************************************************************************
.InitCrc
        ld      a,d
        or      e
        jr      nz,crc_from_mem                 ; DE > 0, read CRC-32 from (DE)
        exx
        ld      bc,$ffff                        ; initialise CRC register D'E'B'C' to FFFFFFFFh
        ld      d,b
        ld      e,c
.preserve_crc
        call    PutOsf_BC
        call    PutOsf_DE                       ; also preserve a copy to GN return registers
        exx
        ret
.crc_from_mem
        push    de
        exx
        pop     hl                              ; initialise CRC register D'E'B'C' from (HL)
        call    Ld_A_HL
        ld      c,a
        inc     hl
        call    Ld_A_HL
        ld      b,a
        inc     hl
        call    Ld_A_HL
        ld      e,a
        inc     hl
        call    Ld_A_HL
        ld      d,a
        jr      preserve_crc
; *************************************************************************************



; *************************************************************************************
;
; Iterate current CRC-32 with memory area in HL (available in s0, s1 or s2), size BC
;
; In:
;    HL       = pointer to start of memory area
;    BC       = size of memory area
;    D'E'B'C' = current CRC-32
; Out:
;    BC       = 0
;    HL       = end of memory area + 1
;    D'E'B'C' = updated
;
; Registers changed after return:
;    ....DE../....../IXIY same
;    AFBC..HL/bcdehl/.... different
;
.CrcIterateBuffer
        ld      a,(hl)                          ; get byte from memory area
        inc     hl
        call    Crc32Byte                       ; calculate CRC-32
        dec     bc
        ld      a,b
        or      c
        jr      nz,CrcIterateBuffer             ; back for more
        ret
; *************************************************************************************



; *************************************************************************************
;
; Iterate current CRC-32 with byte value in A
;
; In:
;    A        = byte to iterate CRC-32
;    D'E'B'C' = current CRC-32
; Out:
;    D'E'B'C' = updated
;
; Registers changed after return:
;    ..BCDEHL/....../IXIY same
;    AF....../bcdehl/.... different
;
.Crc32Byte                    
        exx
        xor     c
        ld      l,a
        xor     a
        sla     l
        rla
        sla     l
        rla                                     ; AL=4xCRC index byte
        add     a,crctable/256
        ld      h,a                             ; HL=index into CRC table
        ld      a,(hl)
        inc     hl
        xor     b
        ld      c,a                             ; shift and XOR 2nd byte to low
        ld      a,(hl)
        inc     hl
        xor     e
        ld      b,a                             ; shift and XOR 3rd byte to 2nd
        ld      a,(hl)
        inc     hl
        xor     d
        ld      e,a                             ; shift and XOR high byte to 3rd
        ld      d,(hl)                          ; get new high byte
        exx
        ret   
; *************************************************************************************



; *************************************************************************************
.ReturnCrcResult
        exx
        ld      a,d
        cpl
        ld      d,a                             ; complement high byte
        ld      a,e
        cpl
        ld      e,a                             ; complement 3rd byte
        ld      a,b
        cpl
        ld      b,a                             ; complement 2nd byte
        ld      a,c
        cpl
        ld      c,a                             ; complement low byte
        call    PutOsf_BC
        call    PutOsf_DE                       ; return DEBC to caller        
        cp      a                               ; ensure Fc = 0, Fz = 1
        ret
; *************************************************************************************



; *************************************************************************************
.preserve_fe_size
        ld      (ix + flentry_size+0),e
        ld      (ix + flentry_size+1),d
        ld      (ix + flentry_size+2),c         ; preserve file entry size for next partial call
        ret
; *************************************************************************************



; *************************************************************************************
.preserve_fptr
        res     7,h
        res     6,h                             ; preserve offset without segment mask
        ld      (ix + flentry_fptr+0),l
        ld      (ix + flentry_fptr+1),h
        ld      (ix + flentry_fptr+2),b         ; preserce BHL for next partial call
        ret
; *************************************************************************************



; *************************************************************************************
; BHL++ (and bank crossing bound automatically)
.IncEprFptr
        ld      a, h                            ; remember original A14-A15
        inc     hl

        xor     h                               ; get changed bits in A14-A15
        and     $c0
        ret     z

        xor     h                               ; offset has crossed bank boundary
        ld      h, a                            ; change A14-A15 back
        inc     b
        push    bc
        ld      c,ms_s1
        rst     OZ_Mpb                          ; page in new bank in segment 1, 
        pop     bc                              ; HL point to beginning of new bank (in S1)
        ret
; *************************************************************************************



; *************************************************************************************
; CDE = 0?
.CheckFeFileSize
        ld      a,d                             ; file size = 0?
        or      e
        ret     nz                              ; file size not yet 0
        inc     c
        dec     c
        ret                                     ; Fz = 1, File entry was successfully CRC-32 scanned (CDE = 0)!
; *************************************************************************************



; *************************************************************************************
; CDE--
.DecFileSize
        ld      a,e
        or      d
        jr      z,overflow_coming
        dec     de
        ret
.overflow_coming
        dec     de
        dec     c                   
        ret
; *************************************************************************************
