; **************************************************************************************************
; OZ Error Message Report Window
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

Module GNError

include "blink.def"
include "director.def"
include "error.def"
include "stdio.def"
include "fileio.def"
include "saverst.def"
include "oz.def"
include "memory.def"
include "z80.def"
include "../mth/mth.def"

xdef    GNErr
xdef    GNEsp

xref    GN_ret1a
xref    PutOsf_BHL

;       ----

;       display interactive error box
;
;IN:    A=error code
;OUT:   Fc=1, A=error:
;               RC_SUSP: error not fatal
;               RC_DRAW: error not fatal, windows corrupted
;               RC_QUIT: error fatal
;
;CHG:   AF....../....

.GNErr
        push    ix
        cp      RC_Quit                         ; we don't do anything for
        jp      z, err_x                        ; quit/draw/suspended
        cp      RC_Draw
        jp      z, err_x
        cp      RC_Susp
        jp      z, err_x

        ld      a, SR_SUS
        OZ      OS_Sr                           ; save screen
        push    af                              ; remember result

        call    InitErrWindow
.get_errstr
        ld      a, (iy+OSFrame_A)               ; get error string
        OZ      GN_Esp
        push    af
        OZ      GN_Soe                          ; and print it
        pop     af
        jr      nz, err_3                       ; non-fatal? handle it

        OZ      OS_Pout
        defm    1,"3@",$20+0,$20+5
        defm    1,"2C",$FD
        defm    1,"2JC"
        defm    1,"T","PRESS ",1,"R"," Q ",1,"R"," TO QUIT - FATAL ERROR",1,"T"
        defm    1,"2JN",0
.fatal_err_wait_key
        call    GetEscQKey
        jr      z,err_7                         ; ESC or Q pressed, exit...
        jr      fatal_err_wait_key

.err_3
        call    EscResumeMsg                    ; "Press ESC to Resume"
.escq_wait_key
        call    GetEscQKey
        jr      nz,escq_wait_key

        pop     af
        ld      a, RC_Draw                      ; need redraw
        jr      c, err_x                        ; couldn't save screen, exit

        ld      a, SR_RUS
        OZ      OS_Sr                           ; restore screen
        ld      a, RC_Draw
        jr      c, err_x                        ; couldn't restore screen, exit

        ld      a, RC_Susp                      ; was pre-empted
        jr      err_x

.err_7
        pop     af
        jr      c, err_8                        ; error saving screen? no restore
        ld      a, SR_RUS
        OZ      OS_Sr                           ; restore screen
.err_8
        ld      a, RC_Quit                      ; fatal error, request quit

.err_x
        scf
        pop     ix
        jp      GN_ret1a

;       ----

.InitErrWindow
        ld      a, 2
        ld      bc, 5<<8|10
        OZ      OS_Blp

        ld      b,0
        ld      hl, ErrWinDef
        oz      GN_Win

        OZ      OS_Pout
        defm    1,"2-C"
        defm    1,"2JC"
        defm    1,"3@",$20+0,$20+2,0
        ret

;       ----

.EscResumeMsg
        OZ      OS_Pout
        defm    1,"3@",$20+0,$20+5
        defm    1,"2C",$FD
        defm    1,"2JC"
        defm    1,"T","PRESS ",1,"R"," ESC ",1,"R"," TO RESUME",1,"T"
        defm    1,"2JN",0
        ret

;       ----

.GetEscQKey
        ld      bc, -1                          ; wait infinitely
        ld      a, CL_RIM                       ; raw input
        OZ      OS_Cli
        jr      nc, check_keycode

        cp      RC_Susp
        jr      z, GetEscQKey
        cp      RC_Esc
        jr      nz, GetEscQKey

        OZ      OS_Esc
        cp      a
        ret

.check_keycode
        ld      a, d                            ; extended key? loop
        or      a
        jr      nz, GetEscQKey
        ld      a, e                            ; not 'Q'? loop
        cp      ESC                             ; not ESC? loop
        ret     z
        and     $df                             ; upper()
        cp      'Q'
        jr      nz, GetEscQKey
        ret

;       ----

;       return extended pointer to system error message
;
;IN:    A=error code
;OUT:   BHL=error message, Fz=1 if error is fatal
;
;CHG:   .FB...HL/....

.GNEsp
        ld      hl, Unknown_err
        cp      RC_Quit
        jr      z, esp_1
        call    FindErrStr
        ld      (iy+OSFrame_F), a               ; set flags
.esp_1
        ld      b, MTH_BNK
        jp      PutOsf_BHL

;       ----

;       find error string
;
;IN:    A=error
;OUT:   HL=string, A=flags

.FindErrStr
        ld      bc, MTH_BNK << 8 | MS_S1
        rst     OZ_Mpb
        push    bc
        call    fes_0
        pop     bc
        rst     OZ_Mpb
        ret

.fes_0
        ld      c, a                            ; error code to search for
        ld      hl, MM_S1 << 8 | ErrStr_tbl

.fes_1
        ld      a, (hl)                         ;  get error code
        inc     hl
        or      a
        jr      nz, fes_2                       ; not end? compare

        ld      hl, Unknown_err                 ; else fallback to unknown
        ld      a, Z80F_Z                       ; Fz=1, error is fatal
        ret

.fes_2
        cp      c
        jr      z, fes_3                        ; match? get string
        inc     hl                              ; skip and loop
        inc     hl
        inc     hl
        jr      fes_1

.fes_3
        ld      c, (hl)                         ; flags
        inc     hl
        ld      a, (hl)
        inc     hl
        ld      h, (hl)
        ld      l, a                            ; HL=string
        ld      a, c                            ; A=flags
        ret

;       ----

.ErrWinDef
        defb @11010000 | 1
        defw $0014
        defw $082D
        defw errbanner
.errbanner
        defm "ERROR", 0

;       ----
