; **************************************************************************************************
; High level file API for file open, close, delete and rename (GN_Opf, GN_Cl, GN_Del, GN_Ren).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module GNFile

        include "director.def"
        include "dor.def"
        include "handle.def"
        include "error.def"
        include "fileio.def"
        include "memory.def"
        include "sysvar.def"
        include "oz.def"
        include "eprom.def"
        include "time.def"

;       ----

xdef    GNCl
xdef    GNDel
xdef    GNOpf
xdef    GNRen

;       ----

xref    CompressFN
xref    FreeDOR
xref    GetFsNodeDOR
xref    GetOsf_BHL
xref    GN_ret1a
xref    IsSegSeparator
xref    PutOsf_Err
xref    RewindFile

;       ----

;       open file/resource
;
;IN:    BHL=name, DE=buffer for explicit filename, C=buffer size,
;       A=mode
;         OP_IN : open for input
;         OP_OUT: open for output
;         OP_UP : open for update
;         OP_MEM: open memory
;         OP_DIR: create directory
;         OP_DOR: return DOR information
;OUT:   IX=handle, DE=end of name, B=#segments in name, C=#chars in name
;       Fc=1, A=error
;CHG:   AFBCDE../IX..


.GNOpf
        OZ      OS_Bix                          ; bind name in
        push    de

        ld      de, GnFnameBuf
        ld      c, 205
        OZ      GN_Fex                          ; expand a filename
        pop     de
        push    af
        OZ      OS_Box
        pop     af
        jp      c, opf_err                      ; bad name?

        bit     7, a                            ; if wildcards were used we
        jp      z, opf_1                        ; reject OP_DIR and OP_OUT
        ex      af, af'
        ld      a, (iy+OSFrame_A)
        cp      OP_DIR
        jp      z, opf_errIvf
        cp      OP_OUT
        jp      z, opf_errIvf

;       check if EPR device

        ex      af, af'
        bit     6, a                            ; device specified
        jr      z, opf_wch                      ; no device, use WCH
        push    bc
        push    de
        push    hl
        ld      de, GnFnameBuf+1
        ld      hl, EprDevStr
        ld      bc, 3
.opf_eck
        ld      a, (de)
        and     @11011111                       ; uppercase
        inc     de
        cpi                                     ; cp (hl), inc hl, dec bc
        jp      po, opf_ewc                     ; strings match, bc = 0
        jr      z, opf_eck                      ; chars match, loop
        pop     hl
        pop     de
        pop     bc

;       find first file matching wildcard string

.opf_wch
        ld      a, 1                            ; backward scan
        ld      b, 0
        ld      hl, GnFnameBuf
        OZ      GN_Opw                          ; open wildcard handler
        jp      c, opf_err
        ld      de, GnFnameBuf                  ; !! ex de, hl
        ld      c, 205
        xor     a
        OZ      GN_Wfn                          ; get next match
        push    af
        OZ      GN_Wcl                          ; close handler
        pop     af
        jr      nc, opf_1                       ; no error? use found file
        cp      RC_Eof                          ; EOF -> object not found
        jp      nz, opf_err                     ; else return as-is
        jp      opf_errOnf

;       we have a valid EPR name in GnFnameBuf with wildcards

.opf_ewc
        inc     de
        ld      a, (de)
        cp      '?'
        jr      z, opf_ew1
        cp      '*'
        jr      z, opf_ew1
        sub     48
        ld      c,a                             ; convert to slot number
        jr      lookup_fe                       ; then try to find FE with wildcard
.opf_ew1
        ld      c, $FF                          ; begin with slot 0-1
.opf_ewl
        inc     c
        ld      a, c
        cp      4
        jr      z, oew_onf
        or      '0'                             ; '0' to '3'
        ld      (de), a
.lookup_fe
        inc     de                              ; filename starting at '/'
        ld      a, EP_Find
        oz      OS_Epr
        dec     de
        jr      c, opf_ewl
        jr      z, opf_xfn
        jr      opf_ewl

.oew_onf
        pop     hl
        pop     de
        pop     bc
        jr      opf_errOnf

; expand wildcard'ed filename with explicit name in BHL file entry
.opf_xfn
        push    bc
        push    hl                              ; preserve ptr to file entry

        ld      bc,6
        push    bc
        ld      hl,GnFnameBuf
        ld      d, (iy+OSFrame_D)
        ld      e, (iy+OSFrame_E)
        push    de
        ldir                                    ; copy :EPR.x device to buffer of caller
        pop     hl
        pop     bc
        add     hl,bc
        ex      de,hl                           ; DE = buffer to write expanded file entry name
        pop     hl
        pop     bc
        ld      c,0
        ld      a,EP_Name
        oz      OS_Epr                          ; copy complete file entry name into input buffer
        add     a,6
        ld      (iy+OSFrame_C),a                ; length of complete EPR filename
.opf_0
        pop     hl
        pop     de
        pop     bc

;       we have valid name in GnFnameBuf

.opf_1
        push    bc
        call    GetFsNodeDOR
        pop     bc
        ld      c, a                            ; remember type

        ld      a, (iy+OSFrame_A)
        jr      c, opf_8                        ; does not exist? ok

        cp      OP_DIR                          ; create dir? already exists
        jr      nz, opf_3
        ld      a, RC_Exis
.opf_2
        push    af
        call    FreeDOR
        pop     af
        jr      opf_err

.opf_3
        cp      OP_OUT                          ; write? allow char device and file
        jr      nz, opf_6
        ld      a, c
        cp      DM_CHD
        jr      z, opf_open                     ; write to char device? ok
        cp      DN_FIL
        jr      z, opf_5                        ; write to file? delete and continue
        cp      DM_EPR
        jp      z, opf_epr_out                  ; write to EPROM

.opf_errFtm
        ld      a, RC_Ftm                       ; file type mismatch
        jr      opf_2

.opf_5
        OZ      OS_Del                          ; delete file
        jr      c, opf_err                      ; error?
        ld      a, OP_OUT
        jr      opf_10                          ; get DOR of previous segment and do opening

.opf_6
        cp      OP_DOR                          ; get DOR?
        jr      nz, opf_7
        ld      (iy+OSFrame_A), c               ; return type
        jr      opf_name                        ; and name

;       file exists, OP_IN/OP_UP/OP_MEM

.opf_7
        ld      a, c                            ; allow char device and file
        cp      DM_CHD
        jr      z, opf_open
        cp      DN_FIL
        jr      z, opf_open
        cp      DM_EPR
        jr      z, opf_epr_in
        jr      opf_errFtm

;       does not exist (or OP_OUT after delete)

.opf_8
        cp      OP_DIR                          ; allow create dir / write file
        jr      z, opf_10
        cp      OP_OUT
        jr      z, opf_10
.opf_errOnf
        ld      a, RC_Onf                       ; object not found
        jr      opf_err

;       get parent DOR

.opf_10
        dec     b                               ; go up one level if possible
        jr      nz, opf_11
        inc     b
.opf_11
        call    GetFsNodeDOR
        jr      c, opf_err                      ; parent not found?
        cp      DM_DEV                          ; allow dev/dir
        jr      z, opf_open
        cp      DN_DIR
        jr      nz, opf_errFtm
.opf_open
        ld      a, (iy+OSFrame_A)               ; OP_xxx
        inc     hl                              ; point to name
        OZ      OS_Op                           ; internal open
        jr      c, opf_err

.opf_name
        ld      b, 0
        ld      hl, GnFnameBuf
        jp      CompressFN                      ; and exit

.opf_errIvf
        ld      a, RC_Ivf                       ; invalid filename
.opf_err
        call    PutOsf_Err
        ld      ix, 0
        ret

.opf_epr_in
        ld      a, (iy+OSFrame_A)
        cp      OP_IN
        ld      a, RC_Ftm                       ; OP_DIR not valid on :EPR.x
        jp      nz, opf_2                       ; free device handle and exit with error

        ld      b, (ix+dhnd_DORBank)            ; grab eprom file entry
        ld      h, (ix+dhnd_DORH)
        ld      l, (ix+dhnd_DOR)
        ld      a, b                            ; if $FFFFFF then valid file area but file not found
        and     h
        and     l
        inc     a
        ld      a, RC_Onf
        jp      z, opf_2
        call    FreeDOR                         ; free this handle
        push    bc
        push    hl
        call    opf_utf                         ; generate unique temporary filename (if several files are left open)
        ld      a, OP_OUT
        call    opf_tmp
        pop     hl
        pop     bc
        jr      c, opf_err                      ; no room
        ld      a, EP_Fetch
        oz      OS_Epr
        push    af
        oz      GN_Cl
        pop     af
        jr      c, opf_err
        ld      a, OP_IN
        call    opf_tmp
        jr      c, opf_err
        set     FFLG_B_DEL, (ix+fhnd_flags)     ; file to be deleted on closure
        ret

.opf_utf                                        ; generate a 12 letter unique filename from machine date and time
        ld      bc, 7
        ld      de, GnTempBuf
        ld      hl, RamDevStr
        ldir
        ex      de, hl
        ld      de, 2
        oz      GN_Gmd                          ; date in ABC
        call    fnm_gen
        ld      de, 2
        oz      GN_Gmt                          ; time in ABC
        call    fnm_gen
        ld      (hl), '.'                       ; extension
        inc     hl
        ld      (hl), 'I'                       ; 'I' for IN
        inc     hl
        ld      (hl), 0                         ; terminator
        ret
.RamDevStr
        defm    ":RAM.-/"
.EprDevStr
        defm    "EPR"
.fnm_gen
        call    gen_reg
        ld      a, b
        call    gen_reg
        ld      a, c
        jp      gen_reg
.gen_reg
        push    af
        and     @11110000
        rrca
        rrca
        rrca
        rrca
        add     a, 'A'
        ld      (hl), a
        inc     hl
        pop     af
        and     @00001111
        add     a, 'A'
        ld      (hl), a
        inc     hl
        ret

.opf_tmp
        ld      hl, GnTempBuf
        ld      de, 3                           ; NUL, ignore name
        ld      bc, $FF
        OZ      GN_Opf                          ; open - BHL=name (B=0, HL=local ptr), A=mode, DE=exp. name buffer, C=buflen
        ret

.opf_epr_out
        call    FreeDOR                         ; free this handle
        ld      hl, -206
        add     hl, sp
        ld      sp, hl
        ld      a, l
        push    hl
        ld      bc, 205
        ld      de, GnFnameBuf
        ex      de, hl
        ldir                                    ; copy :EPR.x/... filename to stack
        call    opf_utf                         ; generate unique temporary filename (if several files are left open)
        ld      hl, GnTempBuf+20
        ld      (hl), 'N'                       ; name extension
        ld      a, OP_OUT
        call    opf_tmp
        jp      c, oeo_opferr                   ; no room
        pop     hl
.oeo_sfn
        ld      a, (hl)
        or      a
        jr      z, oeo_2
        oz      OS_Pb
        jr      c, oeo_pberr
        inc     hl
        jr      oeo_sfn
.oeo_2
        ld      hl, 206                         ; restore stack
        add     hl, sp
        ld      sp, hl
        oz      GN_Cl                           ; close temp file holding filename

        ld      hl, GnTempBuf+20
        ld      (hl), 'O'                       ; name extension
        ld      a, OP_OUT
        call    opf_tmp
        jp      c, opf_err                      ; no room
        set     FFLG_B_DEL, (ix+fhnd_flags)     ; file to be saved/deleted on closure
        ret

.oeo_pberr
        push    af
        set     FFLG_B_DEL, (ix+fhnd_flags)     ; delete on closure
        oz      GN_Cl                           ; close
        pop     af
.oeo_opferr
        ex      af, af'
        ld      hl, 206
        add     hl, sp
        ld      sp, hl
        ex      af, af'
        jp      opf_err

;       ----

;       close file
;
;IN:    IX=handle
;OUT:   IX=0
;       Fc=1, A=error
;
;CHG:   AF....../IX..

.GNCl
        OZ      OS_Cl                           ; pass it to OS_Cl
        jp      GN_ret1a

;       ----

;       rename file/directory
;
;IN:    BHL=old name, DE=new name
;OUT:   Fc=0
;       Fc=1, A=error
;CHG:   AF....../....

;       !! could move file if new name has path rename

.GNRen
        push    ix
        ld      hl, -17                         ; reserve space for new name
        add     hl, sp
        ld      sp, hl

        ex      de, hl                          ; bind in new name
        push    de
        OZ      OS_Bix                          ; Bind in extended address
        ex      (sp), hl                        ; ex (sp),de
        ex      de, hl
        ex      (sp), hl

        ld      b, 16
        ld      a, (hl)                         ; skip leading /\:
        call    IsSegSeparator                  ; !! this allows ':RAM.x' etc.
        jr      nz, ren_1
        inc     hl
        dec     b                               ; !! why?

.ren_1
        ld      a, (hl)                         ; copy name to stack buffer
        ld      (de), a
        inc     hl
        inc     de
        cp      $21
        jr      c, ren_2
        djnz    ren_1                           ; max 16 chars
.ren_2
        xor     a                               ; terminate
        ld      (de), a

        pop     de                              ; restore bindings
        OZ      OS_Box

        call    GetOsf_BHL                      ; bind in old name
        OZ      OS_Bix
        push    de

        push    hl
        ld      hl, 4
        add     hl, sp                          ; HL=new name
        ld      b, 0
        OZ      GN_Pfs                          ; parse it
        pop     hl
        jr      c, ren_errIvf                   ; bad name

        and     $fc
        jr      nz, ren_errIvf                  ; anything but name+extension?

        ld      de, GnFnameBuf
        ld      c, 205
        ld      a, OP_DOR
        OZ      GN_Opf                          ; open old file/dir
        ld      c, b                            ; #segments
        jr      c, ren_err                      ; not found
        cp      DN_FIL                          ; can only rename file/dir
        jr      z, ren_3
        cp      DN_DIR
        jr      nz, ren_errIvf                  ; !! RC_Ftm would be better
.ren_3
        ld      e, c
        dec     c
        jr      z, ren_errIvf                   ; only one segment?

        ld      hl, GnFnameBuf
        ld      b, 0
.ren_4
        OZ      GN_Pfs                          ; parse segment
        jr      c, ren_err                      ; bad?
        dec     c
        jr      nz, ren_4                       ; loop until all but last segments done

        inc     hl                              ; skip '/'
        ld      b, e
        ex      de, hl                          ; DE=destination
        ld      hl, 2                           ; HL=new name
        add     hl, sp
.ren_5
        ld      a, (hl)                         ; overwrite old name
        ld      (de), a
        inc     hl
        inc     de
        cp      $21
        jr      nc, ren_5

        push    ix
        call    GetFsNodeDOR                    ; try to find new file/dir
        jr      c, ren_6                        ; not found? ok
        ld      a, DR_FRE
        OZ      OS_Dor                          ; DOR interface
        pop     ix
        ld      a, RC_Exis                      ; object already exists
        jr      ren_err

.ren_6
        pop     ix                              ; restore old object DOR
        cp      RC_Onf                          ; object not found is ok
        jr      nz, ren_err                     ; otherwise error

        ld      hl, 2                           ; new name
        add     hl, sp
        OZ      OS_Ren                          ; file rename
        jr      ren_9

.ren_errIvf
        ld      a, RC_Ivf                       ; Invalid filename
.ren_err
        scf
.ren_9
        push    af
        push    ix                              ; !! use FreeDOR()
        pop     de
        ld      a, d
        or      e
        jr      z, ren_10
        ld      a, DR_FRE
        OZ      OS_Dor

.ren_10
        pop     af
        pop     de
        ex      af, af'                         ; need to remember Fc
        ld      hl, 17                          ; restore stack
        add     hl, sp
        ld      sp, hl
        ex      af, af'
        push    af
        OZ      OS_Box
        pop     af
        call    c, PutOsf_Err
        pop     ix
        ret

;       ----

;       delete file/directory
;
;IN:    BHL=filename
;OUT:   Fc=0
;       Fc=1, A=error
;
;CHG:   AF....../....

.GNDel
        push    ix
        OZ      OS_Bix                          ; bind name in
        push    de

        ld      de, GnFnameBuf
        ld      c, 205
        OZ      GN_Fex                          ; expand it
        jr      c, del_err                      ; bad name

        and     $80                             ; wildcards?
        jr      z, del_1
        scf                                     ; no wildcard delete
        ld      a, RC_Ivf
        jr      del_err

.del_1
        call    GetFsNodeDOR
        jr      c, del_err                      ; not found?
        cp      DN_FIL                          ; only delete files/dirs
        jr      z, del_2
        cp      DN_DIR
        jr      nz, del_3                       ; !! no error for others, tho
.del_2
        OZ      OS_Del
        jr      c, del_err
.del_3
        call    FreeDOR
.del_err
        call    c, PutOsf_Err
        pop     de
        OZ      OS_Box
        pop     ix
        ret
