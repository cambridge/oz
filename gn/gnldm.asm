; **************************************************************************************************
; GN Time format conversion API calls (GNGdn, GNGdt, GNGtm, GNPdn, GNPdt, GNPtm)
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module GNLdm

        include "error.def"
        include "filter.def"
        include "memory.def"
        include "time.def"
        include "oz.def"
        include "z80.def"
        include "sysvar.def"

        include "gndef.def"

        xdef    GNLdm

        xref    PutOsf_Err
        xref    PutOsf_DE
        xref    Ld_DE_A

; ***************************************************************************************************
;
;       Localized Date Manipulation
;
;       Get country localized full or short string of day of the week or month name
;
; IN :  A = desired string
;       A7 = 1  get day of the week
;       A6 = 1  get month name
;       A5 = 0  full string, 1 short string
;       A4 = 0  get moment (no short string available)
;       A0-A3   day of the week from 1-7 (Monday-Sunday)
;        or     month name 1-12 (January_December)
;        or     moment : 1=today, 2=yesterday, 3=tomorrow
; 
;       HL = Destination buffer (at least 16 bytes required)
;
; OUT:  Fc = 0, HL = null terminated string desired
;               DE = zero at the end of the string
;                B = number of characters written to buffer (except trailing zero)
;       Fc=1, A=error
;
.GNLdm
        push    ix
        ld      bc, [DAF_BNK << 8] | MS_S1      ; bank of date filter table in S1 mandatory (else GN_FLO crashes)
        rst     OZ_MPB
        push    bc

        ld      a, (iy+OSFrame_A)
        ld      b, a

;is_moment
        bit     4, a
        jr      z, is_month
        and     @00000011                       ; moment range 1-3
        jr      z, ldm_err
        or      @10010000                       ; ensure only bit 7 and 4 are set
        ld      (iy+OSFrame_A), a
        jr      ldm_flo

.is_month
        bit     6, a
        jr      z, is_week
        and     @00001111                       ; month range 1-12
        jr      z, ldm_err                      ; 0
        cp      13
        jr      nc, ldm_err                     ; >12
        ld      a, b
        and     @11101111                       ; ensure bit 4 is reset
        ld      (iy+OSFrame_A), a
        jr      ldm_flo

.is_week
        bit     7, a
        jr      z, ldm_err
        and     @00001111                       ; day of the week range 1-7
        jr      z, ldm_err                      ; 0
        cp      8
        jr      nc, ldm_err                     ; >7
        ld      a, b
        and     @10101111                       ; ensure bit 4 is reset
        ld      (iy+OSFrame_A), a

.ldm_flo
        ld      hl, (uwGnDateFilter)            ; get table
        ld      a, FL_REVERSE+FL_MAXBUF         ; reverse mode, max buffer size B bytes
        ld      b, 16
        OZ      GN_Flo                          ; open it
        jr      c, ldm_err
        ld      a, (iy+OSFrame_A)
        or      @10000000                       ; force b7 (if b6 is set alone)
        oz      GN_FLW                          ; write A to filter

        ld      d, (IY+OSFrame_H)
        ld      e, (IY+OSFrame_L)
        ld      b, 0
.ldm_flr
        oz      GN_FLR
        jr      c, ldm_end
        inc     b
        call    Ld_DE_A
        inc     de 
        jr      ldm_flr

.ldm_end
        cp      RC_EOF                          ; ok, done
        jr      z, ldm_flc
        call    PutOsf_Err
.ldm_flc
        call    PutOsf_DE                       ; DE points at the end of the string
        ld      (iy+OSFrame_B), b               ; number of character written in buffer
        xor     a                               ; null terminated string
        call    Ld_DE_A
        oz      GN_FLC
        jr      ldm_exit                        ; restore S1

.ldm_err
        call    PutOsf_Err
.ldm_exit
        pop     bc
        rst     OZ_MPB
        pop     ix
        ret


