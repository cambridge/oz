; **************************************************************************************************
; IM 1 Interrupt handler, called from 0038H.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2006
; (C) Gunther Strube (gstrube@gmail.com), 2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************


.LOWRAM_INT
        push    af                              ; IMPORTANT NOTE :
                                                ; a DI is not necessary at the start of OZ_INT
                                                ; since IFF1 and IFF2 are automaticly cleared
                                                ; when accepting an INT
        push    bc
        push    hl
        call    LOW_INT                         ; de and ix are not preserved
        pop     hl
        pop     bc
        pop     af
        ei                                      ; IFF1 and IFF2 = 1
        ret                                     ; RETI is not necessary since there is no Z80 PIO
                                                ; RET is faster (10T vs 14T)
                                                ; Note : RETI=RETN=RET + copy IFF2 to IFF1

; Timings to enter and exit LOW_INT : 120 T-states

.LOW_INT                                        ; Entry of IM 1 Interrupt Handler (called from 0038H in LOWRAM)
        in      a, (BL_UIT)
        and     BM_UITRDRF
        jp      nz, IntRDRF                     ; priority to serial RX interrupt

        ld      a, (ubSerIntCnt)                ; serial idle counter
        or      a
        jr      z, Do_Int                       ; zero means serial RX interrupt inactive
        dec     a
        ld      (ubSerIntCnt), a                ; else decrement, store and ret
        ret

.Do_Int
        ld      hl, BLSC_INT                    ; soft copy of INT
        in      a, (BL_STA)                     ; current interrupt status (STA)
        and     (hl)                            ; mask
        rra                                     ; test BM_INTGINT & BM_STATIME
        jp      nc, is_sta_key                  ; STA.TIME = 0, not a TIME int, skip ...

        bit     BB_INTTIME, (hl)                ; hl = BLSC_INT
        jp      z, is_sta_key                   ; INT.TIME (RTC) disabled? skip

;       RTC interrupt, determine tick, second, minute

        ld      l, BL_TSTA                      ; point softcopy to RTC enable
        in      a, (BL_TSTA)                    ; RTC int status
        and     (hl)                            ; mask out disabled ones
        
        bit     BB_TACKTICK, a
        jp      z, is_RTCSM                     ; not a TICK? skip

;       TICK handles beeper, updates small timer and does keyboard scan

        ld      a, BM_TACKTICK
        out     (BL_TACK), a                    ; ack TICK
        ld      hl, ubIntTaskToDo
        bit     ITSK_B_PREEMPTION, (hl)
        call    z, IntBeeper

        ld      bc, (uwSmallTimer)
        ld      a, b
        and     c
        inc     a
        jr      z, int_kbd                      ; timer=-1, skip
        ld      a, b
        or      c
        jr      z, int_kbd                      ; timer=0, skip
        call    ResetTimeout
        dec     bc
        ld      (uwSmallTimer), bc
        ld      a, b                            ; if timer=0 set pending SMT
        or      c
        jr      nz, int_kbd
        ld      hl, ubIntTaskToDo               ; reload hl (was corrupted by IntBeeper)
        set     ITSK_B_TIMER, (hl)

.int_kbd
        ld      hl, KbdData+kbd_flags
        bit     KBF_B_SWOFF, (hl)               ; set during switch off (nmi.asm)
        ret     nz                              ; scan active? exit int

        bit     KBF_B_LOCKED, (hl)
        jr      z, int_kbd2

;       if lockout we just check for both shifts

        xor     a                               ; read all columns
        ld      hl, KbdData+kbd_keyflags
        ld      (hl), a                         ; cancel current
        ld      hl, KbdData+kbd_prevflags
        ld      (hl), a                         ; and prev
        in      a, (BL_KBD)                     ; check if any key pressed (all)
        inc     a
        ret     z                               ; no keys? exit
        call    XBothShifts
        ret     c                               ; no shifts? exit
        ld      hl, KbdData+kbd_flags
        res     KBF_B_LOCKED, (hl)              ; remove flag before switching off
        ret                                     ; exit interrupt

.XBothShifts
        call    ExtCallK0
        defw    BothShifts                      ; and RET

;       keyboard scan

.int_kbd2
        xor     a                               ; read all columns
        in      a, (BL_KBD)
        inc     a
        call    nz, ResetTimeout                ; key pressed? reset timeout
        jr      nz, int_kbd3                    ; key pressed? force keyboard scan

        ld      a, (hl)                         ; check if we have active keys
        and     KBF_DMND|KBF_KEY|KBF_SQR
        ld      hl, KbdData+kbd_keyflags        
        or      (hl)                            ; current
        ld      hl, KbdData+kbd_prevflags
        or      (hl)                            ; prev
        ret     z                               ; no key
.int_kbd3
        call    ExtCallK0
        defw    IntKbd                          ; and RET 

;       no ints outside blink or RTC disabled

.is_sta_key
        rra                                     ; ignore STA[2] (always 0)
        rra                                     
        jr      nc, is_sta_btl                  ; STA.KEY = 0 (no KB interrupt), skip
        ld      a, BM_ACKKEY
        out     (BL_ACK), a                     ; ack keyboard interrupt and exit (wake-up from snooze or coma)
        ret

.is_sta_btl
        rra                 
        jr      nc, is_sta_uart                 ; STA.BTL (Battery low) = 0, skip

        res     BB_INTBTL, (hl)                 ; disable int
        ld      a, (hl)
        out     (BL_INT), a                     ; really
        ld      a, BM_ACKBTL
        out     (BL_ACK), a                     ; ack BATLOW
        ld      hl, ubIntStatus
        set     IST_B_BATLOW, (hl)
        dec     hl                              ; request OZ window update
        set     ITSK_B_OZWINDOW, (hl)
        ret

.is_sta_uart
        rra                                     
        jr      nc, is_sta_flap                 ; STA.UART = 0, skip

        ret                                     ; dont process TDRE, CTS or DCD interrupt

.IntRDRF                                        ; 190 T-states to arrive here since RST 38H, 161 T-states below (in the best case to fetch one byte in buffer)
                                                ; 351 T-states to fetch one byte from serial port to buffer
        ld      a, RXB_LAT                      ; latency to process RX buffer before processing all kind of interrupt
        ld      (ubSerIntCnt), a                ; reset serial idle counter every real RDRF interrupt

        ld      hl, (ubRXBWrPos)                ; h = ubRXBRdPos
        ld      c, h                            ; c = ubRXBRdPos
        ld      a, l                            ; l = ubRXBWrPos
        inc     a
        inc     a
        cp      h
        call    z, rxi_intoff                   ; buffer full, disable UART interrupt
                                                ; (16+4x4+7=39)

        ld      a, (ubSerControl)
        ld      b, a
        and     SIC_XONXOFF|SIC_PARITY
        in      a, (BL_RXD)                     ; read serial data and acknowledge UIT.RDRF
        jr      nz, rxi_xp                      ; if XONXOFF or parity bit required
                                                ; (13+4+7+11+7=42)
.rxi_com
        ld      h, >SerRXBuffer                 ; RX Buffer Page
        ld      (hl), a                         ; put byte in buffer
        ld      a, l
        inc     a
        ld      (ubRXBWrPos), a                 ; update WrPos

        sub     c                               ; used = Wr-Rd
        jr      nc, rxi_lim
        neg     
.rxi_lim
        cp      RXB_LIM                         ; block sender if more than RXB_LIM bytes filled
        ret     c
                                                ; (7+7+4+4+13+4+7+11=57)
.BlockSender
        ld      a, (BLSC_RXC)                   ;
        and     BM_RXCIRTS                      ; already low ?
        ret     z

        ld      bc, BLSC_RXC
        ld      a, (bc)
        and     ~(BM_RXCARTS|BM_RXCIRTS)        ; reset RTS line
        ld      (bc), a
        out     (c), a

        ld      a, (ubSerControl)               ; no flow control? exit
        and     SIC_XONXOFF
        ret     z
        call    ExtCallK0                      
        defw    OSSiInt                         ; send XOFF and RET to LOWRAM_INT

.rxi_xp
        bit     SIC_B_PARITY, b                 ; clear parity bit, check for XON/XOFF if needed
        jr      z, rxi_np                       ; no parity? 8-bit data
        and     $7F                             ; else clear parity bit
.rxi_np
        bit     SIC_B_XONXOFF, b
        jr      z, rxi_com                      ; no flow control? get char

        cp      XON                             ;
        jr      z, rxi_xon                      ; send XON

        cp      XOFF
        jr      nz, rxi_com                     ; get the byte
               

        ld      hl, ubSerControl 
        set     SIC_B_TXSTOP, (hl)              ; stop sending
        ret

.rxi_xon
        ld      hl, ubSerControl 
        res     SIC_B_TXSTOP, (hl)              ; allow sending
        ret

.rxi_intoff
        ld      a, (BLSC_INT)
        and     ~BM_INTUART                     ; disable UART interrupt
        ld      (BLSC_INT), a                   ; until buffer is emptied
        out     (BL_INT), a
        ret

.is_sta_flap
        rra
        jr      nc, is_sta_a19
        call    ExtCallK0                       ; handle STA.FLAP interrupt
        defw    IntFlap                         ; and RET to LOWRAM_INT

.is_sta_a19
        rra          
        ret     nc
        ld      a, BM_ACKA19                    ; handle STA.A19 interrupt
        out     (BL_ACK), a                     ; (w) main int. mask
        ret

;       ----

.ResetTimeout
        push    af
        ld      a, (ubTimeout)                  ; get the current Timeout (in minutes) parameter stored by Panel
        or      a
        jr      z, rt_1                         ; zero means no time out, store this value too
        inc     a                               ; if not 30 sec less than expected
.rt_1
        ld      (ubTimeoutCnt), a
        pop     af
        ret

;       ----

.is_RTCSM
        bit     BB_TACKSEC, a
        jr      z, is_RTCM                      ; no SEC? skip
        call    ExtCallK0                       ; acknowledge RTC SEC interrupt
        defw    Int_RTCS                        ; and RET to LOWRAM_INT

.is_RTCM
        bit     BB_TACKMIN, a
        ret     z
        call    ExtCallK0                       ; acknowledge RTC MIN interrupt
        defw    Int_RTCM                        ; and RET to LOWRAM_INT

;       ----

.IntBeeper
        ld      hl, ubSoundCount                ; beep active
        ld      a, (hl)
        or      a
        jr      nz, intb_beep1
        ld      hl, KbdData+kbd_flags
        bit     KBF_B_BEEP, (hl)                ; keyclick pending?
        jr      nz, intb_kclick
        ld      hl, BLSC_COM                    ; silence beeper if not already
        bit     BB_COMSRUN, (hl)
        ret     z
        res     BB_COMSRUN, (hl)
        jr      intb_out

.intb_kclick
        res     KBF_B_BEEP, (hl)
        jr      intb_on                         ; do keyclick

.intb_beep1
        rr      a
        push    hl
        call    intb_beep4
        pop     hl
        dec     hl                              ; ubSoundActive
        dec     (hl)
        ret     nz

.intb_beep2
        or      a
        ld      hl, ubSoundCount
        dec     (hl)
        jr      z, intb_beep4
        bit     0, (hl)                         ; even=space, odd=mark
        inc     hl                              ; space count
        jr      z, intb_beep3
        inc     hl                              ; mark count
        scf

.intb_beep3
        ld      a, (hl)
        ld      (ubSoundActive), a
        inc     a
        dec     a
        jr      z, intb_beep2                   ; zero count? ignore it

.intb_beep4
        ld      hl, BLSC_COM
        res     BB_COMSRUN, (hl)                ; speaker source SBIT
        jr      nc, intb_out
        ld      a, (cSound)
        cp      'Y'
        jr      nz, intb_out

.intb_on
        ld      hl, BLSC_COM
        set     BB_COMSRUN, (hl)                ; speaker source 3K2

.intb_out
        ld      a, (hl)
        out     (BL_COM), a                     ; (w) command register
        ret

;       ----

;       Special fast extended call for Kernel0
;       SP, DE, AF' are preserved 
;       IX,IY,BC',DE',HL' have to preserved in the call

.ExtCallK0
        ex      (sp), hl                        ; push hl, get PC
        push    de                              ; save de if subcall destroys it too
        ex      de, hl                          ; de = (SP)
        ld      hl, 0                           ; stack above $2000? use system stack
        add     hl, sp                          ; bad application like basic moves stack
        ld      a, h
        cp      $20
        jr      c, sys_stack 
        ld      sp, ($1FFE)                     ; read new SP
.sys_stack
        push    hl
        ex      af, af'
        push    af                              ; preserve af'
        ld      a, (BLSC_SR3)                   ; remember S2
        push    af
        call    MS3Kernel0

        ex      de, hl                          ; function = (SP)
        ld      a, (hl)                         ; get function in HL
        inc     hl
        ld      h, (hl)
        ld      l, a
        call    jpHL                            ; and call it
        ex      af, af'                         ; preserve af in af' (BothShift returns Fz and Fc)
        pop     af                              ; restore S2
        call    MS3BankA
        pop     af                              ; restore af'
        ex      af, af'
        pop     hl                              ; stack
        ld      sp, hl                          ; restore stack        
        pop     de
        pop     hl
        ret

;       ----
