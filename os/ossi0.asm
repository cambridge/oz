; **************************************************************************************************
; OZ Low level serial port interface, called by INT handler and high level OS_Gbt/OS_Pbt.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
;***************************************************************************************************

        Module OSSi0

        include "error.def"
        include "blink.def"
        include "stdio.def"
        include "fileio.def"
        include "handle.def"
        include "syspar.def"
        include "sysvar.def"
        include "oz.def"
        include "serintfc.def"
        include "interrpt.def"

        include "lowram.def"

xdef    OSSi
xdef    OSSiInt                                 ; low_int.asm
xdef    OSSiGbt, OSSiPbt                        ; fileio0.asm
xdef    OSSiSft                                 ; spnq.asm

xref    OSSiHrd1
xref    OSSiSft1
xref    OSSiEnq1
xref    OSSiFtx1
xref    OSSiFrx1
xref    OSSiTmo1
xref    OSBixS1, OSBoxS1

; -----------------------------------------------------------------------------
;
;       OS_SI   low level serial interface
;
;       IN :    L=reason code and A, BC, DE, IX according the reason
;       OUT:    depends on the reason called
;
;       NB : with 1 byte OS calls, all registers are exx on entry
; -----------------------------------------------------------------------------
.OSSITBL
        jp      OSSiHrd
        jp      OSSiSft
        jp      OSSiInt
        jp      OSSiGbt
        jp      OSSiPbt
        jp      OSSiEnq
        jp      OSSiFtx
        jp      OSSiFrx
        jp      OSSiTmo
        jp      OSSiGx                          ; SI_GX  ($1B) Get multiple bytes from serial port (OZ 4.5)
        jp      OSSiPx                          ; SI_PX  ($1E) Put multiple bytes to serial port (OZ 4.5)
        jp      OSSiGxT                         ; SI_GXT ($21) Get multiple bytes until terminator (OZ 4.5)

                                                ; this jumper take 65 T-states

.OSSITBL_end

IF (>$linkaddr(OSSITBL)) <> (>$linkaddr(OSSITBL_end))
        ERROR "OS_SI lookup table crosses address page boundary!"
ENDIF

.OSSi
        ld      hl, OZCallReturn1               ; ret with AFBCDEHL
        push    hl                              ; stack the ret
        exx                                     ; restore main registers
        ld      h, OSSITBL/256
        ld      a, l                            ; OS_SI reason code
        add     a, OSSITBL%256
        ld      l, a
        ex      af, af'                         ; restore af (used in OSSiPbt)
        jp      (hl)                            ; jump to routine

.OSSiHrd
        extcall OSSiHrd1, KN1_BNK
        ret

.OSSiSft
        extcall OSSiSft1, KN1_BNK
        ret

.OSSiEnq
        extcall OSSiEnq1, KN1_BNK
        ret

.OSSiFtx
        extcall OSSiFtx1, KN1_BNK
        ret

.OSSiFrx
        extcall OSSiFrx1, KN1_BNK
        ret

.OSSiTmo
        extcall OSSiTmo1, KN1_BNK
        ret

; -----------------------------------------------------------------------------------------
; SIINT
; Interrupt entry, send Xoff
;
.OSSiInt                                        ; from low_int
        ld      a, XOFF                         ; send XOFF
        ld      bc, 10                          ; 100 ms timeout


; -----------------------------------------------------------------------------------------
; SIPBT
; Send a byte to RS232 interface
;
.OSSiPbt
        ld      h, a                            ; preserve byte to be put
        ld      a, b
        and     c
        inc     a
        jr      nz, sipbt_2
        ld      bc, (uwSerTimeout)              ; get default timeout if BC = -1
.sipbt_2
        ld      (uwSmallTimer), bc              ; small timer counts timeout
        ld      b, h                            ; b = byte to write
        ld      hl, ubIntTaskToDo
        res     ITSK_B_TIMER, (hl)              ; reset timeout flag
        ld      a, (ubSerControl)
        and     SIC_XONXOFF|SIC_PARITY
        jr      nz, OSSiPbXP

; Send a byte in B

.OSSiPbB
        in      a, (BL_UIT)                     ; UART interrupt status
        bit     BB_UITTDRE, a                   ; is tx register empty ?
        jr      z, sipbt_wait                   ; not ready, wait
        ld      a, b                            ; preserved before
        ld      bc, $0600 | BL_TXD              ; 2 STOP, 1 START (read DN)
        out     (c), a                          ; send byte
        or      a                               ; reset Fc, no error
        ld      bc, (uwSmallTimer)              ; time remaining
        ret

.sipbt_wait
        ld      hl, ubIntTaskToDo
        bit     ITSK_B_PREEMPTION, (hl)
        jr      nz, si_suspended                ; pre-empted? exit
        bit     ITSK_B_ESC, (hl)
        jr      nz, si_escape                   ; ESC pending? exit
        bit     ITSK_B_TIMER, (hl)
        jr      nz, si_timeout                  ; timeout? if not retry
        jr      OSSiPbB


; Send a byte in B with XONXOFF software handshaking and parity

.OSSiPbXP
        ld      a, (ubSerControl)
        bit     SIC_B_XONXOFF, a
        jr      z, txi_parity                   ; no XONXOFF, parity only

        bit     SIC_B_TXSTOP, a                 ; need to stop transmitting?
        jr      z, txi_parity                   ; wait for XON in RX buffer

        ld      hl, ubIntTaskToDo
        bit     ITSK_B_PREEMPTION, (hl)
        jr      nz, si_suspended                ; pre-empted? exit
        bit     ITSK_B_ESC, (hl)
        jr      nz, si_escape                   ; ESC pending? exit
        bit     ITSK_B_TIMER, (hl)
        jr      nz, si_timeout                  ; timeout? if not retry
        jr      OSSiPbXP                        ; reload a and test TXSTOP

.txi_parity
        ld      a, b                            ; send char in A
        ex      af,af'                          ; save char
        ld      a, (ubSerControl)

        add     a, a                            ; forget 9bit mode
        add     a, a                            ; skip if no parity
        jr      nc, altaf

        add     a, a
        jr      nc, cpar                        ; even/odd 'pair/impair'

        and     2**SIC_B_ODD_MARK               ; $80 space/mark bit
        ld      c, a
        ex      af, af'                         ; merge with 7bit char
        and     $7f
        or      c
        jr      senda

.cpar   add     a,a                             ; skip if odd parity
        jr      c, odd

        ex      af, af'
        and     a
        jp      pe, senda                       ; even already
        jr      xor80

.odd    ex      af, af'                         ;'impair'
        and     a
        jp      po, senda

.xor80  xor     $80                             ; invert parity
        ex      af, af'                         ; faster than 'jp/jr'
.altaf  ex      af, af'

.senda
        ld      b, a
        jr      OSSiPbB                         ; send it, hl = ubIntTaskToDo already

.si_timeout
        res     ITSK_B_TIMER, (hl)              ; ack timeout int
        ld      a, RC_Time

.si_error
        scf                                     ; flag error
        ld      bc, (uwSmallTimer)              ; time remaining
        ret

.si_suspended
        res     ITSK_B_PREEMPTION, (hl)         ; otherwise exit with pre-emption
        ld      a, RC_Susp
        jr      si_error

.si_escape
        ld      a, RC_Esc                       ; esc is acknoledged by user
        jr      si_error


; -----------------------------------------------------------------------------------------
; SIGBT
; Receive a byte from RS232 interface
;
.OSSiGbt
        ld      (uwSmallTimer), bc              ; small timer counts timeout
        ld      hl, ubIntTaskToDo
        res     ITSK_B_TIMER, (hl)              ; reset timeout flag

.OSSiGbB
        di
        ld      hl, (ubRXBWrPos)                ; h = ubRXBRdPos, l = ubRXBWrPos
        ld      a, l                            ; used = WrPos - RdPos
        sub     h
        jr      z, bfgbt_wait                   ; buffer is empty, wait

        ld      b, a                            ; bytes used in b

        ld      a, h                            ; a = l = RdPos will be used below
        ld      l, a
        inc     a
        ld      (ubRXBRdPos), a                 ; inc RdPos

        ld      h, >SerRXBuffer                 ; RX Buffer Page
        ld      a, (hl)                         ; read byte in buffer
        ei

        or      a                               ; ensure Fc = 0
        dec     b                               ; used slots - 1
        ld      bc, (uwSmallTimer)              ; prepare exit
        ret     nz                              ; exit if b > 0 (buffer not empty)

                                                ; 123 T-states to grab a byte in the buffer

                                                ; Buffer is empty :
                                                ; Set IRTS, enable UART interrupt, send XON
        ld      h, a                            ; preserve a
        ld      bc,BLSC_RXC                     ; unblock sender
        ld      a, (bc)
        bit     BB_RXCIRTS, a
        jr      nz, bfgbt_exit                  ; already done
        or      BM_RXCIRTS                      ; set IRTS (inverse RTS level), i.e. request transmission
        ld      (bc), a
        out     (c), a

        ld      a, (BLSC_INT)
        bit     BB_INTUART, a
        jr      nz, bfgbt_xon                   ; IntUART already enabled
        or      BM_INTUART                      ; enable UART interrupt
        ld      (BLSC_INT), a
        out     (BL_INT), a

.bfgbt_xon
        ld      a, (ubSerControl)               ; if Xon/Xoff send XON
        and     SIC_XONXOFF
        jr      z, bfgbt_exit                   ; no XONXOFF handshaking
        ld      a, XON
        exx                                     ; preserve byte read in h
        ld      bc, 10                          ; 100 ms timeout
        call    OSSiPbt
        exx

.bfgbt_exit
        ld      a, h                            ; restore byte read from h
        or      a                               ; clear Fc, no error
        ld      bc, (uwSmallTimer)              ; time remaining
        ret

.bfgbt_wait
        ei                                      ; re-enable interrupt
        ld      hl, ubIntTaskToDo
        bit     ITSK_B_PREEMPTION, (hl)
        jp      nz, si_suspended                ; pre-empted? exit
        bit     ITSK_B_ESC, (hl)
        jp      nz, si_escape                   ; ESC pending? exit
        bit     ITSK_B_TIMER, (hl)
        jp      nz, si_timeout                  ; timeout? if not retry
        jr      OSSiGbB                         ; retry


; -----------------------------------------------------------------------------
; SI_PX (L = $1E), put multiple bytes to serial port, using default timeout (SI_TMO)
;
; IN:
;       BC = number of byte to write to serial port, maximum 16384, one bank
;       DE = input
;           DE = 0, input is a file/device opened with its handle in IX
;           DE > 0, input is memory at (DE)
;       IX = file handle (if DE = 0)
;
; Out if call succeeded:
;       Fc = 0
;       BC = remaining time (SI_TMO)
;
; Out if call failed:
;
;       Fc = 1
;       A = RC_TIME, timeout elapsed
;       A = RC_EOF, end of file reached
;       A = RC_RP, input device is read protected
;
;       BC = number of bytes not written
;
; Registers changed after return:
;
;       ......../IXIY same
;       AFBCDEHL/.... different
;
.OSSiPx
        ld      a, RC_Na
        ld      hl, $4000
        sbc     hl, bc
        ret     c
        ld      hl, (uwSerTimeout)              ; small timer counts default timeout
        ld      (uwSmallTimer), hl
        ld      hl, ubIntTaskToDo
        res     ITSK_B_TIMER, (hl)              ; reset timeout flag

        ld      a, d
        or      e
        jr      z, sipx_file

        ex      de, hl
        bit     7, h
        jr      z, sipx_bound
        bit     6, h
        jr      z, sipx_bound
        ex      de, hl

        ld      hl, 3                           ; get original s3 bank
        add     hl, sp
        ld      a, b                            ; preserve b
        ld      b, (hl)                         ; (sp+3) = BLSC_SR3

        ex      de, hl                          ; hl = start
        call    OSBixS1                         ; bind BHL in S1
        ld      b, a
        jr      sipx_mem

.sipx_bound
        ld      a, (BLSC_SR1)                   ; push S1 for OSBoxS1
        ld      d, a

.sipx_mem
        push    de
.sipx_loop
        ld      a, (hl)
        call    sipx_write                      ; write byte to serial output
        jr      c, sipx_error
        cpi                                     ; cp (hl); inc hl, dec bc
        jp      pe, sipx_loop                   ; pv flag, reset if bc=0

        pop     de
        call    OSBoxS1
        ld      bc, (uwSmallTimer)              ; time remaining
        or      a                               ; Fc = 0
        ret

.sipx_error
        pop     de
        jp      OSBoxS1

.sipx_file
        push    bc                              ; preserve bc
        ld      bc, (uwSmallTimer)              ; use same timeout
        oz      OS_Gbt                          ; get byte from IX file/device
        pop     bc
        ret     c
        call    sipx_write                      ; write byte to serial port
        ret     c
        dec     bc
        ld      a, b
        or      c
        jr      nz, sipx_file

.sipx_write
        exx
        ld      bc, -1                          ; use default timeout
        call    OSSiPbt                         ; write byte to serial port
        exx
        ret                                     ; with or without error


; -----------------------------------------------------------------------------
; SI_GX (L = $1B), get multiple bytes from serial port, using default timeout (SI_TMO)
;
; IN:
;       BC = number of byte to get from serial port, maximum 16384, one bank
;       DE = destination
;           DE = 0, write bytes to a file opened with its handle in IX
;           DE > 0, write bytes to memory at (DE)
;       IX = file handle (if DE = 0)
;
; Out if call succeeded:
;       Fc = 0
;       BC = remaining time (SI_TMO)
;       DE = points at last byte fetched + 1 (if DE(IN) > 0)
;
; Out if call failed:
;
;       Fc = 1
;       A = RC_TIME, timeout elapsed
;       A = RC_EOF, end of file reached
;       A = RC_WP, input device is read protected
;
;       BC = number of bytes not read
;
; Registers changed after return:
;
;       ......../IXIY same
;       AFBCDEHL/.... different
;
.OSSiGx
        ld      a, RC_Na
        ld      hl, $4000
        sbc     hl, bc
        ret     c
        ld      hl, (uwSerTimeout)              ; small timer counts default timeout
        ld      (uwSmallTimer), hl
        ld      hl, ubIntTaskToDo
        res     ITSK_B_TIMER, (hl)              ; reset timeout flag

        ld      a, d
        or      e
        jr      z, sigx_file

        ex      de, hl
        bit     7, h
        jr      z, sigx_bound
        bit     6, h
        jr      z, sigx_bound
        ex      de, hl


        ld      hl, 3                           ; get original s3 bank
        add     hl, sp
        ld      a, b                            ; preserve b
        ld      b, (hl)                         ; (sp+3) = BLSC_SR3

        ex      de, hl                          ; hl = start
        call    OSBixS1                         ; bind BHL in S1
        ld      b, a
        jr      sigx_mem

.sigx_bound
        ld      a, (BLSC_SR1)                   ; push S1 for OSBoxS1
        ld      d, a

.sigx_mem
        push    de

.sigx_loop
        call    sigx_read                       ; read byte from serial port
        jr      c, sigx_error
        ld      (hl), a
        inc     hl
        dec     bc
        ld      a, b
        or      c
        jr      nz, sigx_loop                   ; pv flag, reset if bc=0

        pop     de
        call    OSBoxS1
        ld      bc, (uwSmallTimer)              ; time remaining
        ex      de,hl                           ; DE points at last byte fetched + 1
        ret                                     ; Fc = 0 (from ld a,b + or c)

.sigx_error
        pop     de
        jp      OSBoxS1

.sigx_file
        call    sigx_read                       ; read byte from serial port
        ret     c
        push    bc                              ; preserve bc
        ld      bc, (uwSmallTimer)              ; use same timeout
        oz      OS_Pbt                          ; put byte to IX file/device
        pop     bc
        ret     c
        dec     bc
        ld      a, b
        or      c
        jr      nz, sigx_file
        ret

.sigx_read
if EAZYLINK_TESTPROTOCOL
        oz      Os_Gb                           ; read byte from EazyLink protocol testing file (IX file handle)
else
        exx
        ld      bc, (uwSerTimeout)              ; use default timeout
        call    OSSiGbt                         ; read byte from serial port
        exx
endif
        ret                                     ; with or without error


; -----------------------------------------------------------------------------
; SI_GXT (L = $21), get multiple bytes until ESC <x> terminator, using default timeout (SI_TMO)
;
; IN:
;       BC = number of byte to get from serial port, maximum 16384, one bank
;       DE = destination
;           DE = 0, write bytes to a file opened with its handle in IX
;           DE > 0, write bytes to memory at (DE)
;       IX = file handle (if DE = 0)
;
; Out if call succeeded, Fc = 0:
;       Fz = 1,
;               BC <> 0, A = ESC terminator encountered, number of bytes not read
;       Fz = 0,
;                A = FFh
;               BC = 0, all bytes read, no terminator encountered
;       DE = points at last byte fetched + 1 (if DE(in) > 0)
;
; Out if call failed, Fc = 1:
;       A = RC_TIME, timeout elapsed
;       A = RC_EOF, end of file reached
;       A = RC_WP, input device is read protected
;
;       BC = number of bytes not read
;
; Registers changed after return:
;
;       ......../IX.. same
;       AFBCDEHL/..IY different
;
.OSSiGxT
        ld      a, RC_Na
        ld      hl, $4000
        sbc     hl, bc
        ret     c
        ld      hl, (uwSerTimeout)              ; small timer counts default timeout
        ld      (uwSmallTimer), hl
        ld      hl, ubIntTaskToDo
        res     ITSK_B_TIMER, (hl)              ; reset timeout flag

        ld      a, d
        or      e
        jr      z, sigxt_file

        ex      de, hl
        bit     7, h
        jr      z, sigxt_bound
        bit     6, h
        jr      z, sigxt_bound
        ex      de, hl

        ld      hl, 3                           ; get original s3 bank
        add     hl, sp
        ld      a, b                            ; preserve b
        ld      b, (hl)                         ; (sp+3) = BLSC_SR3

        ex      de, hl                          ; hl = start
        call    OSBixS1                         ; bind BHL in S1
        ld      b, a
        jr      sigxt_mem

.sigxt_bound
        ld      a, (BLSC_SR1)                   ; push S1 for OSBoxS1
        ld      d, a

.sigxt_mem
        push    de

.sigxt_loop
        call    rxbyte                          ; read byte(s) from serial port
        jr      c, sigxt_exit                   ; serial port error, probably timeout
        jr      z, sigxt_exit                   ; ESC terminator encountered
        ld      (hl), e
        inc     hl
        dec     c
        jr      nz, sigxt_loop
        dec     b
        jr      nz, sigxt_loop
        or      $ff                             ; Fc = 0, Fz = 0, indicate buffer full
.sigxt_exit
        pop     de
        call    OSBoxS1
        ex      de,hl                           ; DE points at end of buffer + 1
        ret

.sigxt_file
        call    rxbyte                          ; read byte from serial port
        ret     c                               ; serial port error, probably timeout
        ret     z                               ; ESC terminator encountered
        ld      a, e
        push    bc                              ; preserve bc
        ld      bc, (uwSmallTimer)              ; use same timeout
        oz      OS_Pbt                          ; put byte to IX file/device (this is a slow operation..)
        pop     bc
        ret     c                               ; file I/O error...
        dec     c
        jr      nz, sigxt_file
        dec     b
        jr      nz, sigxt_file
        or      $ff                             ; Fc = 0, Fz = 0, indicate buffer full
        ret

.rxbyte
        call    sigx_read                       ; read byte(s) + ESC handling, from serial port, in (iy+0)
        ret     c
        cp      ESC
        jr      nz, byte_received
        call    sigx_read                       ; read byte from serial port
        ret     c
        cp      'B'
        jr      z, gethexnibbles
        cp      a
        ret                                     ; Fc = 0, Fz = 1 (indicate ESC x terminator encountered)
.gethexnibbles
        call    sigx_read                       ; read first 4bit hex nibble from serial port
        ret     c
        call    ConvHexNibble                   ; convert Ascii hex nibble into integer
        rlca
        rlca
        rlca
        rlca                                    ; first hex nibble * 16
        ld      e, a
        call    sigx_read
        ret     c                               ; serial port error or timeout
        call    ConvHexNibble                   ; calculate second 4 bit nibble
        or      e                               ; 'binary' byte received.
.byte_received
        ld      e, a
        or      $ff                             ; Fc = 0, Fz = 0, indicate byte received (not ESC terminator)
        ret

.ConvHexNibble
        cp      $3a                             ; digit >= "a"?
        jr      nc,hex_alpha                    ; digit is in interval "a" - "f"
        sub     $30                             ; digit is in interval "0" - "9"
        ret
.hex_alpha
        sub     $37
        ret


