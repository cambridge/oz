; **************************************************************************************************
; Memory Initialisation.
; (Kernel 0)
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2006
; (C) Gunther Strube (gstrube@gmail.com), 2005-2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

 Module MemInit

include "blink.def"
include "dor.def"
include "error.def"
include "stdio.def"
include "memory.def"
include "sysvar.def"
include "oz.def"
include "card.def"
include "lowram.def"

xdef    VerifySlotType
xdef    Chk128KB
xdef    Chk128KBslot0
xdef    AddRAMCard                              ; [Kernel1]/card.asm
xdef    MountAllRAM                             ; [Kernel1]/reset.asm
xdef    InitRAM                                 ; [Kernel1]/reset.asm
xdef    InitSlotRAM
xdef    MarkSwapRAM
xdef    MarkSystemRAM

xref    MS2BankK1                               ; [Kernel0]/knlbind.asm
xref    MS1BankA                                ; [Kernel0]/knlbind.asm
xref    MS2BankA
xref    ExpandMachine                           ; [Kernel1]/reset.asm
xref    AddAvailableFsBlock                     ; [Kernel0]/filesys.asm
xref    FilePtr2MemPtr                          ; [Kernel0]/filesys.asm
xref    MemPtr2FilePtr                          ; [Kernel0]/filesys.asm
xref    DecFreeRAM
xref    GetBankMAT0
xref    MarkPageAsAllocated
xref    PageNToMATPtr

; -----------------------------------------------------------------------------
; Verify ROM/EPROMcard
;
;IN:    D=slot in low bits
;       E=bank to test
;OUT:   D=card type
;
; *** IMPORTANT NOTE ***
; This call must not use any stack related instruction
; It is also called on boot when there is still no stack set
;
.VerifySlotType
        ld      a, d                            ; get last bank in slot
        or      a
        jr      nz, extslot
        ld      d,BU_ROM                        ; slot 0 always contains an OZ ROM
        ret
.extslot
        rrca
        rrca
        or      $3f
        ld      (BLSC_SR2), a                   ; and bind into S2
        out     (BL_SR2), a                     ; WE CANT USE MS2BankA (no stack when called from boot)
        ld      hl, (MM_S2<<8|CH_TAG)           ; card header tag

        ld      d, BU_EPR                       ;
        ld      bc, CT_EPR                      ; EPROM 'oz' tag
        sbc     hl, bc
        add     hl, bc
        jr      z, vst_1                        ; "oz" found

        inc     d                               ; BU_ROM
        ld      bc, CT_ROM
        or      a
        sbc     hl, bc
        jr      nz, vst_2                       ; "OZ" not found
.vst_1
        ld      hl, (MM_S2<<8|CH_TAG)           ; verify read
        sbc     hl, bc
        jr      nz, vst_2

        ld      a, (MM_S2<<8|CH_SIZ)            ; card size
        cp      $40                             ; handle this case (else is zero)
        ret     z
        cpl
        and     $3F                             ; a is last unused bank in slot
        cp      e                               ; bank to test
        ret     c                               ; size ok? ret
.vst_2
        ld      d, BU_NOT                       ; nothing inthe bank
        ret

; -----------------------------------------------------------------------------
; Return Fc = 1, if less than 128K RAM was found in slot 0, 1 or 2.
; If expanded RAM was found (>=128K), then return Fc = 0, A = bottom bank of found RAM card.
;
.Chk128KB
        ld      a, (ubSlotRamSize+1)            ; check expanded RAM in slot 1 first.
        cp      128/16
        ld      a, $40
        ret     nc

        ld      a, (ubSlotRamSize+2)            ; then, for expanded RAM in slot 2
        cp      128/16
        ld      a, $80
        ret     nc

; -----------------------------------------------------------------------------
; 
.Chk128KBslot0
        ld      a, (ubSlotRamSize)              ; finally, check if RAM in slot 0 >= 128K
        cp      128/16                          ; (less likely, since it requires hardware modification)
        ld      a, $21
        ret

; -----------------------------------------------------------------------------
; 
.AddRAMCard
        call    InitSlotRAM
        push    af
        call    MS2BankK1
        pop     af
        cp      $40
        push    af
        call    z, ExpandMachine                ; slot1? expand if 128KB or more
        pop     af
        cp      $80
        call    z, ExpandMachine                ; slot2? expand if 128KB or more


; -----------------------------------------------------------------------------
;
.MountAllRAM
        call    MS2BankK1
        ld      hl, RAMDORtable
.maram_1
        ld      a, (hl)                         ; 21 21 40 80 c0  bank
        inc     hl
        or      a
        ret     z
        call    MS1BankA
        ld      d, $40                          ; address high byte
        ld      e, (hl)                         ; 80 40 40 40 40  address low byte
        inc     hl
        ld      c, (hl)                         ;  -  0  1  2  3  RAM number
        inc     hl
        ld      a, c
        cp      '-'
        jr      z, maram_2
        ld      a, (de)                         ; skip if no RAM
        or      a
        jr      nz, maram_1
.maram_2
        push    hl
        ld      a, c
        cp      '-'
        jr      z, maram_3
        ex      af, af'
        ld      hl, $4000
        ld      a, (ubResetType)                ; 0 = hard reset
        and     (hl)
        jr      nz, maram_5                     ; soft reset & already tagged, skip
        ex      af, af'
.maram_3
        ld      hl, RAMFrontDOR
        ld      bc, 17
        ldir
        ld      (de), a
        inc     de
        ld      bc, 2                           ; just copy 00 FF
        ldir
        cp      '-'                             ; install APP device and tag RAM if not RAM.-
        jr      z, maram_4

        ld      bc, CT_RAM                      ; tag RAM
        ld      ($4000), bc
        jr      maram_5
.maram_4
        ld      e, $60                          ; APPFrontDOR at $4060
        ld      hl, APPFrontDOR
        ld      bc, 19
        ldir
.maram_5
        pop     hl
        jr      maram_1

.RAMDORtable                                    ; bank, DOR address low byte, char
        defb    RAX_BNK, $20, '-'
        defb    RA0_BNK, $40, '0'
        defb    RA1_BNK, $40, '1'
        defb    RA2_BNK, $40, '2'
        defb    RA3_BNK, $40, '3'
        defb    0

.RAMFrontDOR
        defm    $FF,0,0, 0,0,0, 0,0,0, DM_DEV, 9, DT_NAM, 6, "RAM.", 0, $FF

.APPFrontDOR
        defm    0,0,0,   0,0,0, 0,0,0, DN_APL, 8, DT_NAM, 5, "APPL", 0, $FF

; -----------------------------------------------------------------------------
; Initialize all RAM (called from Reset)
;
.InitRAM
        ld      c, MS_S2
        call    OZ_MGB
        push    bc                              ; remember current S2 binding
        ld      a, $21                          ; start with first RAM bank (skip system bank)
.ir_1
        call    InitSlotRAM                     ; init one slot
        and     $c0
        add     a, $40                          ; advance to next
        jr      nz, ir_1                        ; and init if not done yet
        pop     bc
        rst     OZ_MPB                          ; restore S2 binding
        ret

;       ----

;IN:    A=first bank to init
;OUT:   A=A(in)

.InitSlotRAM

        cp      $21
        push    af                              ; remember bank, b21 flag

        rlca                                    ; point IX to slot data
        rlca
        and     3
        ld      c, a
        ld      b, 0
        ld      ix, ubSlotRAMoffset
        add     ix, bc

        pop     af                              ; get bank
        push    af
        ld      (ix+0), 0                       ; RAM bank offset  !! ld (ix+n),b
        ld      (ix+4), 0                       ; RAM size
        ld      hl, (ubResetType)               ; ld l, (ubResetType)
        inc     l
        dec     l
        jr      z, initsl_1                     ; hard reset, force init

        call    MS2BankA                        ; bank into S2
        ld      hl, (MM_S2 << 8)                ; if tagged as RAM, leave alone
        ld      bc, $A55A
        or      a
        sbc     hl, bc
        jr      z, initsl_2

.initsl_1
        call    SlotRAMSize                     ; find out RAM size
        or      a
        jr      z, initsl_5                     ; no RAM

        ex      af, af'
        pop     af                              ; bind first bank into S2
        push    af
        call    MS2BankA
        ex      af, af'
        ld      (MM_S2 << 8 | $02), a           ; store size

.initsl_2
        ld      a, (MM_S2 << 8 | $02)
        ld      (ix+4), a                       ; RAM size
        inc     a                               ; A=(A*PAGES_PER_BANK*2)/256 - MAT size in pages
        srl     a
        push    af
        ld      b, a                            ; clear MAT
        ld      c, 0
        ld      de, MM_S2 << 8 | $101
        ld      hl, MM_S2 << 8 | $100
        ld      (hl), c
        dec     bc
        ldir

        ld      hl, MM_S2 << 8
        ld      a, (ubResetType)
        and     (hl)                            ; Fz=1 if hard reset or non-tagged RAM
        pop     hl                              ; H=MAT size in pages
        pop     de                              ; D=bank
        push    de
        ld      e, 1                            ; start from first device
        call    nz, MarkAllFsBlocks             ; soft reset? mark all filesystem blocks
        call    nz, AllocFsBlocks               ; soft reset? allocate all filesystem blocks

        ld      c, h                            ; C=MAT size in pages+1
        inc     c

        pop     af                              ; bank
        push    af
        and     $FE
        ld      (ix+0), a                       ; RAM bank offset
        cp      $40                             ; increment internal RAM size
        jr      nc, initsl_3
        inc     (ix+4)

.initsl_3
        ld      a, (ix+4)                       ; RAM size
        neg                                     ; clear out_of_memory flags
        ld      l, a
        ld      h, MM_S2                        ; end of first page
        xor     a
.initsl_4
        ld      (hl), a
        inc     l
        jr      nz, initsl_4

        ld      l, (ix+4)                       ; RAM size
        pop     af                              ; bank
        push    af
        add     a, l
        dec     a
        ld      d, a                            ; last RAM bank in slot

        pop     af                              ; bank, b21 flag
        push    af
        push    bc
        push    hl
        ld      bc, $3F01                       ; last block
        call    nz, MarkSystemRAM               ; reserve for system
        pop     hl
        pop     bc

        ld      h, 0                            ; HL=RAM size*$40 - number of pages
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        ld      de, (uwFreeRAMPages)            ; add them in
        add     hl, de
        ld      (uwFreeRAMPages), hl

        pop     de                              ; bank
        push    de

        ld      b, 0                            ; first page
        call    MarkSystemRAM                   ; reserve first page and MAT for system

.initsl_5
        pop     af                              ; AF(in)
        ret

;       ----

;       Find RAM size in slot
;
;IN:    A=first bank
;OUT:   A=number of RAM banks

.SlotRAMSize

        push    hl
        ld      b, a                            ; bank
        ld      c, 0                            ; size
        call    InitRAMBank                     ; clear RAM
        jr      c, srsz_2                       ; wasn't RAM

        ld      d, b                            ; remember first bank
        ld      a, $CA
        ld      (MM_S2 << 8), a                 ; mark with $CA

.srsz_1
        inc     c                               ; bump size
        inc     b                               ; bump bank
        ld      a, b                            ; exit if slot done
        and     $3F
        jr      z, srsz_2

        ld      a, b                            ; check for RAM
        call    InitRAMBank
        jr      c, srsz_2                       ; no RAM, exit

        ld      a, d                            ; see if first bank was overwritten
        call    MS2BankA
        ld      a, (MM_S2 << 8)
        sub     $CA
        jr      z, srsz_1                       ; no, loop back

.srsz_2
        ld      a, c                            ; return size
        pop     hl
        ret

;       ----

;       Fill RAM bank with zeroes
;
;IN:    A=bank
;OUT:   Fc=0 if ok, Fc=1 if not RAM

.InitRAMBank

        push    bc
        push    de
        push    hl
        call    CheckRAMBank                    ; check if bank is RAM
        jr      c, zram_0                       ; not, exit

; this check only necessary for internal RAM

        ld      a, ($0000)                      ; remember byte at $0000
        ld      hl, MM_S2 << 8
        ld      (hl), 0                         ; reset byte at $8000
        ld      bc, ($0000)                     ; read back from $0000
        ld      ($0000), a                      ; restore $0000
        inc     c
        dec     c
        scf
        jr      z, zram_0                       ; mirrored, not RAM

        ld      bc, $3FFF                       ; clear bank by copying 0 from
        ld      de, MM_S2 << 8 | $01            ; the first byte onward
        ldir

        or      a                               ; bank OK
.zram_0
        pop     hl
        pop     de
        pop     bc
        ret

;       ----

;       Check if bank is RAM
;
;IN:  A=bank
;OUT: Fc=0 if RAM

.CheckRAMBank

        call    MS2BankA

        ld      hl, MM_S2 << 8
        ld      a, (hl)                         ; get byte and save it
        ld      e, a
        cpl
        ld      (hl), a
        cpl
        xor     (hl)
        ld      (hl), e                         ; restore byte
        cp      $FF
        ret

;       ----


.RebuildMAT
        ld      a, (BLSC_SR1)                   ; remember S1
        push    af
        push    hl

.rbmat_1
        call    MarkFsBlock
        jr      c, rbmat_6                      ; error? exit
        jr      z, rbmat_7                      ; done? exit

        pop     hl
        pop     af
        call    MS1BankA                        ; restore S1

;       here is the entry point
;
;       DE=root filepointer

.MarkAllFsBlocks
        ld      a, (BLSC_SR1)                   ; remember S1
        push    af
        push    hl
        call    BindFilePtr

        ld      de, DOR_TYPE                    ; get DOR type
        add     hl, de
        ld      a, (hl)
        call    DORPtr2FilePtr                  ; son

        cp      Dn_Fil
        jr      nz, rbmat_3

        ld      d, b                            ; DE=BC, son
        ld      e, c
        call    MarkFileBlocks                  ; mark blocks of this file
        jr      rbmat_5                         ; continue to brother

.rbmat_3
        cp      Dn_Dir                          ; dir and device share same code
        jr      z, rbmat_4
        cp      Dm_Dev
        jr      nz, rbmat_6                     ; bad type? exit
.rbmat_4
        call    RebuildMAT                      ; recursive call

.rbmat_5
        call    DORPtr2FilePtr                  ; brother
        jr      rbmat_1


.rbmat_6
        OZ      OS_Pout
        defb    13, 10, 7
        defm    "FS Error",0                    ; filesystem error

.rbmat_7
        pop     hl
        pop     af                              ; restore S1
        jp      MS1BankA

;       ----

;       get DOR pointer just before HL and convert it into FilePtr in DE

.DORPtr2FilePtr

        dec     hl
        ld      b, (hl)
        dec     hl
        ld      d, (hl)
        dec     hl
        ld      e, (hl)
        push    de
        ex      de, hl
        push    de
        call    MemPtr2FilePtr                  ; DE=fileptr(BHL)
        pop     hl                              ; original HL-3
        pop     bc                              ; ptr from DOR, no bank
        ret

;       ----

;       bind in bank specified by FilePtr in DE, BHL=memptr

.BindFilePtr

        call    FilePtr2MemPtr
        ld      a, b
        call    MS1BankA

        res     7, h                            ; S1 fix
        set     6, h
        ret

;       ----

;       mark file blocks as used
;       uses bits 0-3 of MAT temporarily

.MarkFileBlocks

        ld      a, (BLSC_SR1)                   ; remember S1
        push    af
        push    hl
.mfb_1
        call    MarkFsBlock
        jr      c, mfb_2                        ; error? exit
        jr      z, mfb_2                        ; done? exit

        call    BindFilePtr
        ld      e, (hl)                         ; get next fs block
        inc     hl
        ld      d, (hl)
        inc     d
        dec     d
        jr      nz, mfb_1                       ; not end? mark it too

.mfb_2
        jr      rbmat_7                         ; !! 'pop hl; pop af; jp MS1BankA'

;       ----

;       mark single filesystem block
;
;IN:    DE=FilePtr
;OUT:   Fz=1 if DE=0
;       Fc=0 if successfull
;       Fc=1 if block already marked

.MarkFsBlock

        ld      a, d
        or      e
        ret     z                               ; file pointer 0, exit
        push    de
        ex      de, hl                          ; HL=D

        ld      a, (BLSC_SR2)                   ; A=base bank - 20/40/80/C0
        and     $FE
        sub     h
        neg
        ld      h, a                            ; H=bank-base = bank offset
        xor     a                               ; A=L&3 - filesystem block, HL=HL>>2, filesystem page
        srl     h
        rr      l
        rra
        srl     h
        rr      l
        rra
        rlca
        rlca
        call    PageNToMATPtr                   ; point HL to MAT entry

        ld      b, a                            ; A=1<<A
        inc     b
        xor     a
        scf
.mfsb_1
        rla
        djnz    mfsb_1

        ld      b, a                            ; remember mask
        and     (hl)
        scf
        ret     nz                              ; error if already set
        ld      a, b                            ; otherwise set it
        or      (hl)
        ld      (hl), a
        pop     de
        or      a                               ; Fc=0, unnecessary
        ret

;       ----

;       allocate all pages with filesystem blocks, add unused blocks
;       into filesystem chain

.AllocFsBlocks

        push    hl
        ld      b, h                            ; MAT size in pages
        ld      c, 0
        ld      a, (BLSC_SR2)
        and     $FE
        ld      d, a                            ; DE=FsPtr
        ld      e, c
        ld      hl, MM_S2 << 8 | $100           ; MAT start

.afsb_1
        ld      a, (hl)
        or      a
        jr      z, afsb_4                       ; no fs blocks? skip

        push    bc
        push    de
        push    hl

        ld      b, 4                            ; check 4 blocks
.afsb_2
        srl     (hl)
        jr      c, afsb_3                       ; fs block used? skip
        push    bc                              ; make block available to file system
        push    de
        call    AddAvailableFsBlock
        pop     de
        pop     bc
.afsb_3
        inc     de
        djnz    afsb_2

        pop     hl
        push    ix                              ; allocate page for file system
        ld      ix, (pFsMemPool)
        call    MarkPageAsAllocated
        pop     ix

        pop     de
        pop     bc
.afsb_4
        inc     de                              ; HL+=2, DE+=4, BC-=2
        inc     de
        inc     de
        inc     de
        inc     hl
        dec     bc
        cpi
        jp      pe, afsb_1                      ; no BC underflow? loop

        pop     hl
        ret


; -----------------------------------------------------------------------------
; Mark RAM reserved for bad apps
; IN:   D=bank
;       B=first page to mark
;       C=number of pages to mark
;
.MarkSwapRAM
        ld      hl, MAT_SWAP<<8
        jr      msr_1


; -----------------------------------------------------------------------------
; Mark RAM reserved for system
; IN:   D=bank
;       B=first page to mark
;       C=number of pages to mark
;
.MarkSystemRAM
        ld      hl, MAT_SYSTEM
.msr_1
        push    bc
        ld      c, MS_S2                        ; preserve S2, it will be destroyed
        call    OZ_MGB
        push    bc                              ; get current segment 2 binding in AF
        pop     af
        pop     bc                              ; restore bc on entry
        push    af                              ; save S2 binding

        push    bc
        push    hl
        call    GetBankMAT0                     ; bind in bank D
        pop     de                              ; mask
        pop     bc                              ; skip/mark count
        jr      c, msr_6                        ; no RAM? exit  !! ret c

        inc     b                               ; skip B entries
        jr      msr_3
.msr_2
        inc     hl                              ; skip one page MAT entry
        inc     hl
.msr_3
        djnz    msr_2

        ld      b, c                            ; mark C entries
        inc     b
        jr      msr_5
.msr_4
        ld      a, (hl)                         ; word |= in(HL)
        or      e
        ld      (hl), a
        inc     hl
        ld      a, (hl)
        or      d
        ld      (hl), a
        inc     hl
        call    z, DecFreeRAM                   ; if marking system RAM
.msr_5
        djnz    msr_4

        or      a                               ; Fc=0
.msr_6
        pop     bc                              ; restore S2 binding
        push    af                              ; preserve Fc
        rst     OZ_MPB
        pop     af
        ret

