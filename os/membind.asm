; -----------------------------------------------------------------------------
; Memory Segment Binding.
; (Kernel 0)
;
; Important:
;       For OZ V4.1 and higher those calls are redundant and available for application
;       backward compatibility with older ROM versions.
;       The RST 30H instruction with same B, C arguments are used internally by newer OZ
;       versions for faster bank switching (and smaller code size).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2006
; (C) Gunther Strube (gstrube@gmail.com), 2005-2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; -----------------------------------------------------------------------------

Module MemBind

include "memory.def"
include "blink.def"
include "error.def"
include "lowram.def"

xdef    OSFc
xdef    OSMgb
xdef    OSMpb

; -----------------------------------------------------------------------------
; Get current bank binding in segment
;
; IN:
;      C = memory segment specifier (MS_S0, MS_S1, MS_S2 & MS_S3)
;
; OUT, if call successful:
;      Fc = 0
;      B = bank number currently bound to that segment
;      C = C(in)
;
; OUT, if call failed:
;      Fc = 1
;      A = error code:
;           RC_ERR ($0F), C was not valid
;
; Registers changed after return:
;      ...CDEHL/IXIY same
;      AFB...../.... different
;
.OSMgb
        exx
        ld      a, c                            ; segment
        pop     bc                              ; pop S3 bank into B
        push    bc
        ld      c, a
        cp      3
        jr      z, ret_bank_binding             ; seg 3? we're done
        jr      nc, illg_MS_Sx                  ; seg >3? error

        call    OZ_MGB                          ; OZ V4.1: get bank binding status in B for C = MS_Sx
        jr      ret_bank_binding                ; return current bank binding for MS_Sx




; -----------------------------------------------------------------------------
; Set new bank binding in segment.
;
; IN:
;      C = memory segment specifier (MS_S0, MS_S1, MS_S2 & MS_S3)
;      B = bank number to bind into this segment ($00 - $FF)
;
; OUT, if call successful:
;      Fc = 0
;      B = bank number previously bound to that segment
;
; OUT, if call failed:
;      Fc = 1
;      A = error code:
;           RC_Ms ($0F), C was not valid
;
; Registers changed after return:
;      ...CDEHL/IXIY same
;      AFB...../.... different
;
.OSMpb
        exx
        ld      a, c                            ; segment
        cp      3
        jr      z, mpb_1                        ; segment 3? handle separately
        jr      nc, illg_MS_Sx                  ; segment >3? error

        rst     OZ_MPB                          ; OZ V4.1: execute bank binding, B = bank number, C = MS_Sx
        jr      ret_bank_binding                ; return old bank binding in B
.mpb_1
        pop     af                              ; pop S3 into A
        push    bc                              ; push new bank
        ld      b, a                            ; return old bank in B
.ret_bank_binding
        ex      af, af'
        or      a
        jp      OZCallReturn1
.illg_MS_Sx
        ld      a, RC_Ms
        scf
        jp      OZCallReturn1


; -----------------------------------------------------------------------------
; select fast code (fast bank switching)
; for API compatibility (slower than RST 30H)
;
.OSFc
        push    ix
        ex      af, af'
        dec     a
        scf
        ld      a, RC_Unk
        jr      nz, osfc_1                      ; reason not 1? exit

        exx                                     ; copy pointers from alternate registers
        push    hl                              ; and translate slot number into blink port
        push    de
        ld      a, c
        add     a, BL_SR0
        exx
        pop     de                              ; DE=destination
        push    de                              ; IX=DE
        pop     ix
        ld      bc, 6                           ; copy 6 bytes
        ld      hl, osfc_2                      ; from here
        ldir

        ld      (ix+1), a                       ; set softcopy low byte
        ld      (ix+4), a                       ; set output port

        dec     de                              ; point HL to 'ret'
        ex      de, hl

        pop     de                              ; DE=jump address
        ld      a, d
        or      e
        jr      z, osfc_1                       ; no jump, we're done

        ld      (hl), $C3                       ; jp opcode
        inc     hl
        ld      (hl), e
        inc     hl
        ld      (hl), d

.osfc_1
        ld      a, 8                            ; return code size  !! bug - overrides error code
        pop     ix
        jp      OZCallReturn2

.osfc_2
        ld      (BLSC_PAGE<<8), a
        out     (0), a
        ret
