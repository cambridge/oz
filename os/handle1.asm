; **************************************************************************************************
; Handle functions in kernel 1
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2006
; (C) Gunther Strube (gstrube@gmail.com), 2005-2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module Handle1

        include "dor.def"
        include "error.def"
        include "memory.def"
        include "sysvar.def"
        include "oz.def"
        include "handle.def"
        include "card.def"
        include "eprom.def"


xdef    OSFnMain
xdef    InitHandle

xref    AllocHandle                             ; [Kernel0]/handle0.asm
xref    VerifyHandle                            ; [Kernel0]/handle0.asm
xref    FreeHandle                              ; [Kernel0]/handle0.asm
xref    FindHandle                              ; [Kernel0]/handle0.asm
xref    GetDORType                              ; [Kernel0]/dor.asm
xref    S2VerifySlotType                        ; [Kernel0]/memmisc.asm

.OSFnMain
        ld      h, b                            ; exg a,b
        ld      b, a
        ld      a, h

        djnz    osfn_vh

;       FN_AH, allocate handle

;IN:    B=type
;OUT:   Fc=0, IX=handle
;       Fc=1, A=error

        jp      AllocHandle

.osfn_vh
        djnz    osfn_fh

;       FN_VH, verify handle
;IN:    IX=handle, B=type
;OUT:   Fc=0 if OK
;       Fc=1, A=error

        jp      VerifyHandle

.osfn_fh
        djnz    osfn_gh

;       FN_FH, free handle
;IN:    IX=handle, B=type
;OUT:   Fc=0 if OK
;       Fc=1, A=error

        jp      FreeHandle

.osfn_gh
        djnz    osfn_nh

;       FN_GH, get (or find) handle
;IN:    IX=start handle, 0 to start from first handle in list
;       B=type
;       C=dyn id, 0 if no check of dyn id
;OUT:   IX=handle
;       Fc=1, A=error

        ld      b, a                            ; exchanged on entry
        jp      FindHandle

.osfn_nh
        djnz    osfn_unk

;       FN_NH, get next handle (of same type)
;IN:    IX=handle
;OUT:   IX=handle
;       Fc=1, A=error

        push    ix
        pop     hl
        ld      a, h
        or      l
        ld      a, RC_Eof
        scf
        ret     z                               ; last handle
        ld      b, (ix+hnd_Type)                ; get type
        ld      c, 0                            ; dont check dyn id
        ld      l, (ix+hnd_Next)                ; jump to next handle
        ld      h, (ix+hnd_Next+1)
        jp      FindHandle

.osfn_unk
        ld      a, RC_Unk
        scf
        ret

;       ----

.InitHandle
        call    FindHandleDOR
        ret     c                               ; error? exit

        ld      (ix+dhnd_DeviceID), a
        bit     7, a
        jr      z, inh_1                        ; not ROM? skip

        push    af                              ; low 2 bits to top
        dec     a
        rrca
        rrca
        and     $C0
        ld      (ix+dhnd_AppSlot), a            ; application handle start
        pop     af

.inh_1
        ld      (ix+dhnd_DORBank), b
        ld      (ix+dhnd_DORH), h
        ld      (ix+dhnd_DOR), l

        dec     a
        jr      z, inh_2                        ; RAM.-? skip !! why not longer?

        or      $10                             ; !! 'rlca; rlca; rlca; or $80' for clarity
        rlca
        rlca
        rlca

.inh_2
        and     $F8                             ; mask out low bits
        or      c                               ; low bits: RAM=01, others=11
        ld      (ix+dhnd_flags), a
        jp      GetDORType

;       ----

;IN:    A=ID-1
;OUT:   Fc=0, BHL=DOR address, C=device handle flags (bit 0 and 1)
;       Fc=1, A=error if fail
;chg:   ABCDEHL/....

.FindHandleDOR
        inc     a
        ld      c, a

        ld      hl, DevTable-1
.fhd_loop
        inc     hl
        ld      a, (hl)                         ; device ID
        or      a
        jr      z, fhd_err                      ; end? error

        inc     hl
        ld      e, (hl)                         ; DE = DOR address
        inc     hl
        ld      d, (hl)
        inc     hl

        cp      c
        jr      c, fhd_loop                     ; smaller than wanted? loop

        ld      b, a
        dec     a
        jr      z, fhd_dev                      ; RAM.-? C=1

        cp      5
        jr      nc, fhd_2                       ; not RAM.x? skip

        and     3                               ; RAM.0123
        exx
        add     a, <ubSlotRamSize
        ld      l, a
        ld      h, >ubSlotRamSize
        ld      a, (hl)
        exx
        or      a
        jr      z, fhd_loop                     ; no RAM? loop
        jr      fhd_dev

.fhd_2
        cp      $81
        jr      c, fhd_3                        ; SCR|PRT|COM|NUL|INP|OUT|ROM0 ? C=spec
        cp      $84
        jr      nc, fhd_3                       ; not ROM123

; $81-$83 : Check :ROM.x availability in slot 1 to 3

        exx
        ld      d, a                            ; slot
        ld      e, $3F                          ; test last bank
        call    S2VerifySlotType
        bit     BU_B_ROM, d                     ; is application rom ?
        exx
        jr      z, fhd_loop
        jr      fhd_spec

.fhd_3
        cp      $90
        jr      c, fhd_spec                     ; APP ? C=spec
        cp      $94
        jr      nc, fhd_spec                    ; Others C=spec

; $90-$94 : Check :EPR.x availability in slot 0 to 3

        push    bc
        push    de
        push    hl
        and     @00000011                       ; slot mask
        ld      c, a
        ld      a, EP_ActSp                     ; eprom file area valid ?
        oz      OS_Epr
        pop     hl
        pop     de
        pop     bc
        jr      c, fhd_loop
                                                ; EPR C=spec

.fhd_spec
        ld      c, FFLG_DEV+FFLG_SPECDEV
        jr      fhd_x

.fhd_dev
        ld      c, FFLG_DEV

.fhd_x
        ld      a, b                            ; device ID
        ld      b, (hl)                         ; third attribute byte
        ex      de, hl                          ; HL=attribute word
        or      a
        ret
.fhd_err
        ld      a, RC_Fail
        scf
        ret

.fhd_loop1
; could improve device :APP.- verification here
        jr      fhd_3                           ; C=3

;       ----

;       defb    ID
;       defp    DOR address in S2

.DevTable
        defb    1
        defw    MM_S2<<8 | RAX_DOR
        defb    RAX_BNK                         ; RAM.-

        defb    2
        defw    MM_S2<<8 | RAM_DOR
        defb    RA1_BNK                         ; RAM.1

        defb    3
        defw    MM_S2<<8 | RAM_DOR
        defb    RA2_BNK                         ; RAM.2

        defb    4
        defw    MM_S2<<8 | RAM_DOR
        defb    RA3_BNK                         ; RAM.3

        defb    5
        defw    MM_S2<<8 | RAM_DOR
        defb    RA0_BNK                         ; RAM.0

        defb    6                               ; SCR.0
        defw    Scr_dor
        defb    KN1_BNK

        defb    7
        defw    Prt_dor
        defb    KN1_BNK

        defb    8
        defw    Com_dor
        defb    KN1_BNK

        defb    9
        defw    Nul_dor
        defb    KN1_BNK

        defb    10
        defw    Inp_dor
        defb    KN1_BNK

        defb    11
        defw    Out_dor
        defb    KN1_BNK

IF !OZ_SLOT1
; ROM.0 application DOR's only exist when compiling OZ for slot 0
; - booting OZ in slot 1 must ignore all applications in slot 0 due conflict with same applications in slot 1

        defb    $81
        defw    Rom0_dor
        defb    KN1_BNK
ENDIF

        defb    $82
        defw    Rom1_dor
        defb    KN1_BNK

        defb    $83
        defw    Rom2_dor
        defb    KN1_BNK

        defb    $84
        defw    Rom3_dor
        defb    KN1_BNK

        defb    $85                             ; APP.- for installed applications
        defw    Appl_dor
        defb    KN1_BNK

        defb    $91                             ; EPR.0 for Eprom Filing System
        defw    Epr0_dor
        defb    KN1_BNK

        defb    $92                             ; EPR.1 for Eprom Filing System
        defw    Epr1_dor
        defb    KN1_BNK

        defb    $93                             ; EPR.2 for Eprom Filing System
        defw    Epr2_dor
        defb    KN1_BNK

        defb    $94                             ; EPR.3 for Eprom Filing System
        defw    Epr3_dor
        defb    KN1_BNK

        defb    0


.Scr_dor
        dorchd  $8606, "SCR.0"                  ; NQ_Shn

.Prt_dor
        dorchd  $8609, "PRT.0"                  ; NQ_Phn

.Com_dor
        dorchd  $8C1E, "COM.0"                  ; NQ_Chn

.Nul_dor
        dorchd  $860C, "NUL.0"                  ; NQ_Nhn

.Inp_dor
        dorchd  $8615, "INP.0"                  ; NQ_Ihn

.Out_dor
        dorchd  $8618, "OUT.0"                  ; NQ_Ohn

IF !OZ_SLOT1
; ROM.0 application DOR's only exist when compiling OZ for slot 0
; - booting OZ in slot 1 must ignore all applications in slot 0 due conflict with same in slot 1

.Rom0_dor
        dorrom  MM_S2<<8 | ROM_DOR, RO0_BNK, "ROM.0"       ; (point at ROM Front DOR in top of bottom half of slot 0)
ENDIF

.Rom1_dor
        dorrom  MM_S2<<8 | ROM_DOR, RO1_BNK, "ROM.1"       ; (point at ROM Front DOR in top of slot 1)
.Rom2_dor
        dorrom  MM_S2<<8 | ROM_DOR, RO2_BNK, "ROM.2"       ; (point at ROM Front DOR in top of slot 2)
.Rom3_dor
        dorrom  MM_S2<<8 | ROM_DOR, RO3_BNK, "ROM.3"       ; (point at ROM Front DOR in top of slot 3)
.Appl_dor
        dorrom  MM_S2<<8 | APX_DOR, APX_BNK, "APP.-"       ; (point at APP Front DOR in R21)
.Epr0_dor
        dordev  0, 0, "EPR.0", Dm_Epr
.Epr1_dor
        dordev  0, 0, "EPR.1", Dm_Epr
.Epr2_dor
        dordev  0, 0, "EPR.2", Dm_Epr
.Epr3_dor
        dordev  0, 0, "EPR.3", Dm_Epr
