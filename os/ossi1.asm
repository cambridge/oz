; **************************************************************************************************
; OZ Low level serial port interface, called by INT handler and high level OS_Gbt/OS_Pbt.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module OS_Si1

        include "error.def"
        include "blink.def"
        include "stdio.def"
        include "handle.def"
        include "syspar.def"
        include "sysvar.def"
        include "serintfc.def"
        include "interrpt.def"
        include "lowram.def"


xdef    OSSiHrd1
xdef    OSSiSft1
xdef    OSSiEnq1
xdef    OSSiFtx1
xdef    OSSiFrx1
xdef    OSSiTmo1

; -----------------------------------------------------------------------------------------
; Serial interface hard reset
;
; reset UART in blink
; reset UART interrupt mask
; set 9600 baud speed in short word mode
; set UART interrupt
;
.OSSiHrd1
        xor     a
        ld      (ubSerControl), a               ; no XON/XOFF, no parity

        dec     a                          
        out     (BL_UAK), a                     ; ack all int (DCD, RTS)

        out     (BL_TXD), a                     ; ack TDRE
        in      a, (BL_RXD)                     ; ack RDRF

        ld      a, BM_RXCSHTW|BM_RXCUART|BM_RXCIRTS|BR_9600
        call    WrRxC                           ; reset UART in blink

        ld      a, BM_RXCSHTW|BM_RXCIRTS|BR_9600
        call    WrRxC                           ; set SHTW, invert RTS  @ 9600 baud

        ld      a, BM_TXCATX|BR_9600            ; set 9600 baud
        ld      (BLSC_TXC), a
        out     (BL_TXC), a

.si_rst_set_int

        call    OSSiFrx1                        ; flush RX buffer

        ld      a, BM_UMKRDRF                   ; enable RDRF interrupts, disable DCD, CTS, TDRE interrupts
        ld      (BLSC_UMK), a                   
        out     (BL_UMK), a

        ld      a, (BLSC_INT)
        or      BM_INTUART                      ; enable UART interrupt
        ld      (BLSC_INT), a
        out     (BL_INT), a

        or      a
        ret

        
; -----------------------------------------------------------------------------------------
; Serial interface soft reset
;
; set default timeout
; apply panel speed
; set autoTX acknoledge mode
; set UART interrupt
;
.OSSiSft1
        ld      bc, 6000
        call    OSSiTmo1                        ; default timeout 1 minute

        ld      bc, PA_Rxb                      ; get receive speed
        call    EnquireParam
        call    BaudToReg
        or      BM_RXCSHTW|BM_RXCIRTS           ; 8 bits|invert RTS line
        call    WrRxC                           ; RTS=((RDRF*ARTS)XOR IRTS) thus RTS=IRTS

        ld      bc, PA_Txb                      ; get transmit speed
        call    EnquireParam
        call    BaudToReg
        or      BM_TXCATX                       ; Auto TX mode controls CTS line
        ld      (BLSC_TXC), a
        out     (BL_TXC), a

        ld      bc, PA_Par                      ; get parity
        call    EnquireParam
        ld      c, 0                            ; Serial Interface Control byte (SIC)
        ld      a, e                            ; from enquiry
        cp      'N'                             ; none
        jr      z, srst_N

        set     SIC_B_PARITY, c
        cp      'E'                             ; even (00)
        jr      z, srst_1

.srst_O
        cp      'O'                             ; odd (10)
        jr      nz, srst_S
        set     SIC_B_ODD_MARK, c
        jr      srst_1

.srst_S
        cp      'S'                             ; space (01)
        jr      nz, srst_M
        set     SIC_B_STICKY, c
        jr      srst_1

.srst_M
        cp      'M'                             ; mark (11)
        jr      nz, srst_N
        set     SIC_B_STICKY, c
        set     SIC_B_ODD_MARK, c
        jr      srst_1

.srst_N
        res     SIC_B_PARITY,c                  ; no parity

.srst_1
        push    bc                              ; preserve c
        ld      bc, PA_Xon                      ; get Xon/Xoff
        call    EnquireParam
        pop     bc
        ld      a, e                            ; param is in DE
        cp      'Y'
        jr      nz, srst_2
        set     SIC_B_XONXOFF, c                ; set XONXOFF
        res     SIC_B_TXSTOP, c                 ; XON

.srst_2
        ld      a, c
        ld      (ubSerControl), a               ; set serial control byte
        jr      si_rst_set_int                  ; buffered RX routine if XON or parity set

        
; -----------------------------------------------------------------------------------------
; Serial interface buffer status enquiry
; 
; IN : -
; OUT:  BC = TxStatus (B used, C free)
;       DE = RxStatus (D used, E free)
;       A = int status
;       
.OSSiEnq1
        ld      de, (ubRXBWrPos)
        ld      a, e
        sub     d
        ld      d, a                            ; used
        cpl     
        ld      e, a                            ; free = 1 - used

        in      a, (BL_UIT)                     ; 
        bit     BB_UITTDRE, a                   ; is Tx empty ?
        jr      z, si_enq_rx2
        ld      b, 0
        ld      c, 1                            ; 1 byte free in Tx
        ret
.si_enq_rx2
        ld      b, 1                            ; 1 byte used in Tx
        ld      c, 0
        ret

; -----------------------------------------------------------------------------------------
; Serial interface timeout
;
; IN : BC = timeout in centiseconds
; OUT: -
;
.OSSiTmo1
        ld      (uwSerTimeout), bc
        or      a                               ; no error
        ret


; -----------------------------------------------------------------------------------------
; Serial interface buffer flush
; 
; IN : -
;
.OSSiFrx1
        push    hl
        ld      hl, 0
        ld      (ubRXBWrPos), hl                ; buffer empty
        pop     hl
        or      a
        ret

.OSSiFtx1
        ld      a, RC_Na                        ; not available
        scf
        ret



; -----------------------------------------------------------------------------------------
; Serial interface common subroutines
;


.WrRxC
        ld      (BLSC_RXC), a
        out     (BL_RXC), a
        ret

;       ----

.EnquireParam
        ld      de, 2                           ; return parameter in DE
        OZ      OS_Nq
        ret

;       ----

.BaudToReg
        xor     a                               ; count from 0 upwards
        ex      af, af'
        ld      b, d                            ; baud rate into BC
        ld      c, e
        ld      hl, BaudRates
.b2r_1
        ld      e, (hl)                         ; de=(hl)++
        inc     hl
        ld      d, (hl)
        inc     hl
        ld      a, d                            ; use 9600 if end of list
        or      e
        ld      a, BR_9600
        jr      z, b2r_3

        ex      de, hl
        sbc     hl, bc
        ex      de, hl
        jr      z, b2r_2                        ; found? exit

        ex      af, af'                         ; increment and loop
        inc     a
        ex      af, af'
        jr      b2r_1

.b2r_2
        ex      af, af'
.b2r_3
        and     7                               ; be sure to keep 3 first bits
        or      a                               ; reset Fc
        ret

.BaudRates
        defw    75, 300, 600, 1200, 2400, 9600, 19200, 38400
        defw    0

;       ----
