; **************************************************************************************************
; Key Code Mapping Table, used by screen driver for keyboard -> screen conversion.
; The table is located in Kernel 1, bound with the keyboard and screen driver functionality.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2006
; (C) Gunther Strube (gstrube@gmail.com), 2005-2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

module Key2Char_Table


; INTERNATIONAL version
;
; This table is used by ScrDrv1.asm for keyboard / screen conversion
; It is the bridge between key / char code / lores font.
;
; structure : internal key code / iso latin 1 code / lores1 bitmap low byte, high byte
;
; order in the lores1 font bitmap range : 000-1BF (NB: 1C0-1FF are the 64 UGD chars)
;

xdef    Key2Chr_tbl
xdef    Chr2VDU_tbl
xdef    VDU2Chr_tbl

.Key2Chr_tbl
        defb    $A3                             ; £ internal code
.Chr2VDU_tbl
        defb    $A3                             ; £ char code
.VDU2Chr_tbl
        defb            $1F,$00                 ; Lores low byte, high byte in the font
        defb    $9b,$BF,$87,$00                 ; ¿
        defb    $9c,$E1,$80,$00                 ; á
        defb    $9d,$ED,$81,$00                 ; í
        defb    $9e,$F3,$82,$00                 ; ó
        defb    $A1,$A7,$01,$00                 ; §
        defb    $A2,$B0,$02,$00                 ; °
        defb    $A4,$A4,$7F,$00                 ; €
        defb    $A4,$A4,$FF,$00                 ; € bold
        defb    $A4,$A4,$7F,$01                 ; € tiny
        defb    $A5,$F6,$1A,$00                 ; ö
        defb    $A5,$F6,$9A,$00                 ; ö bold
        defb    $A5,$F6,$1A,$01                 ; ö tiny
        defb    $A6,$E4,$0F,$00                 ; ä
        defb    $A6,$E4,$9F,$00                 ; ä bold
        defb    $A6,$E4,$0F,$01                 ; ä tiny
        defb    $A7,$D6,$0B,$00                 ; Ö
        defb    $A7,$D6,$8B,$00                 ; Ö bold
        defb    $A7,$D6,$0B,$01                 ; Ö tiny
        defb    $A8,$C4,$08,$00                 ; Ä
        defb    $A8,$C4,$88,$00                 ; Ä bold
        defb    $A8,$C4,$08,$01                 ; Ä tiny
        defb    $ab,$A1,$86,$00                 ; ¡
        defb    $B9,$E0,$0D,$00                 ; à
        defb    $B9,$E0,$8D,$00                 ; à bold
        defb    $B9,$E0,$0D,$01                 ; à tiny
        defb    $BA,$E2,$0E,$00                 ; â
        defb    $BA,$E2,$8E,$00                 ; â bold
        defb    $BA,$E2,$0E,$01                 ; â tiny
        defb    $BB,$E8,$13,$00                 ; è
        defb    $BB,$E8,$93,$00                 ; è bold
        defb    $BB,$E8,$13,$01                 ; è tiny
        defb    $BC,$E9,$14,$00                 ; é
        defb    $BC,$E9,$94,$00                 ; é bold
        defb    $BC,$E9,$14,$01                 ; é tiny
        defb    $BD,$EA,$15,$00                 ; ê
        defb    $BD,$EA,$95,$00                 ; ê bold
        defb    $BD,$EA,$15,$01                 ; ê tiny
        defb    $BE,$EE,$17,$00                 ; î
        defb    $BE,$EE,$97,$00                 ; î bold
        defb    $BE,$EE,$17,$01                 ; î tiny
        defb    $BF,$EF,$18,$00                 ; ï
        defb    $BF,$EF,$98,$00                 ; ï bold
        defb    $BF,$EF,$18,$01                 ; ï tiny
        defb    $C9,$F4,$19,$00                 ; ô
        defb    $C9,$F4,$99,$00                 ; ô bold
        defb    $C9,$F4,$19,$01                 ; ô tiny
        defb    $CA,$F9,$1C,$00                 ; ù
        defb    $CA,$F9,$9C,$00                 ; ù bold
        defb    $CA,$F9,$1C,$01                 ; ù tiny
        defb    $CB,$FB,$1D,$00                 ; û
        defb    $CB,$FB,$9D,$00                 ; û bold
        defb    $CB,$FB,$1D,$01                 ; û tiny
        defb    $CC,$DF,$06,$00                 ; ß
        defb    $CD,$EC,$00,$01                 ; ì
        defb    $CE,$F2,$01,$01                 ; ò
        defb    $CF,$FA,$83,$00                 ; ú
        defb    $D9,$EB,$16,$00                 ; ë
        defb    $D9,$EB,$96,$00                 ; ë bold
        defb    $D9,$EB,$16,$01                 ; ë tiny
        defb    $DA,$E5,$10,$00                 ; å
        defb    $DA,$E5,$90,$00                 ; å bold
        defb    $DA,$E5,$10,$01                 ; å tiny
        defb    $DB,$E6,$11,$00                 ; æ
        defb    $DB,$E6,$91,$00                 ; æ bold
        defb    $DB,$E6,$11,$01                 ; æ tiny
        defb    $DC,$FC,$1E,$00                 ; ü
        defb    $DC,$FC,$9E,$00                 ; ü bold
        defb    $DC,$FC,$1E,$01                 ; ü tiny
        defb    $DD,$F8,$1B,$00                 ; ø
        defb    $DD,$F8,$9B,$00                 ; ø bold
        defb    $DD,$F8,$1B,$01                 ; ø tiny
        defb    $DE,$F1,$84,$00                 ; ñ
        defb    $DF,$E7,$12,$00                 ; ç
        defb    $DF,$E7,$92,$00                 ; ç bold
        defb    $DF,$E7,$12,$01                 ; ç tiny
        defb    $E9,$CB,$04,$00                 ; Ë
        defb    $EA,$C5,$09,$00                 ; Å
        defb    $EA,$C5,$89,$00                 ; Å bold
        defb    $EA,$C5,$09,$01                 ; Å tiny
        defb    $EB,$C6,$0A,$00                 ; Æ
        defb    $EB,$C6,$8A,$00                 ; Æ bold
        defb    $EB,$C6,$0A,$01                 ; Æ tiny
        defb    $EC,$DC,$07,$00                 ; Ü
        defb    $ED,$D8,$0C,$00                 ; Ø
        defb    $ED,$D8,$8C,$00                 ; Ø bold
        defb    $ED,$D8,$0C,$01                 ; Ø tiny
        defb    $EE,$D1,$85,$00                 ; Ñ
        defb    $EF,$C7,$03,$00                 ; Ç
        defb    $00,$00,$00,$00                 ; table terminator
