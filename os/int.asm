; **************************************************************************************************
; IM 1 Interrupt services called from low ram interrupt handler (0038H).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2006
; (C) Gunther Strube (gstrube@gmail.com), 2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************


module Int

include "blink.def"
include "serintfc.def"
include "sysvar.def"
include "interrpt.def"
include "keyboard.def"
include "handle.def"
include "lowram.def"

xdef    Int_RTCS
xdef    Int_RTCM
xdef    IntSecond
xdef    IncActiveAlm
xdef    DecActiveAlm
xdef    MaySetPendingAlmTask
xdef    IntFlap

xref    Delay300Kclocks                         ; [Kernel0]/boot.asm
xref    ReadRTC                                 ; [Kernel0]/time.asm
xref    MS12BankCB                              ; [Kernel0]/memmisc.asm
xref    MS2BankK1                               ; [Kernel0]/memmisc.asm
xref    NMIMain                                 ; [Kernel0]/nmi.asm
xref    ChkCardChange                           ; [Kernel1]/card.asm
xref    StoreCardIDs                            ; [Kernel1]/card.asm


; -----------------------------------------------------------------------------
; Minute interrupt service
; IN : -
; OUT: -
; CHG: .....E../IXIY same
;
.Int_RTCM                                       ; MIN calls IntMinute and does timeout
        ld      a, BM_TACKMIN                   ; ack MIN
        out     (BL_TACK), a
        call    IntMinute

        ld      hl, ubIntTaskToDo
        bit     ITSK_B_SHUTDOWN, (hl)
        ret     nz                              ; already shutting down? exit

        ld      hl, ubTimeoutCnt
        dec     (hl)
        jp      m, int_RTCM1                    ; negative? restore - no timeout
        ret     nz

        ld      hl, ubIntTaskToDo               ; request shutdown
        set     ITSK_B_SHUTDOWN, (hl)
        ret
.int_RTCM1
        inc     (hl)
        ret

.IntMinute
        call    ReadHWClock
        ret     z                               ; no alarms? exit

        ld      hl, uwNextAlmMinutes+2
        ld      a, (hl)
        dec     hl
        or      (hl)
        dec     hl
        or      (hl)
        ret     z                               ; no next alarm

        inc     (hl)                            ; otherwise 24bit increment
        ret     nz
        inc     hl
        inc     (hl)
        ret     nz
        inc     hl
        inc     (hl)
        ret     nz

        ld      a, (ubNextAlmSeconds)
        or      a
        ret     nz

        ld      hl, ubIntStatus
        jr      ProcessAlm


; -----------------------------------------------------------------------------
; Second interrupt service
; IN : -
; OUT: -
; CHG: .....E../IXIY same
;
.Int_RTCS
        ld      a, BM_TACKSEC                   ; ack SEC
        out     (BL_TACK), a

.IntSecond
        call    ReadHWClock
        ret     z                               ; no alarms? exit

        ld      hl, ubNextAlmMinutesB
        ld      a, (hl)
        dec     hl                              ; ubNextAlmMinutes+1
        or      (hl)
        dec     hl                              ; ubNextAlmMinutes
        or      (hl)
        ret     nz                              ; still minutes to go

        dec     hl
        ld      a, (hl)                         ; ubNextAlmSeconds
        or      a
        jr      z, ProcessAlm                   ; do alarm if 0 or -1 seconds left
        inc     (hl)    
        ret     nz

.ProcessAlm
        ld      hl, ubIntStatus
        res     IST_B_ALARM, (hl)
        
        ld      hl, pNextAlmHandle               ; ld  ix, (pNextAlmHandle)
        ld      a, (hl)
        inc     hl
        ld      h, (hl)
        ld      l, a
        ld      bc, ahnd_Flags                   ; set AHNF_B_ACTIVE, (ix+ahnd_Flags)
        add     hl, bc
        set     AHNF_B_ACTIVE, (hl)

.IncActiveAlm
        ld      hl, ubNumActiveAlm
        inc     (hl)
        jr      MaySetPendingAlmTask

.DecActiveAlm
        ld      hl, ubNumActiveAlm
        dec     (hl)

.MaySetPendingAlmTask
        ld      hl, ubIntTaskToDo
        ld      a, (ubAlmDisableCnt)
        or      a
        jr      nz, intsec_cncalm               ; disabled? remove alarm task

        ld      a, (ubNumActiveAlm)
        or      a
        jr      z, intsec_cncalm                ; none active? remove alarm task

        set     ITSK_B_ALARM, (hl)
        bit     ITSK_B_PREEMPTION, (hl)
        ret     z                               ; no wake up? ret
                         
        ld      hl, KbdData+kbd_flags           ; else lockout
        set     KBF_B_LOCKED, (hl)
        ret

.intsec_cncalm
        res     ITSK_B_ALARM, (hl)
        ret

.ReadHWClock
        ld      hl, ubTIM0_A                    ; read hardware clock to either buffer A or B
        ld      a, (ubTimeBufferSelect)         ; bit 0 selects buffer
        rrca
        jr      c, rhwc_1
        ld      hl, ubTIM0_B                    ;(&255)  ; buffer B

.rhwc_1
        call    ReadRTC
        ld      hl, ubTimeBufferSelect
        inc     (hl)                            ; alternate timer set changing bit 0
        ld      hl, ubIntStatus
        set     IST_B_ALMTIMEOK, (hl)
        bit     IST_B_ALARM, (hl)               ; return alarms status
        ret

; -----------------------------------------------------------------------------
; Flap interrupt service
; IN : -
; OUT: -
; CHG: ......../IXIY same
;
.IntFlap
        ld      bc, (BLSC_SR1)                  ; remember S1/S2
        push    bc
        exx
        push    bc
        push    de
        push    hl
        push    ix
        call    MS2BankK1
        call    StoreCardIDs

        ld      a, (BLSC_COM)                   ; beep
        or      BM_COMSRUN
        out     (BL_COM), a

.intf_1
        push    af
        call    Delay300Kclocks
        ld      a, BM_INTFLAP                   ; ack flap
        out     (BL_ACK), a
        in      a, (BL_STA)
        bit     BB_STAFLAPOPEN, a
        call    nz, NMIMain                     ; halt until flap closed?

        pop     af
        jr      nc, intf_2

        ld      a, (BLSC_COM)                   ; beep
        or      BM_COMSRUN
        out     (BL_COM), a
.intf_2
        call    ChkCardChange
        jr      c, intf_1                       ; go back

        ld      a, (BLSC_COM)                   ; restore BL_COM
        out     (BL_COM), a
        pop     ix
        pop     hl
        pop     de
        pop     bc
        exx
        pop     bc                              ; restore S1/S2
        jp      MS12BankCB
