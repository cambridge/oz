; **************************************************************************************************
; The OZ window display routines
;
; This table was extracted out of Font bitmaps from original V3.x and V4.0 ROMs using FontBitMap tool,
; and combined/re-arranged into the new international font bitmap by Thierry Peycru.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Additional development improvements, comments, definitions and new implementations by
; (C) Thierry Peycru (pek@users.sf.net), 2005-2006
; (C) Gunther Strube (gstrube@gmail.com), 2005-2006
;
; **************************************************************************************************
;
; OZ window layout
; ROW   MESSAGE
; 1     OZ / INDEX / FAIL / LOCKOUT
; 2     localisation (2 letters)
; 3     CLI
; 4     alarm bell
; 5     command
; 6     bat  / command
; 7     low  / command
; 8     caps / command
;

        module  OZWindow

        include "screen.def"
        include "sysvar.def"
        include "interrpt.def"
        include "keyboard.def"

xdef    OZwd_card
xdef    OZwd_index
xdef    OZwd__fail
xdef    DrawOZwd
xdef    SetPendingOZwd

xref    ScreenOpen                              ; [Kernel0]/srcdrv0.asm
xref    ScreenClose                             ; [Kernel0]/srcdrv0.asm



.OZwd__fail
        ld      bc, OZW_FAIL
        call    ScreenOpen
        ld      hl, LCD_ozrow1
        call    OZwd_write_flash
        jr      OZwd__fail     

.OZwd_card
        ld      bc, OZW_CARD
        jr      OZwd_raw1

.OZwd_index
        ld      bc, OZW_INDEX
        
.OZwd_raw1
        call    ScreenOpen
        ld      hl, LCD_ozrow1
        call    OZwd_write_flash
        call    ScreenClose
        ret


.DrawOZwd
        call    ScreenOpen

        ld      hl, ubIntTaskToDo
        res     ITSK_B_OZWINDOW, (hl)           ; ack OZWd refresh

; OZ/LOCK OUT (line 1)
        ld      hl, LCD_ozrow1

        ld      de, (KbdData+kbd_flags)         ; show "lock out" if locked
        bit     KBF_B_LOCKED, e
        jr      nz, d_locked

        ld      bc, OZW_OZ
        call    OZwd_write_grey
        jr      d_country

.d_locked
        ld      bc, OZW_LOCKOUT
        call    OZwd_write_flash

; Country (line 2)
.d_country
        ld      hl, LCD_ozrow2
        ld      bc, (aKmCountry)
        ld      a, b
        or      c
        call    nz, OZwd_write_grey

; CLI (line 3)
        ld      hl, LCD_ozrow3

        ld      a, (ubCLIActiveCnt)
        or      a
        jr      z, d_nocli
        ld      bc, OZW_CLI
        call    OZwd_write_flash
        jr      d_bell
.d_nocli
        call    OZwd_write_blank

; BELL (line 4)
.d_bell
        ld      hl, LCD_ozrow4
        ld      a, (ubAlmDisplayCnt)
        or      a
        jr      z, d_nobell
        ld      bc, OZW_BELL
        call    OZwd_write_flash
        jr      d_batlow
.d_nobell
        call    OZwd_write_blank        

; BATLOW (lines 6 and 7)
.d_batlow
        ld      bc, OZW_BLANK
        ld      d, b
        ld      e, c

        ld      a, (ubIntStatus)
        bit     IST_B_BATLOW, a
        jr      z, d_nobatlow
        ld      bc, OZW_BAT
        ld      de, OZW_LOW

.d_nobatlow
        call    OZcmdActive                     ; get attributes, grey if <> or [] active
        ld      hl, LCD_ozrow6
        call    VDUputBCA
        ld      hl, LCD_ozrow7
        ld      b, d
        ld      c, e
        call    VDUputBCA

; CAPS/caps (line 8)
        ld      hl, LCD_ozrow8
        ld      bc, OZW_BLANK

        ld      a, (KbdData+kbd_flags)
        bit     KBF_B_CAPSE, a
        jr      z, d_caps1
        ld      bc, OZW_HCAPS
        bit     KBF_B_CAPS, a
        jr      z, d_caps1
        ld      bc, OZW_LCAPS
.d_caps1
        call    OZwd_write

; Deadkey (line 5)
        ld      a, (ubKmDeadchar)
        or      a
        jr      nz,d_dk1
        ld      a, OZW_SPC                      ; default char is a space in OZ font (8 bits width)
.d_dk1
        ld      c,  a

; Appkey/Command (lines 5-8)
        call    OZcmdActive
        jr      c, d_key

        ld      hl, ubSysFlags1
        ld      c, OZW_SQUARE                   ; '[]'
        bit     SF1_B_OZSQUARE, (hl)
        jr      nz, d_key
        ld      c, OZW_DIAMND                   ; '<>'

.d_key
        ld      hl, LCD_ozrow5
        ld      (hl), c

        ld      b, 4
        ld      de, OZcmdBuf
        ld      hl, LCD_ozrow5+2                ; column 2 in OZ window
        ld      (hl), OZW_SPC
        jr      c, d_end

.d_keyloop
        ld      a, (de)                         ; cmd char
        or      a
        jr      z, d_end                        ; end of command? exit

        or      $80                             ; into char
        ld      (hl), a                         ; onto screen
        inc     l
        ld      (hl), LCDA_HIRES|LCDA_UNDERLINE|LCDA_CH8
        dec     l
        inc     de                              ; next char
        inc     h                               ; next line
        djnz    d_keyloop

.d_end
        call    ScreenClose

        or      a                               ; Fc=0, why?
        ret

;       ----


.OZcmdActive
        ld      a, (ubSysFlags1)
        and     SF1_OZDMND|SF1_OZSQUARE         ; <> or [] in OZ wd
        ld      hl, OZcmdBuf
        bit     7, (hl)                         ; char in the 00-127 range
        jr      z, oca_1
        cp      a                               ; Fz=1
        jr      oca_2                           ; ret with Fc=1 and A screen attributes
.oca_1
        or      (hl)                            ; first app/command char
.oca_2
        scf
        ld      a, LCDA_HIRES|LCDA_UNDERLINE|LCDA_CH8
        ret     z                               ; Fc=1 if no command
        or      LCDA_GREY                       ; Fc=0 if command
        ret

;       ----

.OZwd_write_blank
        ld      bc, OZW_BLANK
        
.OZwd_write
        ld      a, LCDA_HIRES|LCDA_UNDERLINE|LCDA_CH8

.VDUputBCA
        ld      (hl), b
        inc     l
        ld      (hl), a
        inc     l
        ld      (hl), c
        inc     l
        ld      (hl), a
        ret

.OZwd_write_grey
        ld      a, LCDA_HIRES|LCDA_GREY|LCDA_UNDERLINE|LCDA_CH8
        jr      VDUputBCA

.OZwd_write_flash
        ld      a, LCDA_HIRES|LCDA_FLASH|LCDA_UNDERLINE|LCDA_CH8
        jr      VDUputBCA

;       ----

;       request OZ window redraw

.SetPendingOZwd
        ld      hl, ubIntTaskToDo
        set     ITSK_B_OZWINDOW, (hl)
        ret
