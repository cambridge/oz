; **************************************************************************************************
; Memory Allocation.
; (Kernel 0)
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2006
; (C) Gunther Strube (gstrube@gmail.com), 2005-2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

Module Malloc

include "blink.def"
include "dor.def"
include "error.def"
include "memory.def"
include "handle.def"
include "sysvar.def"
include "oz.def"

xdef    OSMop
xdef    OsMcl
xdef    OSMal
xdef    OSMfr
xdef    OSAxp
xdef    OSAxm                                   ; Allocate eXplicit Memory (OZ 4.6 and newer)
xdef    OSFxm                                   ; Free eXplicit Memory (OZ 4.6 and newer)
xdef    OSFma                                   ; Find Memory for Allocation (OZ 4.6 and newer)
xdef    MarkPageAsAllocated
xdef    FollowPageN
xdef    MATPtrToPagePtr
xdef    PageNToPagePtr
xdef    PageNToMATPtr                           ; [Kernel0]/meminit.asm
xdef    DecFreeRAM                              ; [Kernel0]/meminit.asm
xdef    GetBankMAT0                             ; [Kernel0]/meminit.asm

xref    MS1BankA                                ; [Kernel0]/knlbind.asm
xref    MS2BankA                                ; [Kernel0]/knlbind.asm
xref    PutOSFrame_BHL                          ; [Kernel0]/stkframe.asm
xref    PutOSFrame_HL                           ; [Kernel0]/stkframe.asm
xref    PutOSFrame_BC                           ; [Kernel0]/stkframe.asm
xref    PutOSFrame_DE                           ; [Kernel0]/stkframe.asm
xref    OSFramePop                              ; [Kernel0]/stkframe.asm
xref    OSFramePush                             ; [Kernel0]/stkframe.asm
xref    AllocHandle                             ; [Kernel0]/handle.asm
xref    FreeHandle                              ; [Kernel0]/handle.asm
xref    VerifyHandle                            ; [Kernel0]/handle.asm
xref    OZwd__fail                              ; [Kernel0]/ozwindow.asm

; -----------------------------------------------------------------------------
; open memory (allocate memory pool)
; IN:   A=flags, C=slot/bank (if MM_B_SLT set in A)
; OUT:  IX=MemHandle
;
.OSMop
        call    OSFramePush
        call    OSMopMain
        jp      OSFramePop


.OSMopMain
        ld      a, HND_MEM
        call    AllocHandle
        ret     c                               ; no handle? exit

        ld      a, (iy+OSFrame_A)
        ld      (ix+mhnd_AllocFlags), a

;       set ExclusiveBank range if required  (not documented)

        bit     MM_B_SLT, a
        jr      z, osmop_1
        ld      (ix+mhnd_ExclusiveBank), c

;       set ExclusiveBank unless multiple banks required

.osmop_1
        bit     MM_B_MUL, a                     ; use multiple banks
        call    z, SetExclusiveBank

        cp      a                               ; Fc=0
        ret

; -----------------------------------------------------------------------------
; close memory (free memory pool)
; IN:   IX=MemHandle
; OUT:  Fc=0 if no error
;
.OsMcl
        call    OSFramePush
        call    OSMclMain
        jp      OSFramePop


.OSMclMain
        ld      a, HND_MEM
        call    VerifyHandle
        ret     c                               ; bad handle? exit


        ld      a, 4                            ; do slots 3-0
.osmcl_1
        dec     a                               ; decrement slot
        push    af                              ; and remember it

        push    iy
        call    MS2SlotMAT                      ; bind slot MAT in S2
        pop     iy
        jr      c, osmcl_3                      ; no RAM? skip slot

        call    GetMhndSlotAlloc                ; get allocation chain for this slot
        jr      z, osmcl_3                      ; no mem from this slot? skip it

;       free all memory allocated from this slot

.osmcl_2
        ex      de, hl                          ; HL=MAT entry
        call    PageNToMATPtr                   ; point HL to allocation MAT entry

        ld      e, (hl)                         ; DE=next entry
        inc     l
        ld      a, (hl)
        dec     l
        bit     MAT_B_LAST, a                   ; remember last_flag
        push    af
        and     $0f                             ; remove flags
        ld      d, a

        call    MarkPageFreeMATptr              ; mark page HL as free

        pop     af                              ; restore last_flag
        jr      z, osmcl_2                      ; not last? free next

        ex      de, hl                          ; next*16 should be MemHandle
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        push    ix                              ; does it match?
        pop     de
        or      a
        sbc     hl, de
        jp      nz, OZwd__fail                  ; nope? fail as we just freed unknown memory

.osmcl_3
        pop     af                              ; restore slot
        jr      nz, osmcl_1                     ; was not 0? loop

        ld      a, HND_MEM
        jp      FreeHandle

; -----------------------------------------------------------------------------
; allocate memory
; IN:   A=slot, 1-3
;       BC=allocation size, max. 256 bytes
;       IX=MemHandle
; OUT:  Fc=0 - BHL=memory, C=segment
;       Fc=1, A=error if failed
;
.OSMal
        call    OSFramePush
        ex      af, af'                         ; remember S1
        ld      a, (BLSC_SR1)
        push    af
        ex      af, af'
        call    OSMalMain
        ex      af, af'                         ; restore S1
        pop     af
        call    MS1BankA
        ex      af, af'
        jp      OSFramePop

.OSMalMain
        ld      h, b
        ld      l, c
        ld      de, $0100 + 1
        or      a
        sbc     hl, de 
        jp      nc, mal_block                   ; more than 1 page requested

        call    MemCallAttrVerify
        ret     c                               ; bad size? exit
        jr      z, osmal_page                   ; size=256? allocate whole page

;       allocate partial page
;       first, we check previously allocated pages for big enough chunk

        ld      a, 3                            ; slot 3-2-1-0

.osmal_chk1
        push    af

        push    iy
        call    MS2SlotMAT                      ; bind slot MAT in S2
        pop     iy
        jr      c, osmal_chk4                   ; no RAM in slot? skip slot

        call    GetMhndSlotAlloc                ; MATentry in DE
        jr      z, osmal_chk4                   ; no allocations from this slot? skip slot

        ex      de, hl                          ; HL=MatEntry, DE=ptr to it
.osmal_chk2
        call    PageNToMATPtr

        inc     l
        bit     MAT_B_FULL, (hl)
        dec     hl                              ; dont use 'dec l' it could change Fz, 'dec hl' does not 
        jr      nz, osmal_chk3                  ; non-splitted? skip page

        call    MATPtrToPagePtr                 ; point HL to this page
        call    MS1BankA                        ; and bind it in

;       !! validate page inside AllocChunk - full chunk list traversing can be put
;       !! into good use by allocating from smallest possible chunk inside page
;       !! to reduce fragmentation. Post-allocation validate is unnecessary.

        call    ValidatePage
        jp      c, OZwd__fail                   ; invalid? crash

        ld      c, (iy+OSFrame_C)               ; size, possibly fixed in MemCallAttrVerify
        push    de                              ; ptr to MATentry
        call    AllocChunk                      ; HL=memory if succesfull
        push    af

        push    hl
        call    ValidatePage                    ; !! unnecessary
        pop     hl
        jp      c, OZwd__fail

        pop     af
        pop     de
        jr      nc, osmal_chkok                 ; got chunk

        ex      de, hl                          ; HL=MATptr
.osmal_chk3
        call    FollowMATPtr                    ; get pointer to next
        jp      z, OZwd__fail

        ld      a, d                            ; HL=MAToffset(DE)
        and     $0F                             ; remove flags
        ld      h, a
        ld      l, e

        bit     MAT_B_LAST, d
        jr      z, osmal_chk2                   ; not last? try next page

.osmal_chk4
        pop     af                              ; try previous slot
        dec     a
        jp      p, osmal_chk1

;       ok, we couldn't allocate chunk from previously allocated pages.
;       allocate new page and allocate from it.

        call    osmal_AllocPage                 ; allocate new page - HL=MATEntryPtr
        ret     c                               ; no mem? exit

        inc     l                               ; mark page as splitted
        res     MAT_B_FULL, (hl)
        dec     l

        call    MATPtrToPagePtr                 ; point HL to page
        call    MS1BankA                        ; and bind it in

        ld      (hl), 1                         ; first chunk at H01
        inc     l
        ld      (hl), $FF                       ; chunk size, 255 bytes
        inc     l
        ld      (hl), 0                         ; no more chunks
        dec     l
        dec     l

        call    ValidatePage                    ; !! unnecessary, can't fail
        jp      c, OZwd__fail

        ld      c, (iy+OSFrame_C)
        call    AllocChunk                      ; !! could do first allocation here, it's simple
        jp      c, OZwd__fail                   ; !! unnecessary, can't fail

        push    bc                              ; !! defb OP_LDAn

.osmal_chkok
        pop     bc                              ; fix stack
        ld      a, (BLSC_SR1)                   ; get bank
        jr      osmal_ok                        ; and return

;       allocate a whole page

.osmal_page
        call    osmal_AllocPage                 ; allocate new page - HL=MATEntryPtr, A=bank
        ret     c                               ; no mem? exit
        call    MATPtrToPagePtr                 ; point HL to it

;       got mem, return it

.osmal_ok
        ld      b, a                            ; bank

        ld      a, (ix+mhnd_AllocFlags)
        and     $C0                             ; segment
        jr      nz, osmal_8                     ; not S0? ok
        bit     5, h                            ; address memory at $2000
        set     5, h                            ; if address was already there then address the upper half of bank
        jr      z, osmal_8
        set     0, b                            ; odd bank

.osmal_8
        bit     MM_B_MUL, (ix+mhnd_AllocFlags)  ; set ExclusiveBank if not multiple banks
        jr      nz, osmal_9
        ld      (ix+mhnd_ExclusiveBank), b

.osmal_9
        res     6, h                            ; fix segment
        or      h                               ; segment specifier
        ld      h, a
        call    PutOSFrame_BHL

        rlca                                    ; return segment in C
        rlca
        and     3
        ld      (iy+OSFrame_C), a
        or      a                               ; Fc=0
        ret

.osmal_AllocPage
        ld      e, (ix+mhnd_AllocFlags)
        ld      d, (ix+mhnd_ExclusiveBank)

        ld      a, (iy+OSFrame_A)               ; set ExclusiveBank if it's zero and A=$01-$1F
        or      a                               ; !! check D before A, saves time
        jr      z, osmal_11                     ; !! dec a; cp $1f; jr nc,...; inc a
        cp      $20
        jr      nc, osmal_11

        inc     d
        dec     d
        jr      nz, osmal_11
        ld      d, a                            ; !! undocumented - slot A

.osmal_11
        call    FindFreePage                    ; find a free page
        ret     c                               ; error? exit
                                                ; otherwise drop thru
;       ----

; in: IX=memhandle
;     HL=MAT entry

.MarkPageAsAllocated

        call    DecFreeRAM                      ; one page less

        call    GetMhndSlotAlloc                ; get first allocation from this slot
        jr      nz, mpaa_2                      ; already had mem from this slot

        push    ix                              ; IX>>4 as next pointer
        pop     de
        ld      b, 4
.mpaa_1
        srl     d
        rr      e
        djnz    mpaa_1
        set     MAT_B_LAST, d                   ; mark as last in chain

;       set next_pointer for new page

.mpaa_2
        ld      (hl), e                         ; <next
        inc     l
        ld      a, (hl)                         ; keep SWAP bit
        and     MAT_SWAP
        or      MAT_FULL                        ; no partial allocation possible
        or      d                               ; >next + possibly last flag
        bit     MM_B_FIX, (ix+mhnd_AllocFlags)
        jr      nz, mpaa_3                      ; MAT_ALLOCFIXED = NOT(MM_FIX)
        or      MAT_ALLOCFIXED, a
.mpaa_3
        ld      (hl), a                         ; >next
        dec     l

;       set new page as first in chain

        ld      d, h                            ; DE=MAToffset(HL) - return it in HL
        ld      e, l
        call    MATPtrToPageN
        ex      de, hl                          ; drop thru

;       ----

;       IN: DE=MAT entry to put in MemHandle IX
;       slot selection is based on bank in S2

.SetMhndSlotAlloc

        push    hl
        call    GetMhndSlotAllocPtr
        ld      (hl), e
        inc     l
        ld      (hl), d
        pop     hl
        or      a                               ; Fc=0
        ret

;       ----

;       DE=first allocation in slot bound in S2

.GetMhndSlotAlloc

        push    hl
        call    GetMhndSlotAllocPtr
        ld      e, (hl)                         ; low byte
        inc     l
        ld      a, (hl)                         ; high byte
        and     $0f                             ; remove flags
        ld      d, a
        or      e                               ; Fz=1 if no allocations
        pop     hl
        ret

;       ----

;       HL=ptr to first allocation entry num

.GetMhndSlotAllocPtr

        ld      a, (BLSC_SR2)                   ; get bank
        rlca
        rlca
        and     3                               ; slot
        rlca                                    ; *2 for word table
        add     a, mhnd_Slot0
        push    ix                              ; HL+=A
        pop     hl
        add     a, l                            ; doesn't overflow - could use 'or' as well
        ld      l, a
        ret

;       ----

;       check and adjust memory requested in BC (from OSMal and OSMfr)
;       Fc = 1, more than 1 page requested (or bad handle)
;       FC = 0,
;               Fz = 1, 1 page requested
;               Fz = 0, less than 254 bytes requested, size adjusted

.MemCallAttrVerify
        ld      a, HND_MEM                      ; first verify handle type
        call    VerifyHandle
        ret     c
        inc     b                               ; check allocation size !! wouldn't this be easier with cp?
        dec     b
        jp      z, mcav_1                       ; 0-255, ok
        dec     b
        jr      nz, mcav_err                    ; >256, fail
        inc     c
        dec     c
        ret     z                               ; 256 - ok
        scf                                     ; ?? why like this?
.mcav_1
        inc     c
        inc     c
        jr      nz, mcav_2                      ; not FE
        dec     (iy+OSFrame_C)                  ; ?? FE->FD
        dec     c                               ; ff
        ret
.mcav_2
        dec     c
        dec     c
        dec     c
        jr      nz, mcav_3
        inc     c                               ; 1 -> 2
        inc     c
        ret
.mcav_3
        inc     c
        ret     nz                              ; not 0? ok
.mcav_err
        ld      a, RC_Fail
        scf
        ret

;       ----

;       allocate block

.mal_block
        inc     c
        dec     c
        ld      c, 0
        jr      z, mal_adjusted
        inc     b                               ; adjust BC to upper page
.mal_adjusted
        ld      a, b
        cp      64+1                            ; max. 64 pages = 16K
        ld      a, RC_Fail
        ret     nc
        bit     MM_B_MUL, (ix+mhnd_AllocFlags)  ; only multiple banks usage allowed
        ret     z                               ; Fc = 1 and A = RC_Fail
        ld      h, 0                            ; no offset
        ld      l, b                            ; number of pages requested
        bit     MM_B_SLT, (ix+mhnd_AllocFlags)  ; A from OS_Mop
        jr      nz, mal_setslot

        ld      a, FM_ANY|FM_NOF                ; no offset, any slot

.mal_dofix        
        bit     MM_B_FIX, (ix+mhnd_AllocFlags)
        jr      z, mal_nofix
        or      FM_FIX
.mal_nofix
        bit     7, (ix+mhnd_AllocFlags)
        jr      nz, mal_fma
        bit     6, (ix+mhnd_AllocFlags)
        jr      nz, mal_fma
        or      FM_EVN                          ; force EVEN bank for segment 0 allocation (cannot use odd bank)
.mal_fma
        oz      OS_Fma                          ; request memory
        ret     c
        oz      OS_Axm                          ; allocate memory
        ret     c
        ld      a, h                            ; latest page allocated
        sub     l                               ; number of page allocated
        ld      h, a                            ; adjusted offset
        ld      l, 0                            ; always
        and     @11000000
        rlca
        rlca
        ld      (iy+OSFrame_C), a               ; C segment number
        jp      PutOSFrame_BHL                  ; BHL block allocated

.mal_setslot
        ld      a, (ix+mhnd_ExclusiveBank)      ; exclusive slot/bank from OS_Mop
        and     @11000000                       ; keep slot
        rlca
        rlca                                    ; to A0A1 = slot
        or      FM_NOF                          ; no offset
        jr      mal_dofix

; -----------------------------------------------------------------------------
; free memory
; IN:   IX=MemHandle
;       BC=deallocation size
;       AHL=memory to free
; OUT:  Fc=0, ok
;       Fc=1, A=error if fail
;
.OSMfr
        call    OSFramePush
        ex      af, af'                         ; remember S1
        ld      a, (BLSC_SR1)
        push    af
        ex      af, af'
        call    OSMfrMain
        ex      af, af'                         ; restore S1
        pop     af
        call    MS1BankA
        ex      af, af'
        jp      OSFramePop

.ftl_err
        ld      a, RC_Err                       ; Internal fatal error
        scf
        ret

.mfr_err
        ld      a, RC_Fail                      ; not fatal
        scf
        ret

;       entrypoint here!

.OSMfrMain
        push    hl
        ld      h, b
        ld      l, c
        ld      de, $0100 + 1
        or      a
        sbc     hl, de
        pop     hl
        jp      nc, mfr_block                   ; more than 1 page to deallocate

        call    MemCallAttrVerify
        jr      c, mfr_err                      ; bad size? exit
        jr      z, FreePageAH                   ; size=256? free whole page

        ld      a, (iy+OSFrame_A)               ; bank
        bit     7, h                            ; skip if not segment 0
        jr      nz, mfrchk_1
        bit     6, h
        jr      nz, mfrchk_1

        bit     0, a                            ; clear lower/upper half selector bit,
        res     0, a                            ; address memory at $0000 if bank was even
        jr      nz, mfrchk_1
        res     5, h

.mfrchk_1
        call    MS1BankA

        res     7, h                            ; S1 fix
        set     6, h

; !! validate and unfrag page inside FreeChunk - almost zero overhead
; !! compared to current method

        push    hl
        call    ValidatePage
        pop     hl
        jr      c, ftl_err                      ; fatal error

        call    FreeChunk
        call    DefragmentPage                  ; join chunks if possible

        call    ValidatePage
        jr      c, ftl_err                      ; fatal error

        ld      l, 0                            ; check if page is completely free
        ld      a, (hl)                         ; first chunk at 01, size=255?
        xor     1                               ; Fc=0
        ret     nz
        inc     l
        ld      a, (hl)
        inc     a
        ret     nz

;       free whole page

.FreePageAH
        ld      d, (iy+OSFrame_A)               ; bank
        ld      e, (ix+mhnd_AllocFlags)         ; segment for GetBankMAT
        call    GetPageMAT                      ; d=bank, OSFrame_H=page ; OUT: HL MAT entry
        jr      c, mfr_err                      ; invalid bank? fail

        call    GetMhndSlotAlloc                ; DE=first allocation MATPageN
        jr      z, mfr_err                      ; no mem allocated from this slot? fail

        ld      c, (hl)                         ; BC=next + MAT_LAST
        inc     l
        ld      a, (hl)
        and     MAT_LAST|$0f
        ld      b, a
        dec     l

        call    MATPtrToPageN                   ; translate HL into MATPageN
        ex      de, hl                          ; compare to see if this=first
        or      a
        sbc     hl, de
        add     hl, de
        jr      nz, mfrpage_2                   ; not first? find it in list

        ld      d, b                            ; DE=next
        ld      e, c
        bit     MAT_B_LAST, b
        jr      z, mfrpage_1                    ; not last? skip
        ld      de, 0                           ; this was last, clear ptr
.mfrpage_1
        call    SetMhndSlotAlloc                ; set new pointer into MemHandle
        jp      MarkPageFreePageN               ; mark it free in MAT

;       find page in linked list

.mfrpage_2
        call    PageNToMATPtr

        ld      a, (hl)                         ; <next
        ex      af, af'
        inc     l
        ld      a, (hl)                         ; a'=next bits8-15
        dec     l
        bit     MAT_B_LAST, a
        jp      nz, mfr_err                     ; this was last? fail
        and     $0f                             ; >next

        cp      d                               ; see if Aa=this
        jr      nz, mfrpage_3
        ex      af, af'
        cp      e
        jr      z, mfrpage_4                    ; Aa=DE? remove from list

        ex      af, af'
.mfrpage_3
        ld      h, a                            ; follow link to next
        ex      af, af'
        ld      l, a
        jr      mfrpage_2

;       remove page from list - HL=previous entry, BC=next entry

.mfrpage_4
        ld      (hl), c                         ; <next
        inc     l
        ld      a, (hl)                         ; >next, keep flags
        and     MAT_LAST|MAT_FULL|MAT_ALLOCFIXED|MAT_SWAP
        or      b
        ld      (hl), a

        ex      de, hl                          ; HL=PageN
        jp      MarkPageFreePageN

;       ----

;       Deallocate block

.mfr_block
        inc     c
        dec     c
        ld      c, 0
        jr      z, mfr_adjusted
        inc     b                               ; adjust BC to upper page
.mfr_adjusted
        ld      l, b                            ; number of pages to deallocate
        ld      b, a                            ; bank number, h already set
        oz      OS_Fxm
        ret

; -----------------------------------------------------------------------------
; IN:   IX=MemHandle
;       B=bank
;       H=page address high byte
;       L=number of pages to be allocated
;
.OSAxm
        call    AxmPrepare                      ; verifications and adjust
        ret     c
        push    hl                              ; preserve h and l for allocation loop
        call    axm_vrf
        pop     hl
        ret     c                               ; memory wanted is not free or not available
        ld      (iy+OSFrame_H), h               ; h has been changed during verification

.axm_alloc
        push    hl
        ld      b, (iy+OSFrame_B)               ; remember bank
        call    axp_alloc                       ; allocate page
        pop     hl
        ret     c                               ; error during page allocation
        inc     (iy+OSFrame_H)                  ; inc page
        dec     l
        jr      nz, axm_alloc                   ; loop
        jr      UpdateOOM

.axm_vrf                                        ; verify that all pages can be allocated
        push    hl
        call    axm_vp                          ; verify that page is allocatable
        pop     hl
        ret     c                               ; else ret, no de-allocation needed
        inc     (iy+OSFrame_H)                  ; try next page
        dec     l
        jr      nz, axm_vrf
        ret

.axm_vp                                         ; verify that explicit page is allocatable
        ld      d, (iy+OSFrame_B)               ; remember bank
        ld      a, (ix+mhnd_AllocFlags)         ; allocation flags: segment 0-3
        and     @11000000
        ld      e, a
        call    GetPageMAT                      ; HL points to MAT entry for this page (OSFrame_H)
        ld      a, RC_Fail
        ret     c                               ; no RAM in bank? exit
        call    FollowMATPtr                    ; DE=(hl), next
        ret     z                               ; free, ok
        ld      a, RC_Room
        scf                                     ; already allocated? error
        ret

.UpdateOOM
        ld      d, (iy+OSFrame_B)
        ld      e, (ix+mhnd_AllocFlags)
        call    FindFreePageBankD               ; update OOM flags
        or      a                               ; forget RC_Room if Fc=1
        ret

; -----------------------------------------------------------------------------
; IN:   IX=MemHandle
;       B=bank
;       H=page address high byte
;       L=number of contiguous pages to be released
;
.OSFxm
        call    AxmPrepare                      ; verifications and adjust
        ret     c
        push    hl                              ; preserve h and l for de-allocation loop
        call    fxm_vrf
        pop     hl
        ret     c                               ; memory wanted is not free or not available
        ld      (iy+OSFrame_H), h               ; h has been changed during verification

.fxm_rls
        push    hl
        ld      a, (iy+OSFrame_B)               ; remember bank
        ld      (iy+OSFrame_A), a               ; set a in pushframe
        call    FreePageAH                      ; release page AH
        pop     hl
        ret     c                               ; error during page allocation
        inc     (iy+OSFrame_H)                  ; inc page
        dec     l
        ret     z
        jr      fxm_rls                         ; release loop

.fxm_vrf                                        ; verify that all pages can be released
        push    hl
        call    fxm_vp                          ; verify that page is already allocated
        pop     hl
        ret     c                               ; else ret with RC_Fail
        inc     (iy+OSFrame_H)                  ; try next page
        dec     l
        jr      nz, fxm_vrf
        ret

.fxm_vp                                         ; verify that explicit page is allocatable
        ld      d, (iy+OSFrame_B)               ; remember bank
        ld      a, (ix+mhnd_AllocFlags)         ; allocation flags: segment 0-3
        and     @11000000
        ld      e, a
        call    GetPageMAT                      ; HL points to MAT entry for this page (OSFrame_H)
        ld      a, RC_Fail
        ret     c                               ; no RAM in bank? exit
        call    FollowMATPtr                    ; DE=(hl), next
        ret     nz                              ; used, ok
        ld      a, RC_Fail
        scf                                     ; already released or bad page ? error
        ret

; -----------------------------------------------------------------------------
; IN:   IX=MemHandle
;       B=bank
;       H=page address high byte
;
.OSAxp
        call    AxpPrepare                      ; verifications first
        ret     c
        call    axp_alloc                       ; allocate page
        ret     c
        jr      UpdateOOM                       ; update OOM flags if bank is full

.axp_alloc                                      ; allocate explicit page
        ld      d, b                            ; bank
        ld      a, (ix+mhnd_AllocFlags)         ; allocation flags: segment 0-3
        and     @11000000
        ld      e, a
        call    GetPageMAT
        ld      a, RC_Fail
        ret     c                               ; no RAM in bank? exit

        call    FollowMATPtr                    ; DE=next
        ld      a, RC_Room
        scf
        ret     nz                              ; already allocated? error
        jp      MarkPageAsAllocated

;       ----

.AxmPrepare
        ld      c, 64                           ; 64 pages max, 32 if segment 0
        ld      a, (ix+mhnd_AllocFlags)
        and     @11000000                       ; segment
        jr      nz, xmp1
        ld      c, 32
.xmp1
        ld      a,l
        or      a
        jr      z, xmp_err                      ; not less than 1 page
        cp      c                               ; no more than 64 or 32 pages (16K or 8K)
        jr      z, xmp1a
        jr      nc, xmp_err
        jr      nz, xmp2
.xmp1a
        ld      a, h                            ; l=64 (or 32) verify that h=0
        and     @00111111
        inc     a                               ; if 16K (or 8K), H has to be zero
        dec     a
        jr      nz, xmp_err
        jr      AxpPrepare                      ; ok, h=0

.xmp2
        ld      a, h                            ; verify that length does not exceed bank
        or      @11000000
        or      c                               ; set b5 if segment 0
        add     a, l
        jr      z, AxpPrepare                   ; full bank, ok
        jr      c, xmp_err                      ; bank cross

.AxpPrepare
        ld      a, HND_MEM                      ; verify handle type
        call    VerifyHandle
        ret     c
        ld      a, (ix+mhnd_AllocFlags)         ; adjust page to desired segment (set in OS_Mop)
        and     @11000000                       ; keep segment
        jr      nz, xpp1
        bit     5, h                            ; upper or lower part of bank
        jr      z, xpp0
        set     0, (iy+OSFrame_B)               ; odd number will use upper part
.xpp0
        or      @00100000                       ; mask to upper half of segment ($2000-$3FFF)
.xpp1
        or      h
        ld      h, a
        ld      (iy+OSFrame_H), a               ; put page in stackframe
        ret

.xmp_err
        ld      a, RC_Fail
        scf
        ret

;       ----

;IN:    D=bank E=segment mask
;       OSFrame_H=address high byte
;OUT:   HL points to MAT entry for this page

.GetPageMAT

        call    GetBankMAT                      ; point HL to first MAT entry in bank D
        ret     c                               ; invalid bank? error

        ld      a, (iy+OSFrame_H)               ; check memory address
        cp      $20
        ret     c                               ; below $2000? error
        cp      $40
        jr      nc, gpm_1                       ; not segment 0

        bit     0, d                            ; use lower half if bank is even
        jr      nz, gpm_1
        sub     $20

.gpm_1
        and     $3F                             ; page
        ld      c, a
        ld      b, 0
        add     hl, bc                          ; HL points to page MAT entry
        add     hl, bc
        ret


;       ----

.FindFreePage

        ld      a, d
        or      a
        jp      z, FindFreePageAnySlot          ; no exlusive bank

        cp      $20
        jr      c, FindFreePageSlotD            ; it's not bank, it's slot

;       ----

;       Find free page from bank D
;
;IN:    D=bank
;       E=allocation flags

.FindFreePageBankD

        call    GetBankMAT
        jr      c, ffp_3                        ; no RAM in bank? error

        push    hl
        call    PrepareOutOfMem
        or      OOM_NORMAL                      ; always test bit 0, normal mem
        and     (hl)
        pop     hl
        jr      nz, ffp_3                       ; bank full? error

        ld      bc, 64                          ; scan 64 pages, 32 if segment 0
        ld      a, e
        and     $C0                             ; segment
        jr      nz, ffp_1
        ld      bc, 32                          ; !! ld c,20

        bit     0, d                            ; use upper half of bank if odd bank
        jr      z, ffp_1
        set     6, l

.ffp_1
        call    FindFreePageInBank
        ret     nc                              ; found free page? exit
                                                ; Fc = 1, bank is full, flag OOM
        ld      a, e
        and     $C0
        jr      z, ffp_3                        ; segment 0? don't set error flags
                                                ; because bank is half full (32 pages scanned)
        call    PrepareOutOfMem
        jr      nz, ffp_2                       ; fixed? set OOM_B_FIXED
        inc     a                               ; set OOM_B_NORMAL
.ffp_2
        or      (hl)
        ld      (hl), a

.ffp_3
        ld      a, RC_Room                      ; no empty page found
        scf
        ret

;       ----

; Prepare for out_of_memory flag read/set
;
; IN:  D=slot&bank, E=alloc flags
; OUT:(HL)=outOfMemFlags, A=fixedFlag
;
; !! Could change 'and $1f' into 'or $20' and 'cpl' into 'or $c0' to put flags in natural order

.PrepareOutOfMem
        ld      a, d                            ; limit slot 0 bank to 00-1F
        and     $C0
        ld      a, d
        jr      nz, poom_1
        and     $1F

.poom_1
        and     $3F                             ; bank
        cpl                                     ; from top of page - E0-FF or C0-FF
        ld      l, a
        ld      h, $80                          ; top of first page, at S2
        ld      a, e                            ; A=fixed flag
        and     OOM_FIXED
        ret

;       ----

; point HL into MAT entry of bank D, handle slot 0

.GetBankMAT
        push    de
        ld      a, e
        and     $C0                             ; dest segment
        jr      nz, gbm_1                       ; not seg0? ok
        res     0, d                            ; only use even banks
.gbm_1
        call    GetBankMAT0
        pop     de
        ret

;       ----

;       find free page in slot D

.FindFreePageSlotD
        push    iy
        ld      a, d                            ; slot in bits0-1
        call    MS2SlotMAT
        jr      c, apsd_3                       ; no RAM  !! 'ld b,(iy+4); pop iy' before this to 'ret c'

        ld      b, (iy+4)                       ; RAM size
        ld      d, a                            ; RAM base

        ld      a, e                            ; if segment 0 use forward allocation
        and     $C0
        jr      z, apsd_1

        ld      a, d                            ; D=last bank
        add     a, b
        dec     a
        ld      d, a

.apsd_1
        push    bc
        push    de
        call    FindFreePageBankD
        pop     de
        pop     bc
        jr      nc, apsd_4                      ; found empty page mem

        inc     d                               ; increment bank
        ld      a, e                            ; backward allocation if not segment 0
        and     $C0
        jr      z, apsd_2
        dec     d
        dec     d

.apsd_2
        djnz    apsd_1                          ; try all banks

        scf
.apsd_3
        ld      a, RC_Room

.apsd_4
        pop     iy
        ret

;       ----

;       find free page in any slot

.FindFreePageAnySlot

        xor     a
.apas_1
        inc     a                               ; next slot, 1-2-3-0
        and     3
        ld      d, a
        push    de
        call    FindFreePageSlotD
        pop     de
        jr      nc, apas_2                      ; found free page? exit

        ld      a, d
        or      a
        jr      nz, apas_1                      ; not slot 0? try next slot

        ld      a, RC_Room
        scf
.apas_2
        ret

;       ----

;       find free page in bank D
;
;IN:    HL=MAT start, BC=number of pages to examine (32 or 64)
;OUT:   Fc=0, HL=MatEntry of free page
;       Fc=1 if no page free

.ffp0_0
        inc     hl
        cpi                                     ; HL++, BC--
        scf
        ret     po                              ; BC overflow, Fc=1
                                                ; bank is full, set it in PrepareOutOfMem
;       entry point here

.FindFreePageInBank
        inc     l
        ld      a, (hl)                         ; MAT bits
        dec     l

        bit     MM_B_FIX, e                     ; if we want fixed workspace we
        jr      z, ffp0_1                       ; skip swap memory
        bit     MAT_B_SWAP, a
        jr      nz, ffp0_0

.ffp0_1
        push    de
        call    FollowMATPtr
        pop     de
        jr      nz, ffp0_0                      ; allocated, try next

        or      a                               ;Fc=0
        ret

;       ----

;       convert PageN into MAT pointer

.PageNToMATPtr
        add     hl, hl                          ; entry*WORD_SIZEOF
        set     7, h                            ; S2 fix
        inc     h                               ; skip first page
        ret

;       ----

;       convert MAT pointer into PageN

.MATPtrToPageN
        res     7, h                            ; S2 unfix
        dec     h                               ; unskip first page
        srl     h                               ; offset/WORD_SIZEOF
        rr      l
        ret

;       ----

;       convert PageN into page pointer

.PageNToPagePtr
        call    PageNToMATPtr

;       ----

;       convert MAT pointer into page pointer

; IN:  HL=MATptr - $010 hhhl llll lll0
; OUT: DE=MATptr, AHL=page

.MATPtrToPagePtr

        push    hl
        res     7, h                            ; remove S2 bit
        dec     h                               ; unskip ? page
        ld      a, (BLSC_SR2)                   ; MAT bank
        and     $FE                             ; slot RAM base
        add     hl, hl                          ; 00hh hhll llll ll00, Fc=0
        add     a, h                            ; bank
        ld      h, l                            ; llll ll00
        scf
        rr      h                               ; 1lll lll0
        srl     h                               ; 01ll llll, S1
        ld      l, 0                            ; HL points to page
        pop     de
        ret

;       ----

;       convert PageN into MAT pointer and get next PageN into DE

.FollowPageN

        call    PageNToMATPtr

;       get next PageN into DE
;
;       Fz=1 if next=NULL

.FollowMATPtr

        ld      e, (hl)
        inc     l
        ld      d, (hl)
        dec     l
        ld      a, d
        and     $0f                             ; remove flags
        or      e
        ret

;       ----

;       bind in MAT for slot specified by address high byte in A

.MS2SlotMAT67

        rlca                                    ; high bits into low bits
        rlca

;       bind in MAT for slot A
;
; IN:  A=slot
; OUT: A=base bank, IY points to SlotRAMoffset for this slot
;
; !! could exit before bankswitch if no RAM

.MS2SlotMAT

        and     3
        ld      iy, ubSlotRAMoffset             ; RAM bank offset
        ld      c, a
        ld      b, 0
        add     iy, bc

        ld      a, (iy+0)
        cp      $40                             ; use b21 if internal RAM
        jr      nc, slctr_1
        inc     a
.slctr_1
        call    MS2BankA                        ; bind MAT in

        and     $FE                             ; slot RAM base
        ret     nz                              ; Fc=0
        scf                                     ; no RAM
        ret

;       ----

; bind in MAT for bank D and point HL to the start of bank entries
;

.GetBankMAT0

        push    iy
        ld      a, d                            ; bank
        call    MS2SlotMAT67                    ; bind MAT in S2
        jr      c, gbm0_1                       ; no RAM? exit

        sub     d
        neg                                     ; bank(in)-base bank
        cp      (iy+4)                          ; RAM size
        ccf
        jr      c, gbm0_1                       ; larger than slot RAM size? exit

        ld      l, a                            ; HL=bank_offset*PAGES_PER_BANK*WORD_SIZEOF
        ld      h, 0
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl
        set     7, h                            ; S2 fix
        inc     h                               ; skip first page

.gbm0_1
        pop     iy
        ret


;IN:    HL=PageN of page to free

.MarkPageFreePageN

        call    PageNToMATPtr


;IN:    HL=MAT pointer of page to free

.MarkPageFreeMATptr

        ld      (hl), 0                         ; keep SWAP flag, clear others
        inc     l
        ld      a, (hl)
        and     MAT_SWAP
        ld      (hl), a
        dec     l

        call    MATPtrToPageN                   ; into PageN  !! 'ret 7,h; dec h' here, eliminates one 'add hl,hl'
        add     hl, hl
        add     hl, hl
        ld      a, h                            ; 00hhhhll, bank & $3f
        cpl                                     ; from top of first page
        ld      l, a
        ld      h, MM_S2

        ld      a, (hl)                         ; clear out-of-memory flags !! ld (hl),0
        and     ~(OOM_NORMAL|OOM_FIXED)
        ld      (hl), a
;IncFreeRAM
        push    hl                              ; increment free pages
        ld      hl, (uwFreeRAMPages)
        inc     hl
        ld      (uwFreeRAMPages), hl
        pop     hl
        ret

;       ----

.DecFreeRAM
        push    hl
        ld      hl, (uwFreeRAMPages)
        dec     hl
        ld      (uwFreeRAMPages), hl
        pop     hl
        ret


;       ----

;       allocate chunk from page
;       !! should validate here for smaller overhead
;
;IN:    H=page to allocate memory from
;       C=bytes to allocate
;OUT:   Fc=0, HL=memory ptr
;       Fc=1, error

.AllocChunk

        ld      d, h
        ld      l, 0                            ; start from pointer to first chunk

.ac_1
        ld      e, l                            ; DE=HL
        ld      a, (hl)                         ; pointer to next chunk
        or      a
        ccf
        ret     z                               ; no more? ret, Fc=1
        cp      l
        jr      z, ac_fail                      ; link to itself? fail
        jr      c, ac_fail                      ; link backwards? fail

        ld      l, a                            ; follow link
        ld      a, (hl)                         ; size of chunk
        sub     c
        jr      z, ac_match                     ; exact match
        jr      c, ac_next                      ; doesn't fit

        dec     a                               ; only one byte extra? doesn't fit
        jr      z, ac_next

        inc     a                               ; store new size of current chunk
        ld      (hl), a
        add     a, l                            ; and point HL to allocated chunk
        ld      l, a
        ret

.ac_next
        inc     l                               ; get link to next and jump there
        jr      ac_1

.ac_match
        inc     l                               ; copy link to next to previous chunk
        ldd                                     ; HL--, start of allocated memory
        ret

.ac_fail
        jp      OZwd__fail

;       ----

; IN: HL=memory to free, C=size
;
; !! should validate and degragment here, less overhead

.FreeChunk
        ld      (hl), c                         ; set size
        ld      e, l                            ; remember chunk start
        xor     a
        inc     l
        ld      (hl), a                         ; set next=0
.fc_1
        ld      l, a
        ld      a, (hl)                         ; next
        or      a
        jr      z, fc_2                         ; no more chunks, link freed to this

        cp      e
        inc     a                               ; ptr to link
        jr      c, fc_1                         ; next<freed, loop

        dec     a                               ; next
        ld      (hl), e                         ; link freed to this
        ld      l, e                            ; this=freed
        inc     l                               ; ptr to next
        ld      e, a                            ; freed=next
.fc_2
        ld      (hl), e
        ret

;       ----

.DefragmentPage

        ld      d, h
        ld      l, 0                            ; start from link to first

.defrag_1
        ld      a, (hl)                         ; next chunk
        or      a
        ret     z                               ; no more chunks? exit

.defrag_2
        ld      l, a                            ; move to chunk
        add     a, (hl)                         ; pos+size
        ret     z                               ; till the end of page? exit
        cp      l
        ret     z                               ; link to self? exit !! should fail
        inc     l
        cp      (hl)                            ; compare with next chunk pos
        jr      nz, defrag_1                    ; not same? check next chunk

;       join chunks

        ld      e, a                            ; next pos
        ld      a, (de)                         ; next size
        dec     l
        add     a, (hl)                         ; this size
        ld      (hl), a                         ; remember
        inc     e
        ld      a, (de)                         ; chunk after next
        inc     l
        ld      (hl), a                         ; is chunk after this
        dec     l
        ld      a, l
        jr      defrag_2                        ; try to join with next chunk

;       ----

;       Validate page H00 !! fail here to save bytes elsewhere
;
;OUT:   Fc=0 if page OK, Fc=1 if invalid

.ValidatePage

        ld      l, 0
.vp_1
        ld      a, (hl)                         ; pointer to next chunk
        or      a
        ret     z                               ; no more chunks, Fc=0

        ld      l, a
        ld      a, (hl)                         ; size
        cp      2
        ret     c                               ; <2? fail

        add     a, l                            ; size+start
        jr      z, vp_2                         ; end of page is ok
        ret     c                               ; otherwise overflow means error
.vp_2
        or      a                               ; Fc=0
        inc     l
        inc     (hl)
        dec     (hl)
        ret     z                               ; no more chunks? page ok, Fc=0
        inc     a
        dec     a
        scf
        ret     z                               ; more pages and size+start=256? Fc=1
        cp      (hl)
        jr      c, vp_1                         ; start+size<next? ok, check next

        scf
        ret                                     ; Fc=1

; -----------------------------------------------------------------------------
; Find Memory for Allocation

; IN:
;       A7 = even bank needed (for segment 0 allocation)
;       A6 = fixed memory requested (no subject to swapping)
;       A5 = from any slot
;       A4 = no offset (calculated by call)
;       (A2 used internaly by routine)
;       A0-A1 = slot number if A5=0
;
;       H = page offset ($00-$3F)
;       L = number of pages wanted (1 to 64)
;
; OUT:  Fc = 0 if empty memory found at bank B
;       Fc = 1, A = RC_Room if not enough space in slot(s)
;
.OSFma
        bit     FM_B_ANY, a
        jr      z, fma_0
        and     @11110000                       ; keep EVN/FIX/ANY/NOF options, start with slot 0
.fma_0
        and     @11110011                       ; keep options and slot
        ld      (iy+OSFrame_A), a               ; update A in pushframe
        ld      a, h
        and     $3F                             ; 0000-3F00 range
        ld      (iy+OSFrame_H), a
        ld      a, l
        or      a
        jr      z, fma_fail                     ; request min 1 page
        cp      65                              ; request max 64 pages (16K)
        jr      nc, fma_fail
.fma_any
        ld      a, (iy+OSFrame_A)               ; reload A
        push    hl                              ; keep HL for next slot
        call    fma_slt                         ; scan slot
        pop     hl
        ret     nc                              ; found, return B in pushframe
        bit     FM_B_ANY, (iy+OSFrame_A)
        ret     z                               ; 1 slot requested, ret with error
        inc     (iy+OSFrame_A)                  ; inc slot (Fc unchanged)
        ld      a, RC_Room
        bit     2, (iy+OSFrame_A)               ; =4 -> end of scan (Fc unchanged)
        ret     nz                              ; ret with RC_Room and Fc=1
        call    PutOSFrame_HL                   ; reload pushframe with original values of HL
        jr      fma_any

.fma_fail
        ld      a, RC_Fail
        scf
        ret

.fma_slt
        rrca                                    ; slot number to mask
        rrca
        and     @11000000                       ; slot mask
        jr      nz, fma_1
        ld      a, $20                          ; slot 0 begins at bank $20
.fma_1
        ld      (iy+OSFrame_B), a               ; set bank to scan in OSFrame_B
        push    iy
        call    MS2SlotMAT67                    ; slot a becomes MAT bank bound in s2, bc and iy destroyed
        ld      a, (iy+4)                       ; ubSlotRAMSize 0, 1, 2 or 3 (IY was ubSlotRAMOffset)
        pop     iy                              ; restore pushframe
        ret     c                               ; no RAM in slot ? skip  
        add     a, (iy+OSFrame_B)               ; add number of banks in slot + slot offset 
        ld      (iy+OSFrame_C), a               ; keep last bank+1 in OSFrame_C      

.fma_2
        ld      d, (iy+OSFrame_B)               ; bank to scan
        call    GetBankMAT                      ; MAT entry of bank D is in HL
        or      a                               ; Fc=0
        bit     FM_B_NOF, (iy+OSFrame_A)        ; no offset
        call    nz, fma_nof
        jr      c, fma_4                        ; try next bank in slot
        ex      de, hl                          ; now in DE
        ld      l, (iy+OSFrame_H)               ; page offset
        ld      h, 0                            ; zero
        add     hl, hl                          ; 1 word per page
        or      a                               ; Fc=0
        add     hl, de                          ; HL holds MAT entry
        ld      e, MM_S2                        ; bank in s2
        bit     FM_B_FIX, (iy+OSFrame_A)
        jr      z, fma_3
        set     MM_B_FIX, e                     ; add allocation FIX flag if requested
.fma_3
        ld      b, 0
        ld      c, (iy+OSFrame_L)               ; BC=number of pages to scan
        call    CountUnusedPages
        exx
        ld      a, c                            ; c' number of free pages
        exx
        cp      (iy+OSFrame_L)                  ; number of pages wanted = free pages ?
        call    z, fma_even
        ret     z                               ; found, B bank number (OSFrame_B)
.fma_4
        inc     (iy+OSFrame_B)                  ; pre-increment bank
        ld      a, (iy+OSFrame_B)
        cp      (iy+OSFrame_C)                  ; end of slot ?
        jr      nz, fma_2                       ; loop to next bank
        ld      a, RC_Room                      ; no free memory area
        scf
        ret

.fma_even
        bit     FM_B_EVN, (iy+OSFrame_A)
        ret     z
        bit     0, (iy+OSFrame_B)               ; zero is even Fz=1, odd bank returns Fz=0
        ret                                     ; and thus try next bank in slot

.fma_nof
        ld      d, h                            ; store MAT entry of bank in DE 
        ld      e, l
        ld      b, 64                           ; scan 1 bank (64 pages)
        dec     hl
        dec     hl

.nof_0
        ld      a, b
        neg
        and     @00111111                       ; 64->0, 63->1 ... 2->62, 1->63
        ld      (iy+OSFrame_H), a               ; set offset (0-63) to be tested
        ld      c, (iy+OSFrame_L)               ; reload number of pages requested

.nof_1
        inc     hl                              ; next page in MAT 
        inc     hl

        inc     l                               ; get page attributes
        ld      a, (hl)
        dec     l

        bit     FM_B_FIX, (iy+OSFrame_A)        ; want fixed mem?
        jr      z, nof_2                        ; no? check this
        bit     MAT_B_SWAP, a                   ; verify page is fixed
        jr      nz, nof_4                       ; no? skip this
.nof_2
        push    de
        call    FollowMATPtr                    ; Fz=1 if next=NULL
        pop     de
        jr      nz, nof_4                       ; in use? retry with next page
        dec     c
        jr      z, nof_5                        ; found
        djnz    nof_1

.nof_4
        djnz    nof_0
        scf
        ret
.nof_5
        ex      de, hl                          ; reload HL with MAT entry of bank
        or      a                               ; ok, offset is in (iy+OSFrame_H)
        ret


;       ----

.SetExclusiveBank
        exx
        ld      de, 0                           ; reset max_unused_pages variables
        exx

        ld      d, (ix+mhnd_ExclusiveBank)      ; get slot/bank limit
        ld      e, (ix+mhnd_AllocFlags)         ; memory mask will be used by FindUnusedBank

        ld      a, d
        or      a
        jr      nz, seb_1
        call    FindUnusedBankAnySlot           ; any slot
        jr      seb_3
.seb_1
        cp      $20                             ; if it is RAM allocate from single bank
        jr      nc, seb_2                       ;
        call    FindUnusedBank                  ; otherwise allocate from slot D
        jr      seb_3
.seb_2
        call    CountUnusedPagesInBank          ; just bank D

.seb_3
        exx
        ld      a, d                            ; bank with most unused pages
        exx
        or      a
        ret     z                               ; no free pages found? exit
        ld      (ix+mhnd_ExclusiveBank), a
        ret

;       ----

; Count unused pages in bank
;
;IN:    D=bank
;       E=segment mask (bit 6 & 7) and alloc FIX flags
;OUT:   A=c'=unused pages in bank
;       d'=bank with most (c') pages
;       Fc=0 if totally free

.CountUnusedPagesInBank

        call    GetBankMAT
        jr      c, cup_err                      ; no RAM?

        push    hl
        call    PrepareOutOfMem
        or      OOM_NORMAL
        and     (hl)                            ; check if bank full
        pop     hl
        jr      nz, cup_err                     ; out of memory? exit

        ld      bc, 64                          ; 64 pages, 32 if segment 0
        ld      a, e
        and     $C0
        jr      nz, cup_1
        ld      bc, 32                          ; !! ld c,32
        bit     0, d                            ; use upper half of bank if bank LSB=1
        jr      z, cup_1
        set     6, l

.cup_1
        call    CountUnusedPages                ; c'=unused_count

        ld      a, e                            ; 64 pages, 32 if segment 0
        and     $C0                             ; !! push it above, pop here
        ld      a, 64
        jr      nz, cup_2
        ld      a, 32
.cup_2
        exx
        cp      c
        jr      z, cup_3                        ; all free, Fc=0 - all other returns with Fc=1

        ld      a, c                            ; compare with most_free
        cp      e
        jr      c, cup_4                        ; smaller or same
        scf
        jr      z, cup_4
.cup_3
        exx                                     ; d'=bank, e'=max most_free
        ld      a, d
        exx
        ld      d, a
        ld      e, c
.cup_4
        exx
        ret     nc                              ; !! just ret
.cup_err
        scf
        ret

;       ----

; check one slot for completely free bank

;IN:    D=slot
;       E=segment mask (00: 32 pages, 40/80/C0: 64 pages are searched) 
;OUT:   Fc=0 if empty bank found
;       c'=max_free, d'=bank

.FindUnusedBank
        push    iy
        ld      a, d                            ; slot (0-3)
        call    MS2SlotMAT
        jr      c, fub_2                        ; no RAM? skip  !! ld b/pop iy above to optimize exits

        ld      b, (iy+4)                       ; ubSlotRAMSize 0, 1, 2 or 3 (IY was ubSlotRAMOffset)
        ld      d, a                            ; bank of MAT (00-3F) bound in s2, first bank of slot
.fub_1
        push    bc                              ; remember count
        push    de                              ; and bank
        call    CountUnusedPagesInBank          ; in bank D
        pop     de
        pop     bc
        jr      nc, fub_2                       ; totally empty? exit

        inc     d                               ; try next bank
        djnz    fub_1                           ; as long as RAM bank left in slot D
.fub_2
        pop     iy
        ret

;       ----

;       try to find totally unused bank in any slot
;
;IN:    -
;OUT:   'c incremented for each free page
;       Fc=0 if completely free bank found


.FindUnusedBankAnySlot
        xor     a                               ; !! not necessary, A=0 on entry

.fubas_1
        inc     a                               ; next slot, 1-2-3-0
        and     3
        ld      d, a
        push    de
        call    FindUnusedBank

        pop     de
        ret     nc                              ; got totally free bank? exit

        ld      a, d
        or      a
        jr      nz, fubas_1                     ; slots left? loop

        scf                                     ; Fc=1, no free bank found
        ret

;       ----

;       count unused pages in bank
;
;IN:    BC=number of pages
;       HL=MATptr of first page
;OUT:   c' incremented for each free page
;
.CountUnusedPages
        exx
        ld      c, 0                            ; reset counter in c'
        exx
        jr      cup2_0

.cup2_1
        inc     hl
        cpi                                     ; HL++, BC--
        ret     po                              ; BC underflow? return

.cup2_0
        inc     l                               ; get page attributes
        ld      a, (hl)
        dec     l

        bit     MM_B_FIX, e                     ; want fixed mem?
        jr      z, cup2_2                       ; no? check this
        bit     MAT_B_SWAP, a                   ; verify page is fixed
        jr      nz, cup2_1                      ; no? skip this

.cup2_2
        push    de
        call    FollowMATPtr                    ;Fz=1 if next=NULL
        pop     de
        jr      nz, cup2_1                      ; in use? check next

        exx                                     ; increment unused_count
        inc     c
        exx
        jr      cup2_1                          ; and check next

