; **************************************************************************************************
; MTH Management, kernel 0 routines.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2008
; (C) Gunther Strube (gstrube@gmail.com), 2005-2008
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module MTH0

        include "dor.def"
        include "error.def"
        include "fileio.def"
        include "stdio.def"
        include "handle.def"
        include "sysvar.def"
        include "oz.def"
        include "interrpt.def"
        include "../mth/mth.def"


xdef    aRom_Help
xdef    ChgHelpFile
xdef    CopyAppPointers
xdef    DrawCmdHelpWd
xdef    DrawMenuWd
xdef    DrawTopicHelpWd
xdef    FilenameDOR
xdef    FindCmd
xdef    GetAppCommands
xdef    GetAppDOR
xdef    GetAttr
xdef    GetCmdTopicByNum
xdef    GetHlp_sub
xdef    GetHlpCommands
xdef    GetHlpHelp
xdef    GetHlpTokens
xdef    GetHlpTopics
xdef    GetRealCmdPosition
xdef    InputEmpty
xdef    MayMTHPrint
xdef    MTHPrint
xdef    MTHPrintTokenized
xdef    NextAppDOR
xdef    PrevAppDOR
xdef    PrintTopic
xdef    PrntAppname
xdef    SetActiveAppDOR
xdef    SetHlpAppChgFile
xdef    SkipNTopics
xdef    ScrDrv_SOH_A

xref    OSBixS1                                 ; [Kernel0]/stkframe.asm
xref    OSBoxS1                                 ; [Kernel0]/stkframe.asm
xref    AtoN_upper                              ; [Kernel0]/memmisc.asm
xref    MS2BankK1                               ; [Kernel0]/memmisc.asm
xref    fsMS2BankB                              ; [Kernel0]/filesys.asm
xref    fsRestoreS2                             ; [Kernel0]/filesys.asm
xref    GetHandlePtr                            ; [Kernel0]/dor.asm
xref    MayWrt                                  ; [Kernel0]/token.asm
xref    OSWrt                                   ; [Kernel0]/token.asm
xref    ZeroHandleIX                            ; [Kernel0]/handle.asm
xref    PutOZwdBuf                              ; [Kernel0]/osin.asm

xref    MTHPrintKeycode                         ; [Kernel1]/mth1.asm
xref    DrawMenuWd2                             ; [Kernel1]/mth1.asm
xref    Help2Wd_bottom                          ; [Kernel1]/mth1.asm
xref    Help2Wd_Top                             ; [Kernel1]/mth1.asm
xref    InitHelpWd                              ; [Kernel1]/mth1.asm
xref    OpenAppHelpFile                         ; [Kernel1]/mth1.asm
xref    Get2ndCmdHelp                           ; [Kernel1]/mth1.asm
xref    Get2ndTopicHelp                         ; [Kernel1]/mth1.asm
xref    GetFirstCmdHelp                         ; [Kernel1]/mth1.asm
xref    GetTpcAttrByNum                         ; [Kernel1]/mth1.asm
xref    MTH_ToggleLT                            ; [Kernel1]/mth1.asm
xref    InitHandle                              ; [Kernel1]/handle1.asm


;       print MTH string, expand $7f-codes

.mthp_0
        bit     0, c                            ; if C not xxxxxx00 then try again
        jr      z, mthp_1
        bit     1, c
        jr      z, mthp_1

        ld      e, (hl)                         ; jump to function
        inc     hl
        ld      d, (hl)
        call    Jp_DE

.MTHPrint
        ex      (sp), hl                        ; get char from caller PC
        ld      a, (hl)
        inc     hl
        ex      (sp), hl
        or      a
        ret     z                               ; null? exit
        cp      $7f
        jr      nz, mthp_2                      ; not 7F, print

        ex      (sp), hl                        ; get next byte
        ld      a, (hl)
        inc     hl
        ex      (sp), hl
        cp      $7f                             ; 7F, print as is
        jr      z, mthp_2

        and     $df                             ; upper()
        ld      bc, 32
        ld      hl, mthp_tbl                    ; command table
.mthp_1
        cpir                                    ; !! don't use cpir, use sorted table like in new kbd routines
        jr      z, mthp_0                       ; found, execute it

.mthp_2
        OZ      OS_Out                          ; print char and loop back
        jr      MTHPrint

;       char, function, pad

.mthp_tbl
        defb    'A'
        defw    PrntAppname
        defb    0

        defb    'T'
        defw    PrintActiveTopic
        defb    0

        defb    'F'
        defw    PrntCommand
        defb    0

        defb    'C'
        defw    JustifyA
        defb    0

        defb    'L'
        defw    JustifyA
        defb    0

        defb    'N'
        defw    JustifyA
        defb    0

        defb    'R'
        defw    JustifyA
        defb    0

        defb    'D'
        defw    ResetToggles
        defb    0

;       ----

.Jp_DE
        push    de
        ret

;       ----

.JustifyC
        ld      b, 'C'
        jr      just_sub
.JustifyR
        ld      b, 'R'
        jr      just_sub
.JustifyN
        ld      b, 'N'
        jr      just_sub
.JustifyA
        ld      b, a                            ; A keeps justi char
.just_sub
        ld      a, '2'
        call    ScrDrv_SOH_A
        ld      a, 'J'
        OZ      OS_Out
        ld      a, b
        OZ      OS_Out
        ret

;       ----

.DrawTopicHelpWd
        call    InitHelpWd
        ret     c                               ; input pending? exit

        call    MTHPrint
        defm    "The ", $7f,"A", " ", $7f,"T", " topic",10,0

        call    GetHlpTopics
        call    OSBixS1
        push    de

        ld      a, (ubHlpActiveTpc)
        call    SkipNTopics
        call    GetHelpOffs
        ld      b, d
        ld      c, e
        pop     de
        call    OSBoxS1

        call    PrintTopicHelp
        ret     c
        call    Help2Wd_Top
        ret     c

        call    Get2ndTopicHelp
        jr      c, dth_1                        ; no more help? skip

        call    MayMTHPrint
        defm    $7f,"L"
        defm    " other ", $7f,"A", " topics"
        defm    $7f,"R"
        defm    SOH,SD_OUP,"  ",10
        defm    $7f,"R"
        defm    SOH,SD_ODWN,"  ",11,0

.dth_1
        call    MayMTHPrint
        defm    10, 10
        defm    $7f,"L"
        defm    " ", $7f,"A"
        defm    $7f,"R"
        defm    SOH,SD_OLFT,"   ",10,0

        call    GetFirstCmdHelp
        jr      c, dth_2                        ; no command help? skip

        call    MayMTHPrint
        defm    $7f,"L"
        defm    " the ", $7f,"T", " entries",0

        call    MayMTHPrint                     ; !! join this to above
        defm    $7f,"R"
        defm    SOH,SD_ORGT," ",0

.dth_2
        jp      Help2Wd_bottom

;       ----

.ResetToggles
        ld      a, SD_DTS

.ScrDrv_SOH_A
        push    af
        ld      a, SOH
        OZ      OS_Out
        pop     af
        OZ      OS_Out
        ret

;       ----

.DrawCmdHelpWd
        call    InitHelpWd
        ret     c

        call    MTHPrint
        defm    $7f,"A", " ",SOH,SD_GRV, $7f,"F"
        defm    $7f,"D"
        defm    $7f,"C"
        defm    "'",0

        call    GetHlpCommands
        call    OSBixS1
        push    de
        ld      a, (ubHlpActiveTpc)
        call    GetCmdTopicByNum
        ld      a, (ubHlpActiveCmd)
        call    GetRealCmdPosition
        call    PrintCmdSequence
        call    GetHelpOffs
        ld      b, d
        ld      c, e
        pop     de
        call    OSBoxS1

        call    PrintTopicHelp
        ret     c
        call    Help2Wd_Top
        call    Get2ndCmdHelp
        jr      c, dch_1                        ; no other enties? skip

        call    MayMTHPrint
        defm    $7f,"L"
        defm    " other ", $7f,"T", " entries"
        defm    $7f,"R"
        defm    SOH,SD_OUP,"  ",10
        defm    $7f,"R"
        defm    SOH,SD_ODWN,"  ",11,0

.dch_1
        ld      a, 10
        OZ      OS_Out

        call    MayMTHPrint
        defm    10
        defm    $7f,"L"
        defm    " the ", $7f,"T", " topic"
        defm    $7f,"R"
        defm    SOH,SD_OLFT,"   ",10,0

        jp      Help2Wd_bottom

;       ----

.PrntAppname
        ld      a, (ubHlpActiveApp)
        call    GetAppDOR
        call    OSBixS1
        ld      bc, ADOR_NAME                   ; skip to DOR name
        add     hl, bc
        oz      OS_Bout                         ; print string
        call    OSBoxS1
        ret

;       ----

.PrintActiveTopic
        ld      a, (ubHlpActiveTpc)

.PrintTopic
        push    af
        push    af
        call    GetHlpTopics
        call    OSBixS1
        pop     af
        push    de

        call    SkipNTopics
        inc     hl                              ; skip length byte
        call    MTH_ToggleLT

.ptpc_1
        ld      a, (hl)                         ; print tokenized string
        inc     hl
        call    MayWrt
        jr      nc, ptpc_1

        call    MTH_ToggleLT
        pop     de
        call    OSBoxS1
        pop     af
        ret

;       ----

.PrntCommand
        ld      a, (ubHlpActiveCmd)             ; !! get this after OS_Bix to avoid push/pop
        push    af
        call    GetHlpCommands
        call    OSBixS1
        pop     af
        push    de

        push    af
        ld      a, (ubHlpActiveTpc)
        call    GetCmdTopicByNum
        pop     af

        call    GetRealCmdPosition
        inc     hl                              ; skip length/command code
        inc     hl

.prc_1
        ld      a, (hl)                         ; skip kbd sequence
        inc     hl
        or      a
        jr      nz, prc_1

.prc_2
        ld      a, (hl)                         ; print tokenized sring
        inc     hl
        call    MayWrt
        jr      nc, prc_2

        pop     de
        call    OSBoxS1
        ret

;       ----

.PrintTopicHelp
        call    InputEmpty
        ret     c                               ; input bending? exit
        ld      a, b
        or      c
        ret     z                               ; no help text? exit

        ld      d, b                            ; help offset into DE
        ld      e, c
        call    GetHlpHelp                      ; get help base
        add     hl, de                          ; go to help text start
        ld      a, h                            ; handle bank change
        rlca
        rlca
        and     3
        add     a, b
        ld      b, a
        res     7, h                            ; S0 fix
        res     6, h

;       ----

.MTHPrintTokenized
        call    InputEmpty
        ret     c
        call    OSBixS1
        push    de
        call    JustifyC
.mpt_1
        call    InputEmpty
        jr      c, mpt_2
        ld      a, (hl)
        inc     hl
        call    OSWrt
        jr      nc, mpt_1                       ; no error/end? print more
.mpt_2
        pop     de
        call    OSBoxS1
                                                ; drop thru

;       ----

;OUT:   Fc=1 if input bending

.InputEmpty
        OZ      OS_Xin                          ; examine input
        exx
        ld      hl, ubSysFlags1
        res     SF1_B_INPUTPENDING, (hl)
        jr      c, xin_1
        set     SF1_B_INPUTPENDING, (hl)
.xin_1
        exx
        ccf
        ret

;       ----

.MayMTHPrint
        call    InputEmpty
        jp      nc, MTHPrint                    ; only print if not pre-empted
        inc     sp                              ; double ret  !! should we get rid of this?  it's
        inc     sp                              ; !! potentially dangerous if one forgets it
        ret

;       ----

.aRom_Help
        defm    ":ROM.*/HELP/"

;       ----

; copy topic/command/help/token pointers

.CopyAppPointers
        push    af
        ld      bc, 4<<8|255                    ; 4 loops, C=255 to make sure no underflow from C to B
        ld      de, eHlpTopics
.cap_1
        ldi
        ldi
        ld      a, (hl)
        or      a
        jr      z, cap_2                        ; bank=0? leave alone
        bit     7, a
        jr      nz, cap_2                       ; already fixed (installed app)
        bit     6, a
        jr      nz, cap_2                       ; already fixed (installed app)
        and     $3F                             ; else fix slot
        or      (ix+dhnd_AppSlot)
.cap_2
        ld      (de), a
        inc     hl
        inc     de
        djnz    cap_1
        pop     af
        ret


; --------------------------------------------------------------------------------------
; Return DOR handle in (ActiveAppHandle) to first application in slot A (0-3)
;
.GetSlotApp
        ld      ix, ActiveAppHandle
        call    ZeroHandleIX

        ld      (ix+hnd_Type), HND_DEV
        add     a, $80                          ; ROM.x
        call    InitHandle                      ; init handle
        ret     c

        ld      a, DR_SON                       ; return child DOR
.gsa_1
        OZ      OS_Dor
        ret     c                               ; error? exit

        cp      DN_APL
        ld      a, DR_SIB                       ; return brother DOR
        jr      nz, gsa_1                       ; not app? try brother

        ld      a, DR_SON                       ; return child DOR
        OZ      OS_Dor                          ; DOR interface
        ret

;       ----

.FilenameDOR
        push    bc
        ld      a, OP_DOR                       ; return DOR handle
        ld      bc, 0<<8|255                    ; local pointer, bufsize=255
        ld      de, 3                           ; ouput=3, NOP
        OZ      GN_Opf
        pop     bc
        ret

;       ----

.NextAppDOR
        ld      a, (ubHlpActiveApp)
        inc     a
        jr      AppDOR_sub

.PrevAppDOR
        ld      a, (ubHlpActiveApp)
        dec     a
.AppDOR_sub
        call    GetAppDOR
        ld      (ubHlpActiveApp), a
        ret

;       ----

.SetActiveAppDOR
        ld      (ubHlpActiveApp), a

; IN: A=application ID (A7=ROM/APP, A5-6=slot, A0-4=number)
; OUT: BHL=DOR

.GetAppDOR
        ld      b, KN1_BNK                      ; bind in other part of kernel
        call    fsMS2BankB                      ; remembers old S2 bank binding on stack, returns also B = old bank binding

        push    ix                              ; routine uses IX for temporary DOR handle processing, preserve call IX register...
        push    de
        ld      e, a                            ; remember A
        and     @10011111                       ; mask out slot
        jr      z, appdor_prev                  ; #app=0, goto previous app
        bit     7, a
        jr      z, appdor_rom                   ; slot0-3 application

.appdor_app
        ld      a, e
        and     @01111111
        jr      z, appdor_prev                  ; #app=0, goto previous app
        ld      c, a
        ld      a, 4                            ; app slot 4
        push    bc
        push    de
        call    GetSlotApp
        pop     de
        pop     bc
        jr      c, appdor_notin4

.appdor_nextin4
        dec     c
        jr      z, appdor_foundin4
        ld      a, DR_SIB
        oz      OS_Dor
        jr      nc, appdor_nextin4
.appdor_notin4
        xor     a                               ; wrap goto first app of slot 0 with error
        call    GetSlotApp
        ld      a, RC_Esc
        scf
        jr      appdor_9

.appdor_foundin4
        ld      a, e
        jr      appdor_9

.appdor_prev
        dec     e                               ; previous, app (or last app in prev slot)
.appdor_1
        ld      d, a                            ; remember #app
        inc     a
        call    GetAppDOR
        jr      c, appdor_2                     ; end of list reached, exit to return the last Application DOR pointer to caller
        cp      e
        jr      c, appdor_1                     ; loop until E passed
        jr      z, appdor_1
.appdor_2
        ld      a, d                            ; the previous application ID was the end of the list -
        call    GetAppDOR                       ; return pointer to last application DOR
        jr      appdor_10

.appdor_rom
        ld      a, e                            ; restore A
.appdor_4
        push    af
        and     $1f
        ld      c, a                            ; application number without slot mask
        pop     af
        xor     c
        ld      b, a                            ; slot mask (without application number) in B5B6
        rlca
        rlca
        rlca                                    ; A = slot number 0-3
        push    bc
        call    GetSlotApp                      ; return IX = ActiveAppHandle of first Application DOR in slot
        pop     bc
        jr      c, appdor_5                     ; no apps in slot? go into next slot and get first App DOR
        ld      a, (ix+dhnd_AppSlot)
        and     $c0                             ; slot mask
        rrca                                    ; to A5A6
        xor     b                               ; compare to B
        ld      a, b                            ; reload slot maskB5B6
        jr      z, appdor_7                     ; same slot
.appdor_5
        ld      a, b
        rlca                                    ; slot in A6A7
        and     $c0                             ; !! useless (already done)
        add     a, $40                          ; next slot mask, (and top of slot)
        jr      z, appdor_8                     ; wrapped slot 3, goto first of slot 4
        rrca
        inc     a                               ; 0xx0 0001 - get first app handle in slot
        jr      appdor_4

.appdor_6
        ld      a, DR_SIB                       ; return brother DOR
        OZ      OS_Dor
        jr      c, appdor_5                     ; no brother? next slot

.appdor_7
        inc     b                               ; next app
        dec     c                               ; dec count
        jr      nz, appdor_6

        ld      a, b                            ; return original application ID, but with possible different slot mask
        or      a                               ; Fc=0 to indicate successfull fetch of DOR pointer
        jr      appdor_9

.appdor_8
        ld      e, $81                          ; first of slot 4
        jr      appdor_app

.appdor_9
        push    af
        call    GetHandlePtr                    ; validate device handle before fetching pointer to DOR
        ld      (eHlpAppDOR+2), a
        ld      (eHlpAppDOR), hl
        ld      bc, ADOR_TOPICS
        add     hl, bc
        ld      a, (pMTHHelpHandle+1)
        or      a
        call    z, CopyAppPointers
        call    MS2BankK1
        pop     af
.appdor_10
        pop     de
        pop     ix
        call    fsRestoreS2
        ld      hl, eHlpAppDOR+2                ; point at bank of fetched application DOR pointer
        jr      GetBHLBackw                     ; read it into BHL and return back to caller

;       ----

.GetHlpHelp
        ld      l, <(eHlpHelp+2)
        jr      GetHlp_sub

.GetHlpTokens
        ld      hl, ubSysFlags1
        bit     SF1_B_SYSTOKENS, (hl)           ; use default system tokens if SYSTOKENS set
        jr      nz, UseSysTokens
        ld      l, <(eHlpTokens+2)
        call    GetHlp_sub
        ld      a, b                            ; $FFFFFF will use default system tokens
        and     h
        or      @11000000                       ; $3F3FFF will use default system tokens too
        and     l
        inc     a
        ret     nz                              ; not $FFFFFF
.UseSysTokens
        ld      b, MTH_BNK
        ld      hl, SysTokenBase
        ret        

.GetHlpTopics
        ld      l, <(eHlpTopics+2)
        jr      GetHlp_sub

; out: BHL=commands for suspended app
.GetAppCommands
        ld      hl, [eAppCommands_2+2]
        jr      GetBHLBackw

; out: BHL=help
.GetHlpCommands
        ld      l, <(eHlpCommands+2)

.GetHlp_sub
        ld      h, >eHlpTopics

        push    hl                              ; if help DOR matches app DOR, use app pointer
        ld      hl, (eAppDOR_2)                 ; otherwise use help pointer
        ld      bc, (eHlpAppDOR)
        or      a
        sbc     hl, bc
        pop     hl                              ; !! 'jr nz' to lower pop to save pop/push here
        jr      nz, GetBHLBackw                 ; not same, use help pointers
        push    hl
        ld      hl, [eAppDOR_2+2]
        ld      a, (eHlpAppDOR+2)
        cp      (hl)
        pop     hl
        jr      nz, GetBHLBackw

        ld      bc, eAppTopics_2-eHlpTopics
        add     hl, bc

.GetBHLBackw
        ld      b, (hl)
        dec     hl
        ld      c, (hl)
        dec     hl
        ld      l, (hl)
        ld      h, c
        res     7, h                            ; S0 fix
        res     6, h
        ret

;       ----

; IN: A=count
; OUT: Fc=1, A=0 - last command

.SkipNTopics
        push    af                              ; store count
        inc     hl                              ; skip start mark
        call    NextCommand                     ; validate pointer by going forward and back
        call    PrevCommand
        pop     bc                              ; count into B
        ld      a, 0
        ret     c                               ; only one command/topic? exit Fc=1
        ld      c, a                            ; c = 0
.gcn_1
        inc     c
        djnz    gcn_2
        ld      a, c                            ; Fc=0, A=count
        ret
.gcn_2
        call    NextCommand
        jr      nc, gcn_1                       ; not end of list? loop
        ld      a, b
        or      a
        jp      p, gcn_3
        call    PrevCommand                     ; go back to last vommand/topic
        ld      a, c                            ; return count
        scf
        ret
.gcn_3
        call    PrevCommand                     ; go back to start of topic
        jr      nc, gcn_3
        ld      a, 1
        scf
        ret

;       ----

.GetCmdTopicByNum
        inc     hl                              ; skip start mark
        ld      b, a
.sct_1
        djnz    sct_2
        or      a
        ret
.sct_2
        call    NextTopic
        jr      nc, sct_1
        ret

;       ----

.GetRealCmdPosition
        push    af
.grcp_1
        call    PrevCommand                     ; back to start of topic
        jr      nc, grcp_1
        call    NextCommand                     ; validate by going forward and backward
        call    PrevCommand
        pop     bc                              ; B=count
        ld      a, 0
        ret     c                               ; start of list? Fc=1, A=0
        scf
        ret     z                               ; start of topic? Fc=1
        ld      de, 1<<8|0                      ; column=1, row=0
        ld      c, e                            ; c = 0
.grcp_2
        inc     c                               ; bump actual position
        djnz    grcp_3                          ; not done yet? go forward
        inc     b                               ; B=1 for hidden  !! do this only if necessary
        call    GetAttr
        bit     CMDF_B_HIDDEN, a
        jr      nz, grcp_3                      ; hidden? skip
        ld      a, c                            ; return position in A, Fc=0
        or      a
        ret
.grcp_3
        call    NextCommand
        jr      c, grcp_4                       ; not end of list or topic? go back
        jr      nz, grcp_2
.grcp_4
        ld      a, b                            ; !! bit 7,b; jr z
        or      a                               ; B<0? wanted was hidden, go to previous
        jp      p, grcp_5                       ; else rewind to topic start and search for cmd 1
        call    PrevCommand
        call    GetAttr
        bit     CMDF_B_HIDDEN, a
        jr      nz, grcp_5
        ld      a, c
        scf
        ret
.grcp_5
        call    PrevCommand                     ; back to start of topic
        jr      nc, grcp_5
        ld      a, 1                            ; only way we could here is we searched with A=0, try with 1
        call    GetRealCmdPosition
        ld      de, 1<<8|0                      ; column=1, row=0
        scf
        ret

;       ----

.NextTopic
        call    NextCommand
        ret     c                               ; end mark
        jr      nz, NextTopic                   ; not end_of_topic? loop
        inc     hl                              ; skip eot mark
        ret

;       ----

; IN: HL=command/topic
; OUT: Fc=1, Fz=0 - start of list
; Fc=1, Fz=1 - start of topic
; Fc=0 - HL=command/topic

.PrevCommand
        dec     hl
        ld      a, (hl)                         ; length byte
        cp      1
        inc     hl
        ret     c                               ; 0, start of list- Fc=1, Fz=0
        scf
        ret     z                               ; 1, start of topic - Fc=1, Fz=1
        push    de                              ; HL-=A
        ld      e, a
        ld      d, 0
        or      a
        sbc     hl, de
        pop     de
        jr      c, $PC                          ; error? crash
        ret

;       ----

; IN: HL=command/topic, E=row, D=column
; OUT: Fc=1 - end of list
; Fc=0, Fz=1 - end of topic
; Fc=0, Fz=0 - HL=command/topic, E=row, D=column

.NextCommand
        push    de
        ld      e, (hl)                         ; get length
        ld      d, 0
        add     hl, de                          ; skip current command/topic
        jr      c, $PC                          ; invalid? crash
        pop     de
        ld      a, (hl)                         ; length
        cp      1
        ret     c                               ; 0=start/end mark, Fc=1
        ret     z                               ; 1=topic end, Fz=1
        push    af                              ; store Fc=0, Fz=0
        call    GetAttr                         ; command attribute
        bit     CMDF_B_HIDDEN, a                ; hidden
        jr      nz, nxc_3                       ; yes? skip
        inc     e                               ; advance row
        bit     CMDF_B_COLUMN, a                ; new column
        jr      nz, nxc_2                       ; yes? skip
        ld      a, e
        cp      8                               ; row too big?
        jr      c, nxc_3                        ; no? skip
.nxc_2
        inc     d                               ; advance column
        ld      e, 0                            ; reset row
.nxc_3
        pop     af
        ret

;       ----

.GetHelpOffs
        call    GetAttr_Help
        and     CMDF_HELP                       ; help
        ret     nz                              ; Fc=0, DE=help
        ld      d, a                            ; ld de, 0
        ld      e, a
        ret                                     ; Fc=1, DE=0

;       ----

; IN: HL=command definition
; OUT: A=attribute byte

.GetAttr
        push    de
        call    GetAttr_Help
        pop     de
        ret

;       ----

; IN: HL=command definition
; OUT: A=attribute byte, DE=help text

.GetAttr_Help
        push    bc
        push    hl
        ld      c, (hl)                         ; length byte
        ld      b, 0
        add     hl, bc
        dec     hl
        dec     hl
        ld      a, (hl)                         ; attribute byte
        dec     hl
        ld      e, (hl)                         ; help pointer
        dec     hl
        ld      d, (hl)
        pop     hl
        pop     bc
        ret

;       ----

.SetHlpAppChgFile
        ld      a, (ubHlpActiveApp)             ; !! could do these 2 instructions at caller
        call    GetAppDOR                       ; !! to reduce number of subroutines

.ChgHelpFile
        push    af
        ld      ix, (pMTHHelpHandle)
        ld      a, DR_FRE                       ; free DOR handle
        OZ      OS_Dor                          ; DOR interface
        ld      (pMTHHelpHandle), ix
        pop     af
        or      a
        ret     z
        call    OSBixS1                         ; Bind in extended address
        push    de
        ld      bc, ADOR_NAME
        add     hl, bc
        call    MS2BankK1
        call    OpenAppHelpFile
        ld      (pMTHHelpHandle), ix
        pop     de
        call    OSBoxS1
        ret

;       ----

.DrawMenuWd
        OZ      OS_Pout
        defm    1,"6#6",$20+0,$20+0,$20+94,$20+8
        defm    1, "2C6"
        defm    0

        call    GetHlpCommands
        call    OSBixS1
        push    de

        push    hl
        ld      a, (ubHlpActiveTpc)
        call    GetTpcAttrByNum
        pop     hl
        jr      nc, dmwd_1                      ; has topics? skip

        pop     de
        call    OSBoxS1

        call    InitMenuColumnDE
        call    MayMTHPrint
        defm    10,10,10
        defm    $7f,"C"
        defm    $7f,"A", " has no topics",0

        jr      dmwd_2                          ; Fc=1

.dmwd_1
        ld      a, (ubHlpActiveTpc)
        call    GetCmdTopicByNum
        call    InitMenuColumnDE
        ld      a, (hl)
        cp      2
        jr      nc, dmwd_3                      ; not eol/eot? skip
        pop     de
        call    OSBoxS1

        call    MayMTHPrint
        defm    10,10,10
        defm    $7f,"C"
        defm    "The ", $7f,"A", " ", $7f,"T", " topic",10
        defm    $7f,"C"
        defm    "has no functions",0

.dmwd_2
        scf
        ret

.dmwd_3
        call    InputEmpty
        jp      c, dmwd_10

        call    GetAttr
        bit     CMDF_B_HIDDEN, a
        jr      nz, dmwd_9                      ; hidden? skip
        push    hl
        inc     hl                              ; move to kbd sequence
        inc     hl

        ex      af, af'
.dmwd_4
        ld      a, (hl)                         ; skip kbd sequence to get cmd name
        inc     hl
        or      a
        jr      nz, dmwd_4
        ex      af, af'

        bit     CMDF_B_COLUMN, a
        jr      nz, dmwd_5                      ; column change? handle
        ld      a, e
        cp      8
        inc     e
        jr      c, dmwd_7                       ; not last row? skip
.dmwd_5
        call    InitMenuColumnE
.dmwd_6
        jr      c, dmwd_11
.dmwd_7
        push    de
        call    JustifyN
        ld      a, ' '
        OZ      OS_Out                          ; write a byte to std. output
.dmwd_8
        ld      a, (hl)                         ; print command name
        inc     hl
        call    MayWrt
        jr      nc, dmwd_8
        call    ResetToggles
        pop     de
        pop     hl
        call    PrintCmdSequence
.dmwd_9
        ld      c, (hl)
        ld      b, 0
        add     hl, bc
        ld      a, (hl)
        cp      2
        jr      nc, dmwd_3
        ld      e, 8
        call    InitMenuColumn
        or      a
.dmwd_10
        pop     de
        call    OSBoxS1
        call    nc, DrawMenuWd2
        ret
.dmwd_11
        pop     hl
        pop     hl
        jr      dmwd_10

;       ----

.InitMenuColumnDE
        ld      d, 1

.InitMenuColumnE
        ld      e, 27

; e=width
.InitMenuColumn
        push    hl
        ld      b, d
        ld      a, -28                          ; calculate xpos
.imc_1
        add     a, 28                           ; 0/28/56/84
        djnz    imc_1
        add     a, e
        cp      93
        jr      nc, imc_3
        sub     e
        add     a, $21
        bit     4, e
        jr      nz, imc_2
        ld      a, $75
.imc_2
        push    af
        OZ      OS_Pout
        defm    1,"7#6",0
        pop     af
        OZ      OS_Out                          ; x
        ld      a, $20
        OZ      OS_Out                          ; y
        ld      a, e
        add     a, $20
        OZ      OS_Out                          ; w

        OZ      OS_Pout
        defm    $20+8,$81
        defm    1,"2C6"
        defm    0

        inc     d
        ld      e, 0
        or      a
.imc_3
        pop     hl
        ret

;       ----



;       ----

.PrintCmdSequence
        push    de
        push    hl

        inc     hl                              ; skip length/code
        inc     hl
        call    ResetToggles
        call    JustifyR

        ld      a, (hl)
        call    AtoN_upper
        jr      nc, pcs_1                       ; a-z/A-Z
        call    MTHPrintKeycode
        jr      pcs_3

.pcs_1
        ld      a, SD_DIAM                      ; print <>
        call    ScrDrv_SOH_A

.pcs_2
        ld      a, (hl)                         ; print string
        inc     hl
        OZ      OS_Out
        or      a
        jr      nz, pcs_2

.pcs_3
        push    af
        OZ      OS_Pout
        defm    " ",13,10,0
        pop     af

        pop     hl
        pop     de
        ret


; -----------------------------------------------------------------------------
; Called from OS_In to find the command
; OUT: Fc=1 - no command matches
; Fc=0, Fz=0, A=code - partial match, buffer not ready yet
; Fc=0, Fz=1, A=code - perfect match
; -----------------------------------------------------------------------------
.FindCmd
        call    PutOZwdBuf
        ret     c

        call    GetAppCommands                  ; commands in BHL
        call    OSBixS1
        push    de

        inc     hl                              ; skip start mark
.fcmd_1
        ld      a, (hl)
        cp      1
        jr      c, fcmd_4                       ; end of list
        jr      z, fcmd_2                       ; end of topic? skip it
        push    hl
        inc     hl
        ld      c, (hl)                         ; command code
        inc     hl
        ld      de, OZcmdBuf
        call    CompareCmd
        pop     hl
        jr      nc, fcmd_3                      ; match? return C

.fcmd_2
        ld      e, (hl)                         ; get length
        ld      d, 0
        add     hl, de                          ; skip command
        jr      fcmd_1                          ; compare next command
.fcmd_3
        ld      a, c                            ; get command code
.fcmd_4
        pop     de
        call    OSBoxS1
        ret

.CompareCmd
        ld      a, (hl)
        or      a
        scf
        ret     z                               ; cmd end? Fc=1
        ld      a, (de)
        or      a
        jr      nz, cc_1                        ; buffer not end yet? skip
        ld      a, (hl)                         ; cmd char
        cp      '@'
        ret     z                               ; '@'? Fc=0, Fz=1
        scf
        ret                                     ; otherwise Fc=1
.cc_1
        push    de
        push    hl
.cc_2
        ld      a, (de)
        or      (hl)
        jr      z, cc_4                         ; end? return Fc=0, Fz=1
        ld      a, (de)
        or      a
        jr      z, cc_3                         ; buf end? Fc=0, A=1
        cp      (hl)
        inc     de
        inc     hl
        jr      z, cc_2                         ; same? continue compare
        scf                                     ; different? Fc=1
.cc_3
        inc     a
.cc_4
        pop     hl
        pop     de
        ret
