; **************************************************************************************************
; File Area library functionality (copied standard library functions, optimized for OZ)
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
;
; ***************************************************************************************************

        module FileEprLibrary

        xdef SetBlinkScreen, SetBlinkScreenOn
        xdef FileEprTransferBlockSize
        xdef ApplEprType, MemAbsPtr
        xdef MemReadByte, MemReadWord, MemWriteByte, MemWriteWord
        xdef AddPointerDistance, ConvPtrToAddr, ConvAddrToPtr

        include "error.def"
        include "memory.def"
        include "blink.def"


; ***************************************************************************************************
; Switch Z88 LCD Screen On or Off.
;
; In:
;    Fz = 1, Turn LCD screen On
;    Fz = 0, Turn LCD screen Off
;
; Out:
;    None
;
; Registers changed after return:
;    AFBCDEHL/IXIY same
;    ......../.... different
;
; ------------------------------------------------------------------------
; Design & programming by Gunther Strube, Aug 2006
; ------------------------------------------------------------------------
;
.SetBlinkScreen     PUSH AF
                    PUSH BC
                    LD   BC,BLSC_COM         ; Address of soft copy of COM register
                    LD   A,(BC)
                    RES  BB_COMLCDON,A       ; Screen LCD Off by default
                    CALL Z, lcdon            ; but caller wants to switch On...
                    LD   (BC),A
                    OUT  (C),A               ; signal to Blink COM register to do the LCD...
                    POP  BC
                    POP  AF
                    RET
.lcdon              SET  BB_COMLCDON,A       ; Screen LCD On
                    RET

.SetBlinkScreenOn                            ; Public routine to enable LCD without specifying parameters.
                    PUSH AF
                    CP   A
                    CALL SetBlinkScreen
                    POP  AF
                    RET


; ************************************************************************
;
; Evaluate Standard Z88 Application ROM Format (Front DOR/ROM Header).
;
; Return Standard Application ROM "OZ" status in slot x (0, 1, 2 or 3).
;
; In:
;    C = slot number (0, 1, 2 or 3)
;
; Out:
;    Success:
;         Fc = 0,
;              A = Application Eprom type (with Rom Front Dor):
;                   $80 = external EPROM,
;                   $81 = system/internal OZ ROM,
;                   $82 = external RAM
;              B = size of reserved Application space in 16K banks.
;              C = size of physical Application Card in 16K banks.
;
;    Failure:
;         Fc = 1,
;              A = RC_ONF, Application Eprom not found
;
; Registers changed after return:
;    ....DEHL/IXIY same
;    AFBC..../.... different
;
; ------------------------------------------------------------------------
; Design & programming by Gunther Strube, Apr-Aug 1998, Aug 2006, Aug 2012
; ------------------------------------------------------------------------
;
.ApplEprType
                    PUSH DE
                    PUSH HL

                    LD   A,C                 ; slot C
                    LD   B,$3F
                    LD   HL,$3F00            ; top 256 byte page of top bank in slot C
                    CALL MemAbsPtr           ; Convert to physical pointer...

                    LD   A,$FB
                    CALL MemReadByte
                    LD   C,A                 ; get Application ROM type ($80 or $81)
                    LD   A,$FC               ; offset $FC
                    CALL MemReadByte
                    PUSH AF                  ; size of reserved Application space, $3FFC
                    CALL CheckRomId
                    JR   NZ,no_applrom       ; "OZ" watermark not found...

                    LD   A,C                 ; Application Rom found
                    AND  @10000001
                    JR   Z, no_applrom       ; invalid Application Type Code...

                    CALL CheckRamCard        ; If FRONT DOR is located in a RAM card, return type $82

                    POP  DE                  ; D = size of reserved Application space (16K bank entities)
                    LD   E,C                 ; E = Application ROM Type Code ($80 or $81)
                    LD   A,D
                    CALL GetCardSize
                    LD   C,A                 ; C = physical card size (16K bank entities)
                    LD   B,D                 ; B = size of reserved Application space (16K bank entities)
                    LD   A,E                 ; A = Application ROM Type Code ($80 or $81)
.exit_ApplEprType
                    POP  HL                  ; original HL restored
                    POP  DE                  ; original DE restored
                    RET
.no_applrom
                    POP  AF
                    LD   A,RC_ONF
                    SCF
                    JR   exit_ApplEprType


; ************************************************************************
;
; Calculate physical size of Card by scanning for "OZ" header from
; bank $3E downwards in available 1MB slot.
;
; In:
;    B = top bank of slot containing Front DOR.
;    A = size of reserved Application space (16K bank entities)
;
;    HL = offset $3F00 (mapped to free segment in address space).
;
; Out:
;    A = physical size of card in 16K entities.
;
; Registers changed after return:
;    ..BCDEHL/IXIY same
;    AF....../.... different
;
.GetCardSize        PUSH DE
                    LD   D,B            ; preserve top bank number in slot
                    LD   E,A
                    LD   A,B
                    SUB  E              ; TopBank - ReservedSpace
                    LD   B,A            ; begin parsing at bank below reserved appl. space
                    LD   A,64
                    SUB  E              ; parse max. 64 - ReservedSpace banks...
.scan_loop
                    CALL CheckRomId
                    JR   Z, oz_found
                    DEC  B
                    DEC  A
                    JR   NZ,scan_loop

.oz_found           RES  7,B            ; slot size always max 64 * 16K banks...
                    RES  6,B
                    LD   A,D
                    AND  @00111111
                    SUB  B              ; Card Size = TopBank - TopBank' (0-64)
                    OR   A              ; Fc = 0...
                    POP  DE
                    RET  NZ
                    LD   A,64           ; Card was 1MB...
                    RET

; ************************************************************************
;
; IN:
;    A = original Application Card Type Code ($80 or $81)
;    BHL = <top bank> $3F00
;
; OUT:
;    A = $82, if FRONT DOR in RAM card, otherwise original code.
;
; Registers changed after return:
;    ..BCDEHL/IXIY same
;    AF....../.... different
;
.CheckRamCard       PUSH BC
                    PUSH DE

                    LD   E,A
                    LD   A,$F8
                    CALL MemReadByte         ; get low byte card ID
                    INC  A
                    LD   C,A
                    LD   A,$F8
                    CALL MemWriteByte        ; try to write another value to "RAM"
                    LD   A,$F8
                    CALL MemReadByte         ; then read it back
                    CP   C                   ; changed?
                    JR   Z, ramcard
                         LD   A,E            ; Eprom Card - return original type code
                         JR   exit_CheckRamCard
.ramcard
                         DEC  C
                         LD   A,$F8
                         CALL MemWriteByte        ; write back original card id
                         LD   A,$82               ; return $82 for RAM based application card
.exit_CheckRamCard
                    OR   A
                    POP  DE
                    POP  BC
                    RET


; ************************************************************************
;
; Check for "OZ" watermark at bank B, offset $3FFE.
;
; In:
;    B = Bank (usually $3F, mapped to slot X) containing possible Front DOR.
;    HL = offset $3F00 (mapped to free segment).
;
; Out:
;    Success:
;         Fz = 1,
;              "OZ" watermark found in bank B
;
;    Failure:
;         Fz = 0,
;              "OZ" watermark not found.
;
; Registers changed after return:
;    A.BCDEHL/IXIY same
;    .F....../.... different
;
.CheckRomId         PUSH DE
                    PUSH AF

                    LD   A,$FE
                    CALL MemReadWord

                    PUSH HL
                    LD   HL,$5A4F
                    SBC  HL,DE               ; 'OZ' ?
                    POP  HL

                    POP  DE
                    LD   A,D                 ; original A restored
                    POP  DE
                    RET                      ; Fz = ?


; ************************************************************************
;
; Convert relative BHL pointer for slot number A (0 to 3) to absolute
; pointer, addressed for binding MS_S1 segment.
;
; Internal Support Library Routine.
;
; IN:
;    A = slot number (0 to 3)
;    BHL = relative pointer
;
; OUT:
;    BHL pointer, absolute addressed for physical slot A, and free segment.
;
; Registers changed after return:
;    ...CDE../IXIY same
;    AFB...HL/.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, April 1998, July 2006, Aug 2012
; ----------------------------------------------------------------------
;
.MemAbsPtr
                    RES  7,B
                    RES  6,B                      ; clear before masking to assure proper effect...
                    AND  @00000011                ; only slot 0 - 3 allowed...
                    RRCA                          ;
                    RRCA                          ; Slot number A converted to slot mask
                    OR   A
                    JR   NZ, MemAbsPtr_slotx
                    RES  5,B                      ; slot 0 rom bank range is $00 - $1F
.MemAbsPtr_slotx
                    OR   B
                    LD   B,A                      ; B = converted to physical bank of slot A
                    RES  7,H
                    SET  6,H
                    OR   H                        ; for bank I/O (outside this executing code)
                    LD   H,A                      ; offset mapped for MS_S1 segment
                    RET


; ***************************************************************************************************
;
; Read byte at base record pointer in BHL, offset A.
; Byte is returned in A.
;
; If B<>0, the byte is read from extended address.
; If B=0, the byte is read from local address space.
;
;    Register affected on return:
;         ..BCDEHL/IXIY same
;         AF....../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1997, Sep 2004, Oct 2005, Aug 2012
; ----------------------------------------------------------------------
;
.MemReadByte        PUSH HL
                    PUSH DE
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    LD   A,(HL)                   ; read byte at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  DE
                    POP  HL
                    RET


; ***************************************************************************************************
;
; Read 16bit word at record defined as extended (base) address in BHL, offset A.
;
; If B<>0, the 16bit word is read from extended address.
; If B=0, the 16bit word is read from local address space.
;
; Return 16bit word in DE.
;
;    Register affected on return:
;         A.BC..HL/IXIY same
;         .F..DE../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, July 1998, Sep 2004, Oct 2005
; ----------------------------------------------------------------------
;
.MemReadWord        PUSH HL
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    LD   E,(HL)
                    INC  HL
                    LD   D,(HL)                   ; read 16bit word at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  HL
                    RET


; ***************************************************************************************************
;
; Set byte in C, at base record pointer in BHL, offset A.
;
;    Register affected on return:
;         A.BCDEHL/IXIY same
;         .F....../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1995-97, Sep 2004, Oct 2005, Aug 2012
; ----------------------------------------------------------------------
;
.MemWriteByte       PUSH HL
                    PUSH DE
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    POP  DE
                    LD   (HL),E                   ; write byte at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    LD   B,D
                    LD   C,E                      ; original BC restored...
                    POP  DE
                    POP  HL
                    RET


; ***************************************************************************************************
;
; Write word in DE, at base record pointer in BHL, offset A.
;
;    Register affected on return:
;         A.BCDEHL/IXIY same
;         .F....../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1995-98, Sep 2004, Oct 2005, Aug 2012
; ----------------------------------------------------------------------
;
.MemWriteWord       PUSH HL
                    PUSH BC
                    PUSH DE

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    POP  DE
                    LD   (HL),E                   ; write word at extended or local address
                    INC  HL
                    LD   (HL),D
                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  HL
                    RET


; **************************************************************************
;
; Add distance CDE (24bit integer) to current pointer address BHL
;
; A new pointer is returned in BHL, preserving original
; slot mask and segment mask.
;
; This routine is primarily used for File Eprom management.
;
; Registers changed after return:
;    AF.CDE../IXIY same
;    ..B...HL/.... different
;
; --------------------------------------------------------------------------
; Design & programming by Gunther Strube, InterLogic, Dec 1997
; --------------------------------------------------------------------------
;
.AddPointerDistance
                    PUSH AF
                    PUSH DE

                    LD   A,C
                    PUSH AF                  ; preserve C register

                    LD   A,H
                    AND  @11000000
                    PUSH AF                  ; preserve segment mask
                    RES  7,H
                    RES  6,H

                    LD   A,B
                    AND  @11000000
                    PUSH AF                  ; preserve slot mask
                    RES  7,B
                    RES  6,B

                    LD   A,C
                    PUSH DE                  ; preserve distance in ADE

                    CALL ConvPtrToAddr       ; BHL -> DEBC address

                    POP  HL
                    ADD  HL,BC
                    LD   B,H
                    LD   C,L
                    ADC  A,E                 ; distance added to DEBC,
                    LD   E,A                 ; result in DEBC, new abs. address

                    CALL ConvAddrToPtr       ; new abs. address to BHL logic...

                    POP  AF
                    OR   B
                    LD   B,A                 ; slot mask restored in bank number

                    POP  AF
                    OR   H
                    LD   H,A                 ; segment mask restored in offset

                    POP  AF
                    LD   C,A                 ; C register restored

                    POP  DE
                    POP  AF
                    RET


; ***************************************************************************
;
; Convert 1MB 20bit address to relative pointer in BHL
; (B = 00h - 3Fh, HL = 0000h - 3FFFh).
;
; This routine is primarily used File Eprom management
;
; IN:
;    EBC = 24bit integer (actually 20bit 1MB address)
;
; OUT:
;    BHL = pointer
;
; Registers changed after return:
;    AF.CDE../IXIY same
;    ..B...HL/.... different
;
; --------------------------------------------------------------------------
; Design & programming by Gunther Strube, InterLogic, Dec 1997
; --------------------------------------------------------------------------
;
.ConvAddrToPtr
                    PUSH AF
                    LD   A,B
                    AND  @11000000
                    LD   H,B
                    RES  7,H
                    RES  6,H
                    LD   L,C                 ; OFFSET READY...

                    LD   B,E                 ; now divide top 6 address bit with 16K
                    SLA  A                   ; and place it into B (bank) register
                    RL   B
                    SLA  A
                    RL   B
                    POP  AF
                    RET                      ; BHL now (relative) ext. address


; ***************************************************************************
;
; Convert relative pointer BHL (B = 00h - 3Fh, HL = 0000h - 3FFFh)
; to absolute 20bit 1MB address.
;
; This routine primarily used for File Eprom Management.
;
; IN:
;    BHL = pointer
;
; OUT:
;    DEBC = 32bit integer (actually 24bit)
;
; Registers changed after return:
;    AF....HL/IXIY same
;    ..BCDE../.... different
;
; ------------------------------------------------------------------------
; Design & programming by Gunther Strube, InterLogic, Dec 1997
; ------------------------------------------------------------------------
;
.ConvPtrToAddr      PUSH AF
                    PUSH HL
                    LD   D,0
                    LD   E,B
                    LD   BC,0
                    SRA  E
                    RR   B
                    SRA  E
                    RR   B
                    ADD  HL,BC               ; DEBC = <BankNumber> * 16K + offset
                    LD   B,H
                    LD   C,L                 ; DEBC = BHL changed to absolute address in Eprom
                    POP  HL
                    POP  AF
                    RET


; ***************************************************************************************************
; File Entry Management:
;       Internal support library routine for FileEprFetchFile & FlashEprCopyFileEntry.
;
; Define a block size to transfer, which is from the current bank offset and within 16K bank boundary,
; considering the remaining file size to copy.
;
; IN:
;    BHL = current pointer to file block data
;    cde' = current file size
;
; OUT:
;    hl' = size of block in File Area to transfer
;    cde' = updated according to adjusted block size (that can be transferred within the bank boundary)
;
; Registers changed after return:
;    ..BCDEHL/IXIY ........ same
;    AF....../.... afbcdehl different
;
.FileEprTransferBlockSize
                    PUSH BC
                    PUSH DE
                    PUSH HL

                    EX   DE,HL
                    LD   HL,$4000                 ; 16K bank boundary...
                    CP   A                        ; Fc = 0
                    SBC  HL,DE                    ; HL = <BankSpace>

                    EXX
                    PUSH DE
                    PUSH BC                       ; get a copy of current file size (CDE)
                    PUSH DE
                    PUSH BC                       ; and preserve a copy...
                    EXX
                    POP  BC
                    POP  DE                       ; divisor in CDE (current size of file)
                    LD   B,0                      ; dividend in BHL (remaining bytes of bank)
                    call Divu24                   ; <blocksize> = <FileSize> MOD <BankSpace>
                    EXX
                    POP  BC
                    POP  DE                       ; (restore current file size)
                    EXX

                    LD   A,H
                    OR   L
                    JR   Z, last_block
                    EXX
                    PUSH DE
                    EXX
                    POP  DE
                    CALL fsize_larger             ; copy remaining bank space from file inside current bank
                    JR   exit_TransferBlockSize
.last_block
                    LD   A,D
                    OR   E                        ; <blocksize> = 0 ?
                    CALL NZ, fsize_larger         ; no, FileSize > BankSpace
                    CALL Z, fsize_smaller         ; Yes, FileSize <= BankSpace
.exit_TransferBlockSize
                    POP  HL
                    POP  DE
                    POP  BC
                    RET
.fsize_smaller
                    EXX                           ; remaining file image to be copied is
                    EX   DE,HL                    ; smaller than <BankSpace>, therefore
                    LD   DE,0                     ; the last image block is resident in the
                    EXX                           ; current bank...
                    RET                           ; HL' = FileSize (max. 16K)
.fsize_larger
                    PUSH AF                       ; size of remaining file image crosses current
                    PUSH DE                       ; bank boundary...
                    EXX                           ; define block size only of <BankSpace> size.
                    POP  HL
                    PUSH HL
                    EX   DE,HL
                    SBC  HL,DE
                    LD   D,H
                    LD   E,L
                    LD   A,C
                    SBC  A,0
                    LD   C,A                      ; FileSize = FileSize - BankSpace
                    POP  HL                       ; HL' = BankSpace ...
                    EXX
                    POP  AF
                    RET

;       BHL/CDE -> BHL=quotient, CDE=remainder
.Divu24
                    push    hl
                    xor     a
                    ld      hl, 0
                    exx                                     ;       alt
                    pop     hl
                    ld      b, 24
.d24_2
                    rl      l
                    rl      h
                    exx                                     ;       main
                    rl      b
                    rl      l
                    rl      h
                    rl      a
                    push    af
                    push    hl
                    sbc     hl, de
                    sbc     a, c
                    ccf
                    jr      c, d24_3
                    pop     hl
                    pop     af
                    or      a
                    jr      d24_4
.d24_3
                    inc     sp
                    inc     sp
                    inc     sp
                    inc     sp
.d24_4
                    exx                                     ;       alt
                    djnz    d24_2

                    rl      l
                    rl      h
                    push    hl
                    exx                                     ;       main
                    rl      b
                    ex      de, hl
                    ld      c, a
                    pop     hl
                    or      a
.d24_5
                    ret