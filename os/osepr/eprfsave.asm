; **************************************************************************************************
; File Area functionality.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

        module FileEprSaveRamFile

        xdef FileEprSaveRamFile,FileEprSaveAsRamFile

        xref FlashEprCardId, FlashEprWriteBlock, FileEprFindFile
        xref FileEprFreeSpace, FileEprDeleteFile, FileEprNewFileEntry, GetUvProgMode
        xref SetBlinkScreen, SetBlinkScreenOn, BlowMem

        include "z80.def"
        include "error.def"
        include "fileio.def"
        include "memory.def"
        include "director.def"
        include "handle.def"

        defc SizeOfWorkSpace = 256         ; size of Workspace on stack, IY points at base...

        ; Relative offset definitions for allocated work buffer on stack
        defvars 0
             IObuffer  ds.w 1              ; Pointer to I/O buffer
             IObufSize ds.w 1              ; Size of I/O buffer
             Fhandle   ds.w 1              ; Handle of openend file
             CardType  ds.b 1              ; card/chip type (FE_28F, Fe_29F or 0 for UV Eprom)
             UvHandle  ds.w 1              ; Handle for UV Eprom programming settings
             FileEntry ds.p 1              ; pointer to File Entry
             CardSlot  ds.b 1              ; slot number of File Eprom Card
             StackBuf                      ; Internal Workspace
        enddef


; **************************************************************************
; Standard Z88 File Eprom Format.
;
; Save RAM file to Flash Memory or UV Eprom file area in slot C.
; (files to UV Eprom only in slot 3)
;
; The routine does NOT handle automatical "deletion" of existing files
; that matches the filename (excl. device). This must be used by a call
; to <FlashEprFileDelete>.
;
; Should the actual process of blowing the file image fail, the new
; File Entry will be marked as deleted, if possible.
;
; -------------------------------------------------------------------------
; The screen is turned off while saving a file to flash file area that is in
; the same slot as the OZ ROM. During saving, no interference should happen
; from Blink, because the Blink reads the font bitmaps each 1/100 second:
;    When saving a file is part of OZ ROM chip, the font bitmaps are suddenly
;    unavailable which creates violent screen flickering during chip command mode.
;    Further, and most importantly, avoid Blink doing read-cycles while
;    chip is in command mode.
; By switching off the screen, the Blink doesn't read the font bit maps in
; OZ ROM, and the Flash chip can be in command mode without being disturbed
; by the Blink.
; -------------------------------------------------------------------------
;
; Important:
; INTEL I28Fxxxx series Flash chips require the 12V VPP pin in slot 3
; to successfully blow data to the memory chip. If the Flash Eprom card
; is inserted in slot 1 or 2, this routine will report a programming failure.
;
; It is the responsibility of the application (before using this call) to
; evaluate the Flash Memory (using the FlashEprCardId routine) and warn the
; user that an INTEL Flash Memory Card requires the Z88 slot 3 hardware, so
; this type of unnecessary error can be avoided.
;
; Equally, the application should evaluate that saving to a file that is on an
; UV Eprom only can be performed in slot 3. This routine will report failure
; if saving a file to slots 0, 1 or 2.
;
; IN:
;          C = slot number (0, 1, 2 or 3)
;         IX = size of I/O buffer.
;         DE = pointer to I/O buffer, in segment 0/1.
;         HL = pointer to filename string (null-terminated), in segment 0/1.
;              Filename may contain wildcards (to find first match)
; OUT:
;         Fc = 0, File successfully saved to Flash File Eprom.
;              BHL = pointer to created File Entry in slot C.
;
;         Fc = 1,
;              File (Flash) Eprom not available in slot A:
;                   A = RC_NFE (not a recognized Flash Memory Chip)
;              Not sufficient space to store file (and File Entry Header):
;                   A = RC_Room
;              Flash Eprom Write Errors:
;                   If possible, the new File Entry is marked as deleted.
;                   A = RC_VPL, RC_BWR (see "error.def" for details)
;
;              RAM File was not found, or other filename related problems:
;                   A = RC_Onf
;                   A = RC_Ivf
;                   A = RC_use
;
; Registers changed on return:
;    ...CDE../IXIY same
;    AFB...HL/.... different
;
; -------------------------------------------------------------------------
; Design & Programming, Gunther Strube
;       Dec 1997-Apr 1998, Sep 2004, Aug 2006, Nov 2006,
;       Mar 2007, Feb 2009, Nov 2015
; -------------------------------------------------------------------------
;
.FileEprSaveRamFile
        push    ix                              ; preserve IX
        push    de
        push    bc                              ; preserve CDE
        push    iy                              ; preserve original IY

.process_file
        call    SetupStackWorkSpace             ; prepare workspace on Stack, IY = base pointer

        ld      a,c
        and     @00000011
        ld      (iy + CardSlot),a               ; preserve slot number of File Eprom Card
        ld      (iy + IObuffer),e
        ld      (iy + IObuffer+1),d             ; preserve pointer to external IO buffer
        push    ix
        pop     bc
        ld      (iy + IObufSize),c
        ld      (iy + IObufSize+1),b            ; preserve size of external IO buffer

        push    hl                              ; preserve ptr. to filename...
        call    GetStackBufferPtr               ; HL = IY + StackBuf
        ld      d,h
        ld      e,l                             ; DE points at space for File Entry
        ex      (sp),hl                         ; preserve pointer to File Entry
        ld      bc, SizeOfWorkSpace-StackBuf-16  ; B=0 (local ptr), C = max. size of exp. filename (w. entry header)
        ld      a, OP_IN                        ; HL = ptr. to filename...
        oz      GN_Opf                          ; open file for input...
        pop     hl                              ; ptr. to expanded filename
        jp      c, exit_filesave                ; Ups - system error, return back to caller...
        call    preserve_fhandle                ; IX -> (IY + Fhandle)

        ld      de,5
        add     hl,de                           ; point at character before "/" (device skipped)

        ld      a,c
        sub     7                               ; length of filename excl. device name...
        call    BuildFileEntryHdr
        jr      c, end_filesave                 ; file area not found, or no room for file entry

        ld      a,(iy + CardSlot)
        oz      OS_Ploz                         ; is OZ running in slot A?
        call    nz,SetBlinkScreen               ; yes, saving file to file area in OZ ROM (slot 0 or 1) requires LCD turned off

        call    BlowFileEntryHdr                ; Now, blow file to Flash or UV Eprom...
        call    nc,BlowRamFile                  ; BHL points to image for RAM file, stream from RAM into file area...
        call    SetBlinkScreenOn                ; then always turn on screen after save file operation

.end_filesave
        push    af                              ; preserve error status...
        call    GetFhandle                      ; get file handle of open file
        oz      Gn_Cl                           ; close file
        pop     af

        call    GetFileEntry                    ; return BHL <- (IY + FileEntry)
.exit_filesave
        exx
        pop     hl
        ld      sp,hl                           ; install original SP
        exx
        pop     iy                              ; original IY restored

        pop     de
        ld      c,e                             ; original C restored
        pop     de
        pop     ix
        ret


; **************************************************************************
; Standard Z88 File Eprom Format.
;
; Save opened RAM file (via IX handle) to Flash Memory or UV Eprom file area
; in slot C (files to UV Eprom only in slot 3), using specified file entry
; filename (beginning with "/").
;
; Should the actual process of blowing the file image fail, the new
; File Entry will be marked as deleted, if possible.
;
; The routine handles automatical "deletion" of existing file
; that matches the filename.
; -------------------------------------------------------------------------
; The screen is turned off while saving a file to flash file area that is in
; the same slot as the OZ ROM. During saving, no interference should happen
; from Blink, because the Blink reads the font bitmaps each 1/100 second:
;    When saving a file is part of OZ ROM chip, the font bitmaps are suddenly
;    unavailable which creates violent screen flickering during chip command mode.
;    Further, and most importantly, avoid Blink doing read-cycles while
;    chip is in command mode.
; By switching off the screen, the Blink doesn't read the font bit maps in
; OZ ROM, and the Flash chip can be in command mode without being disturbed
; by the Blink.
; -------------------------------------------------------------------------
;
; Important:
; INTEL I28Fxxxx series Flash chips require the 12V VPP pin in slot 3
; to successfully blow data to the memory chip. If the Flash Eprom card
; is inserted in slot 1 or 2, this routine will report a programming failure.
;
; It is the responsibility of the caller to evaluate the Flash Memory
; (using the FlashEprCardId routine) and warn the user that an INTEL Flash
; Memory Card requires the Z88 slot 3 hardware, so this type of unnecessary
; error can be avoided.
;
; Equally, the caller should evaluate that saving to a file that is on an
; UV Eprom only can be performed in slot 3. This routine will report failure
; if saving a file to slots 0, 1 or 2.
;
; IN:
;           C = slot number (0, 1, 2 or 3)
;          IX = RAM file handle
;           D = size of File entry name
;         BHL = pointer to explicit File entry name string
;
; OUT:
;         Fc = 0, File successfully saved to Flash File Eprom.
;              BHL = pointer to created File Entry in slot C.
;
;         Fc = 1,
;              File (Flash) Eprom not available in slot A:
;                   A = RC_NFE (not a recognized Flash Memory Chip)
;              Not sufficient space to store file (and File Entry Header):
;                   A = RC_Room
;              Flash Eprom Write Errors:
;                   If possible, the new File Entry is marked as deleted.
;                   A = RC_VPL, RC_BWR (see "error.def" for details)
;
;              RAM File was not found, or other filename related problems:
;                   A = RC_Onf
;                   A = RC_Ivf
;                   A = RC_use
;
; Registers changed on return:
;    ...CDE../IXIY same
;    AFB...HL/.... different
;
; -------------------------------------------------------------------------
; Design & Programming
;       Gunther Strube, Nov 2015
; -------------------------------------------------------------------------
;
.FileEprSaveAsRamFile
        push    ix                              ; preserve IX
        push    de
        push    bc                              ; preserve CDE
        push    iy                              ; preserve original IY

        call    SetupStackWorkSpace             ; prepare workspace on Stack, IY = base pointer

        ld      a,c
        and     @00000011
        ld      (iy + CardSlot),a               ; preserve slot number of File Eprom Card
        call    preserve_fhandle                ; preserve IX file handle

        ld      c,d                             ; use default name size as specified (in D)..
        ld      a, SizeOfWorkSpace-StackBuf-17  ; max size file explicit file entry name
        cp      d
        jr      nc,preserve_flenmsz
        ld      c,a                             ; filename bigger, truncate to stack buffer size..
.preserve_flenmsz
        ld      (iy + IObufSize),c              ; preserve length of file entry name, temporarily

        push    hl
        call    GetStackBufferPtr               ; HL = IY + StackBuf
        ld      (iy + IObuffer),l
        ld      (iy + IObuffer+1),h             ; This buffer will also be used for RAM file streaming..

        inc     hl
        ex      de,hl                           ; DE = IY + StackBuf+1
        pop     hl

        oz      OS_Bhl                          ; copy filename at (BHL) -> (DE), C length
        ld      b,0
        push    de                              ; remember start file entry name
        ex      de,hl
        add     hl,bc
        ld      (hl),0                          ; always null-terminate (needed for EP_find)
        pop     de

        ld      a,FN_VH
        ld      b,HND_FILE
        oz      OS_Fn                           ; ensure that file handle is valid
        jr      c,exit_filesave

        ld      c,(iy + CardSlot)               ; find in slot C, DE = file entry name
        call    FileEprFindFile                 ; find existing entry (to be marked as deleted later)
        push    af                              ; remember found status
        push    bc
        push    hl

        ld      a,(iy + IObufSize)              ; length of file entry name
        ex      de,hl
        dec     hl                              ; point at length byte of file entry header...
        call    BuildFileEntryHdr
        jr      c, err_saveas                   ; file area not found, or no room for file entry

        ld      a,(iy + CardSlot)
        oz      OS_Ploz                         ; is OZ running in slot A?
        call    nz,SetBlinkScreen               ; yes, saving file to file area in OZ ROM (slot 0 or 1) requires LCD turned off

        call    BlowFileEntryHdr                ; Now, blow file entry header to Flash or UV Eprom...
        jr      c, err_saveas

        ld      (iy + IObufSize),SizeOfWorkSpace-StackBuf
        ld      (iy + IObufSize+1),0            ; define IO buffer size (as used by LoadBuffer routine) here in stack buffer

        call    nc,BlowRamFile                  ; BHL points to image for RAM file, stream from RAM into file area...
        call    SetBlinkScreenOn                ; then always turn on screen after save file operation

.err_saveas
        pop     hl                              ; get old file entry (if previously found)
        pop     bc
        pop     de
        jr      c, exit_saveas                  ; error writing new file entry?

        bit     Z80F_B_Z,E
        jr      z, exit_saveas                  ; no earlier version (Fz = 0)?

        call    FileEprDeleteFile               ; new version saved, mark old file entry as deleted (in BHL)
        or      a
.exit_saveas
        call    GetFhandle                      ; return IX = IX(in), caller is responsible for closing ressource
        call    GetFileEntry                    ; return pointer to new file entry: BHL <- (IY + FileEntry)
        jp      exit_filesave


; **************************************************************************
; Build the file entry header
; IN:
;     A = length of filename (incl. '/' - first byte of filename)
;    HL = pointer to File Entry Header on Stack Buffer (first byte for length, followed by '/')
;    IX = handle of opened RAM file
;
; OUT:
;    HL = HL(in)
;    Fc = 0: File Entry ready, file area exist; ready to blow hdr + stream data
;    Fc = 1, File area not found or No Room.
.BuildFileEntryHdr
        push    hl                              ; (length byte) - This is start of File Entry Header...

        ld      (hl),a
        push    af                              ; preserve length of filename
        inc     a
        ld      d,0
        ld      e,a
        add     hl,de                           ; point at beyond last character of filename...

        ld      a, FA_EXT
        ld      e,0                             ; DE = 0
        oz      OS_Frm                          ; get size of file image in DEBC (32bit integer)
        ld      (hl),c
        inc     hl
        ld      (hl),b
        inc     hl
        ld      (hl),e
        inc     hl
        ld      (hl),d                          ; File Entry now ready...

        pop     af                              ; length of filename (excl. device)
        add     a,4+1                           ; total size = length of filename + 1 + file length
        ld      h,0                             ;                                       (4 bytes)
        ld      l,a
        add     hl,bc
        ld      b,h
        ld      c,l
        ld      hl,0
        adc     hl,de
        push    hl
        push    bc

        ld      c,(iy + CardSlot)               ; scan File Eprom in slot X for free space
        call    FlashEprCardId
        jr      nc, get_freespace
        ld      a,3                             ; Flash card not found in slot C...
        cp      c
        jr      nz, get_freespace               ; and we're not in slot 3 (saving file will fail...)
        call    GetUvProgMode                   ; IX = handle to UV Eprom programming settings in slot 3
        push    ix
        pop     hl
        ld      (iy + UvHandle),l
        ld      (iy + UvHandle+1),h             ; preserve UV Eprom programming handle
        xor     a                               ; identify UV Eprom as Card type 0
.get_freespace
        ld      (IY + CardType),A               ; preserve card type of chip in slot C
        call    FileEprFreeSpace                ; returned in DEBC (Fc = 0, Eprom available...)
        jr      c, no_filearea

        ld      h,b
        ld      l,c                             ; HL = low word of 32bit free space...
        pop     bc
        sbc     hl,bc
        ex      de,hl                           ; HL = high word of 32bit free space...
        pop     de                              ; DE = high word of file size
        sbc     hl,de
        pop     hl                              ; ptr. to File Entry
        ld      a, RC_Room
        ret                                     ; return A=RC_Room, Fc =1, if file size (incl. File Entry Header) > free space...
.no_filearea
        pop     hl
        pop     hl
        pop     hl                              ; get rid of push'ed stack values, HL=HL(in)
        ld      a, RC_Onf
        scf                                     ; indicate "No File area available"...
        ret

; **************************************************************************
; Establish a <SizeOfWorkSpace> buffer to build file entry header, IY = base of SizeOfWorkSpace
;
.SetupStackWorkSpace
        exx                                     ; use alternate registers temporarily
        pop     de                              ; get RET address
        ld      hl,0
        add     hl,sp                           ; point at IY on stack
        ld      iy, -SizeOfWorkSpace            ; create temporary work buffer on stack
        add     iy,sp
        ld      sp,iy
        push    hl                              ; restore original SP when .end_filesave
        push    de
        exx
        ret

; **************************************************************************
; Return HL = pointer to buffer (above variables, for filename or I/O)
.GetStackBufferPtr
        push    iy
        pop     hl
        push    bc
        ld      bc,StackBuf
        add     hl,bc                           ; point at workspace for File Entry Header...
        pop     bc
        ret

; **************************************************************************
; Preserve IX file handle
.preserve_fhandle
        push    bc
        push    ix
        pop     bc
        ld      (iy + Fhandle),c
        ld      (iy + Fhandle+1),b              ; preserve IX handle of open file
        pop     bc
        ret

; *****************************************************************************
; IX <- (IY + Fhandle)
;
.GetFhandle
        push    bc
        ld      c,(iy + Fhandle)
        ld      b,(iy + Fhandle+1)
        push    bc
        pop     ix                              ; get file handle of open file
        pop     bc
        ret

; **************************************************************************
; Blow the file entry header to UV Eprom or Amd/Intel Flash
; IN:
;    HL = pointer to File Entry Header (in Stack Buffer)
;
.BlowFileEntryHdr
        push    hl
        ld      c,(iy + CardSlot)
        call    FileEprNewFileEntry             ; BHL = ptr. to free file space on File Eprom Card
        ld      (iy + FileEntry),l
        ld      (iy + FileEntry+1),h
        ld      (iy + FileEntry+2),b            ; preserve pointer to new File Entry
        pop     de
        ld      a,(de)                          ; length of filename
        add     a,4+1                           ; total size = length of filename + 1 (file length byte)
        ld      c,a                             ;                                 + 4 (32bit file length)

        xor     a
        or      (iy + CardType)
        jr      z, save_uvfileentry             ; card type indicates that file is to be blown on UV Eprom or Flash...

        push    bc                              ; Blow File Entry Header of C length to Flash chip
        ld      b,0
        push    bc                              ; DE = ptr. to File Entry
        pop     ix                              ; length of File Entry in IX
        pop     bc                              ; BHL = pointer to free space on Eprom
        ld      c,(IY + CardType)               ; flash chip card type...
        res     7,h
        set     6,h                             ; use segment 1 to blow bytes...
        call    FlashEprWriteBlock              ; blow File Entry to Flash Eprom
        ret     nc                              ; Fc = 0, A = FE_xx chip type

        ; File Entry was not blown properly, mark it as 'deleted'...

.mark_fentryDeleted
        push    af
        call    GetFileEntry                    ; BHL <- (IY + FileEntry)
        call    FileEprDeleteFile               ; mark entry as deleted
        pop     af
        ret

.save_uvfileentry
        call    GetUVhandle
        ld      a,c
        exx
        ld      b,0
        ld      c,a
        exx
        call    BlowMem                         ; Blow File Entry Header of C length to UV Eprom at (BHL)
        jr      c,mark_fentryDeleted
        ret


; **************************************************************************
; IN:
;    BHL = pointer to File Entry image offset (first byte after entry)
;
.BlowRamFile
        xor     a
        or      (iy + CardType)
        jr      z, save_uv_file_loop            ; card type indicates that file is to be blown on UV Eprom or Flash...

.save_flash_file_loop                           ; A = chip type to blow data
        call    LoadBuffer                      ; Load block of bytes from file into external buffer
        ret     z                               ; EOF reached...

        call    FlashEprWriteBlock              ; blow buffer to Flash Eprom at BHL...
        jr      c,mark_fentryDeleted            ; exit saving, File was not blown properly (try to mark it as deleted)...
        jr      save_flash_file_loop

.save_uv_file_loop
        call    LoadBuffer                      ; Load block of bytes from file into external buffer
        ret     z                               ; EOF reached...
        exx
        push    ix
        pop     bc
        exx
        call    GetUVhandle
        call    BlowMem                         ; Blow buffer of BC' length to UV Eprom at (BHL)
        jr      c,mark_fentryDeleted            ; exit saving, File was not blown properly (try to mark it as deleted)...
        jr      save_uv_file_loop


; *****************************************************************************
; Load a chunk from the file into buffer of <BufferSize> bytes
;
; IN:
;    None.
;
; OUT:
;    Fz = 1, if EOF was reached...
;
;    Fz = 0, buffer loaded with file contents...
;         IX = actual size of buffer to save, less than or equal to <IObufsize>.
;         DE = pointer to start of external buffer
;
; Register changed after return:
;    A.BC..HL/..IY same
;    .F..DE../IX.. different
;
.LoadBuffer
        push    bc
        push    af
        push    hl

        call    GetFhandle                      ; get IX file handle of open file
        ld      a,FA_EOF
        ld      de,0
        oz      Os_Frm
        jr      z, exit_loadbuffer              ; EOF!

        ld      c,(iy + IObufsize)
        ld      b,(iy + IObufsize+1)            ; Buffer Size
        push    bc
        ld      e,(iy + IObuffer)
        ld      d,(iy + IObuffer+1)             ; Pointer to Buffer Start
        push    de
        ld      hl,0
        oz      Os_Mv                           ; load max. BC bytes of file into buffer
        pop     de
        cp      a                               ; Fc = 0
        pop     hl
        sbc     hl,bc                           ; BC = possible bytes read past EOF (or none)
        push    hl                              ; Fz = 1, indicates EOF!
        pop     ix                              ; actual size of buffer
.exit_loadbuffer
        pop     hl
        pop     bc
        ld      a,b                             ; restore original A
        pop     bc
        ret

; *****************************************************************************
; IX <- (IY + UvHandle)
;
.GetUVhandle
        push    bc
        ld      c,(iy + UvHandle)
        ld      b,(iy + UvHandle+1)             ; get UV Eprom programming handle for this UV Eprom
        push    bc
        pop     ix
        pop     bc
        ret

; *****************************************************************************
; BHL <- (IY + FileEntry)
;
.GetFileEntry
        ld      l,(iy + FileEntry)
        ld      h,(iy + FileEntry+1)
        ld      b,(iy + FileEntry+2)            ; get File Entry pointer
        ret
