; **************************************************************************************************
; File Area functionality.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
;
; ***************************************************************************************************

        module FileEprFindFile

        xdef FileEprFindFile, FileEprFindFileAt

        xref FileEprRequest, FileEprFileStatus, FileEprNextFile
        xref IncBHL, PeekBHL, PeekBHLinc

        include "error.def"


; ***************************************************************************************************
;
; Standard Z88 File Eprom Format, including support for sub File Eprom
; area in application cards (below application banks in first free 64K boundary)
;
; Find active File(name) on Standard File Eprom in slot C.
;
; (wildcard search algorithm implemented by Kirk J. Krauss <kjkrauss@us.ibm.com>,
;  adapted & optimized for Z80 assembler by G. Strube)
;
; IN:
;    C = slot number of File Eprom (Area)
;    DE = pointer to null-terminated filename to be searched for.
;         The filename is excl. device name and must begin with '/'.
;         '*' and '?' wildcards may be used in filename
; OUT:
;    Fc = 0, File Eprom available
;         Fz = 1, File Entry found.
;              BHL = pointer to File Entry in card at slot
;         Fz = 0, No file were found on the File Eprom.
;              BHL = pointer to free byte on File Eprom in slot
;    Fc = 1,
;         A = RC_Onf
;         File Eprom was not found at slot C
;
; Registers changed after return:
;    A..CDE../IXIY same
;    .FB...HL/.... different
;
; ---------------------------------------------------------------------------------------------------
; Design & programming by G. Strube, Dec 1997-Aug 1998, Sep 2004, Nov 2015
; ---------------------------------------------------------------------------------------------------
;
.FileEprFindFile
        push    ix
        push    de
        push    af
        push    bc

        push    de                              ; preserve ptr to filename
        ld      e,c                             ; preserve slot number
        call    FileEprRequest                  ; check for presence of "oz" File Eprom in slot C
        pop     hl
        jr      c,no_eprom
        jr      nz,no_eprom                     ; File Eprom not available in slot...

        ld      a,e
        and     @00000011                       ; slots (0), 1, 2 or 3 possible
        rrca
        rrca                                    ; converted to Slot mask $40, $80 or $C0
        or      b
        sub     c                               ; C = total banks of File Eprom Area
        inc     a
        ld      b,a                             ; B is now bottom bank of File Eprom Area
        ex      de,hl                           ; DE points at local null-terminated filename
        ld      hl, $0000                       ; BHL points at first File Entry

.find_file
        call    PeekBHL                         ; get length of file entry name
        cp      $ff
        jr      z, finished                     ; last File Entry was searched in File Eprom
        cp      $00
        jr      z, finished                     ; pointing at start of ROM header!
        push    bc
        push    hl
        call    IncBHL                          ; BHL = beginning of filename
        call    CompareFilenames                ; found file in File Eprom?
        pop     hl
        pop     bc
        jr      z, file_found                   ; Yes, return ptr. to current File Entry...

        call    FileEprNextFile                 ; get pointer to next File Entry in slot C...
        jr      find_file

.finished
        or      b                               ; Fc = 0, Fz = 0, File not found.
.file_found
        pop     de
        ld      c,e                             ; original C restored
        pop     de
        ld      a,d                             ; original A restored
        pop     de
        pop     ix                              ; original IX restored
        ret
.no_eprom
        scf
        ld      a,RC_ONF
        pop     bc
        pop     bc                              ; ignore old AF...
        pop     de
        pop     ix                              ; original IX restored
        ret


; ***************************************************************************************************
;
; Standard Z88 File Eprom Format, including support for sub File Eprom
; area in application cards (below application banks in first free 64K boundary)
;
; Find active File(name) on Standard File Eprom, beginning at File Entry in BHL, forwards.
;
; (wildcard search algorithm implemented by Kirk J. Krauss <kjkrauss@us.ibm.com>,
;  adapted & optimized for Z80 assembler by G. Strube)
;
; IN:
;    BHL = pointer to File Entry in card at slot
;    DE = pointer to null-terminated filename to be searched for.
;         The filename is excl. device name and must begin with '/'.
;         '*' and '?' wildcards may be used as part of filename
; OUT:
;    Fc = 0, File Eprom available
;         Fz = 1, File Entry found.
;              BHL = pointer to File Entry in card at slot
;         Fz = 0, No file were found on the File Eprom.
;              BHL = pointer to free byte on File Eprom in slot
;    Fc = 1,
;         A = RC_Onf
;         File Eprom was not found at slot C
;
; Registers changed after return:
;    A..CDE../IXIY same
;    .FB...HL/.... different
;
.FileEprFindFileAt
        push    ix
        push    de
        push    af
        push    bc

        call    FileEprFileStatus
        jr      c, no_eprom                     ; File Entry was not found...
        jr      find_file


; ************************************************************************
;
; Compare file entry name (BHL) with wild card search string (DE)
;
; IN:
;     A = length of file entry name
;   BHL = points at first character of file entry name
;    DE = local pointer to null-terminated wild card search string
;
; OUT:
;    Fz = 1, file entry name found (case independent comparison)
;    Fz = 0, this file entry name did not match
;
; Registers changed after return:
;    ..BCDEHL/..IY same
;    AF....../IX.. different
;
.CompareFilenames
        push    bc
        push    de                                              ; DE = pWildText
        push    hl                                              ; BHL = pFenText (Fen = File Entry Name)

        ld      c,a                                             ; C = length of file entry name in File Area...
        ld      ix,0                                            ; pWildBookmark = 0
        exx
        ld      bc,0                                            ; pFenBookmark = 0 (BHL' = File Entry Name Book Mark pointer)
        exx
.cmp_strings
        ld      a,(de)                                          ; while(1) {
        cp      '*'
        jr      nz,cmp_flnm                                     ;       if (*pWildtxt == '*') {
.skip_wldc      inc     de
                ld      a,(de)                                  ;               while (*(++pWildTxt) == '*'); // "xy" matches "x**y"
                cp      '*'
                jr      z,skip_wldc
                or      a                                       ;               if (!*pWildText) return Fz = 1; // "x" matches "*"
                jr      z,exit_strcmp
                cp      '?'                                     ;               if (*pWildText != '?') {
                jr      z,set_bmrks                             ;                       // Fast-forward to next possible match.
.fast_frwrd     call    CmpFlnmChar                             ;                       while (*pFenText != *pWildText) {
                jr      z,set_bmrks
                        call    IncBHL                          ;                               if (!(*(++pFenText))) {
                        dec     c
                        jr      nz,fast_frwrd
                        inc     c                               ;                                       return Fz = 0; // "x" doesn't match "*y*"
                        jr      exit_strcmp                     ;                               }
.set_bmrks                                                      ;                       }
                                                                ;               }
                        ld      ixh,d                           ;               pWtxtBookmark = pWildText;
                        ld      ixl,e
                        push    hl
                        push    bc
                        exx
                        pop     bc
                        pop     hl                              ;               pFenBookmark = pFenText;
                        exx
                        jr      inctxtptrs                      ;       } else
.cmp_flnm       cp      '?'                                     ;               if (*pFenText != *pWildText && *pWildText != '?') {
                jr      z,inctxtptrs                            ;                       // Got a non-match.  If we've set our bookmarks, back up to one
                call    CmpFlnmChar                             ;                       // or both of them and retry.
                jr      z,inctxtptrs
                        ld      a,ixh                           ;                       if (pWildBookmark) {
                        or      ixl
                        jr      z, nonmatch                     ;                               if (pWildText != pWtxtBookmark) {
                                ld      a,d
                                cp      ixh
                                jr      nz,adjustbm
                                ld      a,e
                                cp      ixl
                                jr      z,chk_eoffen
.adjustbm                               ld      d,ixh           ;                                       pWildText = pWtxtBookmark;
                                        ld      e,ixl
                                        call    CmpFlnmChar     ;                                       if (*pFenText != *pWildText) {
                                        jr      z,nextwldcptr
                                        exx                     ;                                               // Don't go this far back again.
                                        call    IncBHL
                                        dec     c
                                        push    hl
                                        push    bc
                                        exx
                                        pop     bc
                                        pop     hl              ;                                               pFenText = ++pFenBookmark;
                                        jr      cmp_strings     ;                                               continue; // "xy" matches "*y"
.nextwldcptr                                                    ;                                       } else {
                                        inc     de              ;                                              pWildText++;
                                                                ;                                       }
                                                                ;                               }
.chk_eoffen                     inc     c                       ;                               if (*pFenText) {   // (check if length of BHL > 0)
                                dec     c
                                jr      z,nonmatch
                                        call    IncBHL          ;                                       pFenText++;
                                        dec     c               ;                                       continue; // "mississippi" matches "*sip*"
                                        jr      cmp_strings     ;                               }
                                                                ;                       }
.nonmatch               inc     a                               ;               return Fz = 0; // "xy" doesn't match "x"
                        jr      exit_strcmp                     ;       }
.inctxtptrs
                call    IncBHL                                  ;       pFenText++;
                dec     c                                       ;       (also decrease remaining length of File Entry Name)
                inc     de                                      ;       pWildText++;

                inc     c
                dec     c                                       ;       // How do you match a tame text string?
                jr      nz, cmp_strings                         ;       if (!*pFenText) {
.skip_wldc2             ld      a,(de)                          ;               // The tame way: unique up on it!
                        cp      '*'                             ;               while (*pWildText == '*') {
                        jr      nz,chk_eofwldc
                        inc     de                              ;                       pWildText++;     // "x" matches "x*"
                        jr      skip_wldc2                      ;               }
.chk_eofwldc            or      a                               ;               if (!*pWildText)
                                                                ;                       return Fz = 1;   // "x" matches "x" (EOF wildcard string)
                                                                ;               else
                                                                ;                       return Fz = 0;   // "x" doesn't match "xy"
                                                                ;       }
.exit_strcmp                                                    ; }
        pop     hl                                              ; original HL restored
        pop     de                                              ; original DE restored
        pop     bc                                              ; original BC restored
        ret


; ******************************************************************************
; (BHL) = (DE)?
;
; return Fz = 1 if they match, otherwise Fz = 0
;
.CmpFlnmChar
        push    bc
        call    PeekBHL
        call    ToUpper
        ld      c,a
        ld      a,(de)
        call    ToUpper
        cp      c
        pop     bc
        ret


; ******************************************************************************
;
; ToUpper - convert character to upper case if possible.
;
;  IN:    A = ASCII byte
; OUT:    A = converted ASCII byte
;
; Registers changed after return:
;
;    ..BCDEHL/IXIY  same
;    AF....../....  different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, Copyright (C) InterLogic 1995
; ----------------------------------------------------------------------
;
.ToUpper
        cp   '{'                                ; if a <= 'z'  &&
        jr   nc, check_latin1
        cp   'a'                                ; if a >= 'a'
        ret  c
        sub  32                                 ; then a = a - 32
        ret
.check_latin1
        cp   $ff                                ; if a <= $fe  &&
        ret  z
        cp   $e1                                ; if a >= $e1
        ret  c
        sub  32                                 ; then a = a - 32
        ret
