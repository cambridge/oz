; **************************************************************************************************
; OS_Poll and OS_Nq process handle interface. The routines are located in Kernel 0.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module Process1

        include "error.def"
        include "saverst.def"
        include "sysvar.def"
        include "handle.def"
        include "stdio.def"
        include "fileio.def"
        include "director.def"
        include "z80.def"
        include "oz.def"
        include "dor.def"
        include "ozrevision.def"
        include "interrpt.def"

xdef    OSPoll, OSPloz, OSUash
xdef    ClearUnsafeArea
xdef    ClearMemDE_HL
xdef    ChkStkLimits
xdef    Mailbox2Stack
xdef    OSNqProcess

xref    GetAppDOR                               ; [Kernel0]/mth0.asm
xref    PutOSFrame_BC                           ; [Kernel0]/memmisc.asm
xref    CopyMemBHL_DE                           ; [Kernel0]/memmisc.asm
xref    PutOSFrame_BHL                          ; [Kernel0]/memmisc.asm
xref    PutOSFrame_DE                           ; [Kernel0]/memmisc.asm
xref    OSBixS1                                 ; [Kernel0]/stkframe.asm
xref    OSBoxS1                                 ; [Kernel0]/stkframe.asm
xref    CopyMTHApp_Help                         ; [Kernel1]/mth1.asm
xref    UpdateAppStaticHnd                      ; [KErnel0]/filesys.asm


;       ----

;       update application static handle in env process
;IN:    IX=current application, BC= new application static handle
;OUT:   -
;

.OSUash
        jp      UpdateAppStaticHnd
 
;       ----

;       poll for an application
;IN:    IX=current application, 0 for start of list
;OUT:   IX=next application
;       Fc=0 if ok
;       Fc=1, A=error if fail

.OSPoll
        push    ix
        pop     bc
        call    getAppHandle
        ret     c
        push    bc
        pop     ix
        ret
.getAppHandle
        ld      a, c
IF OZ_SLOT1
        or      b
        ld      a, c
        jr      nz,next_app_id
        or      @00100000                       ; first app is in slot 1...
.next_app_id
ENDIF
        inc     a                               ; next application
        call    GetAppDOR                       ; go find it
        ld      c, a
        ld      b, 0
        ld      a, RC_Eof
        ret

;       ----

;       Poll for OZ usage in slot
;IN:    a=slot number (0-3)
;OUT:   Fz=0, if OZ is running in slot, otherwise Fz = 1
;       No registers changed except Fz
;
.OSPloz
        bit     1, a                            ; exclude slot 2 and 3
        jr      nz, no_oz
        or      a       
IF !OZ_SLOT1
        ret     z                               ; A=0, OZ in slot 0
ENDIF
IF OZ_SLOT1
        ret     nz                              ; A=1, OZ in slot 1
ENDIF
.no_oz
        set     Z80F_B_Z, (iy+OSFrame_F)        ; Fz=1, OZ not found
        cp      a                               ; Fc = 0, dont return error from this system call..
        ret

;       ----

;       clear unsafe stack area

.ClearUnsafeArea
        ld      hl, $1FFE                       ; stack top
        ld      de, (pAppUnSafeArea)            ; unsafe area start

;       clear memory from DE (inclusive) to HL (exclusive)

.ClearMemDE_HL
        xor     a                               ; A=0, Fc=0
        sbc     hl, de
        ret     z                               ; HL=DE? exit
        add     hl, de                          ; restore HL
        ld      (de), a                         ; clear first byte
        inc     de
        sbc     hl, de
        ret     z                               ; HL=DE? exit
        ld      b, h                            ; BC=end-start
        ld      c, l
        ld      h, d                            ; HL=start
        ld      l, e
        dec     hl                              ; over zero byte
        ldir                                    ; copy forward, ie. zero fill
        ret

;       ----

;       check that stack pointer and unsafe area are within stack limits
;       freeze if either outside limits

.ChkStkLimits
        ld      hl, $1FFE                       ; upper limit
        ld      bc, $1820                       ; lower limit
        ld      de, (pAppStackPtr)
        call    ChkLimits
        jr      c, chkstk_1
        ld      de, (pAppUnSafeArea)
        call    ChkLimits
        ret     nc
.chkstk_1
        xor     a                               ; freeze
        jr      chkstk_1


.ChkLimits
        push    bc
        push    de                              ; !! can do without pushing DE
        push    hl
        or      a
        sbc     hl, de
        jr      c, chklm_1                      ; HL<DE? Fc=1
        ex      de, hl
        sbc     hl, bc                          ; DE<BC? Fc=1
.chklm_1
        pop     hl
        pop     de
        pop     bc
        ret

;       ----

;       copy mailbox data into low stack area
;       if $1852 contains $aa then $1811 is length of data starting at $1812
;       data length can't exceed 64 bytes

;
.Mailbox2Stack
        ld      hl, (pMailbox)
        ld      bc, (ubMailboxSize)             ; B=ubMailboxBank
        ld      a, c
        or      a                               ; !! 'dec a; cp 64; ld a,0; jr nc'
        jr      z, mb2s_1
        cp      MAILBOXMAXLEN+1
        ld      a, 0
        jr      nc, mb2s_1                      ; >64? exit
        ld      (ubMailboxLength), bc
        ld      de, MailboxData
        call    CopyMemBHL_DE
        ld      a, MAILBOXID                    ; mark as valid

.mb2s_1
        ld      (ubMailBoxID), a                ; store identifier
        ret

;       ----

.OSNqProcess
        cp      $27                             ; range check
        ccf
        ld      a, RC_Unk
        ret     c

        ld      hl, OSNqPrcssTable
        add     hl, bc
        jp      (hl)

.OSNqPrcssTable
        jp      NQAin
        jp      NQKhn
        jp      NQShn
        jp      NQPhn
        jp      NQNhn
        jp      NQWai
        jp      NQCom
        jp      NQIhn
        jp      NQOhn
        jp      NQRhn
        jp      NQVoz
        jp      NQRoz
        jp      NqBtl

;       application information
;IN:    IX=application ID
;OUT:   application data: A=flags, C=key, BHL=name, BDE=DOR
.NQAin
        push    ix
        pop     bc
        ld      a, c                            ; <IX
        call    GetAppDOR
        ld      a, RC_Hand
        jr      c, nqain_x                      ; not found? exit

        ex      de, hl
        call    PutOSFrame_DE                   ; appl DOR
        ld      hl, ADOR_NAME
        add     hl, de
        call    PutOSFrame_BHL                  ; application name

        ex      de, hl                          ; bind in BHL
        call    OSBixS1

        push    ix
        push    hl                              ; IX=HL
        pop     ix
        ld      a, (ix+ADOR_FLAGS)
        ld      (iy+OSFrame_A), a               ; flags1 - good, bad, ugly etc
        ld      a, (ix+ADOR_APPKEY)
        ld      (iy+OSFrame_C), a               ; code letter
        pop     ix

        call    OSBoxS1
        or      a
.nqain_x
        jp      CopyMTHApp_Help                 ; copy app pointers over help pointers

;       read keyboard handle
.NQKhn
        ld      ix, phnd_Khn
        ret

;       read screen handle
.NQShn
        ld      ix, phnd_Shn
        ret

;       read printer indirected handle
.NQPhn
        ld      ix, phnd_Phn
        ret

;       read null handle
.NQNhn
        ld      ix, phnd_Nhn
        ret

;       read comms handle
.NQCom
        ld      ix, phnd_Com
        ret

;       read IN handle
.NQIhn
        ld      ix, phnd_Ihn
        ret

;       read OUT handle
.NQOhn
        ld      ix, phnd_Ohn
        ret

;       read direct printer handle
.NQRhn
        ld      ix, phnd_Rhn
        ret

;       Who am I?
.NQWai
        ld      ix, (uwAppStaticHnd)
        ld      bc, (ubAppDynID)                ; !! just for C
        ld      b, 0
        jp      PutOSFrame_BC


;       Application validates running OZ
;
;IN:    A = OZ version number, eg. $45 for OZV4.5
;OUT:   Fc=0, Application is running on accepted OZ
;OUT:   Fc=1, RC_QUIT is returned (application is obliged to suicide).
;
.NQVoz
        call    GetOZVersion                    ; in C
        ld      a, (iy+OSFrame_A)               ; get application-specific OZ version to compare against
        cp      c
        jr      z,exit_NQVoz
        jr      nc, oz_not_compatible           ; CURRENT_OZ (C) < VERIFY_OZ (A)
.exit_NQVoz
        cp      a
        ret
.oz_not_compatible
        ld      a, 2
        ld      bc, 5<<8|10
        OZ      OS_Blp

        ld      b,0
        ld      hl, ErrWinDef
        oz      GN_Win

        OZ      OS_Pout
        defm    1,"2-C"
        defm    1,"2JC"
        defm    1,"3@",$20+0,$20+2,0

        oz      OS_Pout
        defm    1,"2-C"
        defm    1,"2JC"
        defm    1,"3@",$20+0,$20+2
        defm    "This application requires OZ V",0
        ld      a, (iy+OSFrame_A)               ; get application-specific OZ version to compare against
        push    af
        srl     a
        srl     a
        srl     a
        srl     a
        or      48
        oz      OS_out
        ld      a,'.'
        oz      OS_Out
        pop     af
        and     $0f
        or      48
        oz      OS_Out
        OZ      OS_Pout
        defm    " or newer",0

        OZ      OS_Pout                         ; "Press ESC to Resume"
        defm    1,"3@",$20+0,$20+5
        defm    1,"2C",$FD
        defm    1,"2JC"
        defm    1,"T","PRESS ",1,"R"," ESC ",1,"R"," TO RESUME",1,"T"
        defm    1,"2JN",0

.fatal_err_wait_key
        call    GetEscQKey
        jr      nz,fatal_err_wait_key

        ld      a, RC_Quit                      ; fatal error, request quit
        scf
        ret

.GetEscQKey
        ld      bc, -1                          ; wait infinitely
        ld      a, CL_RIM                       ; raw input
        OZ      OS_Cli
        jr      nc, check_keycode

        cp      RC_Susp
        jr      z, GetEscQKey
        cp      RC_Esc
        jr      nz, GetEscQKey

        OZ      OS_Esc
        cp      a
        ret

.check_keycode
        ld      a, d                            ; extended key? loop
        or      a
        jr      nz, GetEscQKey
        ld      a, e                            ; not 'Q'? loop
        cp      ESC                             ; not ESC? loop
        ret     z
        and     $df                             ; upper()
        cp      'Q'
        jr      nz, GetEscQKey
        ret
.ErrWinDef
        defb @11010000 | 1
        defw $0014
        defw $082D
        defw errbanner
.errbanner
        defm "ERROR", 0

.GetOZVersion
        push    ix
        ld      a,FA_PTR
        ld      ix,-1
        ld      de,0                            ; result in DEBC
        oz      OS_Frm
        pop     ix
        ret

;       Read OZ revision number
;
;IN:    -
;OUT:   DEBC= 32 bits revision number
;
.NQRoz
        ld      de, (OZRevision+2)
        ld      bc, (OZRevision)
        call    PutOSFrame_DE
        jp      PutOSFrame_BC                  ; OZ revision in DEBC

.OZRevision
        defl    ozrev

;       Read battery status
;
;IN:    -
;OUT:   Fz = 0, battery is good
;       Fz = 1, battery is low
;
.NqBtl
        ld      a, (ubIntStatus)
        bit     IST_B_BATLOW, a                 ; this bit is updated by INT
        ret     z
        set     Z80F_B_Z, (iy+OSFrame_F)        ; Fz = 1 on Batt low
        ret


