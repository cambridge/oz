#!/bin/bash

# **************************************************************************************************
# OZ ROM slot 0/1 compilation script for Unix.
# (C) Gunther Strube (gstrube@gmail.com) 2005-2016
#
# This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
#                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
# OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
# or modify it under the terms of the GNU General      0000            0000            ZZZZZ
# Public License as published by the Free Software     0000            0000          ZZZZZ
# Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
# any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
# that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
# without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
# BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
# the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with OZ; see the file
# COPYING. If not, write to:
#                                  Free Software Foundation, Inc.
#                                  59 Temple Place-Suite 330,
#                                  Boston, MA 02111-1307, USA.
#
# ***************************************************************************************************

command -v git >/dev/null 2>&1 || { echo >&2 "Git revision tool not found in PATH"; exit 1; }
command -v mpm >/dev/null 2>&1 || { echo >&2 "Mpm assembler tool not found in PATH"; exit 1; }
command -v z88card >/dev/null 2>&1 || { echo >&2 "Z88Card build tool not found in PATH"; exit 1; }

# return version of Mpm to command line environment.
# validate that MPM is V1.5 or later - only this version or later supports source file dependency
MPM_VERSIONTEXT=`mpm -version`

if test $? -lt 15; then
  echo Mpm version is less than V1.5, OZ compilation aborted.
  echo Mpm displays the following:
  mpm
  exit 1
fi

# return version of Z88Card to command line environment.
# validate that Z88Card is V2.0 or later - only this version or later supports advanced loadmap syntax
Z88CARD_VERSIONTEXT=`z88card -v`

if test $? -lt 20; then
  echo Z88Card version is less than V2.0, OZ compilation aborted.
  echo Z88Card displays the following:
  z88card
  exit 1
fi

if test $# -eq 0; then
  # no slot directive is specified, compile ROM for slot 1 as default
  ozslot=1
elif (test $1 != "0") && (test $1 != "1"); then
  # illegal parameters are ignored and preset with slot 1
  ozslot=1
else
  ozslot=$1
fi

if (test $ozslot == "0"); then
  os_bin="oz.bin"
else
  os_bin="oz.epr"
fi

# output current Git revision as a 32bit hex integer, to be included as part of OZ build
git log --pretty=format:'defc ozrev=$'%h -n 1 > def/ozrevision.def

echo compiling OZ ROM for slot $ozslot

# -------------------------------------------------------------------------------------------------
# delete previous error and warning files...
find . -name "*.err" | xargs rm -f
find . -name "*.wrn" | xargs rm -f
# delete all compile output files, if available..
rm -f oz-*.?? romupdate.cfg

# -------------------------------------------------------------------------------------------------
echo compiling Diary application
cd apps/diary
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling PipeDream application
cd apps/pipedream
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Imp/Export popdown
cd apps/impexport
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Clock, Alarm and Calendar popdowns
cd apps/clock
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi


# -------------------------------------------------------------------------------------------------
echo compiling MTH structures
cd mth
. mth.sh $ozslot
cd ..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling OZ kernel
. kernel.sh $ozslot
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Index popdown / DC System calls
cd dc
. dc.sh $ozslot
cd ..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Floating Point Package
cd fp
. fpp.sh $ozslot
cd ..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling compiling Terminal popdown
cd apps/terminal
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Filer popdown
cd apps/filer
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling GN system calls
cd gn
. gn.sh $ozslot
cd ..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

echo compiling Calculator popdown
cd apps/calculator
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Panel and PrinterEd applications
cd apps/panelprted
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling EazyLink
cd apps/eazylink
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling Flashstore
cd apps/flashstore
./makeapp.sh $ozslot
cd ../..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
echo compiling OZ ROM Header
cd mth
mpm -b -DOZ_SLOT$ozslot -I../def @romhdr.prj
cd ..
if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

if test `find . -name '*.err' | wc -l` != 0; then
  echo Script aborted.
  exit 1
fi

# -------------------------------------------------------------------------------------------------
# ROM was compiled successfully, combine the compiled 16K banks into a complete 512K binary
echo Compiled Z88 ROM, now being combined into $os_bin file.
z88card -DOZ_SLOT$ozslot -Idef -f rom.ldm
