; **************************************************************************************************
; OZ Application/Popdown DOR definitions (top bank of ROM).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module AppDOR

        include "dor.def"
        include "oz.def"
        include "sysapps.def"

xdef    IndexDOR, DiaryDOR, PipeDreamDOR, BasicDOR, CalculatorDOR, AlarmDOR, CalendarDOR
xdef    ClockDOR, FilerDOR, TerminalDOR, PrEdDOR, PanelDOR, ImpExpDOR, EasyLinkDOR, FlashstoreDOR

xref    SysTokenBase
xref    IndexTopics, IndexCommands, IndexHelp
xref    DiaryTopics, DiaryCommands, DiaryHelp
xref    PipeDreamTopics, PipeDreamCommands, PipeDreamHelp
xref    FilerTopics, FilerCommands, FilerHelp
xref    BasicHelp
xref    PrinterEdTopics, PrinterEdCommands, PrinterEdHelp
xref    PanelTopics, PanelCommands, PanelHelp
xref    CalculatorHelp
xref    CalendarHelp
xref    ClockHelp
xref    AlarmTopics, AlarmCommands, AlarmHelp
xref    TerminalTopics, TerminalCommands, TerminalHelp
xref    ImpExpTopics, ImpExpCommands, ImpExpHelp
xref    EazyLinkTopics, EazyLinkCommands, EazyLinkHelp
xref    FlashStoreTopics, FlashStoreCommands, FlashStoreHelp

defc    MTHB = MTH_BNK & $3F                            ; widely used below

.IndexDOR
        dorappl "Index", 'I',                           \ Application Name and Key
                AT_Good|AT_Popd|AT_Ones,,               \ Application type settings
                IDX_ENT,                                \ Application entry point
                0, 0, 0, IDX_BNK,                       \ s0 - s3 bank binding on entry (slot-relative)
                IDX_BAD, IDX_ENV,                       \ Continous RAM pages, Estimate of environment overhead
                IDX_UWS, IDX_SWS,                       \ Application workspace on stack
                [IndexTopics,MTHB],                     \ Pointer to MTH Topics
                [IndexCommands,MTHB],                   \ Pointer to MTH Commands
                [IndexHelp,MTHB],                       \ Pointer to Application Help page
                [SysTokenBase,MTHB],                    \ Pointer to Token base
                MTHB,                                   \ Bank of this Application DOR
                [DiaryDOR,MTHB]                         \ Pointer to next appl. DOR

.DiaryDOR
        dorappl "Diary", 'D',                           \ Name and Key
                AT_Good,,                               \ Appl type mutiple diaries (was AT_Good|AT_Ones)
                DIA_ENT,                                \ Entry
                ,,, DIA_BNK,                            \ s3
                DIA_BAD, DIA_ENV, DIA_UWS, DIA_SWS,     \ Memory info
                [DiaryTopics, MTHB],                    \ MTH
                [DiaryCommands, MTHB],                  \
                [DiaryHelp, MTHB],                      \
                [SysTokenBase, MTHB],                   \
                MTHB,                                   \
                [PipeDreamDOR, MTHB]                    \ Brother

.PipeDreamDOR
        dorappl "Pipedream", 'P',                       \ Name and Key
                AT_Good,,                               \ Type
                PDR_ENT,                                \ Entry
                ,, PDR_BNK, PDR_BNK+1,                  \ s2, s3
                PDR_BAD, PDR_ENV, PDR_UWS, PDR_SWS,     \ Memory info
                [PipeDreamTopics, MTHB],                \ MTH
                [PipeDreamCommands, MTHB],              \
                [PipeDreamHelp, MTHB],                  \
                [SysTokenBase, MTHB],                   \
                MTHB,                                   \
                [BasicDOR, MTHB]                        \ Brother

.BasicDOR
        dorappl "BBC Basic", 'B',                       \ Application Name and Key
                AT_Bad|AT_Draw, AT2_Cl,                 \ Application type settings
                BAS_ENT,                                \ Application entry point
                ,,, BAS_BNK,                            \ s3
                BAS_BAD, BAS_ENV,                       \ Continous RAM pages, Estimate of environment overhead
                BAS_UWS, BAS_SWS,                       \ Application workspace on stack
                ,, [BasicHelp, MTHB],                   \ No topic, no command, Help
                [SysTokenBase, MTHB],                   \ Pointer to Token base
                MTHB,                                   \ Bank of this Application DOR
                [CalculatorDOR, MTHB]                   \ Pointer to next appl. DOR

.CalculatorDOR
        dorappl "Calculator", 'R',                      \ Name and Key
                AT_Good|AT_Popd,,                       \ Type
                CCL_ENT,                                \ Entry
                ,,, CCL_BNK,                            \ s3
                CCL_BAD, CCL_ENV, CCL_UWS, CCL_SWS,     \ Memory info
                ,, [CalculatorHelp, MTHB],              \ No topic, no command, Help
                [SysTokenBase, MTHB], MTHB,             \
                [CalendarDOR, MTHB]                     \ Brother

.CalendarDOR
        dorappl "Calendar", 'C',                        \ Name and Key
                AT_Good|AT_Popd, AT2_Ie,                \ Type
                CAL_ENT,                                \ Entry
                ,,, CAL_BNK,                            \ s3
                CAL_BAD, CAL_ENV, CAL_UWS, CAL_SWS,     \ Memory info
                ,, [CalendarHelp, MTHB],                \ No topic, no command, Help
                [SysTokenBase, MTHB], MTHB,             \
                [ClockDOR, MTHB]                        \ Brother

.ClockDOR
        dorappl "Clock", 'T',                           \ Name and Key
                AT_Good|AT_Popd, AT2_Ie,                \ Type
                CLK_ENT,                                \ Entry
                ,,, CLK_BNK,                            \ s3
                CLK_BAD, CLK_ENV, CLK_UWS, CLK_SWS,     \ Memory info
                ,, [ClockHelp, MTHB],                   \ No topic, no command, Help
                [SysTokenBase, MTHB], MTHB,             \
                [AlarmDOR, MTHB]                        \ Brother

.AlarmDOR
        dorappl "Alarm", 'A',                           \ Name and Key
                AT_Good|AT_Popd, AT2_Ie,                \ Type
                ALM_ENT,                                \ Entry
                ,,, ALM_BNK,                            \ s3
                ALM_BAD, ALM_ENV, ALM_UWS, ALM_SWS,     \ Memory info
                [AlarmTopics, MTHB],                    \ MTH
                [AlarmCommands, MTHB],                  \
                [AlarmHelp, MTHB],                      \
                [SysTokenBase, MTHB], MTHB,             \
                [FilerDor, MTHB]                        \ Brother

.FilerDOR
        dorappl "Filer", 'F',                           \ Name and Key
                AT_Good|AT_Popd|AT_Film,,               \ Type
                FLR_ENT,                                \ Entry
                ,,, FLR_BNK,                            \ s3
                FLR_BAD, FLR_ENV, FLR_UWS, FLR_SWS,     \ Memory info
                [FilerTopics, MTHB],                    \ MTH
                [FilerCommands, MTHB],                  \
                [FilerHelp, MTHB],                      \
                [SysTokenBase, MTHB], MTHB,             \
                [FlashstoreDOR, MTHB]                   \ Brother

.FlashstoreDOR
        dorappl "FlashStore", 'J',                      \ Name and Key
                AT_Ugly|AT_Popd,,                       \ Ugly popdown
                FST_ENT,                                \ Entry
                ,,, FST_BNK,                            \ s3
                FST_BAD, FST_ENV, FST_UWS, FST_SWS,     \ Memory info
                [FlashStoreTopics, MTHB],               \ MTH
                [FlashStoreCommands, MTHB],             \
                [FlashStoreHelp, MTHB],                 \
                [SysTokenBase, MTHB], MTHB,             \
                [PrEdDOR, MTHB]                         \ Brother

.PrEdDOR
        dorappl "PrinterEd", 'E',                       \ Name and Key
                AT_Good|AT_Ones,,                       \ One application
                PED_ENT,                                \ Entry
                ,,, PED_BNK,                            \ s3
                PED_BAD, PED_ENV, PED_UWS, PED_SWS,     \ Memory info
                [PrinterEdTopics, MTHB],                \ MTH
                [PrinterEdCommands, MTHB],              \
                [PrinterEdHelp, MTHB],                  \
                [SysTokenBase, MTHB], MTHB,             \
                [PanelDOR, MTHB],

.PanelDOR
        dorappl "Panel", 'S',                           \ Name and Key
                AT_Good|AT_Popd,,                       \ Type
                PNL_ENT,                                \ Entry
                ,,, PNL_BNK,                            \ s3
                PNL_BAD, PNL_ENV, PNL_UWS, PNL_SWS,     \ Memory info
                [PanelTopics, MTHB],                    \ MTH
                [PanelCommands, MTHB],                  \
                [PanelHelp, MTHB],                      \
                [SysTokenBase, MTHB], MTHB,             \
                [TerminalDOR, MTHB]                     \ Brother

.TerminalDOR
        dorappl "Terminal", 'V',                        \ Name and Key
                AT_Good|AT_Ones|AT_Draw, AT2_Ie,        \ Type
                TRM_ENT,                                \ Entry
                ,,, TRM_BNK,                            \ s3
                TRM_BAD, TRM_ENV, TRM_UWS, TRM_SWS,     \ Memory info
                [TerminalTopics, MTHB],                 \ MTH
                [TerminalCommands, MTHB],               \
                [TerminalHelp, MTHB],                   \
                [SysTokenBase, MTHB], MTHB,             \
                [ImpExpDOR, MTHB]                       \ Brother

.ImpExpDOR
        dorappl "Imp-Export", 'X',                      \ Name and Key
                AT_Ugly | AT_Popd,,                     \ Type
                IXP_ENT,                                \ Entry
                ,,, IXP_BNK,                            \ s3
                IXP_BAD, IXP_ENV, IXP_UWS, IXP_SWS,     \ Memory info
                [ImpExpTopics, MTHB],                   \ MTH
                [ImpExpCommands, MTHB],                 \
                [ImpExpHelp, MTHB],                     \
                [SysTokenBase, MTHB], MTHB,             \
                [EasyLinkDOR, MTHB]                     \ Brother

.EasyLinkDOR
        dorappl "EazyLink", 'L',                        \ Name and Key
                AT_Ugly | AT_Popd,,                     \ Type
                EAZ_ENT,                                \ Entry
                ,,EAZ_BNK,0,                            \ s2
                EAZ_BAD, EAZ_ENV, EAZ_UWS, EAZ_SWS,     \ Memory info
                [EazyLinkTopics, MTHB],                 \ MTH
                [EazyLinkCommands, MTHB],               \
                [EazyLinkHelp, MTHB],                   \
                [SysTokenBase, MTHB], MTHB
