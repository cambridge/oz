; **************************************************************************************************
; OZ Default values static structures.
;
; This table was extracted out of Font bitmap from original V4.0 ROM,
; using FontBitMap tool that auto-generated the token table sources.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

module PrefTbl

xdef    PrefTbl1
xdef    PrefTbl2

; Panel default values
; Format : pointer to default value, data length is next_ptr-ptr

; Default ISO translations refers to Epson FX-80 compatible printers

.PrefTbl1
        defb    defMct - PrefTbl1
        defb    defMct - PrefTbl1               ; timeout
        defb    defRep - PrefTbl1               ; repeat
        defb    defKcl - PrefTbl1               ; click
        defb    defSnd - PrefTbl1               ; sound
        defb    defBad - PrefTbl1               ; bad process size
        defb    defLoc - PrefTbl1               ; country
        defb    defIov - PrefTbl1               ; 07-0F unused
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1
        defb    defIov - PrefTbl1               ; insert/overwrite
        defb    defDat - PrefTbl1               ; date format
        defb    defMap - PrefTbl1               ; pipedream map
        defb    defMsz - PrefTbl1               ; map size
        defb    defDev - PrefTbl1               ; default dir ""
        defb    defDev - PrefTbl1               ; default dev
        defb    defTxb - PrefTbl1               ; tx baud rate
        defb    defRxb - PrefTbl1               ; rx baud rate
        defb    defXon - PrefTbl1               ; xon/xoff
        defb    defPar - PrefTbl1               ; parity
        defb    defPtr - PrefTbl1               ; 1a-1f unused
        defb    defPtr - PrefTbl1
        defb    defPtr - PrefTbl1
        defb    defPtr - PrefTbl1
        defb    defPtr - PrefTbl1
        defb    defPtr - PrefTbl1
        defb    defPtr - PrefTbl1               ; printer name
        defb    defAlf - PrefTbl1               ; allow linefeed
        defb    defPon - PrefTbl1               ; printer on
        defb    defPof - PrefTbl1               ; printer off ""
        defb    defPof - PrefTbl1               ; end of page
        defb    defOn1 - PrefTbl1               ; HMI prefix
        defb    defOn1 - PrefTbl1               ; HMI suffix
        defb    defOn1 - PrefTbl1               ; HMI offset
        defb    defOn1 - PrefTbl1               ; underline
        defb    defOff1 - PrefTbl1
        defb    defRes1 - PrefTbl1
        defb    defOn2 - PrefTbl1
        defb    defOn2 - PrefTbl1               ; bold
        defb    defOff2 - PrefTbl1
        defb    defRes2 - PrefTbl1
        defb    defOn4 - PrefTbl1
        defb    defOn4 - PrefTbl1               ; extended
        defb    defOn4 - PrefTbl1
        defb    defOn4 - PrefTbl1
        defb    defOn4 - PrefTbl1
        defb    defOn4 - PrefTbl1               ; italics
        defb    defOff4 - PrefTbl1
        defb    defRes4 - PrefTbl1
        defb    defOn5 - PrefTbl1
        defb    defOn5 - PrefTbl1               ; subscript
        defb    defOff5 - PrefTbl1
        defb    defRes5 - PrefTbl1
        defb    defOn6 - PrefTbl1
        defb    defOn6 - PrefTbl1               ; superscript
        defb    defOff6 - PrefTbl1
        defb    defRes6 - PrefTbl1
        defb    defOn7 - PrefTbl1
        defb    defOn7 - PrefTbl1               ; alt font
        defb    defOff7 - PrefTbl1
        defb    defRes7 - PrefTbl1
        defb    defOn8 - PrefTbl1
        defb    defOn8 - PrefTbl1               ; user defined
        defb    defOff8 - PrefTbl1
        defb    defRes8 - PrefTbl1
        defb    defTr1 - PrefTbl1
        defb    defTr1 - PrefTbl1               ; tr1
        defb    Tr1out - PrefTbl1
        defb    defTr2 - PrefTbl1               ; tr2
        defb    Tr2out - PrefTbl1
        defb    defTr3 - PrefTbl1               ; tr3
        defb    Tr3out - PrefTbl1
        defb    defTr4 - PrefTbl1               ; tr4
        defb    Tr4out - PrefTbl1
        defb    defTr5 - PrefTbl1               ; tr5
        defb    Tr5out - PrefTbl1
        defb    defTr6 - PrefTbl1               ; tr6
        defb    Tr6out - PrefTbl1
        defb    defTr7 - PrefTbl1               ; tr7
        defb    Tr7out - PrefTbl1
        defb    defTr8 - PrefTbl1               ; tr8
        defb    Tr8out - PrefTbl1
        defb    defTr9 - PrefTbl1               ; tr9
        defb    Tr9out - PrefTbl1
        defb    PrefTbl2 - PrefTbl1             ; here for previous pointer length

.defMct         defb    5
.defRep         defb    6
.defKcl         defb    'N'
.defSnd         defb    'Y'
.defBad         defb    40
.defLoc         defb    'U'                     ; default keyboad is UK
.defIov         defb    'I'
.defDat         defb    'E'
.defMap         defb    'Y'
.defMsz         defb    'P'
.defDev         defm    ":RAM.0"
.defTxb         defw    9600
.defRxb         defw    9600
.defXon         defb    'N'         ; default no XON/XOFF
.defPar         defb    'N'         ; default no PARITY
.defPtr         defm    "Epson",0
.defAlf         defb    'Y'
.defPon         defb    27,64,27,82,0
.defPof         defb    12
.defOn1         defb    27,45,1
.defOff1        defb    27,45,0
.defRes1        defb    'Y'
.defOn2         defb    27,69
.defOff2        defb    27,70
.defRes2        defb    'Y'
.defOn4         defb    27,52
.defOff4        defb    27,53
.defRes4        defb    'Y'
.defOn5         defb    27,83,1
.defOff5        defb    27,84
.defRes5        defb    'Y'
.defOn6         defb    27,83,0
.defOff6        defb    27,84
.defRes6        defb    'Y'
.defOn7         defb    15
.defOff7        defb    18
.defRes7        defb    'N'
.defOn8         defb    27,120,1
.defOff8        defb    27,120,0
.defRes8        defb    'N'
.defTr1         defb    161                     ; ¡
.Tr1out         defb    27,82,7,91,27,82,0
.defTr2         defb    163                     ; £
.Tr2out         defb    27,82,3,35,27,82,0
.defTr3         defb    167                     ; §
.Tr3out         defb    27,82,1,93,27,82,0
.defTr4         defb    176                     ; °
.Tr4out         defb    27,82,1,91,27,82,0
.defTr5         defb    191                     ; ¿
.Tr5out         defb    27,82,7,93,27,82,0
.defTr6         defb    196                     ; Ä
.Tr6out         defb    27,82,2,91,27,82,0
.defTr7         defb    197                     ; Å
.Tr7out         defb    27,82,4,93,27,82,0
.defTr8         defb    198                     ; Æ
.Tr8out         defb    27,82,4,91,27,82,0
.defTr9         defb    199                     ; Ç
.Tr9out         defb    27,82,14,92,27,82,0

; ISO translations default values

.PrefTbl2
        defb    defTr10 - PrefTbl2              ; tr10
        defb    Tr10out - PrefTbl2
        defb    defTr11 - PrefTbl2              ; tr11
        defb    Tr11out - PrefTbl2
        defb    defTr12 - PrefTbl2              ; tr12
        defb    Tr12out - PrefTbl2
        defb    defTr13 - PrefTbl2              ; tr13
        defb    Tr13out - PrefTbl2
        defb    defTr14 - PrefTbl2              ; tr14
        defb    Tr14out - PrefTbl2
        defb    defTr15 - PrefTbl2              ; tr15
        defb    Tr15out - PrefTbl2
        defb    defTr16 - PrefTbl2              ; tr16
        defb    Tr16out - PrefTbl2
        defb    defTr17 - PrefTbl2              ; tr17
        defb    Tr17out - PrefTbl2
        defb    defTr18 - PrefTbl2              ; tr18
        defb    Tr18out - PrefTbl2
        defb    defTr19 - PrefTbl2              ; tr19
        defb    Tr19out - PrefTbl2
        defb    defTr20 - PrefTbl2              ; tr20
        defb    Tr20out - PrefTbl2
        defb    defTr21 - PrefTbl2              ; tr21
        defb    Tr21out - PrefTbl2
        defb    defTr22 - PrefTbl2              ; tr22
        defb    Tr22out - PrefTbl2
        defb    defTr23 - PrefTbl2              ; tr23
        defb    Tr23out - PrefTbl2
        defb    defTr24 - PrefTbl2              ; tr24
        defb    Tr24out - PrefTbl2
        defb    defTr25 - PrefTbl2              ; tr25
        defb    Tr25out - PrefTbl2
        defb    defTr26 - PrefTbl2              ; tr26
        defb    Tr26out - PrefTbl2
        defb    defTr27 - PrefTbl2              ; tr27
        defb    Tr27out - PrefTbl2
        defb    defTr28 - PrefTbl2              ; tr28
        defb    Tr28out - PrefTbl2
        defb    defTr29 - PrefTbl2              ; tr29
        defb    Tr29out - PrefTbl2
        defb    defTr30 - PrefTbl2              ; tr30
        defb    Tr30out - PrefTbl2
        defb    defTr31 - PrefTbl2              ; tr31
        defb    Tr31out - PrefTbl2
        defb    defTr32 - PrefTbl2              ; tr32
        defb    Tr32out - PrefTbl2
        defb    defTr33 - PrefTbl2              ; tr33
        defb    Tr33out - PrefTbl2
        defb    defTr34 - PrefTbl2              ; tr34
        defb    Tr34out - PrefTbl2
        defb    defTr35 - PrefTbl2              ; tr35
        defb    Tr35out - PrefTbl2
        defb    defTr36 - PrefTbl2              ; tr36
        defb    Tr36out - PrefTbl2
        defb    defTr37 - PrefTbl2              ; tr37
        defb    Tr37out - PrefTbl2
        defb    defTr38 - PrefTbl2              ; tr38
        defb    Tr38out - PrefTbl2
        defb    PrefTblEnd - PrefTbl2           ; tr39
        defb    PrefTblEnd - PrefTbl2
        defb    PrefTblEnd - PrefTbl2           ; tr40
        defb    PrefTblEnd - PrefTbl2
        defb    PrefTblEnd - PrefTbl2           ; tr41
        defb    PrefTblEnd - PrefTbl2
        defb    PrefTblEnd - PrefTbl2           ; here for previous pointer length

.defTr10        defb    208                     ; Ñ
.Tr10out        defb    27,82,7,92,27,82,0
.defTr11        defb    214                     ; Ö
.Tr11out        defb    27,82,2,92,27,82,0
.defTr12        defb    216                     ; Ø
.Tr12out        defb    27,82,4,92,27,82,0
.defTr13        defb    220                     ; Ü
.Tr13out        defb    27,82,2,93,27,82,0
.defTr14        defb    223                     ; ß
.Tr14out        defb    27,82,2,126,27,82,0
.defTr15        defb    224                     ; à
.Tr15out        defb    27,82,1,64,27,82,0
.defTr16        defb    225                     ; á
.Tr16out        defb    27,82,11,64,27,82,0
.defTr17        defb    226                     ; â
.Tr17out        defb    27,82,14,36,27,82,0
.defTr18        defb    228                     ; ä
.Tr18out        defb    27,82,2,123,27,82,0
.defTr19        defb    229                     ; å
.Tr19out        defb    27,82,4,125,27,82,0
.defTr20        defb    230                     ; æ
.Tr20out        defb    27,82,4,123,27,82,0
.defTr21        defb    231                     ; ç
.Tr21out        defb    27,82,1,92,27,82,0
.defTr22        defb    232                     ; è
.Tr22out        defb    27,82,14,125,27,82,0
.defTr23        defb    233                     ; é
.Tr23out        defb    27,82,14,123,27,82,0
.defTr24        defb    234                     ; ê
.Tr24out        defb    27,82,14,93,27,82,0
.defTr25        defb    235                     ; ë
.Tr25out        defb    27,82,15,123,27,82,0
.defTr26        defb    237                     ; í
.Tr26out        defb    27,82,11,123,27,82,0
.defTr27        defb    238                     ; î
.Tr27out        defb    27,82,14,91,27,82,0
.defTr28        defb    239                     ; ï
.Tr28out        defb    27,82,14,126,27,82,0
.defTr29        defb    241                     ; ñ
.Tr29out        defb    27,82,7,124,27,82,0
.defTr30        defb    242                     ; ò
.Tr30out        defb    27,82,6,124,27,82,0
.defTr31        defb    243                     ; ó
.Tr31out        defb    27,82,11,125,27,82,0
.defTr32        defb    244                     ; ô
.Tr32out        defb    27,82,14,96,27,82,0
.defTr33        defb    246                     ; ö
.Tr33out        defb    27,82,2,124,27,82,0
.defTr34        defb    248                     ; ø
.Tr34out        defb    27,82,4,124,27,82,0
.defTr35        defb    249                     ; ù
.Tr35out        defb    27,82,6,96,27,82,0
.defTr36        defb    250                     ; ú
.Tr36out        defb    27,82,11,126,27,82,0
.defTr37        defb    251                     ; û
.Tr37out        defb    27,82,14,94,27,82,0
.defTr38        defb    252                     ; ü
.Tr38out        defb    27,82,2,125,27,82,0
;.defTr39        defb    
;.Tr39out        defb    
;.defTr40        defb    
;.Tr40out        defb    
;.defTr41        defb    
;.Tr41out        defb    
.PrefTblEnd
