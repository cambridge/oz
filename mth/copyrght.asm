; **************************************************************************************************
; OZ Copyrights strings.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

module Copyrights

        include "oz.def"

xdef    CopyrightMsg
xdef    HallOfFameMsg

.CopyrightMsg
        defm "Th",$82,"C",$ED,"bridg",$82,$AB,"mput",$EF,"Z88 P",$8F,"tabl",$82,"V",$86,$F3,$BC
        defm " ", (OZVERSION>>4)+48, '.', (OZVERSION&$0F)+48
        defm $7F,$7F
        defm $94,"Tr",$85,$FC,$D6,$AB,"n",$C9,"pt",$8E,"Ltd",$A4,"Pro",$AF,$B4,"nic ",$AB,"mput",$86,$8E,"Lt",$B2,"1987,88",$7F
        defm $94,"Op",$86,$91,$C0,"Sys",$AF,"m",$8E,"Ltd",$A4,$AB,"l",$9E,"n Softw",$8C,$82,"Lt",$B2,"1987,88",$7F
        defm "PipeD",$8D,$ED,$B7,"a t",$E2,$E4,"m",$8C,"k ",$89,$AB,"l",$9E,"n Softw",$8C,$82,"Ltd",$7F,$7F
        defm $94,"c",$ED,"bridgez88.ji",$E2,".co",$D7,"2003-2016",$00

.HallOfFameMsg
        defm "Deve",$A5,"p",$86,"s",$7F,$7F
        defm "C",$ED,"bridg",$82,$AB,"mput",$EF,"Lt",$B2,"r",$C7,"a",$EB,$8E,$D0,"t",$AE," V4.0",$7F
        defm "Paul B",$BC,"d",$A4,$DD,"k ",$AB,"l",$9E,"n",$A4,"Eric Entw",$E5,$F0,$8B,$A4,"Ri",$B4,$8C,$B2,"H",$85,$B4,"cliff",$7F
        defm "Ri",$B4,$8C,$B2,"M",$AE,"l",$86,$A4,"Ri",$B4,$8C,$B2,"Rus",$EB,"ll",$A4,"Ti",$D7,"R",$81,"t",$F3,"s",$A4,"M",$91,$88,"ew So",$8C,$A4,"Ji",$D7,"We",$F0,"wood",$7F,$7F
        defm "F",$C6,$88,$EF,"v",$86,$F3,$BC,"s",$7F
        defm "J",$8F,$CD," Oks",$93,$A2,$A4,"T",$E5,$86,"r",$D6,"Peycru",$A4,"G",$D0,$88,$EF,"Strube",$00
