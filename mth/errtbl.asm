; **************************************************************************************************
; OZ Error string table.
;
; This table was extracted out of Font bitmap from original V4.0 ROM,
; using FontBitMap tool that auto-generated the token table sources.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

module ErrTbl

include "error.def"
include "z80.def"

xdef    ErrStr_tbl
xdef    Unknown_err


; Error strings reported by GN_ESP

.ErrStr_tbl
        defb    RC_Esc, 0
        defw    Escape_err

        defb    RC_Time,0
        defw    Timeout_err

        defb    RC_Unk,Z80F_Z
        defw    Internal_err

        defb    RC_Bad,Z80F_Z
        defw    Internal_err

        defb    RC_Ms,Z80F_Z
        defw    Internal_err

        defb    RC_Na,Z80F_Z
        defw    NotApplicable_err

        defb    RC_Room,0
        defw    NoRoom_err

        defb    RC_Hand,Z80F_Z
        defw    BadHandle_err

        defb    RC_Eof,0
        defw    EndOfFile_err

        defb    RC_Flf,0
        defw    FilterFull_err

        defb    RC_Ovf,0
        defw    Overflow_err

        defb    RC_Sntx,0
        defw    BadSyntax_err

        defb    RC_Wrap,0
        defw    Wrap_er

        defb    RC_Push,0
        defw    CannotSatReq_err

        defb    RC_Err,Z80F_Z
        defw    Internal_err

        defb    RC_Type,Z80F_Z
        defw    Unexpected_err

        defb    RC_Pre,0
        defw    NoRoom_err

        defb    RC_Onf,0
        defw    FileNF_err

        defb    RC_Rp,0
        defw    ReadProt_err

        defb    RC_Wp,0
        defw    WriteProt_err

        defb    RC_Use,0
        defw    InUse_err

        defb    RC_Dvz,0
        defw    DivByZero_err

        defb    RC_Tbg,0
        defw    NumTooBig_err

        defb    RC_Nvr,0
        defw    NegRoot_Err

        defb    RC_Lgr,0
        defw    LogRange_err

        defb    RC_Acl,0
        defw    Accuracy_Err

        defb    RC_Exr,0
        defw    RxpRange_err

        defb    RC_Bdn,0
        defw    BadNum_err

        defb    RC_Ivf,0
        defw    BadFName_err

        defb    RC_Fail,0
        defw    CannotSatReq_err

        defb    RC_Exis,0
        defw    AlreadyEx_err

        defb    RC_Ftm,0
        defw    Filetype_err

        defb    RC_Frm,0
        defw    Frame_err

        defb    RC_Susp,0
        defw    Suspended_err

        defb    RC_Draw,0
        defw    Redraw_Err

        defb    RC_VPL,0
        defw    VppLow_Err

        defb    RC_BWR,0
        defw    FlashWrite_Err

        defb    RC_BER,0
        defw    FlashErase_Err

        defb    RC_EPNE,0
        defw    EpromNotErased_Err

        defb    RC_NFE,0
        defw    FlashNotRecgn_Err

        defb    RC_NOZ,0
        defw    FlashOZEra_Err

        defb    RC_BTL,0
        defw    BatLow_Err

        defb    0

.Unknown_err
        defm    "Unknown error",0
.Escape_err
        defm    "Escape",0
.Timeout_err
        defm    "Timeout",0
.NotApplicable_err
        defm    "Not applicable",0
.NoRoom_err
        defm    "No room",0
.BadHandle_err
        defm    "Bad handle",0
.EndOfFile_err
        defm    "End of file",0
.FilterFull_err
        defm    "Filter full",0
.Overflow_err
        defm    "Overflow",0
.BadSyntax_err
        defm    "Bad syntax",0
.Wrap_er
        defm    "Wrap",0
.Internal_err
        defm    "Internal error",0
.Unexpected_err
        defm    "Unexpected type",0
.FileNF_err
        defm    "File not found",0
.ReadProt_err
        defm    "Read protected",0
.WriteProt_err
        defm    "Write protected",0
.InUse_err
        defm    "In use",0
.DivByZero_err
        defm    "Divide by 0",0
.NumTooBig_err
        defm    "Number too big",0
.NegRoot_Err
        defm    "-ve root",0
.LogRange_err
        defm    "Log range",0
.Accuracy_Err
        defm    "Accuracy lost",0
.RxpRange_err
        defm    "Exponent range",0
.BadNum_err
        defm    "Bad number",0
.BadFName_err
        defm    "Bad filename",0
.CannotSatReq_err
        defm    "Cannot satisfy request",0
.AlreadyEx_err
        defm    "Already exists",0
.Filetype_err
        defm    "File type mismatch",0
.Frame_err
        defm    "Frame error",0
.Suspended_err
        defm    "Suspended",0
.Redraw_Err
        defm    "Redraw",0
.VppLow_Err
        defm    "Flash VPP low",0
.EpromNotErased_Err
        defm    "UV Eprom not fully cleared, erase again",0
.FlashWrite_Err
        defm    "Flash/UV Eprom Write error",0
.FlashErase_Err
        defm    "Flash Erase error",0
.FlashNotRecgn_Err
        defm    "Flash chip unknown",0
.FlashOZEra_Err
        defm    "Flash chip contains OZ",0
.BatLow_Err
        defm    "Battery Low",0
