; **************************************************************************************************
; OZ Application/Popdown MTH definitions (top bank of ROM).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005,2006
; (C) Gunther Strube (gstrube@gmail.com), 2005,2006
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module AppMth

        include "stdio.def"

xdef    IndexTopics, IndexCommands, IndexHelp
xdef    DiaryTopics, DiaryCommands, DiaryHelp
xdef    PipeDreamTopics, PipeDreamCommands, PipeDreamHelp
xdef    FilerTopics, FilerCommands, FilerHelp
xdef    BasicHelp
xdef    PrinterEdTopics, PrinterEdCommands, PrinterEdHelp
xdef    PanelTopics, PanelCommands, PanelHelp
xdef    CalculatorHelp
xdef    CalendarHelp
xdef    ClockHelp
xdef    AlarmTopics, AlarmCommands, AlarmHelp
xdef    TerminalTopics, TerminalCommands, TerminalHelp
xdef    ImpExpTopics, ImpExpCommands, ImpExpHelp
xdef    EazyLinkTopics,EazyLinkCommands,EazyLinkHelp
xdef    FlashStoreTopics, FlashStoreCommands, FlashStoreHelp

; ********************************************************************************************************************
; MTH for Index popdown...
;
.IndexTopics
        defb    TPC_MARK
        mthtpc  $d8, TPCF_HELP, (IndexCmdHelp-IndexHelp)                                                ; "Commands"
        mthtpc  ["Info"], TPCF_AN|TPCF_INFO|TPCF_HELP, (Index_help_itpc0-IndexHelp)                     ; "Info" special help pages
        defb    TPC_MARK

.IndexCommands
        defb    CMD_MARK

        ; COMMANDS topic
        mthcmd  $05, IN_ENTER, [$E8."ecu".$AF]                                                          ; "Execute", ENTER
        mthcmd  $06, IN_ESC, $B1                                                                        ; "Escape", ESC
        mthcmd  $08, ["CARD"], ["C".$8C.$B2."D".$F4.$D9.$FB], CMDF_HELP, (IndexCardHelp-IndexHelp)      ; "Card Display", <>CARD
        mthcmd  $0A, ["INFO"], ["Sys".$AF.$D7."Inf".$8F.$CD.$87]                                        ; "System Information", <>INFO
        mthcmd  $01, IN_RGT, $A7, CMDF_COLUMN                                                           ; "Cursor Right"
        mthcmd  $02, IN_LFT, $A8                                                                        ; "Cursor Left"
        mthcmd  $03, IN_UP, $DA                                                                         ; "Cursor Up"
        mthcmd  $04, IN_DWN, $DB                                                                        ; "Cursor Down"
        mthcmd  $07, ["KILL"], [$80."KILL ACTIVITY"], CMDF_COLUMN|CMDF_SAFE                             ; "Kill Activity", <>KILL
        mthcmd  $09, ["PURGE"], [$80."PURGE SYSTEM"], CMDF_SAFE                                         ; "Purge System", <>PURGE
        mthcmd  $0F, ["HARD"], , CMDF_HIDDEN                                                            ; Hard reset (HIDDEN)
        mthcmd  $0B, ["INS"], ["In".$F0.$95."l A".$BF], CMDF_HELP, (IndexInstHelp-IndexHelp)
        mthcmd  $0C, ["RMV"], [$80."REMOVE APPLICATION"], CMDF_SAFE|CMDF_HELP, (IndexRmvHelp-IndexHelp)
        mthcmd  $0D, ["NQ"], , CMDF_HIDDEN                                                              ; Application or Process Enquiry (HIDDEN)
        mthcmd  $0E, ["UPD"], [$80."UPDATE OZ"], CMDF_SAFE|CMDF_HELP, (IndexUpdHelp-IndexHelp)          ; Update OZ

        defb    TPC_SEP                                                                                 ; Command topic separator

        ; INFO topic
        mthcmd  , , ["Descrip",$87], CMDF_HELP, (Index_help_itpc1-IndexHelp)                            ; Description Info topic
        mthcmd  , , ["C",$8C,$B2,$85,"s",$86,$87], CMDF_HELP, (Index_help_itpc2-IndexHelp)              ; Card insertion Info topic
        defb    CMD_MARK

.IndexHelp
defm $7F,"Th",$82,"a",$BF," m",$93,"ag",$86,".",$00

.Index_help_itpc0
defb    0

.Index_help_itpc1
defm $7F,"T",$E5,$8E,"popd",$9D,"n p",$86,"f",$8F,"m",$8E,"a",$BF," ",$CF,$D0,$B4,$85,"g,",$7F
defm $8D,"viv",$95," ",$8F," k",$AE,"l",$C0,$89,"su",$C2,$A2,$E4,$B2,"activ",$FC,"ies,",$7F
defm $85,$F0,$95,$CF,$87," ",$8F," ",$8D,$9B,"v",$95," ",$89,"new a",$BF,"s",$7F
defm $93,$B2,"di",$C2,$CF,"y",$8E,$85,"f",$8F,$CD,$87,$8E,"ab",$81,$84,"sys",$AF,"m.",$00

.Index_help_itpc2
defm $7F,"All ",$85,"s",$86,$87," ",$93,$B2,$8D,$9B,"v",$95," ",$89,"c",$8C,"d",$8E,"mu",$CA,"b",$82,"d",$BC,$82,"fr",$E9,$7F
defm $92,"INDEX",$CE,"Do no",$84,$8D,$9B,"v",$82,$93,$D6,"Ra",$D7,"c",$8C,"d",$A4,$8F," a ROM",$F2,$8C,"d",$7F
defm "w",$E5,$B4,$B7,$A3,"u",$EB,$CE,"A",$F2,$BC,"t",$85,"u",$81,$8E,$9E,"n",$82,"ask",$8E,"f",$8F," a ROM",$7F
defm "c",$8C,$B2,$BB,"b",$82,$8D,"-",$85,"s",$86,$AF,$B2,$85,$BB,$FC,$8E,$8F,$90,$85,$95," ",$FA,$9A
defm "A ",$80,"FAIL",$80," messag",$82,$8D,"qui",$8D,$8E,$92,$CD,$B4,$85,$82,$BB,"b",$82,$8D,$EB,"t.",$00

.IndexCmdHelp
defb    0

.IndexCardHelp
defm $7F,"Sh",$9D,$8E,$F9,"a",$AE,"abl",$82,$8D,"ss",$81,"r",$C9,$8E,$89,$85,"s",$86,$AF,$B2,"c",$8C,"d",$8E,$93,$B2,"f",$8D,$82,$C2,"a",$C9,$7F
defm $F9,"a",$AE,"abl",$82,$BC," ",$93,$D6,"s",$C7,"c",$AF,$B2,"Ra",$D7,$C4,$CE,"F",$C6,$88,$86,$A4,"i",$84,"di",$C2,$CF,"ys",$7F
defm "a g",$E2,"p",$E5,"c",$95," ",$CD,"p ",$89,$92,"u",$EB,$B2,$93,$B2,"f",$8D,$82,"mem",$8F,$D6,$89,$92,"Ra",$D7,"c",$8C,"d",$9A
defm "An ",$A2,"ab",$8B,$B2,"pixel (d",$8C,"k) id",$A2,"tifie",$8E,"u",$EB,$B2,$C2,"ac",$82,"(u",$EB,$B2,"b",$D6,"fi",$8B,"s",$7F
defm $8F," sys",$AF,"m)",$CE,"Voi",$B2,"pixel",$8E,$8C,$82,"f",$8D,$82,$C2,"a",$C9,".",$00
        
.IndexInstHelp
defm $7F,"Ent",$EF,"a `.app' fi",$8B,"n",$ED,$82,$BB,"p",$86,"f",$8F,$D7,$85,$F0,$95,$CF,$87,$9A
defm "T",$E5,$8E,"f",$AE,$82,"c",$93," b",$82,"m",$8C,$B3,$B2,"fro",$D7,$92,$BA,"r",$9A,$7F
defm "All ",$8D,"qui",$8D,$B2,"fi",$8B,$8E,"(ap0...) h",$F9,$82,$BB,"b",$82,$A3,$92,"s",$ED,$82,"di",$8D,"ct",$8F,"y",$9A
defm "The",$D6,"c",$93," b",$82,$86,"a",$EB,$B2,"aft",$EF,$85,$F0,$95,$CF,$87,".",$00

.IndexRmvHelp
defm $7F,"P",$86,"f",$8F,"m",$8E,$D0,$85,$F0,$95,$CF,$87," ",$89,"s",$C7,"c",$AF,$B2,"a",$BF,$9A
defm "Su",$C2,$A2,$E4,$B2,"activ",$FC,"ie",$8E,$89,$88,"i",$8E,"a",$BF," h",$F9,$82,$9E,$7F
defm "b",$82,"k",$AE,$8B,$B2,"bef",$8F,$82,$8D,$9B,"v",$95,$9A,$7F
defm "Multi",$D9,$82,"a",$BF,$8E,$85,$F0,$95,$CF,$87,$B7,$8D,$9B,"ve",$B2,"by",$7F
defm "s",$C7,"ct",$C0,$92,"fir",$CA,"a",$BF," ",$85,$F0,$95,$8B,"d.",$00

.IndexUpdHelp
defm $7F,"P",$86,"f",$8F,"m",$8E,$93," upd",$91,$82,$89,"OZ ",$85," s",$A5,$84,"0 ",$8F," 1 acc",$8F,"d",$85,"g ",$9E,$7F
defm $88,$82,"upd",$91,$82,"packag",$82,"fi",$8B,$8E,"d",$9D,"n",$A5,"a",$E4,$B2,$9E," RAM ",$C4,".",$00

; ********************************************************************************************************************
; MTH for PipeDream application...
;
.PipeDreamTopics
        defb    TPC_MARK
        mthtpc  ["Info"], TPCF_AN|TPCF_INFO|TPCF_HELP, (PipeDream_help_itpc0-PipeDreamHelp)             ; "Info" special help pages
        mthtpc  [$C5."s"]                                                                               ; "Blocks"
        mthtpc  $DC                                                                                     ; "Cursor"
        mthtpc  ["Ed".$FC]                                                                              ; "Edit"
        mthtpc  $FD                                                                                     ; "Files"
        mthtpc  ["L".$FB.$81."t"]                                                                       ; "Layout"
        mthtpc  [$EC."s"]                                                                               ; "Options"
        mthtpc  $FE                                                                                     ; "Print"
        defb    TPC_MARK

.PipeDreamCommands
        defb    CMD_MARK

        ; INFO topic
        mthcmd  , , ["Tr".$90.$ce.$9c."s"], CMDF_HELP, (PipeDream_help_itpc1-PipeDreamHelp)             ; Info topic "Trig. Functions"
        mthcmd  , , ["Log".$ce.$9c."s"], CMDF_HELP, (PipeDream_help_itpc2-PipeDreamHelp)                ; "Log. Functions"
        mthcmd  , , ["Li".$ca.$9c."s"], CMDF_HELP, (PipeDream_help_itpc3-PipeDreamHelp)                 ; "List Functions"
        mthcmd  , , ["S".$a5.$84.$9c."s"], CMDF_HELP, (PipeDream_help_itpc4-PipeDreamHelp)              ; "Slot Functions"
        mthcmd  , , ["In".$af."g".$ef."& D".$91.$82.$9c."s"], CMDF_HELP, (PipeDream_help_itpc5-PipeDreamHelp) ; "Integer & Date Functions"
        mthcmd  , , [$ab."ndi".$87.$95."s"], CMDF_HELP, (PipeDream_help_itpc6-PipeDreamHelp)            ; "Conditionals"
        mthcmd  , , $F6, CMDF_HELP, (PipeDream_help_itpc7-PipeDreamHelp)                                ; "Operators"
        mthcmd  , , [$B8.$F6], CMDF_HELP, (PipeDream_help_itpc8-PipeDreamHelp)                          ; "Logical Operators"
        mthcmd  , , [$c8.$CF.$87.$95." ".$f6], CMDF_HELP, (PipeDream_help_itpc9-PipeDreamHelp)          ; "Relational Operators"
        mthcmd  , , ["W".$AE."dc".$8C."ds"], CMDF_HELP, (PipeDream_help_itpc10-PipeDreamHelp)           ; "Wildcards"
        defb    TPC_SEP                                                                                 ; Command topic separator

        ; BLOCKS topic
        mthcmd  1, ["Z"], [$DD."k ".$C5]                                                                ; Mark Block
        mthcmd  2, ["Q"], ["C".$8b.$8c." ".$dd."k"]                                                     ; Clear Mark
        mthcmd  4, ["BC"], $DE                                                                          ; Copy
        mthcmd  5, ["BM"], ["Move"]                                                                     ; Move
        mthcmd  6, ["BD"], ["D".$c7.$af]                                                                ; Delete
        mthcmd  7, ["BSO"], ["S".$8f."t"]                                                               ; Sort
        mthcmd  3, ["BRE"], [$c8.$d9."ic".$c3]                                                          ; Replicate

        mthcmd  8, ["BSE"], ["Se".$8c.$b4], CMDF_COLUMN                                                 ; Search
        mthcmd  10, ["BRP"], [$c8.$d9."a".$c9]                                                          ; Replace
        mthcmd  9, ["BNM"], [$97.$F7]                                                                   ; Next Match

        mthcmd  11, ["BWC"], ["W".$8F.$B2.$AB.$D0."t"], CMDF_COLUMN                                     ; Word Count
        mthcmd  12, ["BNEW"], ["New"]                                                                   ; New
        mthcmd  13, ["A"], [$c8."c".$95."cul".$c3]                                                      ; Recalculate
        defb    TPC_SEP

        ; CURSOR topic
        mthcmd  16, IN_DRGT, [$DF.$FA]                                                                  ; End of Slot
        mthcmd  15, IN_DLFT, ["St".$8c.$84.$89.$fa]                                                     ; Start of Slot
        mthcmd  32, IN_DUP, ["Top ".$89.$b5]                                                            ; Top of Column
        mthcmd  33, ["COBRA"], , CMDF_HIDDEN                                                            ; COBRA Easter Egg, "Mark Colton" (HIDDEN)
        mthcmd  34, IN_DDWN, ["Bot".$9e.$d7.$89.$b5]                                                    ; Bottom of Column
        mthcmd  20, ["CSP"], [$bd.$be]                                                                  ; Save Position
        mthcmd  21, ["CRP"], [$c8.$F0.$8f.$be]                                                          ; Restore Position
        mthcmd  14, ["CGS"], ["Go ".$bb.$fa]                                                            ; Go to Slot
        mthcmd  19, IN_ENTER, $D2                                                                       ; ENTER

        mthcmd  26, IN_SRGT, [$97.$D3], CMDF_COLUMN                                                     ; Next Word
        mthcmd  27, IN_SLFT, [$98.$D3]                                                                  ; Previous Word
        mthcmd  28, IN_SUP, [$d4.$b9]                                                                   ; Screen Up
        mthcmd  29, IN_SDWN, [$d4.$ac]                                                                  ; Screen Down
        mthcmd  22, IN_RGT, $A7                                                                         ; Cursor Right
        mthcmd  23, IN_LFT, $A8                                                                         ; Cursor Left
        mthcmd  25, IN_UP, $DA                                                                          ; Cursor Up
        mthcmd  24, IN_DWN, $DB                                                                         ; Cursor Down

        mthcmd  31, IN_TAB0, [$97.$B5], CMDF_COLUMN                                                     ; Next Column
        mthcmd  30, IN_STAB, [$98.$B5]                                                                  ; Previous Column
        mthcmd  17, ["CFC"], [$F8.$B5]                                                                  ; First Column
        mthcmd  17, IN_DTAB, , CMDF_HIDDEN                                                              ; First Column (HIDDEN)
        mthcmd  18, ["CLC"], ["La".$ca.$b5]                                                             ; Last Column
        defb    TPC_SEP

        ; EDIT topic
        mthcmd  35, IN_DELX, $e0                                                                        ; Rubout
        mthcmd  36, ["G"], [$96."C".$99]                                                                ; Delete Character
        mthcmd  36, IN_SDEL, , CMDF_HIDDEN                                                              ; Delete Character (HIDDEN)
        mthcmd  37, ["U"], [$cb."C".$99]                                                                ; Insert Character
        mthcmd  38, ["T"], [$96.$d3]                                                                    ; Delete Word
        mthcmd  39, ["D"], [$96.$bb.$df.$fa]                                                            ; Delete to End of Slot
        mthcmd  41, ["Y"], [$96."R".$9d]                                                                ; Delete Row
        mthcmd  41, IN_DDEL, , CMDF_HIDDEN                                                              ; Delete Row (HIDDEN)
        mthcmd  47, ["N"], [$cb."R".$9d]                                                                ; Insert Row
        mthcmd  54, IN_ESC, $B1                                                                         ; ESCAPE

        mthcmd  56, ["V"], $EA, CMDF_COLUMN                                                             ; Insert/Overtype
        mthcmd  53, ["S"], $FF                                                                          ; Swap Case
        mthcmd  55, ["J"], [$97.$ec]                                                                    ; Next Option
        mthcmd  52, ["X"], ["Edi".$84.$E8."p".$8d."s".$F3.$BC]                                          ; Edit Expression
        mthcmd  44, ["K"], [$cb.$c8."f".$86.$a2.$c9]                                                    ; Insert Reference
        mthcmd  57, ["ENT"], ["Numb".$86."<>Text"]                                                      ; Number<>Text
        mthcmd  51, ["R"], ["F".$8F.$CD.$84."P".$8C."ag".$E2."ph"]                                      ; Format Paragraph

        mthcmd  49, ["ESL"], ["S".$d9."i".$84.$a9], CMDF_COLUMN                                         ; Split Line
        mthcmd  43, ["EJL"], ["Jo".$a3.$a9."s"]                                                         ; Join Lines
        mthcmd  24, ["EDRC"], [$96."R".$9d." ".$a3.$b5]                                                 ; Delete Row in Column
        mthcmd  46, ["EIRC"], [$cb."R".$9d." ".$a3.$b5]                                                 ; Insert Row in Column
        mthcmd  42, ["EDC"], [$96.$B5]                                                                  ; Delete Column
        mthcmd  48, ["EIC"], [$cb.$b5]                                                                  ; Insert Column
        mthcmd  50, ["EAC"], ["Ad".$b2.$b5]                                                             ; Add Column
        mthcmd  44, ["EIP"], [$cb."Page"]                                                               ; Insert Page
        defb    TPC_SEP

        ; FILES topic
        mthcmd  58, ["FL"], $E1                                                                         ; Load
        mthcmd  59, ["FS"], [$bd."e"]                                                                   ; Save
        mthcmd  60, ["FC"], ["N".$ed."e"]                                                               ; Name

        mthcmd  61, ["FN"], [$97.$ba], CMDF_COLUMN                                                      ; Next File
        mthcmd  62, ["FP"], [$98.$ba]                                                                   ; Previous File
        mthcmd  63, ["FT"], ["Top ".$ba]                                                                ; Top File
        mthcmd  64, ["FB"], ["Bot".$9e.$d7.$ba]                                                         ; Bottom File
        defb    TPC_SEP

        ; LAYOUT topic
        mthcmd  65, ["W"], ["Wid".$88]                                                                  ; Width
        mthcmd  66, ["H"], ["Se".$84.$dd."g".$85]                                                       ; Set Margin
        mthcmd  67, ["LFR"], [$a6."x R".$9d]                                                            ; Fix Row
        mthcmd  68, ["LFC"], [$a6."x ".$b5]                                                             ; Fix Column
        mthcmd  70, IN_ARGT, [$dd."g".$a3."R".$cc."t"]                                                  ; Margin Right
        mthcmd  69, IN_ALFT, [$dd."g".$a3."Left"]                                                       ; Margin Left

        mthcmd  71, ["LAR"], ["R".$cc.$84.$d1], CMDF_COLUMN                                             ; Right Align
        mthcmd  72, ["LAL"], ["Lef".$84.$d1]                                                            ; Left Align
        mthcmd  73, ["LAC"], ["C".$a2."tr".$82.$d1]                                                     ; Centre Align
        mthcmd  74, ["LLCR"], ["LCR ".$d1]                                                              ; LCR Align
        mthcmd  75, ["LAF"], ["F".$8d.$82.$d1]                                                          ; Free Align

        mthcmd  76, ["LDP"], ["Decim".$95." P".$CF.$C9."s"], CMDF_COLUMN                                ; Decimal Places
        mthcmd  77, ["LSB"], ["S".$90."n B".$e2."c".$B3."ts"]                                           ; Sign Brackets
        mthcmd  78, ["LSM"], ["S".$90."n M".$85."us"]                                                   ; Sign Minus
        mthcmd  79, ["LCL"], ["Lead".$c0."C".$99."s"]                                                   ; Leading Characters
        mthcmd  80, ["LCT"], ["T".$E2.$AE.$C0."C".$99."s"]                                              ; Trailing Characters
        mthcmd  81, ["LDF"], ["Defaul".$84."F".$8f."m".$91]                                             ; Default Format
        defb    TPC_SEP

        ; OPTIONS topic
        mthcmd  82, ["O"], [$ec.$8e."Page"]                                                             ; Options Page
        defb    TPC_SEP

        ; PRINT topic
        mthcmd  83, ["PO"], $FE                                                                         ; Print
        mthcmd  84, ["PM"], ["Microspac".$82."P".$fc.$b4]                                               ; Microspace Pitch

        mthcmd  85, ["PU"], ["Und".$86."l".$85."e"], CMDF_COLUMN                                        ; Underline
        mthcmd  86, ["PB"], ["Bold"]                                                                    ; Bold
        mthcmd  87, ["PX"], [$e8."t".$ce."Sequ".$a2.$c9]                                                ; Ext. Sequence
        mthcmd  88, ["PI"], ["It".$95."ic"]                                                             ; Italic
        mthcmd  89, ["PL"], ["Subscript"]                                                               ; Subscript
        mthcmd  90, ["PR"], ["Sup".$86."script"]                                                        ; Superscript
        mthcmd  91, ["PA"], ["Alt".$ce."F".$bc."t"]                                                     ; Alt. Font
        mthcmd  92, ["PE"], ["Us".$ef."Def".$85."ed"]                                                   ; User Defined

        mthcmd  93, ["PHI"], [$cb."H".$cc."l".$cc."ts"], CMDF_COLUMN                                    ; Insert Highlights
        mthcmd  94, ["PHR"], [$c8.$9B."v".$82."H".$cc."l".$cc."ts"]                                     ; Remove Highlights
        mthcmd  95, ["PHB"], ["H".$cc."l".$cc.$84.$c5]                                                  ; Highlight Block
        defb    CMD_MARK

.PipeDreamHelp
defm $7F,"PipeD",$8D,$ED,$B7,"a",$F2,$E9,"b",$85,"e",$B2,$D3,"-",$F1,"o",$C9,"ss",$8F,$A4,"S",$F1,"eadsheet",$7F
defm $93,$B2,"D",$91,"abas",$82,"a",$BF,".",$00

.PipeDream_help_itpc0
defm $7F,"Describe",$8E,"f",$D0,"c",$87,"s",$A4,"op",$86,$91,$8F,$8E,$93,$B2,"w",$AE,"dc",$8C,"ds.",$00

.PipeDream_help_itpc1
defm "COS(",$9F,")",$A4,"SIN(",$9F,")",$A4,"TAN(",$9F,")",$7F
defm $8D,"t",$C6,"n ",$92,"cos",$85,"e",$A4,"s",$85,$82,$8F," t",$93,"g",$A2,$84,$89,$92,$8C,"gum",$A2,"t",$9A
defm "ACS",$C1,$A4,"ASN",$C1,$A4,"ATN",$C1,$7F
defm $8D,"t",$C6,"n ",$92,$8C,"c",$F2,"os",$85,"e",$A4,"s",$85,$82,$8F," t",$93,"g",$A2,$84,$89,'"',"n",'"',"",$7F
defm "(",$A3,$9F,")",$9A
defm "RAD(",$E4,"g",$8D,"es)",$83,'"',$E4,"g",$8D,"es",'"',"",$F2,$BC,"v",$86,$AF,$B2,$85,$BB,$9F,$9A
defm "DEG(",$9F,")",$83,'"',$9F,'"',$F2,$BC,"v",$86,$AF,$B2,$85,$BB,$E4,"g",$8D,"es.",$00

.PipeDream_help_itpc2
defm $7F,"LN",$C1,$83,$92,"n",$91,$C6,$95," ",$A5,"g",$8C,"i",$88,"m",$A4,"(",$A5,"g e) ",$89,'"',"n",$B0
defm "LOG",$C1,$83,$92,$A5,"g",$8C,"i",$88,$D7,$BB,"bas",$82,"10 ",$89,'"',"n",$B0
defm "EXP",$C1,$83,$92,"c",$BC,$F0,$93,$84,"e",$A4,$BB,$92,"p",$9D,$EF,$89,'"',"n",'"',".",$00

.PipeDream_help_itpc3
defm "CHOOSE(",$A0,")",$83,$93," ",$C7,"m",$A2,$84,"fro",$D7,'"',$A0,'"',$A4,"us",$C0,$88,"e",$7F
defm "fir",$CA,$C7,"m",$A2,$84,"a",$8E,$93," ",$85,$E4,"x ",$85,$BB,$92,$8D,$CD,$85,$C0,$C7,"m",$A2,"ts",$9A
defm "eg",$CE,"CHOOSE(3",$A4,"4",$A4,"5",$A4,"6)",$B7,"6",$9A
defm "COUNT(",$A0,")",$B7,$92,$A1,$89,"n",$BC,"-bl",$93,"k s",$A5,"t",$8E,$A3,'"',$A0,$B0
defm "MAX(",$A0,")",$B7,"i",$AF,$D7,"wi",$88," ",$CD,"x",$CE,$E6,$89,"s",$A5,"t",$8E,$A3,'"',$A0,$B0
defm "MIN(",$A0,")",$B7,"i",$AF,$D7,"wi",$88," m",$85,$CE,$E6,$89,"s",$A5,"t",$8E,$A3,'"',$A0,$B0
defm "SUM(",$A0,")",$B7,$92,$92,"su",$D7,$89,$92,"i",$AF,"m",$8E,$A3,'"',$A0,'"',".",$00

.PipeDream_help_itpc4
defm "COL",$83,$92,"c",$8A," ",$A1,$A3,"w",$E5,$B4," ",$FC,$B7,"ev",$95,"u",$C3,"d",$9A
defm "INDEX(c",$8A,",r",$9D,")",$83,$92,$E6,$89,$92,"s",$A5,"t",$7F
defm $8D,"f",$86,$A2,$C9,$B2,"b",$D6,'"',"c",$8A,'"'," ",$93,$B2,'"',"r",$9D,$B0
defm "LOOKUP(",$B3,"y,",$E7,"1,",$E7,"2)",$83,$92,$E6,$A3,'"',$E7,"2",'"',"",$7F
defm "c",$8F,$8D,$C2,$BC,"d",$C0,$BB,$92,"po",$F3,$87," ",$92,$B3,$D6,"occ",$C6,$8E,$A3,'"',$E7,"1",$B0
defm "W",$AE,"dc",$8C,"d",$8E,$CD,$D6,"b",$82,"u",$EB,$B2,$A3,'"',$B3,"y",'"',"",$CE,"If no m",$91,$B4," = ",'"',"Lookup",$B0
defm "ROW",$83,$92,"r",$9D," ",$A1,$A3,"w",$E5,$B4," ",$FC,$B7,"ev",$95,"u",$C3,"d.",$00

.PipeDream_help_itpc5
defm "ABS",$C1,$83,$92,"absolut",$82,$E6,$89,'"',"n",$B0
defm "DAY(",$F5,")",$83,$92,"da",$D6,$A1,$A3,'"',$F5,$B0
defm "MONTH(",$F5,")",$83,$92,$9B,"n",$88," ",$A1,$A3,'"',$F5,$B0
defm "PI",$83,$92,$E6,"3.141592653",$9A
defm "SGN",$C1,$83,"-1",$A4,"0",$A4,"1 ",$E4,"p",$A2,"d",$C0,$BC," ",$92,"s",$90,"n ",$89,'"',"n",$B0
defm "SQR",$C1,$83,$92,"squ",$8C,$82,"roo",$84,$89,'"',"n",$B0
defm "YEAR(",$F5,")",$83,$92,"ye",$8C," ",$A1,$89,'"',$F5,'"',".",$00

.PipeDream_help_itpc6
defm $7F,"IF(boo",$8B,$93,",",$88,$A2,",el",$EB,")",$9A,$7F
defm "If ",$92,$E6,$89,$92,'"',"boo",$8B,$93,'"',$B7,"TRUE (n",$BC,"-z",$86,"o)",$A4,"IF",$7F
defm $8D,"t",$C6,"n",$8E,'"',$88,$A2,'"',$A4,"o",$88,$86,"w",$F4,$82,"IF",$83,'"',"el",$EB,$B0,$7F
defm "eg",$CE,"IF(5>1",$A4,'"',$9B,$8D,'"',$A4,'"',$8B,"ss",'"',")",$B7,'"',$9B,$8D,'"',".",$00

.PipeDream_help_itpc7
defm $7F,"Add",$AA,"+",$7F,"Subt",$E2,"ct",$AA,"-",$7F
defm "Multi",$D9,"y",$AA,"*",$7F,"Divi",$E4,$AA,"/",$7F
defm "Ra",$F4,$82,$BB,"p",$9D,$86,$AA,"^",$00

.PipeDream_help_itpc8
defm $7F,$7F,$B8,"AND",$AA,"&",$7F
defm $B8,"OR",$AA,"|",$7F
defm $B8,"NOT",$AA,"!",$00

.PipeDream_help_itpc9
defm $7F,"Les",$8E,$88,$93,$AA,"<",$7F
defm "Les",$8E,$88,$93," ",$8F," e",$D5,$AA,"<=",$7F
defm "No",$84,"e",$D5,$AA,"<>",$7F
defm "E",$D5,$AA,"=",$7F
defm "G",$8D,$C3,"r ",$88,$93,$AA,">",$7F
defm "G",$8D,$C3,"r ",$88,$93," ",$8F," e",$D5,$AA,">=",$00

.PipeDream_help_itpc10
defm $7F,$7F,"An",$D6,"s",$85,"gl",$82,"c",$99,$AA,"^?",$7F
defm "An",$D6,$A1,$89,"c",$99,"s",$AA,"^#",$00

; ********************************************************************************************************************
; MTH for Diary application...
;
.DiaryTopics
        defb    TPC_MARK
        mthtpc  [$C5."s"]                                                                               ; Blocks topic
        mthtpc  $DC                                                                                     ; Cursor topic
        mthtpc  ["Ed".$FC]                                                                              ; Edit topic
        mthtpc  $FD                                                                                     ; Files topic
        defb    TPC_MARK

.DiaryCommands
        defb    CMD_MARK

        ; BLOCKS topic
        mthcmd  32, ["Z"], [$dd."k ".$c5]                                                               ; Mark Block
        mthcmd  33, ["Q"], ["C".$8b.$8c." ".$dd."k"]                                                    ; Clear Mark
        mthcmd  34, ["BC"], $DE                                                                         ; Copy
        mthcmd  35, ["BM"], ["Move"]                                                                    ; Move
        mthcmd  36, ["BD"], ["D".$c7.$af]                                                               ; Delete
        mthcmd  37, ["BL"],  ["L".$F4."t/".$fe]                                                         ; List/Print

        mthcmd  38, ["BSE"], ["Se".$8c.$b4], CMDF_COLUMN                                                ; Search
        mthcmd  41, ["BRP"], [$c8.$d9."a".$c9]                                                          ; Replace
        mthcmd  39, ["BNM"], [$97.$F7]                                                                  ; Next Match
        mthcmd  40, ["BPM"], [$98.$F7]                                                                  ; Previous Match
        defb    TPC_SEP

        ; CURSOR topic
        mthcmd  $F5, IN_DRGT, [$DF.$A9]                                                                 ; End of Line
        mthcmd  $F4, IN_DLFT, ["St".$8c.$84.$89.$a9]                                                    ; Start of Line
        mthcmd  48, IN_DUP, [$f8.$a9]                                                                   ; First Line
        mthcmd  47, IN_DDWN, ["La", $ca, $a9]                                                           ; Last Line
        mthcmd  43, ["CSP"], [$bd.$be]                                                                  ; Save position
        mthcmd  44, ["CRP"],[$C8.$F0.$8F.$BE]                                                           ; Restore position
        mthcmd  13, IN_ENTER, $D2                                                                       ; ENTER

        mthcmd  $F9, IN_SRGT, [$97.$d3], CMDF_COLUMN                                                    ; Next Word
        mthcmd  $F8, IN_SLFT, [$98.$d3]                                                                 ; Previous Word
        mthcmd  50, IN_SUP, [$D4.$B9]                                                                   ; Screen Up
        mthcmd  49, IN_SDWN, [$D4.$AC]                                                                  ; Screen Down
        mthcmd  $FD, IN_RGT, $A7                                                                        ; Cursor Right
        mthcmd  $FC, IN_LFT, $A8                                                                        ; Cursor Left
        mthcmd  $2E, IN_UP, $DA                                                                         ; Cursor Up
        mthcmd  $2D, IN_DWN, $DB                                                                        ; Cursor Down

        mthcmd  $2A, IN_TAB0, ["TAB"], CMDF_COLUMN                                                      ; Tab
        mthcmd  $33, ["CT"], ["Tod".$fb]                                                                ; Today
        mthcmd  $39, ["CFAD"], [$F8.$AD]                                                                ; First Active Day
        mthcmd  $38, ["CLAD"], ["La".$ca.$ad]                                                           ; Last Active Day
        mthcmd  $36, IN_ARGT, [$97.$AD]                                                                 ; Next Active Day
        mthcmd  $37, IN_ALFT, [$98.$AD]                                                                 ; Previous Active Day
        mthcmd  $35, IN_AUP, [$98."D".$fb]                                                              ; Previous Day
        mthcmd  $34, IN_ADWN, [$97."D".$fb]                                                             ; Next Day
        defb    TPC_SEP

        ; EDIT topic
        mthcmd  $7F, IN_DELX, $E0                                                                       ; Rubout
        mthcmd  7, ["G"], [$96."C".$99]                                                                     ; Delete Character
        mthcmd  7, IN_SDEL, , CMDF_HIDDEN                                                               ; Delete Character (HIDDEN)
        mthcmd  21, ["U"], [$cb."C".$99]                                                                    ; Insert Character
        mthcmd  20, ["T"], [$96.$d3]                                                                    ; Delete Word
        mthcmd  4, ["D"], [$96.$bb.$df.$a9]                                                             ; Delete to End of Line
        mthcmd  58, ["Y"], [$96.$a9]                                                                    ; Delete Line
        mthcmd  58, IN_DDEL, , CMDF_HIDDEN                                                              ; Delete Line (HIDDEN)
        mthcmd  60, ["N"], [$cb.$a9]                                                                    ; Insert Line

        mthcmd  22, ["V"], $EA, CMDF_COLUMN                                                             ; Insert/Overtype
        mthcmd  19, ["S"], $ff                                                                          ; Swap Case
        mthcmd  62, ["J"], [$97.$ec]                                                                    ; Next Option

        mthcmd  61, ["ESL"], ["S".$d9."i".$84.$a9], CMDF_COLUMN                                         ; Split Line
        mthcmd  59, ["EJL"], ["Jo".$a3.$a9."s"]                                                         ; Join Lines
        defb    TPC_SEP

        ; FILES topic
        mthcmd  63, ["FL"], $E1                                                                         ; Load
        mthcmd  64, ["FS"], [$bd."e"]                                                                   ; Save

        defb    CMD_MARK

.DiaryHelp
defm $7F,"A 'Pag",$82,"a D",$FB,"' di",$8C,"y",$9A
defm $7F,"Th",$82,"Ca",$8B,"nd",$8C," popd",$9D,"n wh",$A2," s",$C7,"c",$AF,$B2,"fro",$D7,$92,"Di",$8C,"y",$7F
defm "c",$93," b",$82,"u",$EB,$B2,$BB,"view activ",$82,"d",$FB,$8E,$93,$B2,"n",$F9,$90,$C3," ",$8C,$81,"nd.",$00

; ********************************************************************************************************************
; MTH for PrinterEd application...
;
.PrinterEdTopics
        defb    TPC_MARK
        mthtpc  $DC                                                                                     ; CURSOR topic
        mthtpc  $FD                                                                                     ; FILES topic
        defb    TPC_MARK

.PrinterEdCommands
        defb    CMD_MARK

        ; CURSOR topic
        mthcmd  $26, ["J"], [$97.$ec]                                                                   ; Next Option
        mthcmd  $0d, IN_ENTER, $d2                                                                      ; ENTER
        mthcmd  $1b, IN_ESC, $b1                                                                        ; ESCAPE

        mthcmd  $fd, IN_RGT, $a7, CMDF_COLUMN                                                           ; Cursor Right
        mthcmd  $fc, IN_LFT, $a8                                                                        ; Cursor Left
        mthcmd  $24, IN_UP, $da                                                                         ; Cursor Up
        mthcmd  $25, IN_DWN, $db                                                                        ; Cursor Down

        mthcmd  $27, IN_SUP, ["Page 1/2"], CMDF_COLUMN                                                  ; Page 1/2
        mthcmd  $28, IN_SDWN, ["Page 2/2"]
        mthcmd  $2e, ["ISO"], ["ISO Tr".$93."s".$CF.$87."s"]
        defb    TPC_SEP

        ; FILES topic
        mthcmd  $29, ["FL"], $e1                                                                        ; Load
        mthcmd  $2a, ["FS"], [$bd."e"]                                                                  ; Save
        mthcmd  $2b, ["FC"], ["N".$ed."e"]                                                              ; Name
        mthcmd  $2c, ["FNEW"], ["New"]                                                                  ; New
        mthcmd  $2d, ["FU"], [$b9."d".$91.$82."Driv".$86]                                               ; Update Driver

        defb    CMD_MARK

.PrinterEdHelp
defm $7F,"Th",$82,$F1,$85,"t",$EF,"driv",$EF,"ed",$FC,$8F,$9A
defm "Th",$82,$E4,"faul",$84,"driv",$EF,"imp",$8B,"m",$A2,"t",$8E,$92,"Eps",$BC," FX ",$F0,$93,"d",$8C,"d",$9A
defm $7F,"Us",$82,$92,$BA," ",$B9,$F5,$F2,$E9,"m",$93,$B2,$BB,$85,$F0,$95,"l ",$92,$EB,"tt",$85,"gs",$7F
defm "aft",$EF,$A5,"ad",$C0,$8F," ed",$FC,$C0,$92,$F1,$85,"t",$EF,$D8,".",$00

; ********************************************************************************************************************
; MTH for Panel popdown...
;

.PanelTopics
        defb    TPC_MARK
        mthtpc  $DC                                                                                     ; CURSOR topic
        mthtpc  $FD                                                                                     ; FILES topic
        defb    TPC_MARK

.PanelCommands
        defb    CMD_MARK

        ; CURSOR topic
        mthcmd  $26, ["J"], [$97.$ec]                                                                   ; Next Option
        mthcmd  $0d, IN_ENTER, $d2                                                                      ; ENTER
        mthcmd  $1b, IN_ESC, $b1                                                                        ; ESCAPE

        mthcmd  $fd, IN_RGT, $a7, CMDF_COLUMN                                                           ; Cursor Right
        mthcmd  $fc, IN_LFT, $a8                                                                        ; Cursor Left
        mthcmd  $24, IN_UP, $da                                                                         ; Cursor Up
        mthcmd  $25, IN_DWN, $db                                                                        ; Cursor Down
        defb    TPC_SEP

        ; FILES topic
        mthcmd  $29, ["FL"], $E1                                                                        ; Load
        mthcmd  $2A, ["FS"], [$BD."e"]                                                                  ; Save
        mthcmd  $2C, ["FNEW"], ["New"]                                                                  ; New

        defb    CMD_MARK

.PanelHelp
defm $7F,"Th",$82,"P",$93,"el",$F2,$BC,"trol",$8E,"sys",$AF,$D7,$F1,"ef",$86,$A2,"c",$82,$EB,"tt",$85,"gs.",$00

; *******************************************************************************************************************
; MTH for Filer popdown...
;
.FilerTopics
        defb    TPC_MARK
        mthtpc  $d8                                                                                     ; "Commands"
        defb    TPC_MARK

.FilerCommands
        defb    CMD_MARK

        ; COMMANDS topic
        mthcmd  $21, ["CF"], ["C".$91.$95."ogu".$82.$fd]                                                ; Catalogue Files
        mthcmd  $25, ["CO"], $DE                                                                        ; Copy
        mthcmd  $26, ["RE"], ["R".$a2.$ed."e"]                                                          ; Rename
        mthcmd  $27, ["ER"], ["E".$e2.$eb]                                                              ; Erase
        mthcmd  $2C, ["EX"], [$e8."ecu".$af]                                                            ; Execute
        mthcmd  $0D, IN_ENTER, [$e3.$f8.$ba]                                                            ; Select First File
        mthcmd  $20, IN_SENT, [$e3.$e8."t".$e2." ".$ba]                                                 ; Select Extra File
        mthcmd  $20, IN_TAB0, , CMDF_HIDDEN                                                             ; TAB, Select Extra File (HIDDEN)
        mthcmd  $28, ["VF"], ["View ".$ba]                                                              ; View File

        mthcmd  $2D, ["CD"], ["C".$8d.$91.$82.$b6], CMDF_COLUMN                                         ; Create Directory
        mthcmd  $29, ["SI"], [$E3.$B6]                                                                  ; Select Directory
        mthcmd  $31, IN_SUP, [$B9." ".$B6]                                                              ; Up Directory
        mthcmd  $32, IN_SDWN, [$AC." ".$B6]                                                             ; Down Directory
        mthcmd  $FD, IN_RGT, $A7                                                                        ; Cursor Right
        mthcmd  $FC, IN_LFT, $A8                                                                        ; Cursor Left
        mthcmd  $FF, IN_UP, $DA                                                                         ; Cursor Up
        mthcmd  $FE, IN_DWN, $DB                                                                        ; Cursor Down

        mthcmd  $22, ["CE"], ["C".$91.$95."ogu".$82.$ee], CMDF_COLUMN                                   ; Catalogue File Card
        mthcmd  $23, ["ES"], [$bd.$82.$bb.$ee]                                                          ; Save to File Card
        mthcmd  $24, ["EF"], ["Fet".$b4." fro".$d7.$ee]                                                 ; Fetch from File Card
        mthcmd  $2A, ["SV"], [$E3.$C4]                                                                  ; Select Device
        mthcmd  $2B, ["SE"], [$E3.$EE]                                                                  ; Select File Card
        mthcmd  $2E, ["EC"], ["C".$8d.$91.$82.$ee]                                                      ; Create File Card
        mthcmd  $2F, ["TC"], ["T".$8d.$82.$de]                                                          ; Tree Copy
        mthcmd  $30, ["NM"], ["N".$ed.$82.$f7]                                                          ; Name Match

        defb    CMD_MARK

.FilerHelp
defm $7F,"Th",$82,$BA," m",$93,"ag",$EF,"a",$BF,$9A
defm $7F,"Wh",$A2," ",$92,$BA,"r",$B7,"s",$C7,"c",$AF,$B2,"fro",$D7,$85,$F3,"d",$82,$93," a",$BF,$7F
defm $BA," ",$E1,$F2,$E9,"m",$93,"d",$A4,"a ",$BA,$F2,$93," b",$82,"m",$8C,$B3,$B2,"wi",$88," ",$92,$D2," ",$B3,"y",$7F
defm "foll",$9D,"e",$B2,"b",$D6,$92,$B1," ",$B3,$D6,$BB,"s",$F9,$82,"typ",$C0,$A3,$92,$BA," n",$ED,"e",$7F
defm $BB,"b",$82,$A5,"a",$E4,$B2,$A3,$92,"a",$BF,".",$00

; ********************************************************************************************************************
; MTH for Alarm popdown...
;
.AlarmTopics
        defb    TPC_MARK
        mthtpc  $d8                                                                                     ; "Commands"
        defb    TPC_MARK

.AlarmCommands
        defb    CMD_MARK

        ; COMMANDS topic
        mthcmd  1, ["SET"], ["Se".$84."Al".$8C."m"]                                                     ; Set Alarm
        mthcmd  2, ["CLR"], ["C".$8B.$8C." Al".$8C."m"]                                                 ; Clear Alarm
        mthcmd  3, ["VW"], ["View Al".$8C."m"]                                                          ; View Alarm
        mthcmd  4, IN_ESC, [$e8.$fc]                                                                    ; Exit

        mthcmd  5, ["FL"], [$E1." Al".$8C."ms"], CMDF_COLUMN                                            ; Load ALarms
        mthcmd  6, ["FS"], [$BD.$82."Al".$8C."ms"]                                                      ; Save Alarms

        defb    CMD_MARK

.AlarmHelp
defm $7F,"Se",$84,"ei",$88,$EF,"s",$85,"gl",$82,$8F," ",$8D,"pe",$91,$C0,$95,$8C,$D7,"ev",$A2,"ts",$7F
defm $BB,"s",$81,"n",$B2,$92,"bell ",$8F," execut",$82,$D8," ",$BB,$CF,$D0,$B4,$7F
defm "sys",$AF,$D7,"a",$BF,$8E,$8F," ",$8D,"s",$81,"r",$C9,"s.",$00

; ********************************************************************************************************************
; MTH for Terminal popdown...
;
.TerminalTopics
        defb    TPC_MARK
        mthtpc  $d8                                                                                     ; "Commands"
        defb    TPC_MARK

.TerminalCommands
        defb    CMD_MARK

        ; COMMANDS topic
        mthcmd   1, IN_SENT, [$e8.$fc]                                                                  ; Exit
        mthcmd   2, IN_DDEL, ["C".$8B.$8C." ".$D4]                                                      ; Clear Screen
        mthcmd   3, IN_DELX, $E0                                                                        ; Rubout
        mthcmd   4, ["H"], ["BS"]                                                                       ; Backspace
        mthcmd   4, IN_SDEL, , CMDF_HIDDEN                                                              ; Backspace, original OZ compatibility (hidden)
        mthcmd   5, ["G"], ["BEL"]                                                                      ; Bell
        mthcmd   6, ["I"], ["HT"]                                                                       ; Horizontal Tab
        mthcmd   7, ["J"], ["LF"]                                                                       ; Line Feed
        mthcmd   8, ["L"], ["FF"]                                                                       ; Form Feed

        mthcmd   9, ["M"], ["CR"], CMDF_COLUMN                                                          ; Carriage Return
        mthcmd  10, ["N"], ["SO"]                                                                       ; Shift Out
        mthcmd  11, ["O"], ["SI"]                                                                       ; Shift In
        mthcmd  12, ["Q"], ["DC1 (XON)"]                                                                ; Resume Transmitting
        mthcmd  13, ["S"], ["DC3 (XOFF)"]                                                               ; Stop Transmitting
        mthcmd  14, ["X"], ["CAN"]                                                                      ; Cancel
        mthcmd  15, ["Z"], ["SUB"]                                                                      ; Idem as Cancel
        mthcmd  16, IN_ESC, ["ESC"]                                                                     ; Escape

        mthcmd  17, IN_RGT, $A7, CMDF_COLUMN                                                            ; Cursor Right
        mthcmd  18, IN_LFT, $A8                                                                         ; Cursor Left
        mthcmd  19, IN_UP, $DA                                                                          ; Cursor Up
        mthcmd  20, IN_DWN, $DB                                                                         ; Cursor Down
        mthcmd  21, IN_SLFT, [$9C." 1"]                                                                 ; Function 1
        mthcmd  22, IN_SRGT, [$9C." 2"]                                                                 ; Function 2
        mthcmd  23, IN_SDWN, [$9C." 3"]                                                                 ; Function 3
        mthcmd  24, IN_SUP, [$9C." 4"]                                                                  ; FUnction 4

        defb    CMD_MARK

.TerminalHelp
defm $7F,"T",$86,"m",$85,$95," ",$95,"l",$9D,$8E,"D",$90,$FC,$95," VT52",$F2,$E9,"m",$D0,"ica",$87,$7F
defm $BB,$93,"o",$88,$EF,"c",$E9,"put",$EF,"us",$C0,$92,"S",$86,"i",$95," P",$8F,"t.",$00

; ********************************************************************************************************************
; MTH for Imp/Export popdown...
;
.ImpExpTopics
        defb    TPC_MARK
        mthtpc  ["Info"], TPCF_AN|TPCF_INFO|TPCF_HELP, (ImpExp_help_itpc0-ImpExpHelp)                   ; INFO topic
        defb    TPC_MARK

.ImpExpCommands
        defb    CMD_MARK

        ; INFO topic
        mthcmd  , , ["XY-".$9B.$E4.$D7."tr".$93."sfer pro".$9E."col"], CMDF_HELP, (ImpExp_help_itpc1-ImpExpHelp)

        defb    CMD_MARK

.ImpExpHelp
defm $7F,"T",$E5,$8E,$BA," Tr",$93,"sf",$EF,"Prog",$E2,$D7,$95,"l",$9D,$8E,$FD,$7F
defm $BB,"b",$82,"sh",$8C,"e",$B2,"wi",$88," o",$88,$EF,"c",$E9,"put",$86,"s",$9A
defm $7F,"S",$86,"i",$95," p",$8F,$84,$C2,"ee",$B2,$93,$B2,$F1,"o",$9E,"col",$B7,$EB,$84,$A3,$92,"P",$93,"el.",$00

.ImpExp_help_itpc0
        defb    0

.ImpExp_help_itpc1
defm $7F,"Ke",$D6,"X ",$93,$B2,"Y s",$C7,"ct",$8E,"X",$9B,$E4,$D7,$8F," Y",$9B,$E4,$D7,$F1,"o",$9E,"col ",$A3,$CD,$A3,"m",$A2,"u",$9A
defm "Ke",$D6,"C ",$9E,"gg",$8B,$8E,"Checksum/CRC",$A4,"K ",$9E,"gg",$8B,$8E,"128b/1kb b",$A5,"ck ",$F3,"ze",$9A
defm "F",$8F," be",$CA,"p",$86,"f",$8F,"m",$93,"c",$82,"us",$82,"38400 BPS ",$93,$B2,"Y",$9B,$E4,"m/CRC-1K",$9A
defm "On ",$8D,$9B,$AF,$A4,"us",$82,"Y",$9B,$E4,"m-1K ",$BB,"s",$A2,$B2,$93,$B2,"Y",$9B,$E4,"m-G ",$BB,$8D,$C9,"ive.",$00

; ********************************************************************************************************************
; MTH for Eazylink popdown
;
.EazyLinkTopics
        defb    TPC_MARK
        mthtpc  $d8                                                                                     ; "Commands"
        defb    TPC_MARK

.EazyLinkCommands
        defb    CMD_MARK

        ; COMMANDS topic
        mthcmd  $80, ["D"], ["Enab".$8B." RS232 logg".$85."g"]                                          ; Enable RS232 logging
        mthcmd  $81, ["Z"], ["D".$F4."ab".$8B." RS232 logg".$85."g"]                                    ; Disable RS232 logging

        defb    CMD_MARK

.EazyLinkHelp
defm $7F,"Fa",$CA,"Cli",$A2,"t/S",$86,"v",$EF,$C8,$9B,"t",$82,$BA," M",$93,"agem",$A2,$84,"betwe",$A2," Z88",$7F
defm $93,$B2,$E4,"sk",$9E,"p ",$85,"clud",$C0,"supp",$8F,$84,"f",$8F," PC-LINK II",$F2,"li",$A2,"ts.",$00

; ********************************************************************************************************************
; MTH for FlashStore popdown...
;
.FlashStoreTopics
        defb    TPC_MARK
        mthtpc  $d8, TPCF_HELP, (fs_cmd_help-FlashStoreHelp)                                                               ; "Commands"
        defb    TPC_MARK

.FlashStoreCommands
        defb    CMD_MARK

        ; COMMANDS topic
        mthcmd  $80, ["SC"], [$E3."C".$8C."d"], CMDF_HELP, (cmd_sc_help - FlashStoreHelp)                                  ; <>SC Select Card
        mthcmd  $81, ["CF"], ["C".$91.$95."ogue C".$8C."d ".$FD], CMDF_HELP, (cmd_cf_help - FlashStoreHelp)                ; <>CF Catalogue Files
        mthcmd  $81, ["CE"], , CMDF_HIDDEN                                                                                 ; <>CE Catalogue Files (hidden)
        mthcmd  $82, ["SV"], [$E3."R".$ED." ".$c4], CMDF_HELP, (cmd_sv_help - FlashStoreHelp)                              ; <>SV Select RAM Device
        mthcmd  $85, ["FE"], ["Era".$EB." ".$BA." from C".$8C."d"], CMDF_HELP|CMDF_SAFE, (cmd_fe_help - FlashStoreHelp)    ; <>FE File Erase
        mthcmd  $85, ["ER"], , CMDF_HIDDEN                                                                                 ; <>ER File Erase (Hidden)
        mthcmd  $83, ["FS"], [$BD.$82.$FD." ".$9E." C".$8C."d"], CMDF_HELP, (cmd_fs_help - FlashStoreHelp)                 ; <>FS File Save
        mthcmd  $84, ["FL"], ["Fet".$B4." ".$BA." from C".$8C."d"], CMDF_HELP, (cmd_fl_help - FlashStoreHelp)              ; <>FL File Load
        mthcmd  $84, ["EF"], , CMDF_HIDDEN                                                                                 ; <>EF File Load (Hidden)

        mthcmd  $86, ["BF"], ["Backup ".$FD." from R".$ED], CMDF_HELP|CMDF_COLUMN, (cmd_bf_help - FlashStoreHelp)          ; <>BF Backup RAM Files
        mthcmd  $87, ["RF"], ["Res".$9E."re ".$FD." ".$9E." R".$ED], CMDF_HELP, (cmd_rf_help - FlashStoreHelp)             ; <>RF Restore RAM Files
        mthcmd  $8A, ["FC"], [$DE." ".$95."l ".$FD." ".$9E." C".$8C."d"], CMDF_HELP, (cmd_fc_help - FlashStoreHelp)        ; <>FC Copy all files to Card
        mthcmd  $88, ["FFA"], ["For".$CD.$84.$BA." A".$8D."a"], CMDF_HELP|CMDF_SAFE, (cmd_ffa_help - FlashStoreHelp)       ; <>FFA Format File Area

        mthcmd  $89, ["TFV"], ["Toggle ".$BA." View"], CMDF_HELP|CMDF_COLUMN, (cmd_tfv_help - FlashStoreHelp)              ; <>TFV Change File View
        mthcmd  IN_ENT, MU_ENT, ["Fet".$B4." ".$BA." a".$84.$DC]                                                           ; ENTER Fetch file at cursor
        mthcmd  IN_DEL, MU_DEL, [$96.$BA." a".$84.$DC]                                                                     ; DEL Delete file at cursor

        defb    CMD_MARK

.FlashStoreHelp
defm $7F,"Ra",$B3,"well F",$CF,"sh C",$8C,"d",$8E,$93,$B2,"Ra",$D7,$BA," m",$93,"ag",$86,".",$00

.fs_cmd_help
        defb    0                        
.cmd_sc_help
defm $7F,$E3,"w",$E5,$B4," ",$EE," ",$BB,"us",$82,"wh",$A2," y",$81," h",$F9,$82,"m",$8F,$82,$88,$93," ",$BC,"e.",$00

.cmd_cf_help
defm $7F,"Li",$F0,$8E,$BA,"n",$ED,"e",$8E,$BC," ",$EE," A",$8D,"a ",$BB,"PipeD",$8D,"a",$D7,$BA,".",$00

.cmd_sv_help
defm $7F,"Ch",$93,"ge",$8E,$E4,"faul",$84,"Ra",$D7,$C4," f",$8F," ",$88,"i",$8E,$EB,"s",$F3,$BC,".",$00

.cmd_fs_help
defm $7F,$BD,"e",$8E,$FD," fro",$D7,"Ra",$D7,$C4," ",$BB,$EE," A",$8D,"a.",$00

.cmd_fl_help
defm $7F,"Fet",$B4,"e",$8E,"a ",$BA," fro",$D7,$EE," A",$8D,"a ",$BB,"Ra",$D7,$C4,".",$00

.cmd_fe_help
defm $7F,$DD,"k",$8E,"a ",$BA," ",$A3,$EE," A",$8D,"a a",$8E,"d",$C7,$AF,"d.",$00

.cmd_bf_help
defm $7F,$BD,"e",$8E,$95,"l ",$FD," fro",$D7,"Ra",$D7,$C4," ",$BB,$EE," A",$8D,"a.",$00

.cmd_rf_help
defm $7F,"Fet",$B4,"e",$8E,$95,"l ",$FD," fro",$D7,$EE," A",$8D,"a ",$BB,"Ra",$D7,$C4,".",$00

.cmd_ffa_help
defm $7F,"F",$8F,"m",$91,$8E,$93,$B2,$86,"a",$EB,$8E,"c",$E9,"p",$8B,"t",$82,$EE," A",$8D,"a.",$00

.cmd_tfv_help
defm $7F,"Ch",$93,"ge",$8E,"betwe",$A2," br",$9D,"s",$C0,$BC,"l",$D6,"s",$F9,"e",$B2,$FD," ",$8F,$7F
defm $95,"so ",$FD," m",$8C,$B3,$B2,"a",$8E,"d",$C7,$AF,"d.",$00

.cmd_fc_help
defm $7F,$DE," s",$F9,"e",$B2,$FD," ",$A3,"c",$C6,$8D,"n",$84,$EE," A",$8D,"a ",$9E,$7F
defm $93,"o",$88,$EF,"f",$CF,"sh",$F2,$8C,$B2,$A3,"a diff",$86,$A2,$84,$FA,".",$00

; *******************************************************************************************************************
.BasicHelp
defm $7F,"A BBC BASIC l",$93,"guage",$9A
defm $7F,"A bu",$AE,"t-",$A3,"Z80 as",$EB,"mbl",$EF,$A2,"ab",$8B,$8E,"c",$E9,"p",$AE,"a",$87," ",$93,$B2,"embedd",$85,"g",$7F
defm $89,$CD,$B4,$85,$82,"cod",$82,$85,$F3,"d",$82,"y",$81,"r ",$F1,"og",$E2,"ms.",$00

; *******************************************************************************************************************
.CalculatorHelp
defm $7F,"A ",$F3,"m",$D9,$82,"poc",$B3,$84,"c",$95,"cul",$91,$8F," wi",$88," s",$E9,$82,"u",$EB,"ful",$7F
defm "Imp",$86,"i",$95," ",$BB,"Metric",$F2,$BC,"v",$86,$F3,$BC," f",$D0,"c",$87,"s.",$00

; *******************************************************************************************************************
.CalendarHelp
defm $7F,"A ",$9B,"n",$88,"l",$D6,"view",$F2,"a",$8B,"nd",$8C,$9A
defm $7F," To m",$93,"u",$95,"l",$D6,"jump ",$BB,"a ",$F5,$A4,$F1,"es",$8E,$92,$D2," ",$B3,"y",$9A
defm "Us",$82,$92,$DC," ",$B3,"y",$8E,$9E,"ge",$88,$EF,"wi",$88," ",$92,"s",$E5,"f",$84,$93,$B2,"dia",$9B,"n",$B2,$B3,"ys",$7F
defm $BB,"n",$F9,$90,$C3," ",$88,"r",$81,"gh d",$FB,"s",$A4,$9B,"n",$88,$8E,$93,$B2,"ye",$8C,"s",$9A
defm "A m",$8C,"k ",$BC," a da",$D6,$85,"dic",$C3,$8E,$93," ",$A2,"try",$B7,$CD,"d",$82,$A3,$92,"Di",$8C,"y.",$00

; *******************************************************************************************************************
.ClockHelp
defm $7F,"Di",$C2,$CF,"y",$8E,$92,"c",$C6,$8D,"n",$84,"d",$FB,$A4,$F5," ",$93,$B2,"time",$9A
defm $7F,"Se",$84,$92,"c",$C6,$8D,"n",$84,$F5," ",$93,$B2,"tim",$82,"h",$86,$82,$BB,$A2,"s",$C6,$82,$88,"a",$84,$95,$8C,"m",$7F
defm "ev",$A2,"t",$8E,"execut",$82,$BC," tim",$82,$93,$B2,"a",$BF,$8E,"us",$82,"c",$8F,$8D,"c",$84,"d",$91,"a.",$00

; *******************************************************************************************************************

