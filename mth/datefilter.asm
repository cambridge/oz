; **************************************************************************************************
; Date filter table, used by OZ to display localized dates in applications and popdowns.
;
; This table was extracted out of Font bitmap from original V4.0 ROM,
; using FontBitMap tool that auto-generated the token table sources.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

module DateFilter

include "oz.def"
include "filter.def"

xdef    DateFilter_UK
xdef    DateFilter_FR
xdef    DateFilter_DK
xdef    DateFilter_NO
xdef    DateFilter_SE
xdef    DateFilter_FI
xdef    DateFilter_IT
xdef    DateFilter_ES
xdef    DateFilter_DE

org     DAF_ORG

.DateFilter_UK
        defw    end_DateFilter_UK-DateFilter_UK
        defb    FL_ALPHA                        ; left-hand has Alpha
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    7,"Monday",           2,$81
        defm    4,"Mon",              2,$A1
        defm    8,"Tuesday",          2,$82
        defm    4,"Tue",              2,$A2
        defm    10,"Wednesday",       2,$83
        defm    4,"Wed",              2,$A3
        defm    9,"Thursday",         2,$84
        defm    4,"Thu",              2,$A4
        defm    7,"Friday",           2,$85
        defm    4,"Fri",              2,$A5
        defm    9,"Saturday",         2,$86
        defm    4,"Sat",              2,$A6
        defm    7,"Sunday",           2,$87
        defm    4,"Sun",              2,$A7
        defm    3,"st",               2,$88
        defm    3,"nd",               2,$89
        defm    3,"rd",               2,$8A
        defm    3,"th",               2,$8B
        defm    3,"AD",               2,$8C
        defm    3,"BC",               2,$8D
        defm    8,"January",          2,$C1
        defm    4,"Jan",              2,$E1
        defm    9,"February",         2,$C2
        defm    4,"Feb",              2,$E2
        defm    6,"March",            2,$C3
        defm    4,"Mar",              2,$E3
        defm    6,"April",            2,$C4
        defm    4,"Apr",              2,$E4
        defm    4,"May",              2,$C5
        defm    4,"May",              2,$E5
        defm    5,"June",             2,$C6
        defm    4,"Jun",              2,$E6
        defm    5,"July",             2,$C7
        defm    4,"Jul",              2,$E7
        defm    7,"August",           2,$C8
        defm    4,"Aug",              2,$E8
        defm    10,"September",       2,$C9
        defm    4,"Sep",              2,$E9
        defm    8,"October",          2,$CA
        defm    4,"Oct",              2,$EA
        defm    9,"November",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"December",         2,$CC
        defm    4,"Dec",              2,$EC
        defm    6,"Today",            2,$91
        defm    10,"Yesterday",       2,$92
        defm    9,"Tomorrow",         2,$93
.end_DateFilter_UK

.DateFilter_FR
        defw    end_DateFilter_FR-DateFilter_FR
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    6,"Lundi",            2,$81
        defm    4,"Lun",              2,$A1
        defm    6,"Mardi",            2,$82
        defm    4,"Mar",              2,$A2
        defm    9,"Mercredi",         2,$83
        defm    4,"Mer",              2,$A3
        defm    6,"Jeudi",            2,$84
        defm    4,"Jeu",              2,$A4
        defm    9,"Vendredi",         2,$85
        defm    4,"Ven",              2,$A5
        defm    7,"Samedi",           2,$86
        defm    4,"Sam",              2,$A6
        defm    9,"Dimanche",         2,$87
        defm    4,"Dim",              2,$A7
        defm    1,                    2,$88     ; first (not applicable because apply to 1, 11, 21, 31)
        defm    1,                    2,$89
        defm    1,                    2,$8A
        defm    1,                    2,$8B
        defm    5,"avJC",             2,$8C     ; 'AD'
        defm    5,"apJC",             2,$8D     ; 'BC'
        defm    8,"Janvier",          2,$C1
        defm    5,"Janv",             2,$E1
        defm    8,"F",$e9,"vrier",    2,$C2
        defm    4,"F",$e9,"v",        2,$E2
        defm    5,"Mars",             2,$C3
        defm    5,"Mars",             2,$E3
        defm    6,"Avril",            2,$C4
        defm    4,"Avr",              2,$E4
        defm    4,"Mai",              2,$C5
        defm    4,"Mai",              2,$E5
        defm    5,"Juin",             2,$C6
        defm    5,"Juin",             2,$E6
        defm    8,"Juillet",          2,$C7
        defm    5,"Juil",             2,$E7
        defm    5,"Ao",$FB,"t",       2,$C8
        defm    5,"Ao",$FB,"t",       2,$E8
        defm    10,"Septembre",       2,$C9
        defm    5,"Sept",             2,$E9
        defm    8,"Octobre",          2,$CA
        defm    4,"Oct",              2,$EA
        defm    9,"Novembre",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"D",$e9,"cembre",   2,$CC
        defm    4,"D",$e9,"c",        2,$EC
        defm    12,"Aujourd'hui",     2,$91
        defm    5,"Hier",             2,$92
        defm    7,"Demain",           2,$93
.end_DateFilter_FR

.DateFilter_DK
        defw    end_DateFilter_DK-DateFilter_DK
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    7,"Mandag",           2,$81
        defm    4,"Man",              2,$A1
        defm    8,"Tirsdag",          2,$82
        defm    4,"Tir",              2,$A2
        defm    7,"Onsdag",           2,$83
        defm    4,"Ons",              2,$A3
        defm    8,"Torsdag",          2,$84
        defm    4,"Tor",              2,$A4
        defm    7,"Fredag",           2,$85
        defm    4,"Fre",              2,$A5
        defm    7,"L",$f8,"rdag",     2,$86     ; L�rdag (Saturday)
        defm    4,"L",$f8,"r",        2,$A6     ; L�r (Sat)
        defm    7,"S",$f8,"ndag",     2,$87     ; S�ndag (Sunday)
        defm    4,"S",$f8,"n",        2,$A7     ; S�n (Sun)
        defm    2,".",                2,$88     ; (danish don't use 'first', but '.')
        defm    2,".",                2,$89     ; (danish don't use '2nd', but '.')
        defm    2,".",                2,$8A     ; (danish don't use '3rd', but '.')
        defm    2,".",                2,$8B     ; (danish don't use 'th', but '.')
        defm    4,"eKr",              2,$8C     ; (danish version of 'AD')
        defm    4,"fKr",              2,$8D     ; (danish version of 'BC')
        defm    7,"Januar",           2,$C1
        defm    4,"Jan",              2,$E1
        defm    8,"Februar",          2,$C2
        defm    4,"Feb",              2,$E2
        defm    6,"Marts",            2,$C3
        defm    4,"Mar",              2,$E3
        defm    6,"April",            2,$C4
        defm    4,"Apr",              2,$E4
        defm    4,"Maj",              2,$C5
        defm    4,"Maj",              2,$E5
        defm    5,"Juni",             2,$C6
        defm    4,"Jun",              2,$E6
        defm    5,"Juli",             2,$C7
        defm    4,"Jul",              2,$E7
        defm    7,"August",           2,$C8
        defm    4,"Aug",              2,$E8
        defm    10,"September",       2,$C9
        defm    4,"Sep",              2,$E9
        defm    8,"Oktober",          2,$CA
        defm    4,"Okt",              2,$EA
        defm    9,"November",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"December",         2,$CC
        defm    4,"Dec",              2,$EC
        defm    6,"I dag",            2,$91
        defm    6,"I g",$E5,"r",      2,$92
        defm    9,"I morgen",         2,$93
.end_DateFilter_DK

.DateFilter_NO
        defw    end_DateFilter_NO-DateFilter_NO
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    7,"Mandag",           2,$81
        defm    4,"Man",              2,$A1
        defm    8,"Tirsdag",          2,$82
        defm    4,"Tir",              2,$A2
        defm    7,"Onsdag",           2,$83
        defm    4,"Ons",              2,$A3
        defm    8,"Torsdag",          2,$84
        defm    4,"Tor",              2,$A4
        defm    7,"Fredag",           2,$85
        defm    4,"Fre",              2,$A5
        defm    7,"L",$f8,"rdag",     2,$86     ; L�rdag (Saturday)
        defm    4,"L",$f8,"r",        2,$A6     ; L�r (Sat)
        defm    7,"S",$f8,"ndag",     2,$87     ; S�ndag (Sunday)
        defm    4,"S",$f8,"n",        2,$A7     ; S�n (Sun)
        defm    2,".",                2,$88     ; (norwegian don't use 'first', but '.')
        defm    2,".",                2,$89     ; (norwegian don't use '2nd', but '.')
        defm    2,".",                2,$8A     ; (norwegian don't use '3rd', but '.')
        defm    2,".",                2,$8B     ; (norwegian don't use 'th', but '.')
        defm    4,"eKr",              2,$8C     ; (norwegian version of 'AD')
        defm    4,"fKr",              2,$8D     ; (norwegian version of 'BC')
        defm    7,"Januar",           2,$C1
        defm    4,"Jan",              2,$E1
        defm    8,"Februar",          2,$C2
        defm    4,"Feb",              2,$E2
        defm    5,"Mars",             2,$C3
        defm    4,"Mar",              2,$E3
        defm    6,"April",            2,$C4
        defm    4,"Apr",              2,$E4
        defm    4,"Mai",              2,$C5
        defm    4,"Mai",              2,$E5
        defm    5,"Juni",             2,$C6
        defm    4,"Jun",              2,$E6
        defm    5,"Juli",             2,$C7
        defm    4,"Jul",              2,$E7
        defm    7,"August",           2,$C8
        defm    4,"Aug",              2,$E8
        defm    10,"September",       2,$C9
        defm    4,"Sep",              2,$E9
        defm    8,"Oktober",          2,$CA
        defm    4,"Okt",              2,$EA
        defm    9,"November",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"Desember",         2,$CC
        defm    4,"Des",              2,$EC
        defm    6,"I dag",            2,$91
        defm    6,"I g",$E5,"r",      2,$92
        defm    9,"I morgen",         2,$93
.end_DateFilter_NO

.DateFilter_SE
        defw    end_DateFilter_SE-DateFilter_SE
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    7,"M",$E5,"ndag",     2,$81     ; Måndag
        defm    4,"M",$E5,"n",        2,$A1     ; Mån
        defm    7,"Tisdag",           2,$82
        defm    4,"Tis",              2,$A2
        defm    7,"Onsdag",           2,$83
        defm    4,"Ons",              2,$A3
        defm    8,"Torsdag",          2,$84
        defm    4,"Tor",              2,$A4
        defm    7,"Fredag",           2,$85
        defm    4,"Fre",              2,$A5
        defm    7,"L",$f6,"rdag",     2,$86     ; L�rdag (Saturday)
        defm    4,"L",$f6,"r",        2,$A6     ; L�r (Sat)
        defm    7,"S",$f6,"ndag",     2,$87     ; S�ndag (Sunday)
        defm    4,"S",$f6,"n",        2,$A7     ; S�n (Sun)
        defm    2,".",                2,$88     ; (swedish don't use 'first', but '.')
        defm    2,".",                2,$89     ; (swedish don't use '2nd', but '.')
        defm    2,".",                2,$8A     ; (swedish don't use '3rd', but '.')
        defm    2,".",                2,$8B     ; (swedish don't use 'th', but '.')
        defm    4,"eKr",              2,$8C     ; (swedish version of 'AD')
        defm    4,"fKr",              2,$8D     ; (swedish version of 'BC')
        defm    8,"Januari",          2,$C1
        defm    4,"Jan",              2,$E1
        defm    9,"Februari",         2,$C2
        defm    4,"Feb",              2,$E2
        defm    5,"Mars",             2,$C3
        defm    4,"Mar",              2,$E3
        defm    6,"April",            2,$C4
        defm    4,"Apr",              2,$E4
        defm    4,"Maj",              2,$C5
        defm    4,"Maj",              2,$E5
        defm    5,"Juni",             2,$C6
        defm    4,"Jun",              2,$E6
        defm    5,"Juli",             2,$C7
        defm    4,"Jul",              2,$E7
        defm    8,"Augusti",          2,$C8
        defm    4,"Aug",              2,$E8
        defm    10,"September",       2,$C9
        defm    4,"Sep",              2,$E9
        defm    8,"Oktober",          2,$CA
        defm    4,"Okt",              2,$EA
        defm    9,"November",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"December",         2,$CC
        defm    4,"Dec",              2,$EC
        defm    6,"I dag",            2,$91
        defm    6,"I g",$E5,"r",      2,$92
        defm    9,"I morgon",         2,$93
.end_DateFilter_SE

.DateFilter_FI
        defw    end_DateFilter_FI-DateFilter_FI
        defb    FL_ISO+FL_NUM+FL_ALPHA+FL_PUNCT ; left-hand has ISO chars, numeric, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    10,"Maanantai",       2,$81
        defm    3,"Ma",               2,$A1
        defm    8,"Tiistai",          2,$82
        defm    3,"Ti",               2,$A2
        defm    12,"Keskiviikko",     2,$83
        defm    3,"Ke",               2,$A3
        defm    8,"Torstai",          2,$84
        defm    3,"To",               2,$A4
        defm    10,"Perjantai",       2,$85
        defm    3,"Pe",               2,$A5
        defm    9,"Lauantai",         2,$86
        defm    3,"La",               2,$A6
        defm    10,"Sunnuntai",       2,$87
        defm    3,"Su",               2,$A7
        defm    2,".",                2,$88     ; (finnish don't use 'first', but '.')
        defm    2,".",                2,$89     ; (finnish don't use '2nd', but '.')
        defm    2,".",                2,$8A     ; (finnish don't use '3rd', but '.')
        defm    2,".",                2,$8B     ; (finnish don't use 'th', but '.')
        defm    4,"eKr",              2,$8C
        defm    4,"jKr",              2,$8D
        defm    9,"Tammikuu",         2,$C1
        defm    3,"1.",               2,$E1
        defm    9,"Helmikuu",         2,$C2
        defm    3,"2.",               2,$E2
        defm    10,"Maaliskuu",       2,$C3
        defm    3,"3.",               2,$E3
        defm    9,"Huhtikuu",         2,$C4
        defm    3,"4.",               2,$E4
        defm    9,"Toukokuu",         2,$C5
        defm    3,"5.",               2,$E5
        defm    8,"Kes",$e4,"kuu",    2,$C6
        defm    3,"6.",               2,$E6
        defm    9,"Hein",$e4,"kuu",   2,$C7
        defm    3,"7.",               2,$E7
        defm    7,"Elokuu",           2,$C8
        defm    3,"8.",               2,$E8
        defm    8,"Syyskuu",          2,$C9
        defm    3,"9.",               2,$E9
        defm    8,"Lokakuu",          2,$CA
        defm    4,"10.",              2,$EA
        defm    10,"Marraskuu",       2,$CB
        defm    4,"11.",              2,$EB
        defm    9,"Joulukuu",         2,$CC
        defm    4,"12.",              2,$EC
        defm    7,"T",$E4,"n",$E4,$E4,"n",2,$91
        defm    6,"Eilen",            2,$92
        defm    9,"Huomenna",         2,$93
.end_DateFilter_FI

.DateFilter_IT
        defw    end_DateFilter_IT-DateFilter_IT
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    7,"Luned",$ec,        2,$81
        defm    4,"Lun",              2,$A1
        defm    8,"Marted",$ec,       2,$82
        defm    4,"Mar",              2,$A2
        defm    10,"Mercoled",$ec,    2,$83
        defm    4,"Mer",              2,$A3
        defm    8,"Gioved",$ec,       2,$84
        defm    4,"Gio",              2,$A4
        defm    8,"Venerd",$ec,       2,$85
        defm    4,"Ven",              2,$A5
        defm    7,"Sabato",           2,$86
        defm    4,"Sab",              2,$A6
        defm    9,"Domenica",         2,$87
        defm    4,"Dom",              2,$A7
        defm    1,                    2,$88
        defm    1,                    2,$89
        defm    1,                    2,$8A
        defm    1,                    2,$8B
        defm    5,"a.C.",             2,$8C     ; 'AD'
        defm    5,"d.C.",             2,$8D     ; 'BC'
        defm    8,"Gennaio",          2,$C1
        defm    4,"Gen",              2,$E1
        defm    9,"Febbraio",         2,$C2
        defm    4,"Feb",              2,$E2
        defm    6,"Marzo",            2,$C3
        defm    4,"Mar",              2,$E3
        defm    7,"Aprile",           2,$C4
        defm    4,"Apr",              2,$E4
        defm    7,"Maggio",           2,$C5
        defm    4,"Mag",              2,$E5
        defm    7,"Giugno",           2,$C6
        defm    4,"Giu",              2,$E6
        defm    7,"Luglio",           2,$C7
        defm    4,"Lug",              2,$E7
        defm    7,"Agosto",           2,$C8
        defm    4,"Ago",              2,$E8
        defm    10,"Settembre",       2,$C9
        defm    4,"Set",              2,$E9
        defm    8,"Ottobre",          2,$CA
        defm    4,"Ott",              2,$EA
        defm    9,"Novembre",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"Dicembre",         2,$CC
        defm    4,"Dic",              2,$EC
        defm    5,"Oggi",             2,$91
        defm    5,"Ieri",             2,$92
        defm    7,"Domani",           2,$93
.end_DateFilter_IT

.DateFilter_ES
        defw    end_DateFilter_ES-DateFilter_ES
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    6,"lunes",            2,$81
        defm    3,"Lu",               2,$A1
        defm    7,"martes",           2,$82
        defm    3,"Ma",               2,$A2
        defm    10,"mi",$e9,"rcoles", 2,$83
        defm    3,"Mi",               2,$A3
        defm    7,"jueves",           2,$84
        defm    3,"Ju",               2,$A4
        defm    8,"viernes",          2,$85
        defm    3,"Vi",               2,$A5
        defm    7,"s",$e1,"bado",     2,$86
        defm    3,"Sa",               2,$A6
        defm    8,"domingo",          2,$87
        defm    3,"Do",               2,$A7
        defm    1,                    2,$88
        defm    1,                    2,$89
        defm    1,                    2,$8A
        defm    1,                    2,$8B
        defm    3,"dC",               2,$8C
        defm    3,"aC",               2,$8D
        defm    6,"enero",            2,$C1
        defm    4,"Ene",              2,$E1
        defm    8,"febrero",          2,$C2
        defm    4,"Feb",              2,$E2
        defm    6,"marzo",            2,$C3
        defm    4,"Mar",              2,$E3
        defm    6,"abril",            2,$C4
        defm    4,"Abr",              2,$E4
        defm    5,"mayo",             2,$C5
        defm    4,"May",              2,$E5
        defm    6,"junio",            2,$C6
        defm    4,"Jun",              2,$E6
        defm    6,"julio",            2,$C7
        defm    4,"Jul",              2,$E7
        defm    7,"agosto",           2,$C8
        defm    4,"Ago",              2,$E8
        defm    11,"septiembre",      2,$C9
        defm    4,"Sep",              2,$E9
        defm    8,"octubre",          2,$CA
        defm    4,"Oct",              2,$EA
        defm    10,"noviembre",       2,$CB
        defm    4,"Nov",              2,$EB
        defm    10,"diciembre",       2,$CC
        defm    4,"Dic",              2,$EC
        defm    4,"Hoy",              2,$91
        defm    5,"Ayer",             2,$92
        defm    7,"Ma",$F1,"ana",     2,$93
.end_DateFilter_ES

.DateFilter_DE
        defw    end_DateFilter_DE-DateFilter_DE
        defb    FL_ISO+FL_ALPHA+FL_PUNCT        ; left-hand has ISO chars, Alpha & punctuation
        defb    FL_ISO                          ; right-hand has ISO chars
        defm    7,"Montag",           2,$81
        defm    3,"Mo",               2,$A1
        defm    9,"Dienstag",         2,$82
        defm    3,"Di",               2,$A2
        defm    9,"Mittwoch",         2,$83
        defm    3,"Mi",               2,$A3
        defm    11,"Donnerstag",      2,$84
        defm    3,"Do",               2,$A4
        defm    8,"Freitag",          2,$85
        defm    3,"Fr",               2,$A5
        defm    8,"Samstag",          2,$86
        defm    3,"Sa",               2,$A6
        defm    8,"Sonntag",          2,$87
        defm    3,"So",               2,$A7
        defm    2,".",                2,$88     ; (germans don't use 'first', but '.')
        defm    2,".",                2,$89     ; (germans don't use '2nd', but '.')
        defm    2,".",                2,$8A     ; (germans don't use '3rd', but '.')
        defm    2,".",                2,$8B     ; (germans don't use 'th', but '.')
        defm    6,"n.Chr",            2,$8C     ; (germans version of 'AD')
        defm    6,"v.Chr",            2,$8D     ; (germans version of 'BC')
        defm    7,"Januar",           2,$C1
        defm    4,"Jan",              2,$E1
        defm    8,"Februar",          2,$C2
        defm    4,"Feb",              2,$E2
        defm    5,"M",$e4,"rz",       2,$C3
        defm    4,"Mrz",              2,$E3
        defm    6,"April",            2,$C4
        defm    4,"Apr",              2,$E4
        defm    5,"Mai ",             2,$C5
        defm    4,"Mai",              2,$E5
        defm    5,"Juni",             2,$C6
        defm    4,"Jun",              2,$E6
        defm    5,"Juli",             2,$C7
        defm    4,"Jul",              2,$E7
        defm    7,"August",           2,$C8
        defm    4,"Aug",              2,$E8
        defm    10,"September",       2,$C9
        defm    4,"Sep",              2,$E9
        defm    8,"Oktober",          2,$CA
        defm    4,"Okt",              2,$EA
        defm    9,"November",         2,$CB
        defm    4,"Nov",              2,$EB
        defm    9,"Dezember",         2,$CC
        defm    4,"Dez",              2,$EC
        defm    6,"Heute",            2,$91
        defm    8,"Gestern",          2,$92
        defm    7,"Morgen",           2,$93
.end_DateFilter_DE
