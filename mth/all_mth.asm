; OZ MTH sources used in appmth.asm and copyrght.asm
; Full strings for compilation with MTHTOKEN tool
; Copy/paste in asm sources after compilation

defm "The Cambridge Computer Z88 Portable Version"
defm $7F,$7F
defm "Copyright (C) Trinity Concepts Ltd, Protechnic Computers Ltd 1987,88",$7F
defm "Copyright (C) Operating Systems Ltd, Colton Software Ltd 1987,88",$7F
defm "PipeDream is a trademark of Colton Software Ltd",$7F,$7F
defm "Copyright (C) cambridgez88.jira.com 2003-2016"
defm $00
defm "Developers",$7F,$7F
defm "Cambridge Computer Ltd releases until V4.0",$7F
defm "Paul Bond, Tim Routsis, Matthew Soar, Jim Westwood, Chris Whiteley",$7F,$7F
defm "Further versions",$7F
defm "Jorma Oksanen, Thierry Peycru, Gunther Strube",$00

defm $7F,"All insertion and removal of cards must be done from",$7F
defm "the INDEX. Do not remove any Ram card, or a ROM card",$7F
defm "which is in use. A continuous tone asks for a ROM",$7F
defm "card to be re-inserted into its original Slot.",$7F
defm "A ",$01,"TFAIL",$01,"T message requires the machine to be reset.",$00

defm $7F,"Shows available ressources of inserted cards and free space",$7F
defm "available on any selected Ram device. Further, it displays",$7F
defm "a graphical map of the used and free memory of the Ram card.",$7F
defm "An enabled pixel (dark) identifies used space (used by files",$7F
defm "or system). Void pixels are free space.",$00

defm $7F,"Enter a `.app' filename to perform installation.",$7F
defm "This file can be marked from the Filer.",$7F,$7F
defm "All required files (ap0...) have to be in the same directory.",$7F
defm "They can be erased after installation.",$00

defm $7F,"Performs uninstallation of selected application.",$7F
defm "Suspended activities of this application have to",$7F
defm "be killed before removal.",$7F,$7F
defm "Multiple applications installation is removed by",$7F
defm "selecting the first application installed.",$00

defm $7F,"PipeDream is a combined Word-processor, Spreadsheet",$7F
defm "and Database application.",$7F
defm $7F,"Use the HELP key and browse its INFO topic.",$00

defm $7F,"Describes functions, operators and wildcards.",$00

defm "COS(radians), SIN(radians), TAN(radians)",$7F
defm "return the cosine, sine or tangent of the argument.",$7F
defm "ACS(n), ASN(n), ATN(n)",$7F
defm "return the arc cosine, sine or tangent of ",'"',"n",'"',"",$7F
defm "(in radians).",$7F
defm "RAD(degrees) returns ",'"',"degrees",'"'," converted into radians.",$7F
defm "DEG(radians) returns ",'"',"radians",'"'," converted into degrees.",$00
defm $7F,"LN(n) returns the natural logarithm, (log e) of ",'"',"n",'"',".",$7F
defm "LOG(n) returns the logarithm to base 10 of ",'"',"n",'"',".",$7F
defm "EXP(n) returns the constant e, to the power of ",'"',"n",'"',".",$00
defm "CHOOSE(list) returns an element from ",'"',"list",'"',", using the",$7F
defm "first element as an index into the remaining elements.",$7F
defm "eg. CHOOSE(3, 4, 5, 6) is 6.",$7F
defm "COUNT(list) is the number of non-blank slots in ",'"',"list",'"',".",$7F
defm "MAX(list) is item with max. value of slots in ",'"',"list",'"',".",$7F
defm "MIN(list) is item with min. value of slots in ",'"',"list",'"',".",$7F
defm "SUM(list) is the the sum of the items in ",'"',"list",'"',".",$00
defm "COL returns the column number in which it is evaluated.",$7F
defm "INDEX(column,row) returns the value of the slot",$7F
defm "referenced by ",'"',"column",'"'," and ",'"',"row",'"',".",$7F
defm "LOOKUP(key,range1,range2) returns the value in ",'"',"range2",'"',"",$7F
defm "corresponding to the position the key occurs in ",'"',"range1",'"',".",$7F
defm "Wildcards may be used in ",'"',"key",'"',". If no match = ",'"',"Lookup",'"',".",$7F
defm "ROW returns the row number in which it is evaluated.",$00
defm "ABS(n) returns the absolute value of ",'"',"n",'"',".",$7F
defm "DAY(date) returns the day number in ",'"',"date",'"',".",$7F
defm "MONTH(date) returns the month number in ",'"',"date",'"',".",$7F
defm "PI returns the value 3.141592653.",$7F
defm "SGN(n) returns -1, 0, 1 depending on the sign of ",'"',"n",'"',".",$7F
defm "SQR(n) returns the square root of ",'"',"n",'"',".",$7F
defm "YEAR(date) returns the year number of ",'"',"date",'"',".",$00
defm $7F,"IF(boolean,then,else).",$7F,$7F
defm "If the value of the ",'"',"boolean",'"'," is TRUE (non-zero), IF",$7F
defm "returns ",'"',"then",'"',", otherwise IF returns ",'"',"else",'"',".",$7F,$7F
defm "eg. IF(5>1, ",'"',"more",'"',", ",'"',"less",'"',") is ",'"',"more",'"',".",$00
defm $7F,"Add: +",$7F,"Subtract: -",$7F
defm "Multiply: *",$7F,"Divide: /",$7F
defm "Raise to power: ^",$00
defm $7F,$7F,"Logical AND: &",$7F
defm "Logical OR: |",$7F
defm "Logical NOT: !",$00
defm $7F,"Less than: <",$7F
defm "Less than or equal to: <=",$7F
defm "Not equal to: <>",$7F
defm "Equal to: =",$7F
defm "Greater than: >",$7F
defm "Greater than or equal to: >=",$00
defm $7F,$7F,"Any single character: ^?",$7F
defm "Any number of characters: ^#",$00

defm $7F,"A 'Page a Day' diary.",$7F
defm $7F,"The Calendar popdown when selected from the Diary",$7F
defm "can be used to view active days and navigate around.",$00

defm $7F,"The printer driver editor.",$7F
defm "The default driver implements the Epson FX standard.",$7F
defm "Use the File Update command to install the settings",$7F
defm "after loading or editing the printer commands.",$00

defm $7F,"The Panel controls system preference settings.",$00

defm $7F,"The File manager application.",$7F
defm $7F,"When the Filer is selected from inside an application",$7F
defm "File Load command, a File can be marked with the ENTER key",$7F
defm "followed by the ESCAPE key to save typing in the File name",$7F
defm "to be loaded in the application.",$00

defm $7F,"Set either single or repeating alarm events",$7F
defm "to sound the bell or execute commands to launch",$7F
defm "system applications or resources.",$00

defm $7F,"Terminal allows Digital VT52 communication",$7F
defm "to another computer using the Serial Port.",$00

defm $7F,"This File Transfer Program allows Files",$7F
defm "to be shared with other computers.",$7F
defm $7F,"Serial port speed and protocol is set in the Panel.",$00

defm $7F,"Key X and Y selects Xmodem or Ymodem protocol in main menu.",$7F
defm "Key C toggles Checksum/CRC, K toggles 128b/1kb block size.",$7F
defm "For best performance use 38400 BPS and Ymodem/CRC-1K.",$7F
defm "On remote, use Ymodem-1K to send and Ymodem-G to receive.",$00

defm $7F,"Fast Client/Server Remote File Management between Z88",$7F
defm "and desktop including support for PC-LINK II clients.",$00

defm $7F,"Rakewell Flash Cards and Ram File manager.",$00

defm $7F,"Select which File Card to use when you have more than one.",$00

defm $7F,"Lists Filenames on File Card Area to PipeDream File.",$00

defm $7F,"Changes default Ram device for this session.",$00

defm $7F,"Saves Files from Ram device to File Card Area.",$00

defm $7F,"Fetches a File from File Card Area to Ram device.",$00

defm $7F,"Marks a File in File Card Area as deleted.",$00

defm $7F,"Saves all Files from Ram device to File Card Area.",$00

defm $7F,"Fetches all Files from File Card Area to Ram device.",$00

defm $7F,"Formats and erases complete File Card Area.",$00

defm $7F,"Changes between browsing only saved Files or",$7F
defm "also Files marked as deleted.",$00

defm $7F,"Copy saved Files in current File Card Area to",$7F
defm "another flash card in a different Slot.",$00

defm $7F,"A BBC BASIC language.",$7F
defm $7F,"A built-in Z80 assembler enables compilation and embedding",$7F
defm "of machine code inside your programs.",$00

defm $7F,"A simple pocket calculator with some useful",$7F
defm "Imperial to Metric conversion functions.",$00

defm $7F,"A monthly view calendar.",$7F
defm $7F," To manually jump to a date, press the ENTER key.",$7F
defm "Use the Cursor keys together with the shift and diamond keys",$7F
defm "to navigate through days, months and years.",$7F
defm "A mark on a day indicates an entry is made in the Diary.",$00

defm $7F,"Displays the current day, date and time.",$7F
defm $7F,"Set the current date and time here to ensure that alarm",$7F
defm "events execute on time and applications use correct data.",$00
