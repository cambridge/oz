
lstoff

; **************************************************************************************************
; Standard Z88 Operating System Manifests
;
; This file is part of the Z88 operating system, OZ      0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; (C) Jorma Oksanen (jorma.oksanen@aini.fi), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005-2007
; (C) Gunther Strube (gstrube@gmail.com), 2005-2007
;
; ***************************************************************************************************

; *** INTERNAL OPERATING SYSTEM USAGE ONLY ***

; OZ NAMING CONVENTION
;
; Applications
; ------------
; IDX   Index
; DIA   Diary
; PDR   Pipedream
; BAS   BBC Basic
; FLR   Filer
; PNL   Panel
; ALM   Alarm
; CLK   Clock
; CAL   Calendar
; CCL   Calculator
; TRM   Terminal
; PED   PrinterEd
; FST   FlashStore
; EAZ   EazyLink
; IXP   ImpExport
;
; Manifest
; --------
; ORG   Origin
; ENT   Entry point
; BNK   Bank
; DOR   DOR
; MTH   MTH
; HLP   Help
; TPC   Topic
; CMD   Command
; TKN   Token
; BAD   Contigous bad RAM pages
; ENV   Env. overhead
; UWS   Unsafe Workspace
; SWS   Safe Workspace
; OS    OS Call
; GN    GN Call
; DC    DC Call
; FPP   FPP Call
; TBL   Table
; LOC   Local Address
; LR0   LORES0 font
; LR1   LORES1 font
; HR0   HIRES0 font
; HR1   HIRES1 font
; SBF   Screen Base File
; KN0   Kernel 0
; KN1   Kernel 1
; DAF   Date Filter

IF !__OZ_DEF__
define __OZ_DEF__


; version:

defc    OZVERSION               = $47


; sizes:

defc    ROMSIZE                 = 14            ; Romcombiner needs actual bank count of ROM image.
defc   	FILEAREASIZE            = 4*4           ; Required for slot 0. (FlashStore file area from top of ROM (and downwards) is 4 x 64K sectors (16 x 16K)

; rom map for OZ in slot 1:


IF OZ_SLOT1
        defc FPP_BNK            = $72
        defc GN_BNK             = $7B
        defc KMP_BNK            = $7C           ; localisation keymap tables
        defc LR1_BNK            = $7C
        defc HR1_BNK            = $7C
        defc DC_BNK             = $73
        defc LOR_BNK            = $7C           ; LOWRAM code to be installed during reset (located in EazyLink bank)
        defc KN1_BNK            = $7D           ; kernel bank 1 (lower)
        defc KN0_BNK            = $7E           ; kernel bank 0 (upper)
        defc MTH_BNK            = $7F           ; OZ static structures (DOR's, MTH, Font bitmap)
        defc DAF_BNK            = $7C


; rom map for OZ in internal slot:

ELSE
        defc KN0_BNK            = $00           ; kernel bank 0
        defc KN1_BNK            = $01           ; kernel bank 1
        defc FPP_BNK            = $0D           ; Floating Point Package (located in FlashStore popdown bank)
        defc DC_BNK             = $0C
        defc KMP_BNK            = $02           ; localisation keymap tables
        defc LR1_BNK            = $02
        defc HR1_BNK            = $02
        defc LOR_BNK            = $02           ; LOWRAM code to be installed during reset (located in EazyLink bank)
        defc GN_BNK             = $03
        defc MTH_BNK            = $08           ; OZ static structures (DOR's, MTH, Font bitmap)
        defc DAF_BNK            = $02
ENDIF


; kernel, static structures and tables offsets:

defc    KN0_ORG                 = $C000         ; Kernel upper part
defc    KN1_ORG                 = $8000         ; Kernel lower part

defc    LR1_ORG                 = $1000         ; 6 pixels width bitmap font
defc    HR1_ORG                 = $0C00         ; 8 pixels width bitmap font

defc    MTH_ORG                 = $0000         ; MTH static structures start offset in top ROM bank
defc    DAF_ORG                 = $7300         ; Country localized date filter tables in S1
defc    KMP_ORG                 = $0000         ; keymap tables

defc    OS_TBL                  = $FF00         ; OS table
defc    GN_TBL                  = $8400         ; GN table
defc    DC_TBL                  = $8000         ; DC table
defc    FPP_TBL                 = $C000         ; Floating Point Package table
defc    GN_ORG                  = $C000         ; Start of GN System API and CRC32 table

defc    LOR_ORG                 = $0000         ; RST area and hi-speed core
defc    LOR_LOC                 = $C800         ; where LOWRAM code is stored in ROM, copied during reset


; System RAM:

defc    R20_BNK                 = $20           ; main system RAM bank, the LOWRAM
defc    R21_BNK                 = $21           ; secundary system RAM, slot 0 storage first bank


; device DORs:

defc    RO0_BNK                 = $1F           ; ROM.0
defc    RO1_BNK                 = $7F           ; ROM.1
defc    RO2_BNK                 = $BF           ; ROM.2
defc    RO3_BNK                 = $FF           ; ROM.3
defc    ROM_DOR                 = $3FC0

defc    APX_BNK                 = R21_BNK       ; APP.-
defc    APX_DOR                 = $0060

defc    RA0_BNK                 = R21_BNK       ; RAM.0
defc    RA1_BNK                 = $40           ; RAM.1
defc    RA2_BNK                 = $80           ; RAM.2
defc    RA3_BNK                 = $C0           ; RAM.3
defc    RAM_DOR                 = $0040

defc    RAX_BNK                 = R21_BNK       ; RAM.-
defc    RAX_DOR                 = $0020


; OS pushframe:

defvars 0
        OSFrame_IY              ds.w    1       ; $00
        OSFrame_S2              ds.b    1       ; $02
        OSFrame_S3              ds.b    1       ; $03
        OSFrame_OZCall          ds.w    1       ; $04
        OSFrame_F               ds.b    1       ; $06
        OSFrame_A               ds.b    1       ; $07
        OSFrame_C               ds.b    1       ; $08
        OSFrame_B               ds.b    1       ; $09
        OSFrame_E               ds.b    1       ; $0A
        OSFrame_D               ds.b    1       ; $0B
        OSFrame_L               ds.b    1       ; $0C
        OSFrame_H               ds.b    1       ; $0D
        OSFrame__S2             ds.b    1       ; $0E
        OSFrame__S3             ds.b    1       ; $0F
        OSFrame_OZPC            ds.w    1       ; RET address on SP back to address location of the OZ call
enddef

ENDIF

lston
