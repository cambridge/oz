Git repository for Cambridge Z88 Operating System
GPL V2 software
                                        0000000000000000      ZZZZZZZZZZZZZZZZZZZ
                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
                                      0000            0000              ZZZZZ
                                      0000            0000            ZZZZZ
                                      0000            0000          ZZZZZ
                                      0000            0000        ZZZZZ
                                      0000            0000      ZZZZZ
                                      0000            0000    ZZZZZ
                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
                                        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ


------------------------------------------------------------------------------------
Introduction
------------------------------------------------------------------------------------

This Git repository contains all source code for the Cambridge Z88 Operating System.
It is the result of many years of work from a dedicated group of Cambridge Z88
developers, done through classic reverse-engineering, re-implementation and
optimisation. Through this repository and some 3rd party tools you will be able to
compile your ROM and install it on an external memory card on your Cambridge Z88.

The Cambridge Z88, the iPad of the 80's, was launched in September 1987 at the PCW
computer show in London. The company behind it was Cambridge Computer, a spring-off
company of Sinclair Research, founded by Clive Sinclair.

Read more about the project on our wiki: https://cambridgez88.jira.com/wiki



------------------------------------------------------------------------------------
Getting fresh OZ ROM binaries automatically built on BitBucket.org
------------------------------------------------------------------------------------
We provide OZ ROM binaries archived in Zip files, freshly built from source code:
https://bitbucket.org/cambridge/oz/downloads/

These Zip files contains mostly developer snapshot and pre-releases. They are not
considered public releases. Use them for testing and feedback to the development
team.

The following ROM releases are available from download section
ozrom-master-<commitid>.zip   (public stable releases, V4.7 and later)
ozrom-develop-<commitid>.zip  (stable development builds, V5.0 and later)
ozrom-xxxxxx-<commitid>.zip   (bleeding edge testing builds, V5.0 and later)



------------------------------------------------------------------------------------
Installing the Z88 Assembler Workbench Tools for your desktop operating system
------------------------------------------------------------------------------------

To compile the Cambridge Z88 ROM, you need three utilities - Mpm (assembler),
Z88Card (Application Card builder) and MthToken (tokenize static strings in source
code) - that are necessary to compile the ROM sources into a complete 128K binary.

For convenience, these ready-made tools are available to be downloaded &
installation for Mac OS X, Linux or Windows here:

https://sourceforge.net/projects/z88/files/Z88%20Assembler%20Workbench/

Follow the ReadMe description in the Zip archive on how to install these tools on
your system. Basically, ensure that these tools are available on the operating
system PATH, so they are accessible everywhere on your command shell.

OZ source code compilation require Z88 AWB tools release 2016.5 or later.

Both tools have their own repository (if you are interested to learn about their
implementation):

https://gitlab.com/bits4fun/mpm (Mpm - Z80 Assember utility)
https://gitlab.com/z88/mthtoken (Tokenize static strings for MTH and other use)
https://gitlab.com/z88/z88card (Z88Card - Z88 AppCard Manager)



------------------------------------------------------------------------------------
Installing a Git Client for your desktop operating system
------------------------------------------------------------------------------------

Git tools exists that do not use an installed Git command line client for your
operating system.

Important!
----------

The standard git command-line client must also be available on your system in order
for build revision information to be generated into the Cambridge Z88 ROM software.
If this is not present, then placeholder text of the ROM release information will be
left where the build revision information should be.

Git command line client can be downloaded from here: http://git-scm.com/download/



------------------------------------------------------------------------------------
Compiling the Z88 ROM from the Git repository
------------------------------------------------------------------------------------

Once you have completed the above steps, you've now ready to compile the Z88 rom!

Just execute the makerom.sh script and you will have the latest OZ rom compiled
from source, that can be installed as a binary for slot 0 or slot 1.

To use slot 0 you need to have a 512K flash AMD chip fitted onto the original
motherboard.

To use slot 1 you just need a 1MB flash card in slot 1 (can be purchased from
www.rakewell.com). OZ gets booted automatically by the built-in ROM and using an
external card saves modifying the original Z88 altogether.

OZ has been ported to slots 0 and 1, which enables you to blow the code to either
slot. To compile it, use the following commands.

        ./makerom.sh 0 (for slot 0 - 512K flash chip on motherboard)
        ./makerom.sh 1 (for Amd/Stm/Amic 512K/1Mb flash cards in slot 1)

You use RomUpdate (Main Z88 Git repository, /z88apps/romupdate) to actually blow the
bank binaries to the flash card. Better, use OZ V4.7 own integrated Updater from
Index with <>UPD command (for *.upd files).

Upload "romupdate.bas", "romupdate.crc", and the generated "romupdate.cfg" file with
the oz.* bank files to your Z88.

Start a BBC BASIC application and RUN"romupdate.bas".

The flash card will be cleared and the binaries blown; finally the Z88 is hard reset
and OZ is booted from either slot 0 or 1.

You can install the rom binary in the Z88 emulator
(https://cambridgez88.jira.com/wiki/display/OZVM),

or using a conventional Eprom programmer and re-blow your 128K or larger chip to be
inserted into a real Z88. The easiest way for run the compiled ROM is to execute the
run-oz.bat/sh script which will run the emulator with the latest OZ installed in
slot 1 and boot the "virtual" Z88.

IMPORTANT:
OZ V4.7 sources requires minimum Mpm V1.5, MthToken V0.7, Z88Card V1.99.17 or later
Previous OZ releases requires Mpm V1.4 or Mpm V1.3 (no longer supported).


------------------------------------------------------------------------------------
Source code guidelines for developers
------------------------------------------------------------------------------------

All source files in these folders follow some guide lines.

All file and folder names are lower case.
All ASCII files contain no tabulator encoding (ASCII 9) for tabulating columns.

Z80 mnemonics:
mnemonics are in lower case.
there's 8 spaces between Z80 instruction and register/parameter, eg.
      ld      hl,0.

Here's a complete example:

.Calculator
        ld      iy, $1FAC
        ld      a, 5
        oz      OS_Esc                          ; Examine special condition


Labels begin at column 1
The assembler begins at column 9
Line comments begin at column 50.

All register input/output parameters in functions or other semantic entity uses the
style from the Developer's Notes.

All variable names and language is plain english.
