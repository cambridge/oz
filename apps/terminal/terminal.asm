; **************************************************************************************************
; Terminal application (addressed for segment 3).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module Terminal

        include "char.def"
        include "director.def"
        include "error.def"
        include "fileio.def"
        include "stdio.def"
        include "syspar.def"
        include "serintfc.def"
        include "sysapps.def"
        include "oz.def"

        org TRM_ORG


.Terminal
        ld      a,OZVERSION                     ; OZ V4.5 and later implements OS_Nq, NQ_Voz (verify OZ)
        ld      bc,NQ_Voz
        oz      OS_Nq                           ; This application only runs on latest ROM by principle 
        jr      nc, init_terminal
.exit
        xor     a
        oz      OS_Bye                          ; return to Index in current running OZ... (not the OZ of these sources)

.aVt52
        defm    "VT52",0

.init_terminal
        OZ      OS_Pout
        defm    1,"7#1",$20,$20,$7E,$28,$80     ; define window 1, use whole screen (preserved)
        defm    1,"2C1"                         ; select and clear window 1
        defm    1,"3+SC",0                      ; enable Scrolling and Cursor

        ld      hl, aVt52                       ; "VT52"
        OZ      DC_Nam                          ; Name current application

        ld      bc, NQ_Chn                      ; get :COM handle
        OZ      OS_Nq                           ; enquire (fetch) parameter

        ld      iy, TRM_SWS_START               ; $1FF4

; SAFE AREA
;
; (IY+00)
;       Inflow process control byte
;
;       Bit 0 : ESC F (0, enable SOH), ESC G (1, disable SOH, default)
;       Bit 1 : ESC, catch letter
;       Bit 2 : SOH, catch number (lenght of SOH string)
;       Bit 3 : number >0 (in IY+1 counter), process SOH string
;       Bit 4 : ESC Y, catch row
;       Bit 5 : ESC Y, catch col
;       Bit 6 : unused (set)
;       Bit 7 : unused (set)
;
; (IY+1)
;       SOH counter
;
; (IY+2)
;       row
;


; clear screen

.clearscr
        oz      OS_Pout
        defm    1,"3@  "                        ; cursor at 0,0
        defm    1,"2C",$FE                      ; erase EOS
        defb    0

; read serial input

.get_serial
        ld      bc, 10
        ld      l, SI_GBT
        OZ      OS_SI                           ; read a byte
        jr      c, si_error
        call    process_serial
        jr      get_serial

.si_error
        cp      RC_Time
        jr      z, mainloop
        jr      errorhandler

; loop to handle keyboard input

.mainloop
        ld      bc, 1
        OZ      OS_Tin                          ; read a byte from std. input, with timeout
        jr      nc, keypress

.kb_error
        cp      RC_Time                         ; Timeout, test serial input
        jr      z, get_serial

.errorhandler
        cp      RC_Susp                         ; Suspicion of pre-emption
        jr      z, mainloop
        cp      RC_Draw                         ; Application pre-empted and screen corrupted
        jr      z, clearscr
        oz      GN_Err
        jp      exit                            ; exit if other error

.keypress
        or      a
        jr      nz, sendkey

; it was special char, get second byte

        OZ      OS_In
        jr      c, kb_error

; process menu command

        dec     a                               ; 01 exit
        jr      z, exit
        dec     a                               ; 02 clear screen
        jr      z, clearscr
        dec     a
        cp      14
        jr      nc, crsr_cmd
        ld      c, a
        ld      b, 0
        ld      hl, ctrl_cmd
        add     hl, bc
        ld      a, (hl)
        jr      sendkey
.crsr_cmd
        sub     14
        cp      4
        jr      nc, fn_cmd
        ld      h, 'A'
        add     a, h
        ld      h, a
        ld      a, ESC
        call    putkey
        jr      c, mainloop
        ld      a, h
        jr      sendkey
.fn_cmd
        sub     4
        cp      4
        jr      nc, mainloop
        ld      h, 'P'
        add     a, h
        ld      h, a
        ld      a, ESC
        call    putkey
        jr      c, mainloop
        ld      a, '?'
        call    putkey
        jr      c, mainloop        
        ld      a, h
        jr      sendkey
.sendkey
        call    putkey
        jr      mainloop

.ctrl_cmd
        defb DEL, BS, BEL, HT, LF, FF, CR, SO, SI, DC1, DC3, CAN, SUB, ESC

.putkey
        push    hl
        ld      bc, 10                          ; 100 ms timeout
        ld      l, SI_PBT
        OZ      OS_SI                           ; write byte A to handle IX, BC=timeout
        pop     hl
        ret     nc

        cp      RC_Time                         ; Timeout
        scf
        ret     nz

        ld      a, BEL                          ; bell on error
        OZ      OS_Out                          ; write a byte to std. output
        ret

; process serial flow

.process_serial
        ld      h, (iy+0)
        cp      CAN                             ; CAN (cancel)
        jr      z, PS_CAN
        cp      SUB                             ; ^Z (SUB)
        jr      nz, loc_0_E8A1
.PS_CAN
        ld      a, @11000001                    ; clear b54321
        and     h
        ld      (iy+0), a
        ret

.loc_0_E8A1
        bit     0, h                            ; SOH disabled?
        jr      nz, loc_0_E8D3

; SOH enabled

        bit     2, h                            ; SOH?
        jr      z, loc_0_E8BD

; catch number and put it in SOH counter

        res     2, (iy+0)                       ; number catched
        OZ      GN_Cls                          ; Classify a character
        ret     c
        ret     nz

        sub     '0'                             ; 0-9
        ret     z
        ld      (iy+1), a                       ; counter
        set     3, (iy+0)                       ; got counter
        ret

; skip (iy+1) chars

.loc_0_E8BD
        bit     3, h
        jr      z, loc_0_E8CA
        dec     (iy+1)                          ; if so, ignore so many bytes
        ret     nz
        res     3, (iy+0)
        ret

.loc_0_E8CA
        cp      1                               ; SOH
        jr      nz, loc_0_E8D3
        set     2, (iy+0)                       ; SOH active, catch number
        ret

; SOH disabled, process ESC

.loc_0_E8D3
        bit     1, h                            ; got ESC?
        jr      z, loc_0_E8FF
        bit     4, h                            ; got identifier?
        jr      z, loc_0_E90B
        bit     5, h                            ; got row?
        jr      nz, loc_0_E8E7
        ld      (iy+2), a
        set     5, (iy+0)
        ret

; ESC Y row+32 col+32

.loc_0_E8E7
        push    af
        ld      hl, MoveTo_txt
        OZ      GN_Sop                          ; write string to std. output
        pop     af                              ; row+32
        OZ      OS_Out
        ld      a, (iy+2)                       ; col+32
        OZ      OS_Out
        ld      a, $CD                          ; clear b541
        and     (iy+0)
        ld      (iy+0), a
        ret

.MoveTo_txt
        defm    1,"3@",0

.loc_0_E8FF
        cp      ESC
        jr      nz, loc_0_E908
        set     1, (iy+0)                       ; esc
        ret

.loc_0_E908
        OZ      OS_Out                          ; write a byte to std. output
        ret

; process ESC + LETTER

.loc_0_E90B
        cp      'Z'                             ; who am I
        jr      nz, is_esc_y
        ld      a, ESC                          ; Respond ESC \ Z
        call    putkey                          ; I am a VT52
        jr      c, loc_0_E942
        ld      a, '/'
        call    putkey
        jr      c, loc_0_E942
        ld      a, 'Z'
        call    putkey
        jr      loc_0_E942

.is_esc_y
        cp      'Y'                             ; direct cursor address
        jr      nz, loc_0_E918
        set     4, (iy+0)                       ; ESC Y row+32 col+32
        res     5, (iy+0)
        ret

.loc_0_E918
        cp      'F'                             ; select soft character set
        jr      nz, loc_0_E922
        set     0, (iy+0)                       ; disable SOH
        jr      loc_0_E942

.loc_0_E922
        cp      'G'                             ; select ASCII character set
        jr      nz, loc_0_E92C
        res     0, (iy+0)                       ; enable SOH
        jr      loc_0_E942

.loc_0_E92C
        ld      hl, CtrlTable
        ld      c, a
.loc_0_E930
        ld      a, (hl)
        inc     hl
        cp      c
        jr      z, loc_0_E93F
        or      a
        jr      z, loc_0_E942                   ; if zero (table terminator)
.loc_0_E938
        ld      a, (hl)
        inc     hl
        or      a
        jr      nz, loc_0_E938
        jr      loc_0_E930

.loc_0_E93F
        OZ      GN_Sop                          ; write string to std. output
.loc_0_E942
        res     1, (iy+0)                       ; no ESC
        ret

.CtrlTable
        defb    'A', VT, 0                      ; ESC A - up
        defb    'B', LF, 0                      ; ESC B - down
        defb    'C', HT, 0                      ; ESC C - right
        defb    'D', BS, 0                      ; ESC D - left
        defm    'H', 1,"3@  ",0                 ; ESC H - cursor home
        defb    'I', VT, 0                      ; ESC I - reverse lf
        defb    'J', 1,$32,$43,$FE,0            ; ESC J - erase EOS
        defb    'K', 1,$32,$43,$FD,0            ; ESC K - erase EOL
        defb    'L', FF, 0                      ; ESC L - clear screen
        defb    0


