; **************************************************************************************************
; Info
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module Info

        include "fileio.def"
        include "integer.def"
        include "stdio.def"
        include "syspar.def"
        include "time.def"
        include "sysvar.def"
        include "sysapps.def"
        include "director.def"
        include "error.def"
        include "../apps/index/index.def"


xdef    cmd_info, InfoWdBlock, Updated

xref    MainLoop, PressEsc, IndexRedraw

.cmd_info
        push    ix
        ld      bc, NQ_Ohn
        oz      OS_Nq                           ; stream to :OUT, used by GN_Pdt below

        ld      b, 0
        ld      hl, InfoWdBlock
        ld      de, InfoBanner
        OZ      GN_Win

        oz      OS_Pout                         ; invisible window area for entries
        defm    1,"6#2",$20+13,$20+2,$20+58,$20+6
        defm    1,"2C2"
        defm    1,'T'
        defm    "OZ",1,"2X",$20+30,"BUILD",CR,LF
        defm    "MACHINE",CR,LF
        defm    "TYPE",1,"2X",$20+30,"FREE MEMORY",CR,LF
        defm    "BATTERY",1,"2X",$20+30,"FREE HANDLES",CR,LF
        defm    CR,LF,1,"2X",$20+21,"PRESS ",1,SD_ESC," TO RESUME", 0

        oz      OS_Pout                         ; left area
        defm    1,"6#2",$20+21,$20+2,$20+19,$20+5
        defm    1,"2C2"
        defm    1,"2JR"
        defm    1,'T', 0

;disp_version
        call    disp_ver

.disp_machine
        oz      OS_Pout
        defm    CR, LF, "Z88", CR, LF, 0

        ld      a, FA_EOF                       ; MACHINE TYPE
        call    inf_frm
        ld      hl, UnexpandedZ88
        jr      nz, disp_frm
        inc     hl
        inc     hl
.disp_frm
        oz      GN_Sop
        oz      GN_Nln

        ld      hl, BattGood
        ld      bc, NQ_Btl
        oz      OS_Nq
        jr      nz, disp_batt
        ld      hl, BattLow
.disp_batt
        oz      GN_Sop

        oz      OS_Pout                         ; right area, 2 first row
        defm    1,"6#2",$20+48,$20+2,$20+23,$20+2
        defm    1,"2C2"
        defm    1,"2JR"
        defm    1,'T', 0
;disp_revision
        call    disp_rev
        oz      GN_Nln
;disp_compilation
        ld      de, $year                       ; COMPILATION DATE & TIME
        ld      bc, $month<<8 | $day
        OZ      GN_Dei                          ; convert to internal format
        ld      hl, ulIdxInfo+2
        ld      (hl), a
        dec     hl
        ld      (hl), b
        dec     hl
        ld      (hl), c
        ld      de, 0
        ld      a, $a1                          ; century, C delimeter, zero blanking
        ld      bc, 0<<8|'/'                    ; condensed form, '/' delimeter
        oz      GN_Pdt                          ; display compilation date

        ld      a, ' '
        oz      OS_Out

        defc elapsedtime_centisecs = $hour*60*60*100 + $minute*60*100 + $second*100

        ld      a, elapsedtime_centisecs/65536
        ld      b, [elapsedtime_centisecs - ((elapsedtime_centisecs/65536) * 65536)] / 256
        ld      c, [elapsedtime_centisecs - ((elapsedtime_centisecs/65536) * 65536)] % 256

        ld      hl, ulIdxInfo+2
        ld      (hl), a
        dec     hl
        ld      (hl), b
        dec     hl
        ld      (hl), c
        ld      de, 0
        ld      a, $21
        OZ      GN_Ptm                          ; display compilation time

        oz      OS_Pout                         ; right area, 2 last row
        defm    1,"6#2",$20+55,$20+4,$20+16,$20+2
        defm    1,"2C2"
        defm    1,"2JR"
        defm    1,'T', 0

        ld      a, FA_EXT                       ; FREE MEMORY
        call    inf_frm
                                                ; divide by 1024 (shift right debc 10 times)
        srl     d                               ; shift right deb 2 times on the right (div by 4)
        rr      e
        rr      b
        srl     d
        rr      e
        rr      b
        ld      c, b                            ; shift right debc 8 times i.e. 0deb (div by 256)
        ld      b, e
        ld      e, d
        ld      d, 0

        ld      hl, ulIdxInfo+3                 ; in 64 bits buffer
        ld      (hl), d
        dec     hl
        ld      (hl), e
        dec     hl
        ld      (hl), b
        dec     hl
        ld      (hl), c
        ld      a, 1
        ld      de, 0
        oz      GN_Pdn                          ; convert to decimal
        oz      OS_Pout
        defm    "K",0
        oz      GN_Nln

        ld      a, FA_PTR                       ; FREE HANDLES
        call    inf_frm
        ld      hl, 2
        ld      b, d
        ld      c, e
        ld      de, 0
        oz      GN_Pdn

        ld      a, 3
        ld      (ubIdxActiveWindow), a          ; for cmd_escape
        pop     ix
        jp      MainLoop

; -----------------------------------------------------------------------------
; Updated info window call

.Updated
        xor     a
        ld      (ubUpdated), a                  ; reset updated flag in preserved area
        ld      b, a
        ld      hl, InfoWdBlock
        ld      de, InfoBanner
        OZ      GN_Win

        oz      OS_Pout                         ; invisible window area for entries
        defm    1,"6#2",$20+13,$20+2,$20+58,$20+6
        defm    1,"2C2"
        defm    1,"2JC"
        defm    "Operating system has been successfully updated to ",CR,LF,CR,LF,1,"BOZ ",0
        call    disp_ver
        ld      a, ' '
        oz      OS_Out
        call    disp_rev
        oz      OS_Pout
        defb    BEL                             ; beep
        defm    1,"B",1,"3@",$20+0,$20+5,0
        call    PressEsc
.up_in
        oz      OS_In
        jr      nz, up_in
        oz      OS_In
        cp      6                               ; ESC command number (defined in MTH)
        jr      nz, up_in
        ret

; -----------------------------------------------------------------------------
; Sub-routines

.inf_frm
        push    ix
        ld      ix,-1
        ld      de,0                            ; result in DEBC
        oz      OS_Frm
        pop     ix   
        ret

;       -----

.InfoWdBlock
        defb    @10110000 | 2
        defw    $0009
        defw    $0840
        defw    0
.InfoBanner
        defm    "SYSTEM INFORMATION",0

;       -----

.disp_rev
        oz      OS_Pout                         ; REVISION NUMBER
        defm    "rev. ", 0

        ld      bc, NQ_Roz
        oz      OS_Nq

        ld      a, d
        oz      OS_Hout
        ld      a, e
        oz      OS_Hout
        ld      a, b
        oz      OS_Hout
        ld      a, c
        oz      OS_Hout
        ret

;       -----

.disp_ver
        ld      a, FA_PTR                       ; MAJOR AND MINOR VERSION NUMBER
        call    inf_frm
        ld      a, c
        and     @11110000
        rra
        rra
        rra
        rra
        add     a, '0'                          ; I know that I'll have to handle version 10.10 one day !
        oz      OS_Out
        ld      a, '.'
        oz      OS_Out
        ld      a, c
        and     @00001111
        add     a, '0'
        oz      OS_Out

        xor     a                               ; test slot 0
        oz      OS_Ploz
        ret     nz                              ; internal
        oz      OS_Pout
        defm    " in slot 1",0
        ret

;       -----

.UnexpandedZ88
        defm    "unexpanded",0
.BattGood
        defm    "good",0
.BattLow
        defm    "low",0
