; **************************************************************************************************
; Index popdown and DC system calls.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        module  remove

        include "blink.def"
        include "char.def"
        include "director.def"
        include "dor.def"
        include "error.def"
        include "fileio.def"
        include "integer.def"
        include "memory.def"
        include "serintfc.def"
        include "stdio.def"
        include "syspar.def"
        include "time.def"
        include "printer.def"
        include "sysvar.def"
        include "oz.def"
        include "z80.def"
        include "sysapps.def"
        include "keyboard.def"
        include "handle.def"
        include "dor.def"
        include "../apps/index/index.def"

        xdef    cmd_remove, DoRemove

        xref    GetAppByNum, IndexRedraw, MainLoop
        xref    GetProcByHandle
        xref    PressEsc, AreYousure, InitKeys

.cmd_remove

; Get application handle and verify that there is no running process

        ld      a, (ubIdxTopApplication)
        ld      c, a
        ld      a, (ubIdxSelectorPos)
        add     a, c
        call    GetAppByNum
        ld      a, ixl                          ; installed application static handle from $80 to $FF
        ld      (App_RmvHnd), ix                ; store application handle to be removed
        and     $80
        jr      z, FailError                    ; not an installed application

        ld      a, $80                          ; test if IX application has a running process
        oz      DC_Pol
        jr      z, AskRemove

.InUseError
        ld      a, RC_Use                       ; 'In use'
.TellIdxError
        ld      (ubIdxErrorCode), a
        ld      hl, ubIdxFlags2
        set     IDXF2_B_ERROR, (hl)
        xor     a
        ld      (ubIdxActiveWindow), a
        jp      IndexRedraw        

.FailError
        ld      a, RC_Fail                      ; 'Cannot satisfy request'
        jr      TellIdxError

.AskRemove
        ld      a, 5
        ld      (ubIdxActiveWindow), a

        call    DrawRmvWd

        oz      OS_Pout
        defm    "Remove ",0

        ld      bc, NQ_Ain                      ; IX=application static handle
        oz      OS_Nq                           ; get application data, name in BHL
        oz      GN_Soe
        oz      GN_Nln
        oz      GN_Nln
        ld      a, 'n'
        ld      (ubIdxYesNo), a
        call    AreYousure
        jp      MainLoop

;       ----

.DoRemove
        xor     a
        ld      (ubIdxActiveWindow), a          ; Wd #0 if 'No'

        ld      a, (ubIdxYesNo)
        cp      'y'
        jp      nz, IndexRedraw

; Find its installation handle

        ld      ix, (App_RmvHnd)
        ld      bc, NQ_Ain                      ; IX=application static handle
        oz      OS_Nq                           ; get application data, DOR in BDE
        ex      de, hl                          ; DOR in BHL
        ld      ix, 0                           ; start with first handle
        ld      a, FN_GH                        ; find handle

.FindNextInstHnd
        push    bc
        ld      b, HND_INST                     ; type
        ld      c, 0                            ; no dyn id
        oz      OS_Fn
        pop     bc
        jp      c, FailError
        ld      a, l
        cp      (ix+ihnd_DOR)
        jr      nz, FindInstHnd2
        ld      a, h
        cp      (ix+ihnd_DORH)
        jr      nz, FindInstHnd2
        ld      a, b
        cp      (ix+ihnd_DORB)
        jr      z, InstHndFound
.FindInstHnd2
        ld      a, FN_NH                        ; find next handle
        jr      FindNextInstHnd

.InstHndFound
        ld      (App_InstHnd), ix

; If multiple applications installed, verify that no running process

        ld      c, (ix+ihnd_AppNbOf)
        ld      ix, (App_RmvHnd)

.VerifyMultipleApps
        dec     c
        jr      z, UpdateProcesses
        oz      OS_Poll
        jr      c, FailError                    ; shouldn't occur
        ld      a, $80
        oz      DC_Pol
        jp      nz, InUseError                  ; one of the installed app is running, exit
        jr      VerifyMultipleApps

; Update app static handle in process of other installed application

.UpdateProcesses
        ld      ix, (App_RmvHnd)                ; reload app handle

.TryNextApp
        oz      OS_Poll                         ; get handle of next application in slot 4
        jr      c, UnchainDOR                   ; no more application, continue
.TrySameApp
        call    GetProcByHandle                 ; proc at BHL
        jr      c, TryNextApp
        ld      c, MS_S1
        rst     OZ_Mpb
        ld      a, h                            ; in S1
        and     $3F
        or      MM_S1
        ld      h, a
        push    hl
        pop     iy
        push    ix
        ld      ix, (App_InstHnd)
        ld      c, (ix+ihnd_AppNbOf)            ; number of installed application
        pop     ix
        ld      a, (iy+prc_hndl)                ; app static handle (=IXL too)
        sub     a, c
        ld      (iy+prc_hndl), a
        ld      c, a
        ld      b, 0
        ld      e, (iy+prc_stkProcEnv)
        ld      d, (iy+prc_stkProcEnv+1)
        push    ix
        push    de                              ; the stacked process environment handle (type 4)
        pop     ix
        oz      OS_Uash                         ; update applicatio static handle in stacked process environment
        pop     ix
        jr      TrySameApp                      ; try with same handle in case of several processes for same application

; unchain application DOR

.UnchainDOR
        ld      ix, (App_InstHnd)
        ld      e, (ix+ihnd_DOR)                ; app DOR in CDE
        ld      d, (ix+ihnd_DORH)
        ld      c, (ix+ihnd_DORB)
        ld      iy, $4060
        push    bc
        ld      b, R21_BNK                      ; bind :APP.- Front DOR bank in S1
        ld      c, MS_S1
        rst     OZ_Mpb
        pop     bc
        ld      l, (iy+DOR_SON)
        ld      h, (iy+DOR_SON_H)
        ld      b, (iy+DOR_SON_B)
        ld      a, b
        or      h
        or      l
        jp      z, FailError                    ; APP front DOR empty
        ld      a, l
        cp      e
        jr      nz, GotoBrother
        ld      a, h
        cp      d
        jr      nz, GotoBrother
        ld      a, b
        cp      c
        jr      nz, GotoBrother
        call    GetSmallestBrother
        ld      (iy+DOR_SON), l                 ; chain small brother to APP front DOR
        ld      (iy+DOR_SON_H), h
        ld      (iy+DOR_SON_B), b
        jr      FreeMemory

.GetBrother                                     ; IN: BHL ; OUT: BHL
        push    iy
        push    de
        ld      e, c
        ld      c, MS_S1
        rst     OZ_Mpb
        push    bc
        ld      a, h        
        and     $3F
        or      MM_S1                           ; in S1
        ld      h, a
        push    hl                              ; ld iy, hl
        pop     iy
        ld      l, (iy+DOR_BROTHER)
        ld      h, (iy+DOR_BROTHER_H)
        ld      d, (iy+DOR_BROTHER_B)
        pop     bc
        rst     OZ_Mpb
        ld      b, d
        ld      c, e
        pop     de
        pop     iy
        ret

.GetSmallestBrother                             ; IN: BHL and IX ; OUT: BHL
        ld      a, (ix+ihnd_AppNbOf)
.SmallestBrother
        push    af
        call    GetBrother
        pop     af
        dec     a
        jr      nz, SmallestBrother
        ret

.GotoBrother
        ld      iy, App_GrBrother
        ld      (iy+0), l
        ld      (iy+1), h
        ld      (iy+2), b
        call    GetBrother
        ld      a, b
        or      h
        or      l
        jp      z, FailError                    ;  No brother, not found
        ld      a, l
        cp      e
        jr      nz, GotoBrother
        ld      a, h
        cp      d
        jr      nz, GotoBrother
        ld      a, b
        cp      c
        jr      nz, GotoBrother
        call    GetSmallestBrother              ; smallest brother in BHL
        ld      a, (App_GrBrotherB)             ; grand brother in CDE
        ld      c, a
        push    bc
        ld      b, c
        ld      c, MS_S1
        rst     OZ_Mpb
        pop     bc
        ld      a, (App_GrBrotherH)
        and     $3F
        or      MM_S1
        ld      d, a
        ld      a, (App_GrBrother)
        ld      e, a
        push    de
        pop     iy
        ld      (iy+DOR_BROTHER), l             ; chain small brother to grand brother
        ld      (iy+DOR_BROTHER_H), h
        ld      (iy+DOR_BROTHER_B), b

.FreeMemory
        ld      l, (ix+ihnd_MemHnd)
        ld      h, (ix+ihnd_MemHndH)
        push    hl
        pop     ix
        oz      OS_Mcl                          ; will free whole allocated memory and memory handle
        jp      c, TellIdxError

; Free Installation handle, refresh key letters and display success

.FreeInstHnd
        ld      a, FN_FH
        ld      b, HND_INST
        ld      ix, (App_InstHnd)
        oz      OS_Fn
        jp      c, TellIdxError

        xor     a
        ld      (App_RmvHnd), a
        ld      (App_InstHnd), a

        call    InitKeys                        ; repoll keys

        ld      a, 3
        ld      (ubIdxActiveWindow), a

        call    DrawRmvWd
        oz      OS_Pout
        defm    1,"2JCRemoval completed.",CR,LF
        defm    1,"3@",$20+0,$20+5,0

        call    PressEsc
        jp      MainLoop

.DrawRmvWd
        ld      b, 0
        ld      hl, RemoveWdBlock
        OZ      GN_Win
        oz      OS_Pout                         ; invisible window area
        defm    1,"6#2",$20+13,$20+2,$20+58,$20+6
        defm    1,"2C2",0
        ret

.RemoveWdBlock
        defb    @10110000 | 2
        defw    $0009
        defw    $0840
        defw    RemoveBanner
.RemoveBanner
        defm    "REMOVE APPLICATION",0