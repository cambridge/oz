; **************************************************************************************************
; Info
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module Nq

        include "fileio.def"
        include "integer.def"
        include "stdio.def"
        include "syspar.def"
        include "time.def"
        include "sysvar.def"
        include "sysapps.def"
        include "director.def"
        include "memory.def"
        include "dor.def"
        include "../apps/index/index.def"


xdef    cmd_nq

xref    MainLoop, Index, GetAppByNum, InfoWdBlock, GetProcessByNum

.cmd_nq
        push    ix
        ld      bc, NQ_Ohn
        oz      OS_Nq                           ; stream to :OUT

        ld      a, (ubIdxActiveWindow)
        or      a
        jp      nz, nq_process

; Display selected application informations

        ld      de, BannerApp
        call    nq_win

        ld      a, (ubIdxTopApplication)
        ld      c, a
        ld      a, (ubIdxSelectorPos)
        add     a, c
        call    GetAppByNum
        jp      c, Index                        ; confused

        oz      OS_Pout
        defm    "Name     ",0
        ld      bc, NQ_Ain
        oz      OS_Nq
        oz      GN_Soe
        call    nq_tab
        oz      OS_Pout
        defm    "Appl. type    ",0
        oz      OS_Hout
        oz      OS_Pout
        defm    " (",0
        ld      hl, ap_ugly
        bit     AT_B_Ugly, a
        jr      nz, nq_type
        ld      hl, ap_bad
        bit     AT_B_Bad, a
        jr      nz, nq_type
        ld      hl, ap_popd
        bit     AT_B_Popd, a
        jr      nz, nq_type
        ld      hl, ap_good
.nq_type
        oz      GN_Sop

        oz      OS_Pout
        defm    ")",CR,LF,"Device   :",0
        ld      a, ixl
        bit     7, a 
        jr      nz, nq_inst
        and     @01100000
        rra
        rra
        rra
        rra
        rra
        or      '0'
        oz      OS_Pout
        defm    "ROM.",0
        oz      OS_Out
        jr      nq_hnd

.nq_inst
        oz      OS_Pout
        defm    "APP.-",0

.nq_hnd
        call    nq_tab
        oz      OS_Pout
        defm    "Static handle ",0
        ld      a, ixh
        oz      OS_Hout
        ld      a, ixl
        oz      OS_Hout

        oz      OS_Pout
        defm    CR,LF,"Bindings ",0
        ex      de, hl
        push    hl
        push    bc
        ld      bc, ADOR_ENTRY
        add     hl, bc
        pop     bc
        push    bc
        oz      GN_Rbe
        ld      e, a
        inc     hl
        oz      GN_Rbe
        ld      d, a
        push    de

        inc     hl
        ld      c, 4
.nq_seg
        oz      GN_Rbe
        oz      OS_Hout
        ld      a, ' '
        oz      OS_Out
        inc     hl
        dec     c
        jr      nz, nq_seg

        call    nq_tab
        oz      OS_Pout
        defm    "Entry         ",0
        pop     de
        ld      a, d
        oz      OS_Hout
        ld      a, e
        oz      OS_Hout

        pop     bc
        pop     de
        push    de
        push    bc
        oz      OS_Pout
        defm    CR,LF,"DOR      ",0
        ld      c, b
        call    nq_cde

        pop     bc
        pop     hl
        push    hl
        push    bc
        call    nq_tab
        oz      OS_Pout
        defm    "Brother       ",0
        inc     hl
        inc     hl
        inc     hl
        call    nq_bhl2cde
        call    nq_cde

        oz      OS_Pout
        defm    CR,LF,"Topics   ",0
        pop     bc
        pop     hl
        push    bc
        ld      bc, ADOR_TOPICS
        add     hl, bc
        pop     bc
        call    nq_bhl2cde
        call    nq_cde

        call    nq_tab
        oz      OS_Pout
        defm    "Commands      ",0
        inc     hl
        call    nq_bhl2cde
        call    nq_cde

        oz      OS_Pout
        defm    CR,LF,"Help     ",0
        inc     hl
        call    nq_bhl2cde
        call    nq_cde

        call    nq_tab
        oz      OS_Pout
        defm    "Tokens        ",0
        inc     hl
        call    nq_bhl2cde
        call    nq_cde

.nq_pressesc
        oz      OS_Pout
        defm    CR,LF,1,"2X",$20+21,"PRESS ",1,SD_ESC," TO RESUME", 0        
        ld      a, 3
        ld      (ubIdxActiveWindow), a          ; for cmd_escape
        pop     ix
        jp      MainLoop

; Display selected process informations

.nq_process
        ld      a, (ubIdxNProcDisplayed)
        or      a
        jp      z, MainLoop                     ; no suspended processes, exit

        ld      de, BannerPrc
        call    nq_win

        ld      a, (ubIdxTopProcess)
        ld      c, a
        ld      a, (ubIdxSelectorPos)
        add     a, c
        call    GetProcessByNum
        jp      c, Index                        ; confused
        ld      a, (iy+prc_flags)
        and     PRCF_ISINDEX
        jp      nz, MainLoop

        oz      OS_Pout
        defm    "Dyn. Id (PID)   ",0
        ld      a, (iy+prc_dynid)               ; PID
        oz      OS_Hout

        call    nq_tab
        push    iy
        pop     hl
        ld      bc, prc_name                    ; activity name
        add     hl, bc
        oz      OS_Pout
        defm    "Name          ",0
        oz      GN_Sop

        oz      OS_Pout
        defm    CR,LF,"Static  handle  ",0
        ld      e, (iy+prc_hndl)
        ld      d, (iy+prc_hndl + 1)
        call    nq_de

        call    nq_tab
        oz      OS_Pout
        defm    "Help flag     ",0
        ld      a, (iy+prc_stkProcEnv + 2)
        oz      OS_Hout

        oz      OS_Pout
        defm    CR,LF,"Process handle  ",0
        ld      e, (iy+prc_stkProcEnv)
        ld      d, (iy+prc_stkProcEnv + 1)
        call    nq_de

        call    nq_tab
        oz      OS_Pout
        defm    "Process flag  ",0
        ld      a, (iy+prc_flags)
        oz      OS_Hout

        oz      OS_Pout
        defm    CR,LF,"Associated Ptr  ",0
        ld      e, (iy+prc_assocptr)
        ld      d, (iy+prc_assocptr + 1)
        ld      c, (iy+prc_assocptr + 2)
        call    nq_cde

        call    nq_tab
        oz      OS_Pout
        defm    "Length        ",0
        ld      a, (iy+prc_assoclen)
        oz      OS_Hout

        push    iy
        pop     hl
        ld      bc, prc_dev                     ; default device
        add     hl, bc
        oz      OS_Pout
        defm    CR,LF,"Default Device  ",0
        oz      GN_Sop

        call    nq_tab
        oz      OS_Pout
        defm    "Match string  ",0
        ld      a, (iy+prc_matchstring)
        oz      OS_Out

        push    iy
        pop     hl
        ld      bc, prc_dir                     ; default directory
        add     hl, bc
        oz      OS_Pout
        defm    CR,LF,"Def. Directory  ",0
        oz      GN_Sop

        jp      nq_pressesc

; Subroutines

.nq_cde                                         ; display CDE in hex
        ld      a, c
        oz      OS_Hout

.nq_de
        ld      a, d
        oz      OS_Hout
        ld      a, e
        oz      OS_Hout
        ret 

.nq_bhl2cde                                     ; CDE = (BHL)
        oz      GN_Rbe
        ld      e, a
        inc     hl
        oz      GN_Rbe
        ld      d, a
        inc     hl
        oz      GN_Rbe
        ld      c, a
        ret

.nq_win
        ld      b, 0
        ld      hl, InfoWdBlock
        oz      GN_Win
        oz      OS_Pout
        defm    1,"2C2",1,"T",0
        ret

.nq_tab
        oz      OS_Pout
        defm    1,"2X",$20+30,0
        ret

; Strings

.BannerApp
        defm    "APPLICATION INFORMATION",0
.BannerPrc
        defm    "PROCESS INFORMATION",0
.ap_good
        defm    "good",0
.ap_bad
        defm    "bad",0
.ap_ugly
        defm    "ugly",0
.ap_popd
        defm    "popdown",0

