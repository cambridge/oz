; **************************************************************************************************
; Index popdown, OZ Update command
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; (C) Gunther Strube (gstrube@gmail.com), 2016
; (C) Thierry Peycru (pek@users.sf.net), 2016
;
; ***************************************************************************************************

module  update

        include "director.def"
        include "error.def"
        include "fileio.def"
        include "memory.def"
        include "stdio.def"
        include "sysvar.def"
        include "oz.def"
        include "sysapps.def"
        include "handle.def"
        include "fpp.def"
        include "integer.def"
        include "syspar.def"
        include "crc32.def"
        include "../apps/index/index.def"

xdef    cmd_update, DoUpdate

xref    AreYouSure, PressEsc
xref    MainLoop, IndexRedraw
xref    ci_ixtst, ci_CloseMem                  ; install.asm

defc    CrcBufferSize = 2048                   ; CRC-32 file buffer size
defc    MaxFileNameSize = 66
defc    MaxIdLength = 66
defc    OzBanksSize = 3*64                     ; the max array size of a slot size in 16K banks is 64 * 16K = 1024MB

;  Mnemonics used to symbolize various identifiers in config file:
defgroup
    ; this first section matches the .separators string in configfile.asm
    sym_null, sym_dquote, sym_squote, sym_semicolon, sym_comma, sym_fullstop
    sym_lparen, sym_lcurly, sym_rcurly, sym_rparen, sym_plus, sym_minus, sym_multiply, sym_divi
    sym_mod, sym_power, sym_assign, sym_strconq, sym_and, sym_or, sym_xor, sym_not
    sym_less, sym_greater, sym_constexpr, sym_newline, sym_lf

    sym_name, sym_decmconst, sym_hexconst, sym_binconst, sym_charconst, sym_negated, sym_nil
enddef

; variable name aliases to existing variable Index Unsafe workspace (not used while OZ Update is processing):
defc Ident = App_Buff1                          ; use buffer for config file parsing of identifiers.
defc bankfilename = App_Buff2                   ; use buffer config file bank filenames.
defc longint = ulIdxInfo                        ; define variable for storing a 32bit integer from config file parsing
defc bankfilenum = inpbuf                       ; destination bank no. in slot of OZ ROM bank file
defc oz_slot = slotno                           ; which slot is going to be updated / loaded

; -----------------------------------------------------------------------------
;
.cmd_update
        call    DrawUpdWd                       ; draw "Operating System Update" window
        oz      OS_Pout
        defm    1,"2+S",0                       ; enable vertical scrolling in window

        ld      hl,0
        ld      (uwIdxCfgHnd),hl                ; indicate no open config file
        ld      (App_MemHnd),hl                 ; indicate no memory handle

        ld      a, MM_S0 | MM_MUL               ; prepare to allocate large blocks from multiple banks
        ld      bc, 0
        OZ      OS_Mop                          ; get a handle to allocate memory from any RAM with OS_Mal to S0 (upper 8K)
        jp      c,UpdError                      ; insufficient ressources available to allocate memory...
        ld      (App_MemHnd),ix

        xor     a
        ld      bc, CrcBufferSize               ; allocate 2K buffer for CRC-32 validation
        oz      OS_Mal                          ; find available free memory, anywhere, and return BHL
        jp      c,UpdError                      ; 2K buffer space were not available in RAM...
        ld      a,b
        ld      (crcbuffer),hl
        ld      (crcbuffer+2),a                 ; preserve pointer to allocated 2K CRC-32 file buffer

        xor     a
        ld      bc, OzBanksSize                 ; allocate buffer for Banks array
        oz      OS_Mal                          ; find available free memory, anywhere, and return BHL
        jp      c,UpdError                      ; buffer space for array were not available in RAM...
        ld      a,b
        ld      (ozbanksarray),hl
        ld      (ozbanksarray+2),a              ; preserve pointer to allocated banks array

        ld      a, OP_IN
        ld      bc, MaxFileNameSize
        ld      de, App_Buff1                   ; get explicit filename in buffer
        ld      hl, CfgFilename
        OZ      GN_Opf                          ; open - BHL=name (B=0, HL=local ptr), A=mode, DE=exp. name buffer, C=buflen
        jp      c, UpdError
        ld      (uwIdxCfgHnd),ix                ; save handle to opened config file
        ld      bc, 7
        ld      hl,App_Buff1
        ld      de,ascbuf                       ; copy ":RAM.x/" path of found "ozupdate.cfg" file, needed for bank files (later)
        ldir

        ld      a,FA_EXT
        ld      de,0                            ; get size of config file in DEBC
        oz      OS_Frm
        jp      c,UpdError                      ; problems with config file handle...
        inc     b                               ; config file has usually 1 - 2K file size (16bit)
        ld      c,0
        ld      (cfgfilesize),bc                ; preserve config file size in 256 byte pages

        xor     a
        ld      ix,(App_MemHnd)
        oz      OS_Mal                          ; allocate buffer in SO for config file (BC = size)
        jr      c,UpdError
        ld      (cfgfilebuffer),hl              ; preserve (offset in bank) start address of config file (to be loaded)
        ld      a,b
        ld      (cfgfilebuffer+2),a             ; preserve bank of allocated buffer

        call    LoadRamCfgFile                  ; load entire config file into allocated buffer in S0
        call    ReadConfigFileOzInfo            ; parse inital config version, OZ target slot and total banks

        oz      OS_Pout
        defm    CR,LF,"Update OZ operating system in slot ",0
        ld      a,(oz_slot)
        ld      b,0
        ld      c,a
        call    DisplayNumber
        oz      GN_nln

        ld      a, 6                            ; 6 identifies update command re-entry (DoUpdate)
        ld      (ubIdxActiveWindow), a
        oz      OS_Pout
        defm    1,"2+C",0                       ; enable blinking cursor

        ld      a, 'n'
        ld      (ubIdxYesNo), a
        call    AreYousure
        call    CloseHandles                    ; release memory and file handles, if open
        jp      MainLoop

.CloseHandles
        call    CloseCfgFile                    ; close "ozupdate.cfg" file, if opened
        ld      ix, (App_MemHnd)
        call    ci_ixtst
        call    nz, ci_CloseMem                 ; release allocated memory, if any
        ld      ix, 0
        ld      (App_MemHnd),ix
        ret

.UpdError
        push    af
        call    CloseHandles
        ld      a, 3
        ld      (ubIdxActiveWindow), a
        oz      OS_Pout
        defm    CR,LF,0
        pop     af
        jr      nc,pesc                         ; a specific error message was already displayed, just press ESC
        oz      GN_Esp
        oz      GN_Soe
        oz      OS_Pout
        defm    1,"3@",$20+0,$20+5,0
        oz      GN_Nln
.pesc
        call    PressEsc
        jp      MainLoop

.CloseCfgFile
        ld      ix, (uwIdxCfgHnd)
        call    ci_ixtst
        ret     z
        oz      GN_Cl
        ld      (uwIdxCfgHnd), ix
        ret

; -----------------------------------------------------------------------------
; Parse config, CRC-32 check OZ bank files, then erase & flash destination slot
; Update OZ in slot 0/1, defined via "ozupdate.cfg" file and OZ ROM bank files
; stored in :RAM.*
;
; The config file will be parsed for OZ ROM target, CRC-32 check OZ bank files,
; erase the specified slot containing a AMD 512K flash chip, then blow the bank
; files from RAM into the slot, completed by a soft reset.

.DoUpdate
        xor     a
        ld      (ubIdxActiveWindow), a          ; Wd #0 if 'No'

        ld      a, (ubIdxYesNo)
        cp      'y'
        jr      z, du_2
        call    CloseCfgFile
        jp      IndexRedraw

.du_2
        ld      a, 3
        ld      (ubIdxActiveWindow), a          ; just an info wd

        call    DrawUpdWd
        oz      OS_Pout
        defm    CR,LF,1,"2+S",1,"2-CUpdating to slot ",0
        ld      a,(oz_slot)
        ld      b,0
        ld      c,a
        call    DisplayNumber
        oz      OS_Pout
        defm    " ...",CR,LF,CR,LF,0

        call    ReadConfigFileOzBanks           ; parse bank entries and CRC-32 check bank files

; *************************************************************************************
; Config file loaded, OZ banks evaluated and prepared for flashing.
; Install/Update OZ ROM to slot X.
;
        call    CloseHandles                    ; release handle of allocated memory, so soft reset reports no warnings

        ld      ix, (ozbanksarray)              ; DC_Upd: IX register points at oz banks array
        ld      a,(ozbanksarray+2)
        ld      b,a
        ld      c, MS_S0                        ; ensure that banks array is available in S0 (upper 8K)
        rst     OZ_MPB

        ld      a,(oz_slot)
        ld      c,a                             ; DC_Upd: C = slot number from config file
        ld      a,(total_ozbanks)
        ld      b,a                             ; DC_Upd: B = total bank files to blow in slot
        oz      DC_Upd                          ; update slot with OZ ROM banks (Amd flash chip)
        jp      UpdError                        ; if DC_Upd returns, its because there were an error


; *************************************************************************************
; Load complete config file from RAM filing system into buffer
;
;  IN:
;         BHL = pointer to allocated buffer space for config file in S0
;
; OUT:    HL = pointer to start of buffer information.
;         DE = pointer to end of buffer information
;         Fz = 1, if EOF reached, otherwise Fz = 0
;
; Registers changed after return:
;
;    ......../..IY  same
;    AFBCDEHL/IX..  different
;
.LoadRamCfgFile
        ld      c, MS_S0                        ; bind allocated memory in bank B to S0
        rst     OZ_MPB

        ld      ix,(uwIdxCfgHnd)                ; get handle...
        ld      bc, (cfgfilesize)               ; read max. bytes into buffer
        ld      de,0
        ex      de,hl                           ; HL=0, DE points at buffer to load file contents
        oz      OS_Mv                           ; read bytes from file

        cp      a                               ; Fc = 0
        ld      hl, (cfgfilesize)
        dec     hl
        sbc     hl,bc
        ld      b,h
        ld      c,l                             ; number of bytes read physically from file

        ex      de,hl
.init_line_buffer
        dec     hl                              ; HL points at end of block
        ld      (hl),CR                         ; append a new line if last line of file is missing it...
        inc     hl
        ld      (hl),0                          ; null-terminate end of loaded information
        ld      (bufferend),hl                  ; end of buffer is byte after last newline

        ex      de,hl                           ; DE: return L-end
        ld      hl, (cfgfilebuffer)             ; HL: return L-start
        call    CloseCfgFile
        ret
; *************************************************************************************


; *************************************************************************************
; Parse initial parameters from 'ozupdate.cfg' file:
; 1) validate CFG.V3
; 2) 'OZ',<total banks> (default slot 0)  or  'OZ.x',<total banks> (specified slot number)
;
.ReadConfigFileOzInfo
        ld      (nextline),hl                   ; init pointer to beginning of first line
        ld      hl,0
        ld      (cfgfilelineno),hl              ; lineno = 0 (we haven't yet loaded a line...)

        call    FetchLine                       ; get first line, containing the 'CFG.Vx' identification
        jp      z,ErrMsgCfgSyntax               ; premature EOF!
        call    GetSym
        cp      sym_name
        jp      nz,ErrMsgCfgSyntax              ; 'CFG' was not identified...
        call    GetSym
        cp      sym_fullstop
        jp      nz,ErrMsgCfgSyntax              ; '.' was not identified...
        call    GetSym
        cp      sym_name
        jp      nz,ErrMsgCfgSyntax              ; 'Vx' was not identified...
        ld      hl,(Ident+1)
        push    hl
        ld      de, '3'<<8 | 'V'
        sbc     hl,de
        pop     hl
        jr      z,parse_cfgfile_lines           ; Allow "V3" config file format
        jp      ErrMsgCfgSyntax

.parse_cfgfile_lines
        call    FetchLine                       ; fetch line containing command specification
        jp      z,ErrMsgCfgSyntax               ; premature EOF!
        call    GetSym
        cp      sym_name
        jr      nz,parse_cfgfile_lines          ; nothing recognized in this line, fetch a new one..

        ld      de,'Z'<<8 | 'O'
        ld      hl,(ident+1)
        cp      a
        sbc     hl,de                           ; 'OZ' identifier?
        jp      nz,ErrMsgCfgSyntax              ; 'OZ' configuration task not found...

        xor     a                               ; update OZ bank files to slot 0 (default)
        ld      (oz_slot),a

        call    GetSym
        cp      sym_fullstop                    ; "OZ." (part of V3 format)
        jr      nz, check_comma                 ; no '.', this means default slot 0

        call    GetSym
        call    GetConstant                     ; get total no of ROM banks to update ..
        jp      nz,ErrMsgCfgSyntax              ; OZ slot number specification value was illegal...
        exx
        ld      a,c
        ld      (oz_slot),a
        call    GetSym                          ; after "OZ.x" a "," must appear...
.check_comma
        cp      sym_comma                       ; fetch comma...
        jp      nz,ErrMsgCfgSyntax              ; not found - signal syntax error...
        call    GetSym
        call    GetConstant                     ; a constant after comma identifies total no of ROM banks to update ..
        jp      nz,ErrMsgCfgSyntax              ; specified destination bank value was illegal...

        exx
        ld      a,c
        ld      (total_ozbanks),a
        xor     a
        ld      (parsed_banks),a
        ret


; *************************************************************************************
; Parse entries in the configuration file:
; "<bank file>",<crc>,<destination bank>
;
.ReadConfigFileOzBanks
; ---------------------------------------------------------------------------------------------------------------------
        ld      iy,(ozbanksarray)               ; get ready for first oz bank entry of [total_ozbanks]
; ---------------------------------------------------------------------------------------------------------------------
.parse_oz_banks
        call    FetchLine                       ; fetch line containing oz bank specification
        jp      z,ErrMsgCfgSyntax               ; premature EOF!
        call    GetSym
        cp      sym_dquote
        jr      nz,parse_oz_banks               ; skip comments and empty lines...

        call    ParseBankFileData               ; found a line with an oz bank entry, collect oz bank data into variables...
        call    ValidateBankFile                ; check CRC of oz bank file data just collected...
        ret     c

        inc     iy
        inc     iy
        inc     iy                              ; prepare for next oz data entry

        ld      hl,total_ozbanks
        ld      a,(hl)                          ; total of oz banks to register
        inc     hl
        inc     (hl)                            ; just registered another oz bank
        cp      (hl)                            ; all registered?
        ret     z                               ; yes, all successfully registered and CRC checked
        jr      parse_oz_banks
; *************************************************************************************


; *************************************************************************************
;
; Fetch a line from the config file
;
; return Fz = 1, if EOF file reached.
;
.FetchLine
        push    bc
        push    de
        push    hl

        ld      a,(cfgfilebuffer+2)
        ld      b,a
        ld      c, MS_S0                        ; bind config file contents to S0 (upper 8K)
        rst     OZ_MPB

        ld      hl,(nextline)                   ; get beginning of new line in buffer
        ld      de,(bufferend)
        ld      a,h
        cp      d
        jr      nz, get_next_line
        ld      a,l
        cp      e
        jr      z,exit_fetchline                ; EOF reached, return Fz = 1...
.get_next_line
        ld      (lineptr),hl
        ex      de,hl
        cp      a
        sbc     hl,de                           ; {bufferend - lineptr}
        ld      b,h
        ld      c,l                             ; search max characters for CR
        ex      de,hl
        call    forward_newline

.new_lineptr
        ld      (nextline),hl                   ; HL points at beginning of new line
        ld      hl,(cfgfilelineno)
        inc     hl
        ld      (cfgfilelineno),hl              ; lineno++
        or      a                               ; Fz = 0, EOF not reached yet...
.exit_fetchline
        pop     hl
        pop     de
        pop     bc
        ret
; *************************************************************************************


; *************************************************************************************
;
;    Find NEWLINE character ahead. Search for the following newline characters:
;         1)   search CR
;         2)   if CR was found, check for a trailing LF (MSDOS newline) to be
;              bypassed, pointing at the first char of the next line.
;         3)   if CR wasn't found, then try to search for LF.
;         4)   if LF wasn't found, return pointer to the end of the buffer.
;
;    IN:  HL = start of search pointer, BC = max. number of bytes to search.
;    OUT: HL = pointer to first char of new line or end of buffer.
;
; Registers changed after return:
;
;    ......../IXIY  same
;    AFBCDEHL/....  different
;
.forward_newline
        ld   d, cr                    ; HL = line, BC = bufsize
        ld   e, lf
.srch_nwl_loop                                    ; do while
        ld   a,d                      ; {
        cp   (hl)                          ; if ( *line != CR)
        jr   z, check_trail_lf             ; {
             ld   a,e                      ;
             cp   (hl)                          ; if ( *line++ == LF )
             inc  hl                                 ;
             ret  z                                  ; return line   /* LF */
             dec  bc                       ; }
             ld   a,b
             or   c
             ret  z
             jr   srch_nwl_loop
                                           ; else {
.check_trail_LF
             inc  hl                            ; if (++*line != LF)
             ld   a,e                                ; return line   /* CR */
             cp   (hl)                               ;
             ret  nz                            ; else
             inc  hl                                 ; return ++line /* CRLF */
             ret                           ; }
                                      ; }
                                      ; while (--bufsize)
; *************************************************************************************


; *************************************************************************************
; parse parse "<filename>",<crc>,<dor offset> from line...
; parsed data are stored in [bankfilename], [bankfilecrc] and [bankfilenum] variables.
;
.ParseBankFileData
        push    bc
        push    hl
        ld      bc,7
        ld      hl,ascbuf                       ; copy RAM device name of "ozupdate.cfg" file
        ld      de,bankfilename                 ; as base of bank filename
        ldir
        pop     hl
        pop     bc
        ld      b,MaxFileNameSize
        call    GetString                       ; read "filename" into (DE)...

        call    GetSym
        cp      sym_comma                       ; skip comma...
        jp      nz,ErrMsgCfgSyntax

        call    GetSym                          ; get bank file image CRC
        call    GetConstant
        jp      nz,ErrMsgCfgSyntax              ; specified CRC value was illegal...
        exx
        ld      (bankfilecrc),bc
        ld      (bankfilecrc+2),de              ; 32bit CRC value in 'debc
        exx

        call    GetSym
        cp      sym_comma                       ; skip comma...
        jp      nz,ErrMsgCfgSyntax

        call    GetSym                          ; get location of application DOR in bank file
        call    GetConstant                     ; constant in DEBC
        jp      nz,ErrMsgCfgSyntax              ; specified DOR value was illegal...

        exx
        ld      a,c
        ld      (bankfilenum),a                 ; bank file number in slot
        exx
        ret
; *************************************************************************************


; *************************************************************************************
; CRC check bank file, defined by filename in [bankfilename], by loading file into
; a buffer and issue a CRC32 calculation, then compare the CRC fetched from the
; configuration file in [bankfilecrc].
;
; Return to caller if the CRC matched, otherwise jump to error routine and exit program
;
.ValidateBankFile
        call    MsgCrcCheckBankFile             ; display progress message for CRC check of bank file
        call    OpenRamBankFile                 ; open file again, just to get low level file data
        jp      c,UpdError
        ld      l,(ix+fhnd_firstblk)            ; get first 64 byte sector number of file
        ld      h,(ix+fhnd_firstblk_h)          ; get bank number of first sector of file

        ld      a,(ozbanksarray+2)
        ld      b,a
        ld      c, MS_S0                        ; ensure that bank array is available in S0 (upper 8K)
        rst     OZ_MPB
        ld      (iy+0),l
        ld      (iy+1),h                        ; save sector details into array
        ld      a,(bankfilenum)
        ld      (iy+2),a                        ; register destination bank number in slot X to blow oz bank

        ld      a,(crcbuffer+2)
        ld      b,a
        ld      c, MS_S0                        ; ensure that CRC-32 file buffer is available in S0
        rst     OZ_MPB

        ld      hl,(crcbuffer)
        ld      bc,CrcBufferSize
        ld      de,0                            ; initial CRC = FFFFFFFFh
        ld      a,CRC_FILE
        OZ      GN_Crc                          ; calculate CRC-32 of bank file, returned in DEBC

        push    af
        OZ      GN_Cl
        pop     af

        ld      hl, bankfilecrc
        call    CheckCrc                        ; check the CRC-32 of the bank file with the CRC of the config file
        jp      nz,ErrMsgCrcFailBankFile        ; CRC didn't match: the file is corrupt and will not be updated to card!
        ret
; *************************************************************************************


; *************************************************************************************
; Compare CRC in DEHL with (BC).
;
; IN:
;       DEBC = calculated CRC
;       HL = pointer to start of CRC in memory
; OUT:
;       Fc = 0 (always)
;       Fz = 1, CRC is valid
;       Fz = 0, CRC does not match the CRC supplied in DEHL
;       BC points at byte after CRC in memory
;
; Registers changed after return:
;    ....DEHL/IXIY same
;    AFBC..../.... different
;
.CheckCrc
        ld      a,(hl)
        inc     hl
        cp      c
        jr      nz, return_crc_status
        ld      a,(hl)
        inc     hl
        cp      b
        jr      nz, return_crc_status
        ld      a,(hl)
        inc     hl
        cp      e
        jr      nz, return_crc_status
        ld      a,(hl)
        inc     hl
        cp      d
.return_crc_status
        scf
        ccf                                     ; return Fc = 0 always
        ret                                     ; Fz indicates CRC status
; *************************************************************************************


; *************************************************************************************
;
; GetSym - read a symbol from the current position of the file's current line.
;
;  IN:    None.
; OUT:    A = symbol identifier
;         (sym) contains symbol identifier
;         (Ident) contains symbol (beginning with a length byte)
;         (lineptr) is updated to point at the next character in the line
;
; Registers changed after return:
;
;    ..BCDEHL/IXIY  same
;    AF....../....  different
;
.GetSym
        push    bc
        push    de
        push    hl

        xor     a
        ld      de, ident                       ; DE always points at length byte...
        ld      (de),a                          ; initialise Ident to zero length
        ld      bc, ident+1                     ; point at pos. for first byte

        ld      hl,(lineptr)
.skiplead_spaces
        ld      a,(hl)
        cp      cr
        jr      z, newline_symbol               ; CR or CRLF as newline
        cp      lf
        jr      z, newline_symbol               ; LF as newline

        cp      0
        jr      z, nonspace_found               ; EOL reached
        call    isspace
        jr      nz, nonspace_found
        inc     hl                              ; white space...
        jr      skiplead_spaces
.nonspace_found
        push    hl                              ; preserve lineptr
        ld      hl, separators
        call    strchr                          ; is byte a separator?
        pop     hl
        jr      nz, separ_notfound

        ; found a separator - return
        ld      (sym),a                         ; pos. in string is separator symbol
        inc     hl
        ld      (lineptr),hl                    ; prepare for next read in line
        jr      exit_getsym
.newline_symbol
        ld      a,sym_newline
        ld      (sym),a
        jr      exit_getsym
.separ_notfound
        ld      a,(hl)                          ; get first byte of identifier
        cp      '$'                             ; identifier a hex constant?
        jr      z, found_hexconst
        cp      '@'                             ; identifier a binary constant?
        jr      z, found_binconst
        call    isdigit                         ; identifier a decimal constant?
        jr      z, found_decmconst
.test_alpha
        call    isalpha                         ; identifier a name?
        jr      nz, found_rubbish
.found_name
        ld      a,sym_name
        jr      read_identifier
.found_decmconst
        ld      a, sym_decmconst
        jr      fetch_constant
.found_hexconst
        ld      a,sym_hexconst
        jr      fetch_constant
.found_binconst
        ld      a,sym_binconst
        jr      fetch_constant
.found_rubbish
        ld      a,sym_nil
.read_identifier
        ld      (sym),a                         ; new symbol found - now read it...
        xor     a                               ; Identifier has initial zero length
.name_loop
        cp      MaxIdLength                     ; identifier reached max. length?
        jr      z,exit_getsym
        ld      a,(hl)                          ; get byte from current line position
        call    isspace
        jr      z, ident_complete               ; separator encountered...
        push    hl
        ld      hl,separators                   ; test for other separators
        call    strchr
        pop     hl
        jr      z, ident_complete               ; another separator encountered
        ld      a,(hl)
        call    isalnum                         ; byte alphanumeric?
        jr      nz, illegal_ident
        call    toupper                         ; name is converted to upper case
        ld      (bc),a                          ; new byte in name stored
        inc     bc
        inc     hl
        ld      (lineptr),hl
        ex      de,hl
        inc     (hl)                            ; update length of identifer
        ld      a,(hl)
        ex      de,hl
        jr      name_loop                       ; get next byte for identifier
.illegal_ident
        ld      a,sym_nil
        ld      (sym),a
        jr      exit_getsym
.ident_complete
        xor     a
        ld      (bc),a                          ; null-terminate identifier
.exit_getsym
        ld      a,(sym)
        pop     hl
        pop     de
        pop     bc
        ret
.fetch_constant
        ld      (sym),a                         ; new symbol found - now read it...
        xor     a
.constant_loop
        cp      MaxIdLength                     ; identifier reached max. length?
        jr      z,exit_getsym
        ld      a,(hl)                          ; get byte from current line position
        call    isspace
        jr      z, ident_complete               ; separator encountered...
        push    hl
        ld      hl,separators                   ; test for other separators
        call    strchr
        pop     hl
        jr      z, ident_complete               ; another separator encountered
        ld      a,(hl)
        call    toupper
        ld      (bc),a                          ; new byte of identifier stored
        inc     bc
        inc     hl
        ld      (lineptr),hl                    ; update lineptr variable
        ex      de,hl
        inc     (hl)                            ; update length of identifer
        ld      a,(hl)
        ex      de,hl
        jr      constant_loop                   ; get next byte for identifier
; *************************************************************************************


; ******************************************************************************
;
; IsSpace - check whether the ASCII byte is a white space
;
;  IN:    A = ASCII byte
; OUT:    Fz = 1, if byte was a white space, otherwise Fz = 0
;
; Registers changed after return:
;
;    A.BCDEHL/IXIY  same
;    .F....../....  different
;
.IsSpace
        cp      ' '
        ret     z                               ; byte = ' '
        ret     nc                              ; byte > ' ', not a white space
        cp      a                               ; 0 <= byte < ' '
        ret


; ******************************************************************************
;
; IsDigit - check whether the ASCII byte is a digit or not.
;
;  IN:    A = ASCII byte
; OUT:    Fz = 1, if byte was a digit, otherwise Fz = 0
;
; Registers changed after return:
;
;    A.BCDEHL/IXIY  same
;    .F....../....  different
;
.isdigit
        cp      '9'
        ret     nc                              ; byte >= '9'
        cp      '0'
        ret     c
        cp      a                               ; '0' <= byte < '9'
        ret


; ******************************************************************************
;
; IsAlpha - check whether the ASCII byte is an alphabetic character or not.
; The underscore character is defined as an alphabetic character.
;
;  IN:    A = ASCII byte
; OUT:    Fz = 1, if byte was alphabetic, otherwise Fz = 0
;
; Registers changed after return:
;
;    A.BCDEHL/IXIY  same
;    .F....../....  different
;
.IsAlpha
        cp      'z'
        ret     nc                              ; byte >= 'z'
        cp      'a'
        jr      c, test_underscore
        cp      a                               ; 'a' <= byte < 'z'
        ret
.test_underscore
        cp      '_'
        ret     nc                              ; '_' <= byte < 'a'
        cp      'Z'
        ret     nc                              ; 'z' <= byte < '_'
        cp      'A'
        ret     c                               ; byte < 'a', not alphabetic
        cp      a                               ; 'a' <= byte < 'z', alphabtic found
        ret


; ******************************************************************************
;
; IsAlNum - check whether the ASCII byte is an alphanumeric character or not.
;
;  IN:    A = ASCII byte
; OUT:    Fz = 1, if byte was alphanumeric, otherwise Fz = 0
;
; Registers changed after return:
;
;    A.BCDEHL/IXIY  same
;    .F....../....  different
;
.IsAlNum
        call    isdigit
        ret     z
        call    isalpha
        ret


; ******************************************************************************
;
; StrChr - find character in string.
;
;  IN:    A  = ASCII byte
;         HL = pointer to string, first byte defined as length byte.
; OUT:    Fz = 1: if byte was found in string.
;                 A = position of found character in string (0 = first).
;         Fz = 0, byte not found, A last position of string.
;
; Registers changed after return:
;
;    ..BCDEHL/IXIY  same
;    AF....../....  different
;
.StrChr
        push    bc
        push    hl
        ld      b,0
        ld      c,(hl)                          ; get length of string
        inc     hl                              ; point at first character
        cpir                                    ; search...
        pop     hl
        push    af                              ; preserve search flags
        inc     c
        ld      a,(hl)
        sub     c                               ; a = position of found char.
        ld      b,a
        pop     af                              ; restore search flags
        ld      a,b                             ; position in a
        pop     bc
        ret


; ******************************************************************************
;
; ToUpper - convert character to upper case if possible.
;
;  IN:    A = ASCII byte
; OUT:    A = converted ASCII byte
;
; Registers changed after return:
;
;    ..BCDEHL/IXIY  same
;    AF....../....  different
;
.ToUpper
        cp      '{'                             ; if a <= 'z'  &&
        jr      nc, check_latin1
        cp      'a'                             ; if a >= 'a'
        ret     c
        sub     32                              ; then a = a - 32
        ret
.check_latin1
        cp      $ff                             ; if a <= $fe  &&
        ret     z
        cp      $e1                             ; if a >= $e1
        ret     c
        sub     32                              ; then a = a - 32
        ret


; *************************************************************************************
;
; GetConstant - parse the current line for a constant (decimal, hex or binary)
;               and return a signed long integer.
;
;  IN:    None.
; OUT:    debc = long integer representation of parsed ASCII constant
;         Fc = 0, if integer collected, otherwise Fc = 1 (syntax error)
;
; Registers changed after return:
;
;    ......../IXIY  ........ same
;    AFBCDEHL/....  afbcdehl different
;
.GetConstant
        ld      hl,ident
        ld      a,(sym)
        cp      sym_hexconst
        jr      z, eval_hexconstant
        cp      sym_binconst
        jr      z, eval_binconstant
        cp      sym_decmconst
        jr      z, eval_decmconstant
        scf
        ret                                     ; not a constant...

.eval_binconstant
        ld      a,(hl)                          ; get length of identifier
        inc     hl
        inc     hl                              ; point at first binary digit
        dec     a                               ; binary digits minus binary id '@'
        cp      0
        jr      z, illegal_constant
        cp      9                               ; max 8bit binary number
        jr      nc, illegal_constant
        ld      b,a
        ld      c,0                             ; B = bitcounter, C = bitcollector
.bitcollect_loop
        rlc     c
        ld      a,(hl)                          ; get ASCII bit
        inc     hl
        cp      '0'
        jr      z, get_next_bit
        cp      '1'
        jr      nz, illegal_constant
        set     0,c
.get_next_bit
        djnz    bitcollect_loop
        push    bc                              ; all bits collected & converted in C
        exx
        ld      de,0                            ; most significant word of long
        pop     bc                              ; least significant word of long
        exx
        cp      a                               ; NB: bit constant always unsigned
        ret
.eval_hexconstant
        ld      a,(hl)                          ; get length of identifier
        inc     hl
        dec     a
        cp      0
        jr      z, illegal_constant
        cp      9
        jr      nc, illegal_constant            ; max 8 hex digits (signed long)
        ld      b,0
        ld      c,a
        add     hl,bc                           ; point at least significat nibble
        ld      de,longint                      ; point at space for long integer
        ld      c,0
        ld      (longint),bc                    ; clear long buffer (low word)
        ld      (longint+2),bc                  ; clear long buffer (high word)
        ld      b,a                             ; number of hex nibbles to process
.readhexbyte_loop
        ld      a,(hl)
        dec     hl
        call    convhexnibble                   ; convert towards most significant byte
        ret     c                               ; illegal hex byte encountered
        ld      (de),a                          ; lower nibble of byte processed
        dec     b
        jr      z, nibbles_parsed
        ld      c,a
        ld      a,(hl)
        dec     hl
        call    convhexnibble
        ret     c
        sla     a                               ; upper half of nibble processed
        sla     a
        sla     a
        sla     a                               ; into bit 7 - 4.
        or      c                               ; merge the two nibbles
        ld      (de),a                          ; store converted integer byte
        inc     de
        djnz    readhexbyte_loop                ; continue until all hexnibbles read
.nibbles_parsed
        exx
        ld      de,(longint+2)                  ; high word of hex constant
        ld      bc,(longint)                    ; low word of hex constant
        exx
        cp      a                               ; Fz = 1, successfully converted
        ret                                     ; return hex constant in debc
.eval_decmconstant
        inc     hl                              ; point at first char in identifier
        fpp     (fp_val)                        ; get value of ASCII constant
        ret     c                               ; Fz = 0, Fc = 1 - syntax error
        push    hl
        exx
        pop     de
        ld      b,h
        ld      c,l
        exx
        xor     a
        cp      c                               ; only integer format allowed
        ret
.illegal_constant
        scf                                     ; Fc = 1, syntax error
        ret
.ConvHexNibble
        cp      'A'
        jr      nc,hex_alpha                    ; digit >= "A"
        cp      '0'
        ret     c                               ; digit < "0"
        cp      ':'
        ccf
        ret     c                               ; digit > "9"
        sub     48                              ; digit = ["0"; "9"]
        ret
.hex_alpha
        cp      'G'
        ccf
        ret     c                               ; digit > "F"
        sub     55                              ; digit = ["A"; "F"]
        ret
; *************************************************************************************


; *************************************************************************************
; Read string from current position in config file into DE pointer memory
; Max. B chars is read. String is null-terminated in variable.
;
.GetString
        ld      hl,(lineptr)                    ; point at first char of bank image filename
.bnkflnm_loop
        dec     b
        jr      z, fetched_flnm                 ; read max 127 chars of filename...
        ld      a,(hl)
        cp      '"'
        jr      z, fetched_flnm
        ld      (de),a
        inc     hl
        inc     de
        jr      bnkflnm_loop
.fetched_flnm
        inc     hl                              ; move beyond " terminator...
        ld      (lineptr),hl                    ; (to get ready to read next symbol)
        xor     a
        ld      (de),a                          ; null-terminate filename string
        ret
; *************************************************************************************


; *************************************************************************************
; Open bank file in RAM filing system (for reading only), which is specified in
; [bankfilename] variable.
;
; Returns the usual GN_Opf file parameters.
;
.OpenRamBankFile
        ld      bc,MaxFileNameSize
        ld      hl,bankfilename                 ; (local) filename to card image
        ld      d,h
        ld      e,l                             ; output buffer for expanded filename (max 128 byte)...
        ld      a, OP_IN
        oz      GN_Opf
        ret
; *************************************************************************************


; *************************************************************************************
; Program Progress message "CRC Checking <BankFile> bank file."
;
.MsgCrcCheckBankFile
        ld      hl,crcbankfile1_msg
        oz      GN_Sop

        ld      hl,bankfilename
        oz      GN_Sop
        ld      hl,crcbankfile2_msg
        oz      GN_Sop
        ret
; *************************************************************************************


; *************************************************************************************
; A syntax error was encountered in the configuration file
;
.ErrMsgCfgSyntax
        ld      hl,cfgsyntax1_msg
        oz      GN_Sop
        ld      bc,(cfgfilelineno)

        call    DisplayNumber
        ld      hl,cfgsyntax2_msg               ; "Syntax error at line X in 'romupdate.cfg' file."
        oz      GN_Sop
        cp      a                               ; skip display of system error code - press key to continue
        jp      UpdError
; *************************************************************************************


; *************************************************************************************
; Display number in BC to stdout
;
.DisplayNumber
        push    bc
        ld      bc, NQ_Shn
        oz      OS_Nq                           ; get screen handle in IX
        pop     bc

        xor     a
        ld      d,a
        ld      e,a                             ; result to stream IX (screen)
        ld      h,a
        ld      l,2                             ; integer in BC to be converted to Ascii
        ld      a,1                             ; no leading spaces
        oz      GN_Pdn                          ; output result to current window...
        ret

; *************************************************************************************
.ErrMsgCrcFailBankFile
        ld      hl,crcerr_bfile
        oz      GN_Sop
        ld      hl,bankfilename
        oz      GN_Sop
        oz      GN_nln
        oz      GN_nln

        cp      a                               ; skip display of system error code - press key to continue
        jp      UpdError


; *************************************************************************************
.DrawUpdWd
        ld      b,0
        ld      hl,UpdateWdBlock
        OZ      GN_Win
        oz      OS_Pout                         ; invisible window area
        defm    1,"6#2",$20+13,$20+2,$20+58,$20+6
        defm    1,"2C2",0
        ret

.CfgFilename
        defm    ":RAM.?/ozs?-*.upd",0         ; the config file containing binary filenames and CRC

.crcbankfile1_msg
        defm    "CRC Checking ",0
.crcbankfile2_msg
        defm    " bank file",CR,LF,0
.crcerr_bfile
        defm    "CRC check failed for ", 0
.cfgsyntax1_msg
        defm    "Syntax error at line ",0
.cfgsyntax2_msg
        defm    " in 'ozupdate.cfg' file.",CR,LF,0

.UpdateWdBlock
        defb    @10110000 | 2
        defw    $0009
        defw    $0840
        defw    UpdateBanner
.UpdateBanner
        defm    "OPERATING SYSTEM UPDATE",0

.separators
        defb    end_separators - start_separators
.start_separators
        defm    0, '"', "'", ";,.({})+-*/%^=&~|:!<>#", 13, 10
.end_separators
