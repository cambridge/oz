; **************************************************************************************************
; Index popdown and DC system calls.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        module  install

        include "blink.def"
        include "char.def"
        include "director.def"
        include "dor.def"
        include "error.def"
        include "fileio.def"
        include "integer.def"
        include "memory.def"
        include "serintfc.def"
        include "stdio.def"
        include "syspar.def"
        include "time.def"
        include "printer.def"
        include "sysvar.def"
        include "oz.def"
        include "z80.def"
        include "sysapps.def"
        include "keyboard.def"
        include "handle.def"
        include "dor.def"
        include "../apps/index/index.def"

        xdef    cmd_install, AskInstall, DoInstall
        xdef    ci_ixtst, ci_CloseMem

        xref    AreYouSure, PressEsc, InitKeys
        xref    MainLoop, IndexReEntry, IndexRedraw

;       ----

.cmd_install                                    ; <>INS command
        call    DrawInstWd

        oz      OS_Pout
        defm    CR,LF,1,"2+CFilename : ",0

        ld      a, 8+32
        ld      de, App_Buff1
        ld      b, 128
        ld      l, 20
        oz      gn_sip
        cp      5                               ; ENTER command
        jr      z, AskInstall
        xor     a
        ld      (ubIdxActiveWindow), a
        jp      IndexRedraw
        
;       ----

.AskInstall
        ld      a, 4
        ld      (ubIdxActiveWindow), a

        call    DrawInstWd

        oz      OS_Pout
        defm    "Install ",0

        ld      hl, App_Buff1
        oz      GN_Sop
        oz      GN_Nln
        oz      GN_Nln
        ld      a, 'y'
        ld      (ubIdxYesNo), a
        call    AreYousure
        jp      MainLoop

.DrawInstWd
        ld      b,0
        ld      hl,InstallWdBlock
        OZ      GN_Win
        oz      OS_Pout                         ; invisible window area
        defm    1,"6#2",$20+13,$20+2,$20+58,$20+6
        defm    1,"2C2",0
        ret

.InstallWdBlock
        defb    @10110000 | 2
        defw    $0009
        defw    $0840
        defw    InstallBanner
.InstallBanner
        defm    "INSTALL APPLICATION",0

;       ----

.DoInstall
        xor     a
        ld      (ubIdxActiveWindow), a          ; Wd #0 if 'No'

        ld      a, (ubIdxYesNo)
        cp      'y'
        jp      nz, IndexRedraw

        ld      a, 3
        ld      (ubIdxActiveWindow), a          ; just an info wd

        call    DrawInstWd

; Copy APP file data to unsafe workspace

        ld      hl, 0
        add     hl, sp
        ld      (App_Stack), hl                 ; save stack for return and error box

        ld      a, 'P'
        ld      b, R20_BNK                      ; UWS is in bank $20
        ld      hl, App_Tag
        ld      de, 40                          ; copy 40 bytes (patches are not handled)
        call    ci_copyfile

; verify data (tag, number of bank, patches)

        ld      bc, APF_TAG                     ; identifier
        ld      hl, (App_Tag)
        or      a
        sbc     hl, bc
        jp      nz, ci_err_ftm

        ld      a, (App_BnkNb)
        cp      9
        jp      nc, ci_err_ftm                  ; no more than 8 banks

        ld      a, (App_PatchNb)
        or      a
        jp      nz, ci_err_ftm                  ; patches non implemented

; open memory, create installation handle and store data for uninstallation

        ld      a, MM_S1|MM_FIX|MM_MUL
        ld      bc, 0
        oz      OS_Mop
        jp      c, ci_error
        push    ix
        pop     hl
        ld      (App_MemHnd), hl
        ld      a, FN_AH                        ; allocate new handle
        ld      b, HND_INST                     ; type = installation handle
        oz      OS_Fn                           ; do it (hl is preserved)
        jp      c, ci_error
        ld      (ix+ihnd_MemHnd), l             ; keep MemHnd for uninstallation in INST handle
        ld      (ix+ihnd_MemHndH), h
        push    ix                              ; given installation handle
        pop     hl
        ld      (App_InstHnd), hl

; allocate memory and copy files

        ld      iy, App_AP0_Offset              ; start of memory table (32+32 bytes)
        ld      c, 0                            ; loop number (0-7)
        ld      ix, (App_MemHnd)                ; reload memory handle

.ci_allocmem
        ld      h, (iy+01)                      ; offset MSB = number of pages required
        ld      b, (iy+02)                      ; length LSB
        ld      l, (iy+03)                      ; length MSB = start page
        xor     a
        or      l
        jr      nz, ci_verifypagecross          ; page required >= 1
        or      b
        jr      z, ci_pagelenok                 ; no page required
        inc     l                               ; 1 page required
.ci_verifypagecross
        ld      a, (iy+00)                      ; offset LSB
        add     a, b
        jr      nc, ci_pagelenok
        inc     l                               ; add 1 page if LSBoffset+LSBLength>255
.ci_pagelenok
        xor     a
        ld      (iy+35), a                      ; 0 for 1st byte (unused)
        ld      (iy+34), l                      ; store length in page
        ld      (iy+33), h                      ; store offset page
        ld      a, h                            ; h=offset, l=length
        or      l
        ld      b, 0                            ; store 0 if length=offset=0
        jr      z, ci_nofma
        push    bc                              ; preserve C (loop number)
        push    hl                              ; preserve offset, length
        ld      hl, App_Even
        ld      b, c
        inc     b
        xor     a
        ld      c, (hl)
        pop     hl
        push    hl                              ; reload HL for OS_Fma
.ci_even
        rrc     c
        djnz    ci_even                         ; rotate until bit=APx
        jr      nc, ci_fma
        ld      a, FM_EVN                       ; bit set = ask an even bank
.ci_fma
        or      FM_FIX|FM_ANY
        oz      OS_Fma
        jp      c, ci_error
        pop     hl
        pop     de
        ld      c, e                            ; reload loop number
.ci_nofma
        ld      (iy+32), b                      ; store bank found
        ld      a, b
        or      a
        jr      z, ci_allocmem_1                ; dont allocate if bank = 0
        push    bc
        oz      OS_Axm                          ; allocate memory area
        jp      c, ci_error
        pop     bc                              ; loop number was in C
        ld      a, c
        add     a, $30                          ; to ascii for .AP+'0...7'
        ld      e, (iy+00)                      ; offset LSB
        ld      d, (iy+01)                      ; offset MSB
        ex      de, hl                          ; destination in BHL
        ld      e, (iy+02)                      ; length LSB
        ld      d, (iy+03)                      ; length MSB
        call    ci_copyfile

.ci_allocmem_1
        ld      de, 4
        add     iy, de
        inc     c
        ld      a, c
        cp      7
        jr      nz, ci_allocmem

; chain DORs and translate banks in absolute numbers

        ld      hl, App_DOR
        ld      e, (hl)
        inc     hl
        ld      a, (hl)
        and     $3F
        ld      d, a
        inc     hl
        ld      a, (hl)
        call    ci_TranslateBank
        ld      c, a
        or      d
        or      e
        jr      nz, ci_ChainFirstDOR
        ld      a, 63                           ; ROM front DOR is always in bank 63
        call    ci_TranslateBank
        ld      b, a
        ld      c, MS_S1
        push    bc
        rst     OZ_Mpb
        pop     bc
        ld      hl, $7FC6                       ; son is at $3FC6 in S1
        ld      e, (hl)
        inc     hl
        ld      a, (hl)
        and     $3F
        ld      d, a                            ; sometimes DOR point outside 0000-3FFF...
        inc     hl
        ld      a, (hl)
        call    ci_TranslateBank
        ld      c, a

.ci_ChainFirstDOR
        ld      ix, (App_InstHnd)               ; store DOR to installation handle as identifier
        ld      (ix+ihnd_DOR), e
        ld      (ix+ihnd_DORH), d
        ld      (ix+ihnd_DORB), c
        ld      iy, $4060
        push    bc
        ld      b, R21_BNK                      ; bind :APP.- Front DOR bank in S1
        ld      c, MS_S1
        rst     OZ_Mpb
        pop     bc
        ld      l, (iy+DOR_SON)
        ld      h, (iy+DOR_SON_H)
        ld      b, (iy+DOR_SON_B)
        ld      a, b
        or      h
        or      l
        jr      z, ci_chainDORson

.ci_gotobrother                                 ; goto last DOR
        push    bc
        ld      c, MS_S1                        ; bind B
        rst     OZ_Mpb
        pop     bc
        ld      a, h
        and     $3F
        or      MM_S1
        ld      h, a
        push    hl
        pop     iy                              ; ld iy, hl
        ld      l, (iy+DOR_BROTHER)
        ld      h, (iy+DOR_BROTHER_H)
        ld      b, (iy+DOR_BROTHER_B)
        ld      a, b
        or      a
        jr      nz, ci_gotobrother

        ld      (iy+DOR_BROTHER), e             ; chain CDE to last DOR as brother DOR
        ld      (iy+DOR_BROTHER_H), d
        ld      (iy+DOR_BROTHER_B), c
        jr      ci_TranslateDORs

.ci_chainDORson
        ld      (iy+DOR_SON), e                 ; chain CDE to :APP.- Front DOR as son DOR
        ld      (iy+DOR_SON_H), d
        ld      (iy+DOR_SON_B), c

.ci_TranslateDORs
        inc     (ix+ihnd_AppNbOf)               ; inc number of application installed

        ld      b, c
        ld      c, MS_S1
        rst     OZ_Mpb                          ; bind DOR bank in S1

        ld      a, d
        and     $3F
        or      MM_S1
        ld      d, a
        push    de
        pop     iy

        ld      a, (iy+ADOR_SEG0)               ; translate Segment 0 bank
        call    ci_TranslateBank
        ld      (iy+ADOR_SEG0), a
        ld      a, (iy+ADOR_SEG1)               ; translate Segment 1 bank
        call    ci_TranslateBank
        ld      (iy+ADOR_SEG1), a
        ld      a, (iy+ADOR_SEG2)               ; translate Segment 2 bank
        call    ci_TranslateBank
        ld      (iy+ADOR_SEG2), a
        ld      a, (iy+ADOR_SEG3)               ; translate Segment 3 bank
        call    ci_TranslateBank
        ld      (iy+ADOR_SEG3), a
        ld      a, (iy+ADOR_TOPICS_B)           ; translate Topics bank
        call    ci_TranslateBank
        ld      (iy+ADOR_TOPICS_B), a
        ld      a, (iy+ADOR_COMMANDS_B)         ; translate Commands bank
        call    ci_TranslateBank
        ld      (iy+ADOR_COMMANDS_B), a
        ld      a, (iy+ADOR_HELP_B)             ; translate Help bank
        call    ci_TranslateBank
        ld      (iy+ADOR_HELP_B), a
        ld      a, (iy+ADOR_TOKENS_B)           ; translate Token bank if <>$FFFFFF
        and     (iy+ADOR_TOKENS+1)
        or      @11000000                       ; could be $3F3FFF
        and     (iy+ADOR_TOKENS)
        inc     a
        ld      a, (iy+ADOR_TOKENS_B)           ; reload Token bank
        call    nz, ci_TranslateBank

        ld      (iy+ADOR_TOKENS_B), a
        ld      e, (iy+DOR_BROTHER)
        ld      d, (iy+DOR_BROTHER_H)
        ld      a, (iy+DOR_BROTHER_B)           ; brother bank
        call    ci_TranslateBank
        ld      (iy+DOR_BROTHER_B), a
        ld      c, a
        or      a
        jr      nz, ci_TranslateDORs            ; not zero, has brother (s)

; return to Index asking a key letters refresh

        ld      hl, (App_Stack)
        ld      sp, hl                          ; restore stack

        call    InitKeys                        ; repoll keys

        oz      OS_Pout
        defm    1,"2JCInstallation completed."
        defm    1,"3@",$20+0,$20+5,0

        call    PressEsc
        jp      MainLoop

; -----------
; subroutines

.ci_err_ftm
        ld      a, RC_Ftm
        scf

.ci_error
        push    af
        ld      ix, (App_InstHnd)
        call    ci_ixtst
        call    nz, ci_FreeInstHnd
        
        ld      ix, (App_MemHnd)
        call    ci_ixtst
        call    nz, ci_CloseMem
        
        ld      ix, (App_FileHnd)
        call    ci_ixtst
        call    nz, ci_CloseFile

        pop     af
        ld      hl, (App_Stack)
        ld      sp, hl                          ; restore stack

        ld      (ubIdxErrorCode), a
        ld      hl, ubIdxFlags2
        set     IDXF2_B_ERROR, (hl)
        xor     a
        ld      (ubIdxActiveWindow), a
        jp      IndexRedraw

.ci_FreeInstHnd
        ld      hl, 0
        ld      (App_InstHnd), hl
        ld      a, FN_FH
        ld      b, HND_INST
        oz      OS_Fn
        ret

.ci_CloseMem
        ld      hl, 0
        ld      (App_MemHnd), hl
        oz      OS_Mcl
        ret

.ci_CloseFile
        ld      hl, 0
        ld      (App_FileHnd), hl
        oz      GN_Cl
        ret

.ci_ixtst
        ld      a, ixh
        or      a
        ret     nz
        ld      a, ixl
        or      a
        ret

.ci_copyfile
        push    ix
        push    bc
        push    hl                              ; offset
        push    de                              ; length
        push    bc                              ; bank in B
        push    af                              ; A=extension type 'P' or '0...7'
        ld      hl, App_Buff1
        ld      bc, 128
        xor     a
        cpir                                    ; find terminating null of explicit filename
        dec     hl
        ld      bc, 256
        ld      a, '.'
        cpdr                                    ; find extension
        ld      a, c
        cp      251                             ; check for correct extension length
        jp      nz, ci_err_ftm
        inc     hl
        inc     hl                              ; HL points to start of 3-char extension
        ld      a, (hl)
        inc     hl
        or      $20
        cp      'a'
        jp      nz, ci_err_ftm                  ; first character not A/a
        ld      a, (hl)
        inc     hl
        pop     af                              ; 'P' or '0...7'
        ld      (hl), a
        ld      hl, App_Buff1
        ld      b, 0
        ld      de, App_Buff2
        ld      c, 255
        ld      a, OP_IN
        oz      GN_Opf
        jp      c, ci_error
        ld      (App_FileHnd), ix
        pop     bc                              ; bind bank in S1
        ld      c, MS_S1
        rst     OZ_Mpb
        pop     bc                              ; length
        pop     de
        ld      a, d
        and     @00111111
        or      @01000000
        ld      d, a
        ld      hl, 0
        oz      OS_Mv
        jp      c, ci_error
        oz      GN_Cl                           ; close file
        ld      hl, 0
        ld      (App_FileHnd), hl
        pop     bc
        pop     ix
        ret

.ci_TranslateBank
        or      a
        ret     z                               ; dont process bank 0
        push    bc
        push    hl
        or      @11111000                       ; F8-FF
        cpl                                     ; 00-07
        ld      c, a
        sla     c
        sla     c                               ; *4
        ld      b, 0
        ld      hl, Fma_AP0                     ; first byte of entry in Fma table is bank allocated
        add     hl, bc
        ld      a, (hl)
        pop     hl
        pop     bc
        ret

