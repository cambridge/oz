; **************************************************************************************************
; Index popdown.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************

        Module Index

        include "director.def"
        include "dor.def"
        include "error.def"
        include "fileio.def"
        include "eprom.def"
        include "integer.def"
        include "memory.def"
        include "stdio.def"
        include "syspar.def"
        include "time.def"
        include "sysvar.def"
        include "sysapps.def"
        include "oz.def"
        include "saverst.def"

        include "../os/lowram/lowram.def"
        include "../apps/index/index.def"

xdef    Index
xdef    ldIX_DE
xdef    MainLoop, IndexReEntry, IndexRedraw
xdef    GetAppByNum, AreYousure, PressEsc, InitKeys, GetProcessByNum

xref    InitProc, AddProc, GetNextProc, GetProcHandle, GetProcByHandle, GetProcEnvIDHandle
xref    PutProcHndl_IX
xref    ReadDateTime
xref    GetLinkBHL
xref    ZeroMem
xref    FreeRam
xref    ClsMapWin2, ClsMapWin4, UpdateFreeSpaceRamCard
xref    cmd_info, Updated
xref    cmd_install, AskInstall, DoInstall
xref    cmd_remove, DoRemove
xref    cmd_nq
xref    cmd_update, DoUpdate


; **************************************************************************************************
;
.Index
        xor     a
        ld      h, a
        ld      l, a
        OZ      OS_Erh                          ; default error handler

        ld      a,OZVERSION                     ; OZ V4.5 and later implements OS_Nq, NQ_Voz (verify OZ)
        ld      bc,NQ_Voz
        oz      OS_Nq                           ; This application only runs on latest ROM by principle
        jr      nc, IndexEntry
        xor     a
        oz      OS_Bye                          ; return to Index in current running OZ... (not the OZ of these sources)

.EprBootCLI
        defm    ":EPR.?/Boot.cli",0             ; The global EPR /Boot.cli filename (anywhere in any file area)
.RamBootCLI
        defm    ":RAM.?/Boot.cli",0             ; The global RAM /Boot.cli filename (anywhere in any RAM)

.OpenBootCli
        ex      de, hl                          ; filename in DE
        ld      hl, -256
        add     hl, sp
        ld      sp, hl
        ex      de, hl                          ; filename in HL, stackbuffer in DE for explicit filename
        ld      bc, $FF
        ld      a, OP_IN                        ; schedule a "/Boot.cli" to be executed by CLI...
        OZ      GN_Opf                          ; open - BHL=name (B=0, HL=local ptr), A=mode, DE=exp. name buffer, C=buflen
        ex      af, af'
        ld      hl, 256
        add     hl, sp
        ld      sp, hl
        ex      af, af'
        ret     c                               ; Ups, not possible (file not found)...
        ld      b, 0
        ld      h, b
        ld      l, b
        OZ      DC_Icl                          ; Invoke new CLI and use file as input...
        ret     nc
        OZ      OS_Cl                           ; CLI could not be scheduled, close file handle
        ret                                     ; and continue Index initialisation

.IndexEntry

;       if this is first time Index is started we
;       allocate process block and run boot.cli

        ld      a, (ubIdxFlags2)
        bit     IDXF2_B_REENTRY, a
        jp      nz, IndexReEntry

        ld      a, (ubUpdated)                  ; update flag
        inc     a                               ; $FF if just updated
        call    z, Updated                      ; updated info window

        OZ      OS_Dom                          ; Allocate pIdxMemHandle - no check for errors
        ld      (pIdxMemHandle), ix
        xor     a                               ; now allocate proc struct for Index
        ld      bc, prc_SIZEOF
        OZ      OS_Mal                          ; Allocate memory
        ld      a, b
        ld      (eIdxIndexProc+2), a
        ld      (eIdxIndexProc), hl
        call    AddProc
        call    InitProc

        ld      (iy+prc_flags), PRCF_ISINDEX
        ld      bc, NQ_Wai
        OZ      OS_Nq                           ; returns static handle in IX
        call    PutProcHndl_IX                  ; also copies IX into de
        ex      de, hl
        ld      (pIdxCurrentProcHandle), hl
        ld      (pIdxMyProcHandle), hl
        call    ReadDateTime
        ld      a, IDXF2_REENTRY
        ld      (ubIdxFlags2), a                ; first run done
        call    InitKeys

        ld      hl, RamBootCLI                  ; First, try to execute ":Ram.*/Boot.cli"
        call    OpenBootCli
        jr      nc, IndexReEntry
        ld      hl, EprBootCLI                  ; Second, try to execute ":Epr.*/Boot.cli"
        call    OpenBootCli

.IndexReEntry

;       run autobooting application, if there's one

        ld      de, (pIdxAutoRunAppl)
        call    ldIX_DE
        jr      z, IndexMailBox

        ld      hl, 0                           ; don't run again
        ld      (pIdxAutoRunAppl), hl
        jp      enter_2

;       check mailbox for an install

.MailBoxName2
        defm    "NAME",0

.IndexMailBox
        ld      a, SR_RPD                       ; read mailbox
        ld      de, MailBoxName2                ; with a file
        ld      b, 0
        ld      hl, App_Buff1
        ld      c, 127
        oz      OS_Sr
        jp      nc, AskInstall

.IndexRedraw
        ld      a, (ubIdxFlags2)
        and     IDXF2_ERROR
        jp      nz, ShowExitError

        call    DrawWindow

;       ----

.MainLoop
        ld      hl, ubIdxPubFlags
        push    hl
        call    MayInitIndex
        pop     hl
        set     IDXF1_B_INSIDEOZ, (hl)
        OZ      OS_In                           ; read a byte from std. input
        call    MayInitIndex
        jr      nc, main_2

.main_1
        cp      RC_Quit
        jr      z, main_4
        cp      RC_Draw
        jr      z, IndexRedraw
        jr      MainLoop

.main_2
        cp      '0'
        jp      z,cmd_ramcrd_sel
        cp      '1'
        jp      z,cmd_ramcrd_sel
        cp      '2'
        jp      z,cmd_ramcrd_sel
        cp      '3'
        jp      z,cmd_ramcrd_sel
        cp      'Y'
        jp      z,cmd_insrmv_sel
        cp      'N'
        jp      z,cmd_insrmv_sel
        cp      'y'
        jp      z,cmd_insrmv_sel
        cp      'n'
        jp      z,cmd_insrmv_sel

        or      a                               ; if it isn't extended we ignore it
        jr      nz, MainLoop

        ld      hl, ubIdxPubFlags
        set     IDXF1_B_INSIDEOZ, (hl)
        OZ      OS_In                           ; read a byte from std. input
        call    MayInitIndex
        jr      c, main_1

        ld      hl, main_3
        dec     a                               ; validate input
        cp      15
        jr      nc, MainLoop

        rlca                                    ; fetch command vector and execute
        ld      c, a
        ld      b, 0
        add     hl, bc
        ld      a, (hl)
        inc     hl
        ld      h, (hl)
        ld      l, a
        jp      (hl)

.main_3
        defw cmd_right
        defw cmd_left
        defw cmd_up
        defw cmd_down
        defw cmd_enter
        defw cmd_escape
        defw cmd_kill
        defw cmd_card
        defw 0                                  ; cmd_purge
        defw cmd_info
        defw cmd_install
        defw cmd_remove
        defw cmd_nq
        defw cmd_update
        defw cmd_hard

.main_4
        xor     a
        OZ      OS_Bye                          ; Application exit

;       ----

.MayInitIndex
        push    af
        res     IDXF1_B_INSIDEOZ, (hl)
        bit     IDXF1_B_INIT, (hl)
        res     IDXF1_B_INIT, (hl)
        jr      z, mii_1
        call    InitIndex
        call    InitKeys
        call    DrawWindow
.mii_1
        pop     af
        ret

;       ----

;       hard reset (hidden command)

.cmd_hard
        ld      bc, R21_BNK << 8 | MS_S1
        rst     OZ_MPB                          ; Bank $21 in S1
        ld      h, MM_S1
        xor     a
        ld      l, a
        ld      (hl), a                         ; erase CT_RAM tag
        rst     $00                             ; will force a hard reset

;       ----

;       move from process window into application window

.cmd_left
        ld      a, (ubIdxActiveWindow)
        or      a
        jr      z, GoMainLoop                   ; already in application window
        cp      2
        jr      nc, GoMainLoop                  ; card, info, install, remove

        ld      a, (ubIdxNApplDisplayed)
        or      a
        jr      z, GoMainLoop                   ; no apps, exit

        call    RemoveHighlight
        xor     a
        ld      (ubIdxActiveWindow), a          ; appl window
        ld      a, (ubIdxSelectorPos)           ; pos=min(ubIdxSelectorPos,ubIdxNApplDisplayed-1)
        ld      c, a
        ld      a, (ubIdxNApplDisplayed)
        dec     a
        cp      c
        jr      nc, kl_1
        ld      (ubIdxSelectorPos), a
.kl_1
        call    prt_selinit_wd2                 ; select & init wd2
        jr      klr_2

;       ----

;       move from application window into process window

.cmd_right
        ld      a, (ubIdxActiveWindow)
        or      a
        jr      nz, GoMainLoop                  ; already in process window, card, info, install, remove exclude

        ld      a, (ubIdxNProcDisplayed)
        or      a
        jr      z, GoMainLoop                   ; no suspended processes, exit

        call    RemoveHighlight
        ld      a, 1                            ; process wd
        ld      (ubIdxActiveWindow), a
        ld      a, (ubIdxSelectorPos)           ; pos=min(ubIdxSelectorPos,ubIdxNProcDisplayed-1)
        ld      c, a
        ld      a, (ubIdxNProcDisplayed)
        dec     a
        cp      c
        jr      nc, kr_1
        ld      (ubIdxSelectorPos), a
.kr_1
        call    prt_selinit_wd3                 ; select & init wd3
.klr_2
        call    DrawHighlight

.GoMainLoop
        jp      MainLoop

;       ----

;       move up in application or process window

.cmd_up
        ld      a, (ubIdxActiveWindow)
        cp      2
        jp      nc, MainLoop                    ; card display

        call    RemoveHighlight
        ld      a, (ubIdxSelectorPos)
        or      a
        jr      z, up_1                         ; scroll if selector at top
        dec     a
        ld      (ubIdxSelectorPos), a           ; else just decrement position
        call    DrawHighlight
        jp      MainLoop

.up_1
        ld      a, (ubIdxActiveWindow)
        or      a
        jr      z, up_7                         ; appl window

;       process window

        ld      hl, ubIdxTopProcess
        inc     (hl)
        dec     (hl)
        jr      z, up_2                         ; do full redraw

        call    ScrollDown
        dec     (hl)
        ld      a,(hl)
        call    GetProcessByNum
        jp      c, Index                        ; error, shouldn't happen until confused!
        call    PrintProcess
        jr      up_6

.up_2
        xor     a                               ; !! unnecessary
        call    GetProcessByNum
        jp      c, Index                        ; error, confused!

        ld      a, 5                            ; !! is this necessary?
        ld      (ubIdxSelectorPos), a           ; !! ubIdxSelectorPos is updated below!

        ld      b, 1                            ; count processes
.up_3
        push    bc                              ; !! pre-increment to save 2 bytes
        call    GetNextProc
        pop     bc
        jr      c, up_4
        inc     b
        jr      up_3

.up_4
        ld      a, b
        cp      7
        jr      c, up_5

        sub     6                               ; more than one screenfull of processes
        ld      (ubIdxTopProcess), a
        call    DrawProcWindow
        ld      a, 6

.up_5
        dec     a                               ; select last process
        ld      (ubIdxSelectorPos), a

.up_6
        call    DrawHighlight
        jp      MainLoop

;       application window

.up_7
        ld      a, (ubIdxTopApplication)
        or      a
        jr      z, up_8                         ; do full redraw

        call    ScrollDown                      ; otherwise scroll and update
        ld      a, (ubIdxTopApplication)        ; !! push/pop
        dec     a
        ld      (ubIdxTopApplication), a
        call    GetAppByNum
        jp      c, Index                        ; confused

        ld      bc, NQ_Ain
        OZ      OS_Nq                           ; get static handle
        call    PrintApp
        jr      up_6

.up_8
        xor     a                               ; !! unnecessary
        call    GetAppByNum
        jp      c, Index                        ; confused

        ld      a, 5                            ; !! is this necessary ?
        ld      (ubIdxSelectorPos), a           ; !! ubIdxSelectorPos is updated below!

        ld      b, 1                            ; count applications
.up_9
        call    GetNextApp                      ; !! pre-increment
        jr      c, up_10
        inc     b
        jr      up_9

.up_10
        ld      a, b
        cp      7
        jr      c, up_11

        sub     6                               ; more than one screenfull of applications
        ld      (ubIdxTopApplication), a
        call    DrawAppWindow
        ld      a, 6

.up_11
        dec     a                               ; select last application
        ld      (ubIdxSelectorPos), a
        jr      up_6

;       ----

; Scroll window down and clear first line

.ScrollDown
        OZ      OS_Pout
        defm    1,$FE,0
        ld      bc, 0
        call    MoveXY_BC
        OZ      OS_Pout
        defm    1,"2C",$FD,0
        ret

;       ----

;       move down in application or process window

.cmd_down
        ld      a, (ubIdxActiveWindow)
        cp      2
        jp      nc, MainLoop                    ; card display

        call    RemoveHighlight
        ld      a, (ubIdxNApplDisplayed)
        ld      c, a
        ld      a, (ubIdxActiveWindow)
        or      a
        jr      z, dn_1                         ; application window
        ld      a, (ubIdxNProcDisplayed)
        ld      c, a
.dn_1
        ld      a, (ubIdxSelectorPos)
        inc     a
        cp      c
        jr      nc, dn_2                        ; past last displayed, scroll
        ld      a, (ubIdxSelectorPos)           ; otherwise just increment pos
        inc     a                               ; !! we already have this in A
        ld      (ubIdxSelectorPos), a
        jr      dn_x

.dn_2
        ld      a, (ubIdxActiveWindow)
        or      a
        jr      z, dn_4                         ; appl wd

;       process window

        ld      a, (ubIdxTopProcess)
        ld      c, a
        ld      a, (ubIdxNProcDisplayed)
        add     a, c
        call    GetProcessByNum                 ; get next proc
        jr      c, dn_3                         ; no more, redraw from top

        call    prt_scrollup
        ld      a, (ubIdxTopProcess)            ; increment top process
        inc     a
        ld      (ubIdxTopProcess), a
        ld      bc, 5
        call    MoveXY_BC
        call    PrintProcess
        jr      dn_x

.dn_3
        xor     a
        ld      (ubIdxSelectorPos), a
        ld      a, (ubIdxTopProcess)
        or      a
        jr      z, dn_x                         ; we're done if no more than 6 processes
        xor     a
        ld      (ubIdxTopProcess), a
        call    DrawProcWindow
        jr      dn_x

;       application window

.dn_4
        ld      a, (ubIdxTopApplication)
        ld      c, a
        ld      a, (ubIdxNApplDisplayed)
        add     a, c
        call    GetAppByNum                     ; get next appl
        jr      c, dn_5                         ; no more, redraw from top

        call    prt_scrollup
        ld      a, (ubIdxTopApplication)        ; increment top application
        inc     a
        ld      (ubIdxTopApplication), a
        ld      bc, 5
        call    MoveXY_BC
        ld      bc, NQ_Ain
        OZ      OS_Nq                           ; static handle in IX
        call    PrintApp
        jr      dn_x

.dn_5
        xor     a
        ld      (ubIdxSelectorPos), a
        ld      a, (ubIdxTopApplication)
        or      a
        jr      z, dn_x                         ; we're done if no more than 6 applications
        xor     a
        ld      (ubIdxTopApplication), a
        call    DrawAppWindow

.dn_x
        call    DrawHighlight
        jp      MainLoop

;       ----

;       return to interrupted application or process card display keyboard input

.cmd_ramcrd_sel
        ld      b,a
        ld      a, (ubIdxActiveWindow)
        cp      2                               ; active card display window?
        jp      nz, MainLoop
        ld      a,b                             ; card display window - allow '0', '1', '2' or '3' keys...
        call    UpdateFreeSpaceRamCard
        jp      MainLoop

;       ----

;       update and display Yes/No

.cmd_insrmv_sel
        ld      b,a
        ld      a, (ubIdxActiveWindow)
        cp      4                               ; only for Wd #4 and #5
        jp      c, MainLoop
        ld      a, b
        or      $20                             ; lower case
        ld      (ubIdxYesNo), a
        call    AreYousure                      ; display 'Are you sure ?'
        jp      MainLoop

;       ----

;       escape command

.cmd_escape
        ld      a, (ubIdxActiveWindow)
        cp      2                               ; card display, info display
        jr      c, esc_1

        xor     a                               ; User has ESCaped a display window
        ld      (ubIdxActiveWindow), a          ; ask to redraw a standard index
        call    InitIndex                       ; reset selectors
        call    ClsMapWin2
        call    ClsMapWin4
        jp      IndexRedraw

.esc_1
        push    iy
        ld      a, (eIdxProcList+2)             ; last running process at top
        ld      iy, (eIdxProcList)
        ld      b, a
        ld      c, MS_S1
        rst     OZ_MPB                          ; bind first process to S1
        push    bc
        call    GetLinkBHL                      ; get second process
        ld      d, b
        pop     bc
        rst     OZ_MPB                          ; restore S1
        pop     iy
        ld      b, d
        ld      a, b
        or      h
        or      l
        jp      z, MainLoop                     ; go back if there's only Index
        jr      ExitIndex                       ; return to appl

;       ----

;       start new or return to interrupted application

.cmd_enter
        ld      a, (ubIdxActiveWindow)
        cp      2
        jp      z, MainLoop                     ; card Wd, do nothing
        cp      3
        jp      z, MainLoop                     ; info Wd, do noting
        cp      4
        jp      z, DoInstall                    ; do install
        cp      5
        jp      z, DoRemove                     ; do removal
        cp      6
        jp      z, DoUpdate                     ; do OZ update
        or      a
        jr      z, enter_1

;       process window

        ld      a, (ubIdxTopProcess)
        ld      c, a
        ld      a, (ubIdxSelectorPos)
        add     a, c
        call    GetProcessByNum
        jp      c, Index                        ; confused
        ld      a, (iy+prc_flags)
        and     PRCF_ISINDEX
        jp      nz, MainLoop

        push    iy
        pop     hl
        jr      ExitIndex                       ; go restore it

;       application window

.enter_1
        ld      a, (ubIdxTopApplication)
        ld      c, a
        ld      a, (ubIdxSelectorPos)
        add     a, c
        call    GetAppByNum
        jp      c, Index                        ; confused

.enter_2
        ld      bc, NQ_Ain
        OZ      OS_Nq                           ; get application data
        and     AT_Ones                         ; only one run possible?
        jr      z, enter_3
        call    GetProcByHandle
        jr      nc, ExitIndex                   ; it's running already

.enter_3
        ld      b, 0                            ; start new

.ExitIndex
        ld      (pIdxRunProcIX), ix             ; store static handle
        ld      (eIdxRunProc), hl               ; and process structure
        ld      a, b
        ld      (eIdxRunProc+2), a
        xor     a
        OZ      OS_Bye                          ; exit Index, start eIdxRunProc

.ShowExitError
        ld      hl, ubIdxFlags2
        res     IDXF2_B_ERROR, (hl)
        ld      a, (ubIdxErrorCode)
        OZ      GN_Err                          ; Display an interactive error box
        call    DrawWindow
        jp      MainLoop

;       ----

;       <>KILL selected process

.cmd_kill
        ld      a, (ubIdxActiveWindow)
        or      a
        jp      z, MainLoop                     ; application window
                                                ; !! should check for card display!
        ld      a, (ubIdxTopProcess)
        ld      c, a
        ld      a, (ubIdxSelectorPos)
        add     a, c
        call    GetProcessByNum
        jp      c, Index                        ; confused
        push    iy
        ld      hl, ubIdxFlags2
        set     IDXF2_B_KILL, (hl)
        pop     hl
        jr      ExitIndex                       ; exit Index to let process kill itself

;       ----

;       full window redraw

.DrawWindow
        ld      a, (ubIdxActiveWindow)
        cp      2
        jp      z, DrawCardWd                   ; Wd #2 (Card)
                                                ; No need to redraw something for info, install, remove

        ld      b,0                             ; draw application list window frame
        ld      hl,ApplWdBlock
        OZ      GN_Win

        OZ      OS_Pout
        defm    1,"T", 1,"U"
        defm    " NAME          KEY"
        defm    1,"6#2",$20+1,$20+2,$20+18,$20+6
        defm    1,"2C2"
        defm    0

        ld      hl,ProcWdBlock                  ; draw process window frame
        OZ      GN_Win

        OZ      OS_Pout
        defm    1,"T", 1,"U"
        defm    "YOUR REF.        APPLICATION  WHEN SUSPENDED       CARDS"
        defm    1,"6#3",$20+21,$20+2,$20+56,$20+6
        defm    1,"2C3"
        defm    0

        call    DrawAppWindow
        call    DrawProcWindow

        ld      a, (ubIdxActiveWindow)
        or      a
        jr      z, dwd_1                        ; application window

        ld      a, (ubIdxOldProcRmCount)
        ld      c, a
        ld      a, (ubIdxProcRmCount)
        cp      c
        jr      z, dwd_1

        ld      (ubIdxOldProcRmCount), a        ; if process removed reset selector position
        xor     a
        ld      (ubIdxSelectorPos), a
        ld      a, (ubIdxNProcDisplayed)
        or      a
        jr      nz, dwd_1
        ld      (ubIdxActiveWindow),    a       ; activate appl window if no processes

.dwd_1
        ld      a, (ubIdxActiveWindow)
        or      a
        jr      nz, dwd_2

        call    prt_selinit_wd2                 ; select application window
.dwd_2
        jp      DrawHighlight

;       ----
.ApplWdBlock
        defb @10100000 | 2
        defw $0000
        defw $0812
        defw appl_banner
.appl_banner
        defm "APPLICATIONS",0

.ProcWdBlock
        defb @10100000 | 3
        defw $00014
        defw $0838
        defw proc_banner
.proc_banner
        defm "SUSPENDED ACTIVITIES",0


.DrawAppWindow
        call    prt_selinit_wd2                 ; select & init appl window
        call    prt_cls

        ld      b, 6                            ; lines to print
        ld      a, (ubIdxTopApplication)
        call    GetAppByNum
        jr      nc, appw_2                      ; got top appl
        ld      ix,0                            ; else get first application
.appw_1
        call    GetNextApp

.appw_2
        jr      c, appw_3                       ; no more appls, exit

        push    ix                              ; is this Index?
        ld      de, (pIdxMyProcHandle)
        ex      (sp), hl
        sbc     hl, de
        pop     hl
        jr      z, appw_1                       ; yes, skip

        push    bc
        ld      bc, NQ_Ain
        OZ      OS_Nq                           ; get application data
        call    PrintApp
        OZ      GN_Nln                          ; newline
        pop     bc
        djnz    appw_1

.appw_3
        ld      a, 6
        sub     b
        ld      (ubIdxNApplDisplayed), a
        ret

;       ----

.DrawProcWindow
        call    prt_selinit_wd3                 ; select & init proc window
        call    prt_cls
.actw_1
        ld      a, (ubIdxTopProcess)
        call    GetProcessByNum
        ld      b, 6                            ; lines to print
        jr      nc, actw_3                      ; got top proc

        ld      a, (ubIdxTopProcess)
        or      a
        jr      z, actw_4                       ; no procs, exit

        xor     a
        ld      (ubIdxTopProcess), a
        ld      a, (ubIdxActiveWindow)
        cp      1                               ; process wd? !! use sub
        jr      nz, actw_1
        xor     a                               ; yes, reset ubIdxSelectorPos
        ld      (ubIdxSelectorPos), a
        jr      actw_1

.actw_2
        push    bc
        call    GetNextProc
        pop     bc
        jr      c, actw_4                       ; no more procs, exit
.actw_3
        call    PrintProcess
        djnz    actw_2

.actw_4
        ld      a, 6
        sub     b
        ld      (ubIdxNProcDisplayed), a
        ret     nz

        OZ      OS_Pout                         ; no procs, display "NONE"
        defm    1,"3@",$20+0,$20+2
        defm    1,"2JC"
        defm    1,"T"
        defm    "NONE"
        defm    1,"T"
        defm    1,"2JN"
        defm    0
        ret

;       ----

;       get application number A

.GetAppByNum
        push    bc
        ld      b, a
        inc     b
        ld      ix,0                            ; start at first appl

.gabn_1
        call    GetNextApp
        jr      c, gabn_2                       ; error, exit
        djnz    gabn_1

.gabn_2
        pop     bc
        ret

;       ----

;       follow IX to next app, ignore Index

.GetNextApp
        push    de

.gna_1
        OZ      OS_Poll                         ; get next app
        jr      c, gna_2                        ; no more, exit

        push    ix                              ; cp ix, (pIdxMyProcHandle)
        ld      de, (pIdxMyProcHandle)
        ex      (sp), hl
        sbc     hl, de
        pop     hl
        jr      z, gna_1                        ; Index, ignore

        or      a                               ; Fc=0
.gna_2
        pop     de
        ret

;       ----

;       get process number A

.GetProcessByNum
        ld      e, a
        inc     e
        ld      iy, eIdxProcList                ; start at beginning

.gpbn_1
        call    GetNextProc
        ld      a, RC_Eof
        jr      c, gpbn_2                       ; no more, exit
        dec     e
        jr      nz, gpbn_1

        or      a                               ; Fc=0 !! unnecessary

.gpbn_2
        ret


.DrawHighlight
        call    prt_reverse
        call    RemoveHighlight
        jp      prt_reverse

;       ----

.RemoveHighlight
        ld      a, (ubIdxSelectorPos)
        ld      c, a
        ld      a, (ubIdxActiveWindow)
        or      a
        ld      b, 50                           ; prepare for application window
        jr      z, rh_1
        ld      b, 83                           ; process window
.rh_1
        push    bc
        ld      b, 0
        call    MoveXY_BC
        pop     bc
        ld      a, b
        jp      ApplyA

;       ----

;       print process information
;       IY=process

.PrintProcess
        push    bc
        push    ix

        push    iy
        pop     hl
        ld      bc, prc_Name                    ; process name ("YOUR REF")
        add     hl, bc
        OZ      GN_Sop                          ; write string to std. output

        ld      a, 17
        call    MoveX_A
        call    GetProcHandle
        push    de
        pop     ix
        ld      bc, NQ_Ain
        OZ      OS_Nq                           ; get application name
        OZ      OS_Bout                         ; print it

        ld      a, 30
        call    MoveX_A
        push    iy
        pop     hl
        ld      bc, prc_time                    ; suspension date/time
        add     hl, bc
        OZ      GN_Sdo                          ; print it

        ld      a, 53
        call    MoveX_A
        call    GetProcEnvIDHandle
        push    de
        pop     ix
        OZ      OS_Use                          ; get cards used
        jr      c, pract_x                      ; RC_Hand, skip printing
        or      a
        jr      z, pract_x
        ld      c, a                            ; slot
        cp      4
        ld      a, '-'                          ; installed application
        jr      z, pract_d
        ld      a, '0'
        add     a, c
.pract_d
        call    prt_tiny
        OZ      OS_Out
        call    prt_tiny
.pract_x
        oz      GN_Nln
        pop     ix
        pop     bc
        ret

;       ----

;       print application information
;       IX=application, BHL=name

.PrintApp
        ld      a, ' '
        OZ      OS_Out
        OZ      OS_Bout                         ; print appl name in BHL

        OZ      OS_Pout
        defm    1,"2JR",0

        ld      a, c                            ; command key
        cp      'A'                             ; done if not A-Y
        jr      c, prapp_4
        cp      'Z'
        jr      nc, prapp_4

        sub     'A'
        ld      c, a                            ; find key in key tables
        ld      b, 0
        ld      d, 1                            ; plain key
        ld      hl, IdxKeyTable
        add     hl, bc
        call    CompareIX_indHL
        jr      z, prapp_1                      ; found

        inc     d                               ; Zkey
        ld      hl, IdxZKeyTable
        add     hl, bc        
        call    CompareIX_indHL
        jr      z, prapp_1                      ; found

        inc     d                               ; ZZkey
        ld      hl, IdxZZKeyTable
        add     hl, bc
        call    CompareIX_indHL
        jr      nz, prapp_4                     ; not found, exit

.prapp_1
        OZ      OS_Pout
        defm    1,"*",0                         ; print []

        ld      b, d                            ; check if Zs to print
        jr      prapp_2b
.prapp_2
        ld      a, 'Z'
        OZ      OS_Out
.prapp_2b
        djnz    prapp_2
.prapp_3
        ld      a, c
        add     a, 'A'                          ; get command char back
        OZ      OS_Out
        ld      a, ' '
        OZ      OS_Out

.prapp_4
        jp      prt_justifyN

;       ----

.CompareIX_indHL
        ld      a, ixl
        cp      (hl)
        ret

;       ----

;       scan applications to define [] keys

.InitKeys
        ld      hl, IdxKeyTable                 ; clear all tables
        ld      b, 3*25
        call    ZeroMem

        ld      ix,0                            ; no autorun
        ld      (pIdxAutoRunAppl), ix

.initkeys_1
        OZ      OS_Poll                         ; get next app
        ret     c                               ; no more apps, exit

        ld      bc, NQ_Ain
        OZ      OS_Nq                           ; get app info
        bit     AT_B_Boot, a
        jr      z, initkeys_2

        ld      hl, (pIdxAutoRunAppl)
        ld      a, h
        or      l
        jr      nz, initkeys_2                  ; don't overwrite if there's one already
        ld      (pIdxAutoRunAppl), ix

.initkeys_2
        ld      a, c                            ; command key
        cp      'A'
        jr      c, initkeys_1
        cp      'Z'
        jr      nc, initkeys_1                  ; not A-X, loop

        sub     'A'
        ld      c, a
        ld      b, 0
        ld      hl, IdxKeyTable
        add     hl, bc
        ld      a, (hl)
        or      a
        jr      z, initkeys_3                   ; key free, insert

        ld      bc, 25
        add     hl, bc
        ld      a, (hl)
        or      a
        jr      z, initkeys_3                   ; key free, insert

        ld      bc, 25
        add     hl, bc
        ld      a, (hl)
        or      a
        jr      nz, initkeys_1                  ; no key free, next appl

.initkeys_3
        ld      a, ixl
        ld      (hl), a
        jr      initkeys_1                      ; check next app

;       ----

;       reset application and process windows

.InitIndex
        xor     a
        ld      (ubIdxSelectorPos), a
        ld      (ubIdxTopApplication), a
        ld      (ubIdxNApplDisplayed), a
        ld      (ubIdxTopProcess), a
        ld      (ubIdxNProcDisplayed), a
        ret

;       ----

;       ld IX, DE, check for zero

.ldIX_DE
        ld      a, d
        or      e
        push    de
        pop     ix
        ret

;       ----

.MoveXY_BC
        OZ      OS_Pout
        defm    1,"3@",0
        ld      a, $20
        add     a, b
        OZ      OS_Out                          ; X pos
        ld      a, $20
        add     a, c
        OZ      OS_Out                          ; Y pos
        ret

;       ----

.MoveX_A
        push    af
        OZ      OS_Pout
        defm    1,"2X",0
        pop     af
        add     a, $20
        OZ      OS_Out
        ret

;       ----

.prt_tiny
        OZ      OS_Pout
        defm    1,"T",0
        ret

.prt_reverse
        OZ      OS_Pout
        defm    1,"R",0
        ret

.prt_justifyN
        OZ      OS_Pout
        defm    1,"2JN",0
        ret

.prt_cls
        OZ      OS_Pout
        defm    1,"3@",$20+0,$20+0
        defm    1,"2C",$FE,0
        ret

.prt_selinit_wd2
        OZ      OS_Pout
        defm    1,"2I2",0
        ret

.prt_selinit_wd3
        OZ      OS_Pout
        defm    1,"2I3",0
        ret

.prt_scrollup
        OZ      OS_Pout
        defm    1,$FF,0
        ret

.ApplyA
        push    af
        OZ      OS_Pout
        defm    1,"2A",0
        pop     af
        OZ      OS_Out
        ret


.cmd_card
        ld      hl, CardData
        ld      b, 3*4                          ; each device info block is 3 bytes
        call    ZeroMem
        ld      d, 3                            ; start scan with slot 3, downwards...
        ld      iy, CardData+(3*4)

.poll_slot_loop                                 ; slot loop
        ld      bc, -3
        add     iy, bc

        ld      a,d                             ; poll for total RAM in slot D
        ld      bc, NQ_Mfp
        push    de
        oz      OS_Nq
        pop     de
        jr      c, check_rom                    ; no RAM found, is there a ROM / App Card?
        ld      (iy+0),a                        ; size of RAM card in 16K banks...

.check_rom
        ld      e,$3f                           ; top bank of slots 1-3 for ROM / App Cards is $3f
        inc     d
        dec     d
        jr      nz, poll_rom_bank
        ld      e,$1f                           ; top bank of ROM in slot 0 is $1f
.poll_rom_bank
        ld      bc, NQ_Slt
        oz      OS_Nq                           ; read slot type information
        jr      c, next_slot                    ; no card?
        cp      BU_ROM
        jr      nz, check_eprom

        ld      a,d
        and     @00000011
        rrca
        rrca                                    ; slot number -> to slot mask
        or      e
        ld      b,a                             ; absolute (top) bank number of ROM / App Card
        ld      hl,$3ffc
        oz      GN_Rbe
        ld      (iy+2),a                        ; size of ROM / App Card in 16K banks

.check_eprom
        ld      c,d
        push    de
        ld      a,EP_Req
        oz      OS_Epr                          ; File Area in slot D?
        pop     de
        jr      c, next_slot
        jr      nz, next_slot                   ; no File area, look in next slot..
        ld      (iy+1),c                        ; size of file area in 16K banks...
.next_slot
        dec     d
        ld      a,d
        cp      -1
        jr      nz, poll_slot_loop              ; loop thru slots 0-3

        ld      a, 2                            ; card display
        ld      (ubIdxActiveWindow), a
        xor     a
        ld      (ubIdxSelectorPos), a
        jp      IndexRedraw

;       ----

.DrawCardWd
        ld      b,0
        ld      hl,CardWdBlock
        OZ      GN_Win

        OZ      OS_Pout
        defm    1,"3@",$20+16,$20
        defm    1,"T"
        defm    "APPS"
        defm    1,"2X",$20+26
        defm    "FILES"
        defm    1,"2X",$20+37
        defm    "RAM"
        defm    1,"3@",$20+0,$20+0
        defm    1,"3@",$20+0,$20+1
        defm    " SLOT 0",13,10
        defm    " SLOT 1",13,10
        defm    " SLOT 2",13,10
        defm    " SLOT 3"
        defm    1,"3@",$20+0,$20+6
        defm    1,"2JC"
        defm    "PRESS ", 1, SD_ESC, " TO RESUME"
        defm    1,"2JN"
        defm    1,"T",1,"C"
        defm    0

        call    DisplayCards
        call    FreeRam
        jp      MainLoop

.CardWdBlock
        defb @10110000 | 4
        defw $0009
        defw $082E
        defw cards_banner
.cards_banner
        defm "CARDS",0

.DisplayCards
        ld      d, 3

.dcrds_1
        ld      a, d
        call    GetCardData
        call    DisplayCard
        dec     d
        ld      a,d
        cp      -1
        jr      nz, dcrds_1
        ret

;       ----

.DisplayCard
        push    de
        ld      b, 0
        ld      c, d
        inc     c
        call    MoveXY_BC

        ld      c, (iy+2)
        ld      a, 15
        call    PrintCardSize                   ; ROM

        ld      c, (iy+1)
        ld      a, 25
        call    PrintCardSize                   ; EPROM

        ld      c, (iy+0)
        ld      a, 35
        call    PrintCardSize                   ; RAM

        pop     de
        ret

;       ----

.PrintCardSize
        push    de
        call    MoveX_A

        push    bc
        ld      bc, NQ_Out
        OZ      OS_Nq                           ; get stdout
        pop     bc

        ld      b, 0
        ld      h, b
        ld      l, c
        add     hl, hl
        add     hl, hl
        add     hl, hl
        add     hl, hl                          ; *16
        ld      d, b
        ld      e, b
        ld      c, l
        ld      b, h
        ld      a, b
        or      c
        jr      nz, pcs_1
        OZ      OS_Pout
        defm    "   -",0
        jr      pcs_2
.pcs_1
        ld      hl, 2
        ld      a, $40
        OZ      GN_Pdn                          ; BC to ASCII, 4 chars
        ld      a, 'K'
        OZ      OS_Out
.pcs_2
        pop     de
        ret

;       ----

;       point IY to data of card A

.GetCardData
        ld      iy, CardData-3
        ld      bc, 3
.gcd_1
        add     iy, bc
        dec     a
        cp      -1
        jr      nz, gcd_1
        ret

;       ----

.AreYousure
        oz      OS_Pout
        defm    1,"2X",32,1,"2C",253            ; clear line
        defm    "Are you sure ? ",0
        ld      a, (ubIdxYesNo)
        or      $20
        cp      'y'
        jr      z, SureYes
        oz      OS_Pout
        defm    "No",BS,BS,1,"2+C",0
        ret
.SureYes
        oz      OS_Pout
        defm    "Yes",BS,BS,BS,1,"2+C",0
        ret

;       ----

.PressEsc
        oz      OS_Pout
        defm    1,"2JC",1,"TPRESS ",1,SD_ESC," TO RESUME",1,"T",1,"2JN",0
        ret
