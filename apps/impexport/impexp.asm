; **************************************************************************************************
; Imp/export popdown (Bank 1, addressed for segment 3).
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; Source code was reverse engineered from OZ 4.0 (UK) ROM and made compilable by Jorma Oksanen.
; Additional development improvements, comments, definitions and new implementations by
; (C) Jorma Oksanen (jorma.oksanen@gmail.com), 2003
; (C) Thierry Peycru (pek@users.sf.net), 2005
; (C) Gunther Strube (gstrube@gmail.com), 2005
;
; Copyright of original (binary) implementation, V4.0:
; (C) 1987,88 by Trinity Concepts Limited, Protechnic Computers Limited & Operating Systems Limited.
;
; ***************************************************************************************************


        Module  ImpExp


        include "director.def"
        include "dor.def"
        include "error.def"
        include "fileio.def"
        include "integer.def"
        include "memory.def"
        include "stdio.def"
        include "syspar.def"
        include "sysapps.def"
        include "oz.def"
        include "time.def"
        include "serintfc.def"
        include "impexp.inc"

        org     IXP_ORG

        xref    xymodem_init, load_xymodem_cfg
        xdef    Imp_Export


defgroup
        ERR_System, ERR_Response, ERR_Data, MSG_Eob,
        MSG_Eof, ERR_Esc, ERR_Name, MSG_AllSent
enddef

.Imp_Export
        JP   app_main
        SCF
        RET

.app_main
        LD      A,(IX+$02)                      ; IX points at information block
        CP      $20+IXP_BAD                     ; get end page+1 of contiguous RAM
        JR      Z, continue_impexp              ; end page OK, RAM allocated...

        LD      A,$07                           ; No Room for Imp/Export, return to Index
        OZ      Os_Bye                          ; Imp/Export suicide...
.continue_impexp

        ld      a,OZVERSION                     ; OZ V4.5 and later implements OS_Nq, NQ_Voz (verify OZ)
        ld      bc,NQ_Voz
        oz      OS_Nq                           ; This application only runs on latest ROM by principle 
        jr      nc, init_impexp
.not_compatible
        xor     a
        oz      OS_Bye                          ; return to Index in current running OZ... (not the OZ of these sources)

.init_impexp
        ld      iy, WorkSpace                   ; IY points at base of WorkSpace area                 
                                                ; which is referenced by Imp/Export & XY-modem
        ld      a, SC_ENA
        OZ      OS_Esc
        xor     a
        ld      hl, ErrHandler
        OZ      OS_Erh

.Main_redraw
        call    InitWd

.Main
        call    load_xymodem_cfg                ; load XY-modem configuration (if available)

        ld      a, SC_ACK
        OZ      OS_Esc

        OZ      OS_Pout
        defm    "X)Modem, Y)modem, B)atch receive, E)nd batch, R)eceive file or S)end file? ",0

        xor     a
        ld      c, a
.main_wait
        ld      de, InputBuf
        ld      b, 2
        OZ      GN_Sip
        call    c, MayQuit
        call    c, MayExit
        call    c, Backspace_C
        jr      c, main_wait

        oz      GN_Nln
        ld      a, (de)
        or      a
        jr      z, m_4

        call    ToUpper
        ld      hl, CmdChars_tbl                ; Key
        ld      bc, 6                           ; Number of Command entries
        cpir
        jr      z, m_2
        ld      a, ERR_Response
        call    PrntError
        jr      m_4

.m_2
        sla     c                               ; C = C*2 and B=0
        ld      hl, CmdFuncs_tbl
        add     hl, bc
        ld      e, (hl)                         ; DE=func
        inc     hl
        ld      d, (hl)
        ex      de, hl                          ; HL=entry of command
        ld      de, m_3
        push    de                              ; for the ret
        jp      (hl)
.m_3
        jp      c, Main_redraw                  ; ret here
.m_4
        OZ      GN_Nln
        jp      Main

.CmdFuncs_tbl
        defw    Cmd_Xmodem
        defw    Cmd_Ymodem
        defw    Cmd_BatchRcv
        defw    Cmd_EndBatch
        defw    Cmd_Receive
        defw    cmd_Send
.CmdChars_tbl
        defm    "SREBYX"

;       ----

.Cmd_Xmodem
        res     ymodem,(IY+state_prg-Workspace)
        jp      xymodem_init

.Cmd_Ymodem
        set     ymodem,(IY+state_prg-Workspace)
        jp      xymodem_init

.MayExit
        cp      RC_Esc
        jr      z, Exit
        scf
        ret

.MayQuit
        cp      RC_Quit
        scf
        ret     nz
.Exit
        xor     a
        OZ      OS_Bye
        jr      Exit

.ErrHandler
        ret     z
        cp      RC_Quit
        jr      z, Exit
        cp      a
        ret

;       ----

.brcv_0
        OZ      GN_Nln
        jr      brcv_1

.Cmd_BatchRcv
        call    c, PrntError
        ccf
        ret     nc

.brcv_1
        call    Receive
        ret     c
        cp      MSG_Eof
        jr      z, brcv_0
        or      a
        ret

;       ----

.Cmd_EndBatch
        jr      c, ebat_1
        ld      a, 'Z'                          ; batch end
        call    MaySendB_EscA
.ebat_1
        call    c, PrntError
        or      a
        ret

;       ----

.snd_0
        cp      RC_Eof
        scf
        jp      nz, snd_9
        ld      a, MSG_AllSent
        jp      SndRcv_End

.cmd_Send
        ld      hl, 0
        ld      (FileHandle), hl
        ld      (WildHandle), hl
.snd_1
        call    AskFilename
        ret     c
        ret     z
        ld      a, (LocalName)
        or      a
        jr      z, snd_1

        jp      c, SndRcv_End
        ld      bc, 50
        ld      hl, LocalName
        ld      e, l
        ld      d, h
        OZ      GN_Fex
        jp      c, snd_9
        rla
        jr      nc, snd_5
        xor     a
        ld      b, a
        OZ      GN_Opw                          ; open wildcard handler
        jr      c, snd_9
        ld      (WildHandle), ix
.snd_2
        ld      ix, (WildHandle)
        ld      de, LocalName
        ld      c, 50
        OZ      GN_Wfn                          ; get next filename match from wc.handler
        jr      c, snd_0                        ; no more files?
        cp      DN_FIL
        jr      nz, snd_2                       ; not file? skip
        ld      hl, LocalName
.snd_3
        ld      a, (hl)
        or      a
        jr      z, snd_4
        OZ      OS_Out                          ; write a byte to std. output
        inc     hl
        jr      snd_3
.snd_4
        OZ      GN_Nln
.snd_5
        ld      a, OP_In
        ld      bc, 50
        ld      de, 3
        ld      hl, LocalName
        OZ      GN_Opf                          ; open file/stream (or device)
        jr      c, snd_9
        ld      (FileHandle), ix

        ld      de, 1                           ; line number = 1
        ld      a, 'N'                          ; name start
        call    MaySendB_EscA
        jr      c, snd_10
        ld      hl, LocalName
.snd_6
        ld      a, (hl)
        or      a
        jr      z, snd_7
        call    SendChar
        jr      c, snd_10
        inc     hl
        jr      snd_6

.snd_9
        ld      a, ERR_System
        jp      SndRcv_End

.snd_7
        ld      a, 'F'                         ; name end, data start
        call    MaySendB_EscA
        jr      c, snd_10

.trnsf_file_loop  
        call    loadbuffer                     ; load new block into buffer...
        jr      z, end_transfer_file           ; eof reached...

        ld      bc,(databuflen)
        ld      hl,data_buf0                   ; start of buffer
        call    sendbuffer
        jr      c,end_transfer_file
        jr      trnsf_file_loop

.sendbuffer
        ld      a,(hl)                         ; get byte from file (buffer)
        call    PrntLineNum
        push    bc
        push    hl
        call    SendChar
        pop     hl
        pop     bc
        jr      c,snd_10

        inc     hl
        dec     bc
        ld      a,b
        or      c
        jr      nz,sendbuffer
        ret
.snd_10
        call    SndRcv_End
        scf
        ret

.end_transfer_file
        ld      a, 'E'          ; eof
        call    MaySendB_EscA
        jp      c, SndRcv_End
        ld      hl, (WildHandle)
        ld      a, h
        or      l
        jp      z, SndRcv_End
        call    CloseFile
        call    c, PrntError
        OZ      GN_Nln
        jp      snd_2

;       ----

.SendChar
        cp      $20
        jr      c, sc_1
        cp      $7F
        jp      c, MaySendB
.sc_1
        cp      13
        jp      z, MaySendB
        cp      9
        jp      z, MaySendB
        cp      10
        jp      z, MaySendB
        push    af
        ld      a, 'B'                          ; binary
        call    MaySendB_EscA
        pop     bc
        ret     c
        ld      a, b
        push    af
        srl     a
        srl     a
        srl     a
        srl     a
        call    ItoH
        call    SendChar
        pop     bc
        ret     c
        ld      a, b
        and     $0F
        call    ItoH
        jr      SendChar

;       ----

.ItoH
        or      '0'
        cp      ':'
        ret     c
        add     a, 7
        ret

;       ----

.Cmd_Receive
        call    AskFilename
        ret     c
        ret     z
        call    c, PrntError
        ccf
        ret     nc

.Receive
        ld      de, 0
        ld      (FileHandle), de
        ld      (WildHandle), de
        ld      hl, RemoteName
        ld      (hl), d
.rcv_1
        call    TestEsc
        ld      a, ERR_Esc
        jp      c, SndRcv_End
        call    RcvB
        jp      c, SndRcv_End
        cp      27
        jr      nz, rcv_2
        call    RcvFilename
        jr      c, check_eof
        jp      pe, rcv_1
.rcv_2
        ld      c, a
        ld      a, (iy+RemoteName-WorkSpace)
        or      (iy+LocalName-WorkSpace)
        jr      z, rcv_1
        ld      a, (iy+FileHandle-WorkSpace)
        or      (iy+FileHandle+1-WorkSpace)
        jr      nz, rcv_6
        call    GetName                         ; HL points to filename (local or remote)
        push    bc
        ld      a, OP_Out
        ld      bc, 50
        ld      de, 3
        OZ      GN_Opf
        pop     bc
        jr      c, rcv_7
        ld      (FileHandle), ix                ; file created, based on remote filename 
        call    GetName                         ; HL points to filename (local or remote)
        jr      z, rcv_3
        OZ      OS_Pout
        defm    "Using supplied name: ",0
        jr      rcv_4
.rcv_3
        OZ      OS_Pout
        defm    "Using remote name: ",0
.rcv_4
        OZ      GN_Sop                          ; display filename in IMp/Export window
.rcv_5
        OZ      GN_Nln
        ld      de, 1                           ; Line number = 1
        call    resetbufferptrs                 ; prepare buffer for reception
.rcv_6
        ld      a, c
        call    PrntLineNum
        call    byte2file                       ; write received byte to file (buffer)
        jp      nc, rcv_1
.rcv_7
        ld      a, 0
        jr      SndRcv_End                      ; abort file reception and display OZ error.
.check_eof    
        cp      MSG_Eof
        jr      z, rcv_flush
        cp      MSG_Eob
        jr      nz, rcv_signal_error
.rcv_flush
        ld      d,a
        call    flushbuffer                     ; EOF or EOB occurred, ensure to flush buffer to file before closing.
        jr      c, rcv_7                        ; signal system error message display
        ld      a,d                             ; restore original status code for SndRcv_End...
.rcv_signal_error
        scf                                     ; signal error condition for correct message display

;       ----

.SndRcv_End
        push    af
        call    CloseFile
        jr      nc, sre_2
        pop     de
        bit     0, e
        jr      nz, sre_1
        set     0, e
        ld      d, a
.sre_1
        push    de
.sre_2
        pop     af
        push    af
        call    c, PrntError
        ld      hl, (WildHandle)
        ld      a, h
        or      l
        jr      z, sre_3
        push    hl
        pop     ix
        OZ      GN_Wcl
.sre_3
        pop     af
        or      a
        ret

;       ----

.GetName
        ld      hl, LocalName
        ld      a, (hl)
        or      a
        ret     nz
        ld      hl, RemoteName                  ; !! ld l,<RemoteName
        ret

;       ----

.RcvFilename
        call    RcvB
        ret     c
        call    ToUpper
        cp      'F'
        jr      z, rfn_3                        ; name end? return
        cp      'E'
        jr      z, SetEofErr                    ; file end?
        cp      'Z'
        jr      z, SetEobErr                    ; batch end?
        cp      'B'
        jr      z, GetHexB
        cp      'N'
        jr      nz, SetDataErr

        ld      hl, RemoteName
        ld      b, 50
.rfn_1
        call    RcvB
        ret     c
        cp      27
        jr      z, rfn_2
        ld      (hl), a
        inc     hl
        djnz    rfn_1
        jr      SetDataErr                      ; name too long

.rfn_2
        ld      (hl), 0
        call    RcvB
        ret     c
        call    ToUpper
        cp      'F'                             ; name end
        jr      nz, SetDataErr

.rfn_3
        push    af
        pop     hl
        ld      l, 4                            ; V flag?
        push    hl
        pop     af
        ret

.SetDataErr
        ld      a, ERR_Data
        scf
        ret

.SetEofErr
        ld      a, MSG_Eof
        scf
        ret

.SetEobErr
        ld      a, MSG_Eob
        scf
        ret

.GetHexB
        call    RcvB
        ret     c
        call    AtoI
        jr      c, SetDataErr
        add     a, a                            ; C=A<<4
        add     a, a
        add     a, a
        add     a, a
        ld      c, a
        call    RcvB
        ret     c
        call    AtoI
        jr      c, SetDataErr
        add     a, c
        push    af
        pop     hl
        ld      l, 0                            ; clear flags
        push    hl
        pop     af
        ret

;       ----

;       Fc=0, A=hex digit
;       Fc=1 if bad char

.AtoI
        call    ToUpper
        sub     '0'
        ret     c
        cp      10
        ccf
        ret     nc
        sub     7
        cp      $10
        ccf
        ret

;       ----

.CloseFile
        ld      a, (iy+FileHandle-WorkSpace)
        or      (iy+FileHandle+1-WorkSpace)
        ret     z
        ld      ix, (FileHandle)
        OZ      GN_Cl                           ; close file/stream
        ld      hl, 0
        ld      (FileHandle), hl
        ld      a, l
        ret

; ***************************************************************************
; Display line number in DE, if byte (to be sent, or has been received) 
; is a CR or a LF.
;
; DE increased if displayed (for next time)
.PrntLineNum
        cp      10
        jr      z, pln_1
        cp      13
        ret     nz
.pln_1
        push    af
        push    bc
        push    hl

        ld      a, 13
        OZ      OS_Out                          ; write a byte to std. output
        push    de
        ld      bc, NQ_Out
        OZ      OS_Nq                           ; IX=outhandle
        ld      c, e                            ; BC=DE
        ld      b, d
        ld      hl, 2                           ; convert BC
        xor     a                               ; no formatting
        ld      e, a                            ; DE=0, write to IX
        ld      d, a
        OZ      GN_Pdn
        pop     de
        inc     de

        pop     hl
        pop     bc
        pop     af
        ret

;       ----

.RcvB
        push    bc
        push    hl
        ld      bc, 60000                       ; 60s timeout    
        ld      l, SI_Gbt
        OZ      Os_Si                           ; get a byte from serial port
        jr      nc, gb_1
        ld      a, ERR_System
        pop     hl
        pop     bc
        ret
.gb_1
        and     $7f                             ; only allow 7bit Ascii reception 
        pop     hl                              ; (8bit binary data is received through ESC B xx)
        pop     bc
        ret

;       ----

.MaySendB_EscA
        push    af
        ld      a, 27
        call    MaySendB
        pop     bc
        ret     c
        ld      a, b

.MaySendB
        call    TestEsc
        jr      c, mpb_1
        push    bc
        push    hl
        ld      bc, 60000                       ; 60s timeout    
        ld      l, SI_Pbt
        OZ      Os_Si                           ; send a byte to serial port
        pop     hl
        pop     bc
        ret     nc
        ld      a, ERR_System
        ret

.mpb_1
        ld      a, ERR_Esc
        ret

;       ----

.TestEsc
        push    af
        ld      a, SC_BIT
        OZ      OS_Esc                          ; Examine special condition
        jr      c, tesc_1                       ; !! ex (sp),hl; ld a,h; pop hl; ret
        pop     af
        or      a
        ret
.tesc_1
        pop     af
        scf
        ret

;       ----

;       Fc=1 if error, FZ=1 if ESC (ESC never returned, so checking it is unnecessary)

.AskFilename
        OZ      OS_Pout
        defm    "Filename? ",0
        xor     a
        ld      c, a
.afn_1
        ld      b, 51
        ld      de, LocalName
        OZ      GN_Sip
        call    c, MayQuit
        jr      nc, afn_2
        cp      RC_Esc
        ret     z
        cp      RC_Susp                         ; suspended? clear line and re-read filename
        call    z, Backspace_C
        jr      z, afn_1
        scf
        ret

.afn_2
        OZ      GN_Nln
        ld      a, (de)
        or      a
        jr      z, afn_3                        ; empty name? return
        ex      de, hl
        ld      b, 0
        OZ      GN_Prs
        jr      nc, afn_3                       ; no error? return

        ld      a, ERR_Name
        call    PrntError
        OZ      GN_Nln
        jr      AskFilename

.afn_3
        ld      a, 1                            ; Fc=0, Fz=0
        or      a
        ret


; ***********************************************************************
; A = byte to write to file (buffer flushed automatically when full)
.byte2file  
        push    bc
        push    hl   
        ld      hl,(next_pos)                   ; entry of buffer
        ld      (hl),a                          ; put byte into buffer
        inc     hl
        ld      (next_pos),hl                   ; save adr of next entry into buffer
        ld      hl,(databuflen)                 ; current size of buffer
        inc     hl                              ; updated with new byte
        ld      (databuflen),hl                 ; save back new buffer length
        ld      bc,endofbuf - data_buf0         ; size of buffer
        cp      a
        sbc     hl,bc                           ; is buffer full?
        jr      z, eob_byte2file
        xor     a                               ; clear carry if set (indicate no error)...
        jr      exit_byte2file            
.eob_byte2file        
        call    flushbuffer                     ; yes, write to file, return Fc = 1 if error while writing...
.exit_byte2file        
        pop     hl
        pop     bc
        ret                                     ; return, fetch next byte...


; ***********************************************************************
; flush current buffer contents to file, defined by (FileHandle)
; return Fc = 1, if memory is full in RAM device
.flushbuffer     
        push    bc
        push    de
        push    hl
        ld      a, (iy+FileHandle-WorkSpace)
        or      (iy+FileHandle+1-WorkSpace)
        jr      z,exit_flushbuffer              ; no file is opened..

        ld      ix,(FileHandle)                 ; also called directly if eof before
        ld      bc,(databuflen)
        ld      de,0
        ld      hl,data_buf0
        oz      OS_Mv                           ; write buffer to file.
        call    resetbufferptrs
.exit_flushbuffer
        pop     hl
        pop     de
        pop     bc
        ret                                     ; c detection in calling program


; ***********************************************************************
; load file contents into buffer, returns Fz = 1, if EOF.
; (databuflen) contains size of current buffer
;
; AF changed on return
.loadbuffer
        push    bc
        push    de
        push    hl
        call    resetbufferptrs
        ld      ix,(FileHandle)
        ld      a,FA_Eof
        oz      OS_Frm
        jr      z,exit_loadbuffer               ; eof
        ld      bc,endofbuf - data_buf0         ; size of buffer
        ld      hl,0
        ld      de,data_buf0
        push    bc
        oz      OS_Mv
        pop     hl
        cp      a
        sbc     hl,bc
        ld      (databuflen),hl                 ; actual length of buffer
.exit_loadbuffer        
        pop     hl
        pop     de
        pop     bc        
        ret


; ***********************************************************************
.resetbufferptrs
        push    hl
        ld      hl,data_buf0
        ld      (next_pos),hl
        ld      hl,0
        ld      (databuflen),hl
        pop     hl
        ret


;       ----

.InitWd
        OZ      OS_Pout
        defb    1, $37, $23, $31, $21, $20, $7C, $28, $81
        defb    1, $32, $43, $31
        defb    1, $32, $2B, $43
        defb    1, $53
        defb    0
        ret

;       ----

.PrntError
        push    af

        xor     a
        ld      bc, NQ_WCUR
        OZ      OS_Nq
        inc     c
        dec     c
        jr      z, cont_PrntError
        OZ      GN_Nln                          ; x<>0? crlf
.cont_PrntError
        call    PrntBell
        pop     af
        or      a
        jr      z, pre_3                        ; A=0? print system error
        dec     a
        add     a, a
        ld      e, a
        ld      d, 0
        ld      hl, ErrorTable
        add     hl, de
        ld      e, (hl)
        inc     hl
        ld      d, (hl)
        ex      de, hl
.pre_1
        oz      GN_Sop
.pre_x
        scf
        ret

.pre_3
        OZ      OS_Erc
        OZ      GN_Esp
.pre_5
        OZ      GN_Rbe
        or      a
        jr      z, pre_x
        OZ      OS_Out
        inc     hl
        jr      pre_5

.ErrorTable
        defw    Error_1
        defw    Error_2
        defw    Error_3
        defw    Error_4
        defw    Error_5
        defw    Error_6
        defw    Error_7

.Error_1
        defm    "Bad response",0
.Error_2
        defm    "Poor data received",0
.Error_3
        defm    "End of batch",0
.Error_4
        defm    "End of file",0
.Error_5
        defm    "Escape",0
.Error_6
        defm    "Bad name",0
.Error_7
        defm    "All sent",0

;       ----

.PrntBell
        ld      a, 7
        OZ      OS_Out
        ret

.Backspace_C
        push    af
        ex      (sp), hl
        ld      h, 1
        ex      (sp), hl
        ld      b, c
        inc     b
        dec     b
        jr      z, bsc_2
        ld      a, 8
.bsc_1
        OZ      OS_Out
        djnz    bsc_1
.bsc_2
        pop     af
        ret

;       ----

.ToUpper
        call    IsAlpha
        ret     nc
        and     $df
        ret

;       ----

;       Fc=1 if alpha

.IsAlpha
        cp      'A'
        ccf
        ret     nc
        cp      'Z'+1
        ret     c
        cp      'a'
        ccf
        ret     nc
        cp      'z'+1
        ret
