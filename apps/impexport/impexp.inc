; **************************************************************************************************
;
; Imp/Export & XY-modem Workspace variable definitions
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************


DEFC Workspace  = $2000

DEFVARS Workspace
	; Imp/Export variables
    LocalName       ds.b 51
    RemoteName      ds.b 51
    InputBuf        ds.b 2
    FileHandle      ds.b 2
    WildHandle      ds.b 2
    RXCount         ds.b 2
    RXError         ds.b 2
    vars_e          ds.b 1

    ; XY-Modem variables
    state_prg       ds.b 1
    state_rx        ds.b 1
    state_tx        ds.b 1

    fname_buf   	ds.b 1
    data_buf0   	ds.b 128
    data_buf1   	ds.b 1024-128
    endofbuf
    databuflen      ds.w 1
    oct         	ds.b 11             ;4176241405
    filesize    	ds.l 1              ;
    bytes       	ds.w 1
    blocks      	ds.w 1
    time        	ds.b 3              ;   48002 /     64 = --+
    date        	ds.b 3              ;  255751 - 253D8C =   |
    ;days       	ds.l 1              ;    19C5 *  15180 =   |
    seconds     	ds.l 1              ;21F93780 +            |
    ;seconds1   	ds.l 1              ;     B85 =          <-+
    ;seconds2   	ds.l 1              ;21F94305
    next_pos    	ds.w 1              ;    2014
    wild_handle 	ds.w 1
    retries     	ds.b 1
    blk         	ds.b 1
    fname_buf1  	ds.b 52

    EndWorkspace
enddef


; XY-modem flags:
; state_prg control bit flags
    DEFC crc=0
    DEFC block1k=1
    DEFC ymodem=2
    DEFC g=3
    DEFC end_of_file=4
    DEFC crc_trans=5
    DEFC header=6
    DEFC file_open=7

; state_rx control bit flags
;   DEFC block1k=1
    DEFC date_rx=5

; state_tx control bit flags
    DEFC escape=5

; Contiguous RAM pages required by application are specified in sysapps.def / IXP_BAD : 5 pages
; DEFC ImpExpWorkspace = EndWorkspace - Workspace
; DEFC ImpExpRamPages = ImpExpWorkspace/256 + 1  ; allocate contigous memory from $2000...