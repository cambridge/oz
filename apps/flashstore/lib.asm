; **************************************************************************************************
; Library functionality used by FlashStore (migrated from stdlib and optimized for OZ)
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

        module LibraryFunctions

        xdef FileEprTransferBlockSize
        xdef ApplEprType, MemAbsPtr
        xdef MemReadByte, MemReadWord, MemReadLong, MemWriteByte
        xdef AddPointerDistance, ConvPtrToAddr, ConvAddrToPtr
        xdef divu8, ToLower, createfilename

        include "error.def"
        include "memory.def"
        include "blink.def"
        include "fileio.def"
        include "dor.def"


; ************************************************************************
;
; Evaluate Standard Z88 Application ROM Format (Front DOR/ROM Header).
;
; Return Standard Application ROM "OZ" status in slot x (0, 1, 2 or 3).
;
; In:
;    C = slot number (0, 1, 2 or 3)
;
; Out:
;    Success:
;         Fc = 0,
;              A = Application Eprom type (with Rom Front Dor):
;                   $80 = external EPROM,
;                   $81 = system/internal OZ ROM,
;                   $82 = external RAM
;              B = size of reserved Application space in 16K banks.
;              C = size of physical Application Card in 16K banks.
;
;    Failure:
;         Fc = 1,
;              A = RC_ONF, Application Eprom not found
;
; Registers changed after return:
;    ....DEHL/IXIY same
;    AFBC..../.... different
;
; ------------------------------------------------------------------------
; Design & programming by Gunther Strube, Apr-Aug 1998, Aug 2006, Aug 2012
; ------------------------------------------------------------------------
;
.ApplEprType
                    PUSH DE
                    PUSH HL

                    LD   A,C                 ; slot C
                    LD   B,$3F
                    LD   HL,$3F00            ; top 256 byte page of top bank in slot C
                    CALL MemAbsPtr           ; Convert to physical pointer...

                    LD   A,$FB
                    CALL MemReadByte
                    LD   C,A                 ; get Application ROM type ($80 or $81)
                    LD   A,$FC               ; offset $FC
                    CALL MemReadByte
                    PUSH AF                  ; size of reserved Application space, $3FFC
                    CALL CheckRomId
                    JR   NZ,no_applrom       ; "OZ" watermark not found...

                    LD   A,C                 ; Application Rom found
                    AND  @10000001
                    JR   Z, no_applrom       ; invalid Application Type Code...

                    CALL CheckRamCard        ; If FRONT DOR is located in a RAM card, return type $82

                    POP  DE                  ; D = size of reserved Application space (16K bank entities)
                    LD   E,C                 ; E = Application ROM Type Code ($80 or $81)
                    LD   A,D
                    CALL GetCardSize
                    LD   C,A                 ; C = physical card size (16K bank entities)
                    LD   B,D                 ; B = size of reserved Application space (16K bank entities)
                    LD   A,E                 ; A = Application ROM Type Code ($80 or $81)
.exit_ApplEprType
                    POP  HL                  ; original HL restored
                    POP  DE                  ; original DE restored
                    RET
.no_applrom
                    POP  AF
                    LD   A,RC_ONF
                    SCF
                    JR   exit_ApplEprType


; ************************************************************************
;
; Calculate physical size of Card by scanning for "OZ" header from
; bank $3E downwards in available 1MB slot.
;
; In:
;    B = top bank of slot containing Front DOR.
;    A = size of reserved Application space (16K bank entities)
;
;    HL = offset $3F00 (mapped to free segment in address space).
;
; Out:
;    A = physical size of card in 16K entities.
;
; Registers changed after return:
;    ..BCDEHL/IXIY same
;    AF....../.... different
;
.GetCardSize        PUSH DE
                    LD   D,B            ; preserve top bank number in slot
                    LD   E,A
                    LD   A,B
                    SUB  E              ; TopBank - ReservedSpace
                    LD   B,A            ; begin parsing at bank below reserved appl. space
                    LD   A,64
                    SUB  E              ; parse max. 64 - ReservedSpace banks...
.scan_loop
                    CALL CheckRomId
                    JR   Z, oz_found
                    DEC  B
                    DEC  A
                    JR   NZ,scan_loop

.oz_found           RES  7,B            ; slot size always max 64 * 16K banks...
                    RES  6,B
                    LD   A,D
                    AND  @00111111
                    SUB  B              ; Card Size = TopBank - TopBank' (0-64)
                    OR   A              ; Fc = 0...
                    POP  DE
                    RET  NZ
                    LD   A,64           ; Card was 1MB...
                    RET

; ************************************************************************
;
; IN:
;    A = original Application Card Type Code ($80 or $81)
;    BHL = <top bank> $3F00
;
; OUT:
;    A = $82, if FRONT DOR in RAM card, otherwise original code.
;
; Registers changed after return:
;    ..BCDEHL/IXIY same
;    AF....../.... different
;
.CheckRamCard       PUSH BC
                    PUSH DE

                    LD   E,A
                    LD   A,$F8
                    CALL MemReadByte         ; get low byte card ID
                    INC  A
                    LD   C,A
                    LD   A,$F8
                    CALL MemWriteByte        ; try to write another value to "RAM"
                    LD   A,$F8
                    CALL MemReadByte         ; then read it back
                    CP   C                   ; changed?
                    JR   Z, ramcard
                         LD   A,E            ; Eprom Card - return original type code
                         JR   exit_CheckRamCard
.ramcard
                         DEC  C
                         LD   A,$F8
                         CALL MemWriteByte        ; write back original card id
                         LD   A,$82               ; return $82 for RAM based application card
.exit_CheckRamCard
                    OR   A
                    POP  DE
                    POP  BC
                    RET


; ************************************************************************
;
; Check for "OZ" watermark at bank B, offset $3FFE.
;
; In:
;    B = Bank (usually $3F, mapped to slot X) containing possible Front DOR.
;    HL = offset $3F00 (mapped to free segment).
;
; Out:
;    Success:
;         Fz = 1,
;              "OZ" watermark found in bank B
;
;    Failure:
;         Fz = 0,
;              "OZ" watermark not found.
;
; Registers changed after return:
;    A.BCDEHL/IXIY same
;    .F....../.... different
;
.CheckRomId         PUSH DE
                    PUSH AF

                    LD   A,$FE
                    CALL MemReadWord

                    PUSH HL
                    LD   HL,$5A4F
                    SBC  HL,DE               ; 'OZ' ?
                    POP  HL

                    POP  DE
                    LD   A,D                 ; original A restored
                    POP  DE
                    RET                      ; Fz = ?


; ************************************************************************
;
; Convert relative BHL pointer for slot number A (0 to 3) to absolute
; pointer, addressed for binding MS_S1 segment.
;
; Internal Support Library Routine.
;
; IN:
;    A = slot number (0 to 3)
;    BHL = relative pointer
;
; OUT:
;    BHL pointer, absolute addressed for physical slot A, and free segment.
;
; Registers changed after return:
;    ...CDE../IXIY same
;    AFB...HL/.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, April 1998, July 2006, Aug 2012
; ----------------------------------------------------------------------
;
.MemAbsPtr
                    RES  7,B
                    RES  6,B                      ; clear before masking to assure proper effect...
                    AND  @00000011                ; only slot 0 - 3 allowed...
                    RRCA                          ;
                    RRCA                          ; Slot number A converted to slot mask
                    OR   A
                    JR   NZ, MemAbsPtr_slotx
                    RES  5,B                      ; slot 0 rom bank range is $00 - $1F
.MemAbsPtr_slotx
                    OR   B
                    LD   B,A                      ; B = converted to physical bank of slot A
                    RES  7,H
                    SET  6,H
                    OR   H                        ; for bank I/O (outside this executing code)
                    LD   H,A                      ; offset mapped for MS_S1 segment
                    RET


; ***************************************************************************************************
;
; Read byte at base record pointer in BHL, offset A.
; Byte is returned in A.
;
; If B<>0, the byte is read from extended address.
; If B=0, the byte is read from local address space.
;
;    Register affected on return:
;         ..BCDEHL/IXIY same
;         AF....../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1997, Sep 2004, Oct 2005, Aug 2012
; ----------------------------------------------------------------------
;
.MemReadByte        PUSH HL
                    PUSH DE
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    LD   A,(HL)                   ; read byte at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  DE
                    POP  HL
                    RET


; ***************************************************************************************************
;
; Read 16bit word at record defined as extended (base) address in BHL, offset A.
;
; If B<>0, the 16bit word is read from extended address.
; If B=0, the 16bit word is read from local address space.
;
; Return 16bit word in DE.
;
;    Register affected on return:
;         A.BC..HL/IXIY same
;         .F..DE../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, July 1998, Sep 2004, Oct 2005
; ----------------------------------------------------------------------
;
.MemReadWord        PUSH HL
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    LD   E,(HL)
                    INC  HL
                    LD   D,(HL)                   ; read 16bit word at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  HL
                    RET


;***************************************************************************************************
;
; Read long integer in de'bc' at base record pointer in BHL, offset A.
;
; long integer is read from extended address.
;
;    Register affected on return:
;         A.BCDEHL/IXIY .......  same
;         .F....../.... afbcdehl different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1997, Sep 2004, Oct 2005
; ----------------------------------------------------------------------
;
.MemReadLong        PUSH HL
                    PUSH DE
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment

                    PUSH HL
                    EXX
                    POP  HL
                    LD   C,(HL)
                    INC  HL
                    LD   B,(HL)
                    INC  HL
                    LD   E,(HL)
                    INC  HL
                    LD   D,(HL)
                    EXX

                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  DE
                    POP  HL
                    RET


; ***************************************************************************************************
;
; Set byte in C, at base record pointer in BHL, offset A.
;
;    Register affected on return:
;         A.BCDEHL/IXIY same
;         .F....../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1995-97, Sep 2004, Oct 2005, Aug 2012
; ----------------------------------------------------------------------
;
.MemWriteByte       PUSH HL
                    PUSH DE
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    POP  DE
                    LD   (HL),E                   ; write byte at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    LD   B,D
                    LD   C,E                      ; original BC restored...
                    POP  DE
                    POP  HL
                    RET




; **************************************************************************************************
;
; Unsigned 8bit division
;
; IN:
;    H = Dividend
;    L = Divisor
;
; OUT:
;    Fc = 0, division completed
;         H = Quotient
;         L = Remainder
;    Fc = 1,
;         A = RC_Dvz (divide by 0)
;
; Registers changed after return:
;    ......./IXIY same
;    AF...HL/.... different
;
.divu8
        xor  a
        inc  l
        dec  l
        jr   nz, do_divide
        ld   a, RC_Dvz
        scf
        ret
.do_divide        
        push af
        push bc
        ld   b,8
.div8loop        
        sla  h          ; advancing a bit
        rla             ; ...
        cp   l          ; checking if the divisor divides the digits chosen (in A)
        jr   c,nextbit  ; if not, advance without subtraction
        sub  l          ; subtracting the divisor
        inc  h          ; and setting the next digit of the quotient
.nextbit
        djnz div8loop
        ld   l,a        ; H = quotient, L = remainder
        pop  bc
        pop  af
        ret


; ******************************************************************************
;
; ToLower - convert character to lower case if possible.
;
;  IN:    A = ASCII byte
; OUT:    A = converted ASCII byte
;
; Registers changed after return:
;
;    ..BCDEHL/IXIY  same
;    AF....../....  different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, Copyright (C) InterLogic 1995
; ----------------------------------------------------------------------
;
.ToLower            CP   '['       ; if A <= 'Z'  &&
                    JR   NC, check_latin1
                    CP   'A'       ; if A >= 'A'
                    RET  C
                    ADD  A,32      ; then A = A + 32
                    RET
.check_latin1       CP   $DF       ; if A <= $DE  &&
                    RET  Z
                    CP   $C0       ; if A >= $C0
                    RET  C
                    ADD  A,32      ; then A = A + 32
                    RET


; ******************************************************************************
;
; Create filename and directory path.
;
; Try to create the specified filename using OP_OUT. If a directory path is
; specified, it will automatically be created if not defined in the filing
; system.
;
; The filename must not contain wildcards (standard convention). However if a
; RAM device is not specified, it will automatically be included (the current)
; into the filename.
;
; The buffer of the filename must have space to get itself expanded with a
; device name (additional 6 bytes).
;
; The filename pointer may not point in segment 2, since GN_FEX is unreliable
; in that segment.
;
; The routine deletes an existing file without warning.
;
; in:     bhl = pointer to null-terminated filename, (b = 0, means local)
;
; out, if successful:
;         ix = file handle
;         fc = 0, file (and directory) successfully created
;         (hl) = filename may have been expanded
;
; out, if failed:
;         fc = 1, unable to create file (directory)
;         a = error code:
;              RC_IVF, Invalid filename
;              RC_USE, File already in use
;
;    registers changed after return:
;         ..bcdehl/..iy  same
;         af....../ix..  different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, Copyright (C) 1995
; ----------------------------------------------------------------------
;
.createfilename     push bc                       ; preserve original BC
                    push de                       ; preserve original DE
                    push hl                       ; preserve original HL
                    push iy                       ; preserve original IY

                    push hl
                    pop  iy                       ; IY points at start of filename
                    inc  b
                    dec  b
                    jr   z, local_filename        ; pointer to filename is local

                         ld   a,h
                         and  @11000000
                         rlca
                         rlca
                         ld   c,a                      ; MS_Sx
                         rst  OZ_MPB                   ; page filename into segment

                         push bc                       ; preserve old bank binding
                         call createfile
                         pop  bc
                         push af                       ; preserve error status...
                         rst  OZ_MPB                   ; restore previous bank binding

                         pop  af
                         jr   exit_createflnm

.local_filename          call createfile

.exit_createflnm    pop  iy
                    pop  hl
                    pop  de
                    pop  bc
                    ret


; ******************************************************************************
;
;    Create the filename
;
;    IN:  HL = pointer to filename
;
.createfile         ld   d,h
                    ld   e,l
                    xor  a
                    ld   bc,255
                    cpir                          ; find null-terminator
                    dec  hl
                    ex   de,hl
                    sbc  hl,de                    ; length = end - start
                    ld   c,6
                    add  hl,bc
                    ld   c,l                      ; length of buffer + 6 (max. length 255 bytes)

.openfile           push iy                       ; c = length of buffer
                    pop  hl
                    ld   d,h                      ; hl = pointer to filename
                    ld   e,l                      ; de = pointer to output
                    ld   a, OP_OUT
                    oz   GN_Opf
                    ret  nc                       ; file created successfully, return...
                    cp   RC_ONF
                    jr   z, createdir             ; if error != RC_ONF
                         scf                           return error
                         ret

.createdir          oz   GN_Fex                   ; first expand filename
                    ret  c                        ; invalid filename
                    ld   l,a
                    ld   a, RC_IVF                ; pre-load Fc = 1 & A = RC_IVF, if errors
                    scf
                    bit  7,l                      ; wildcards were used...
                    ret  nz
                    bit  1,l
                    ret  z                        ; filename not specified...

                    cp   a                        ; fc = 0
                    dec  b                        ; deduct filename segment
                    dec  b                        ; deduct device name segment
                    ex   de,hl                    ; hl points at null-terminator
                    call createpath               ; create directory path...
                    jr   nc, openfile             ; then try to create file again...
                    ret                           ; couldn't create directory, return...


; ******************************************************************************
;
;    create directory sub-paths, recursively.
;
; in:     hl = pointer to null-terminator
;         b = number of directory path segments (levels) to create
;         c = length of filename
;
; out:    fc = 0, directory path created successfully, otherwise fc = 1
;
.createpath         ld   a, '/'
.find_separator     cp   (hl)
                    jr   z, found_separator       ; directory separator found at (hl)
                    dec  hl
                    jr   find_separator

.found_separator    ld   (hl),0                   ; null-terminate directory path segment (level)
                    dec  b
                    push hl
                    call nz, createpath           ; if (pathlevel != 0) createpath(pathlevel, sep)
                    pop  hl
                    ret  c                        ; sub-path couldn't be created...
                    call mkdir
                    ld   (hl),'/'                 ; restore directory separator
                    ret

.mkdir              push bc                       ; preserve current level in B
                    push hl                       ; preserve length of scratch buffer in C
                    push iy                       ; preserve pointer to current separator
                    pop  hl
                    ld   b,0                      ; local pointer (filename always paged in)
                    ld   d,h                      ; HL points at start of filename
                    ld   e,l                      ; DE points at scratch buffer
                    ld   a, OP_DIR
                    call_oz(gn_opf)               ; mkdir pathname (returns DOR handle)
                    jr   nc, exit_mkdir
                    cp   RC_USE
                    jr   z, quit_mkdir            ; in use (it exists)...
                    cp   RC_EXIS
                    jr   z, quit_mkdir            ; already created...
                    scf
                    jr   quit_mkdir               ; other error occurred
.exit_mkdir         ld   a, dr_fre
                    call_oz(os_dor)               ; free dor handle
.quit_mkdir         pop  hl
                    pop  bc
                    ret
