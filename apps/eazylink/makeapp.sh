#!/bin/bash

# *************************************************************************************
# EazyLink application OZ ROM make script for Unix/Linux/Mac OS X
# (C) Gunther Strube (gstrube@gmail.com) 2005-2006
#
# EazyLink is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# EazyLink is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with EazyLink;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#
# *************************************************************************************

# compile EazyLink application from scratch
mpm -b -DOZ_SLOT$1 -I../../def @eazylink.prj

#######################################################################################
# edit kernel.sh / kernel.bat too:
# mpm -b -DEAZYLINK_TESTPROTOCOL -DOZ_SLOT%1 -I..\..\def @eazylink.prj
# mpm -b testprotocol.asm
#
# Use these command lines to build a testing popdown that simulate incoming serial port
# data from a specified data block.
#
# In protocol testing mode, the EazyLink popdown simply starts up, enables serial port
# logging then runs the tests from a file. All serial port byte flow data is added
# to the logging files. Once the test-data is "streamed", the popdown closes.
#
# The log files can then be examined via the Filer's <>VF command.
# Using this option, it becomes easy to trace EazyLink in OZvm for debugging to analyse
# code execution.
#
# The incoming serial port data is provided via ":RAM.0/testprotocol.bin"
# (See command line to generate a "testprotocol.bin" file)
#######################################################################################
