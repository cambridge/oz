; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2014
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************

    MODULE Serialport_BlockIO

    XREF ErrHandler, System_Error
    XREF RxByte, Getbyte, TranslateIncoming, TranslateOutgoing
    XREF HexNibble
    XREF Write_Message, Message2, ESC_B, ESC_E, ESC_F, ESC_Y, ESC_Z, ESC_N, ESC_ESC, ESC_1B, CRLF
    XREF Dump_serport_in_byte, Dump_serport_out_byte

    XREF Msg_Protocol_error, Write_buffer, Flush_buffer, Close_file, Abort_file
    XREF Reset_buffer_ptrs
    XREF Msg_No_Room, Msg_file_aborted

    XDEF Send_ESC, Send_ESC_B, Send_CRLF, Send_ESC_E, Send_ESC_F, Send_ESC_Y, Send_ESC_N, Send_ESC_Z
    XDEF UpdateCrc16, ReceiveRamFile, TranslateIncomingBlock
    XDEF FetchBytes, RxBytes, GetHexByte, SendString, FetchBlock, SendFileBuffer
    XDEF ConvAsciiHexNibble, ConvAsciiHexHighNibble, BufCrc16, ConvHexCrc16

    INCLUDE "rtmvars.def"
    INCLUDE "fileio.def"
    INCLUDE "stdio.def"
    INCLUDE "error.def"
    INCLUDE "serintfc.def"


; ***********************************************************************
.Send_ESC_N       PUSH HL
                  LD   HL,ESC_N
                  CALL Send_ESC_Response
                  POP  HL
                  RET

; ***********************************************************************
.Send_ESC_Y       PUSH HL
                  LD   HL,ESC_Y
                  CALL Send_ESC_Response
                  POP  HL
                  RET

; ***********************************************************************
.Send_ESC_Z       PUSH HL
                  LD   HL,ESC_Z
                  CALL Send_ESC_Response
                  POP  HL
                  RET

; ***********************************************************************
.Send_ESC_E       PUSH HL
                  LD   HL, ESC_E
                  CALL Send_ESC_Response
                  POP  HL
                  RET

; ***********************************************************************
.Send_ESC_F       PUSH HL
                  LD   HL, ESC_F
                  CALL Send_ESC_Response
                  POP  HL
                  RET

; ***********************************************************************
.Send_CRLF        PUSH HL
                  LD   HL,CRLF
                  CALL Send_ESC_Response
                  POP  HL
                  RET

; ***********************************************************************
.Send_ESC
                  PUSH BC
                  PUSH HL
                  LD   BC,4
                  LD   HL,ESC_1B
                  CALL SendStringRaw
                  POP  HL
                  POP  BC
                  RET

; ***********************************************************************
.Send_ESC_Response
                  PUSH BC
                  LD   BC,2
                  CALL SendStringRaw
                  POP  BC
                  RET


; ***********************************************************************
; Send ESC B HH sequence of byte in A, (EazyLink protocol level 8+).
;
; Registers changed after return:
;
;       ..BCDEHL/IXIY same
;       AF....../.... different
;
.Send_ESC_B
                  PUSH BC
                  PUSH HL

                  LD   HL,ScratchBuffer
                  PUSH HL
                  LD   (HL),$1B
                  INC  HL
                  LD   (HL),'B'
                  INC  HL

                  LD   B,A                           ; preserve a copy of byte to ESC B encode...
                  AND  $F0
                  RRCA
                  RRCA
                  RRCA
                  RRCA
                  CALL HexNibble
                  LD   (HL),A
                  INC  HL
                  LD   A,B
                  AND  $0F
                  CALL HexNibble
                  LD   (HL),A

                  POP  HL
                  LD   BC,4
                  CALL SendStringRaw
.exit_Send_ESC_B
                  POP  HL
                  POP  BC
                  RET


; ***********************************************************************
; Send sequense of bytes, non-modified
; HL points to sequence to transmit, BC = length
;
; Registers changed after return:
;
;       A.BCDEHL/IXIY same
;       .F....../.... different
;
.SendStringRaw    PUSH HL
                  PUSH DE
                  PUSH BC
                  PUSH AF

                  PUSH HL
                  PUSH BC

                  BIT  BB_SerportLog,(IX + rtmflags)
                  JR   Z, tx_rawblock                ; don't log serial port output

                  LD   B,C                           ; B is DJNZ loop counter...
.debug_block
                  LD   A,(HL)
                  CALL Dump_serport_out_byte         ; dump to debugging serport output file
                  INC  HL
                  DJNZ debug_block
.tx_rawblock
                  POP  BC                            ; length of sequence..
                  POP  DE                            ; DE points at sequence to send
if EAZYLINK_TESTPROTOCOL
                  ; TESTPROTOCOL: don't send anything to real serial port
                  CP   A
else
                  LD   L,SI_PX
                  OZ   OS_Si                         ; send bytes to serial port
endif
                  JR   C,err_SendString
                  JR   exit_SendString


; ***********************************************************************
; Send sequense of bytes (optionally translated if enabled)
; HL points to sequence to transmit, null-terminated.
;
; Registers changed after return:
;
;       A.BCDEHL/IXIY same
;       .F....../.... different
;
.SendString       PUSH HL
                  PUSH DE
                  PUSH BC                            ; HL >> sequence, 0 terminated...
                  PUSH AF

                  XOR  A                             ; Fc = 0, find null=terminator
                  PUSH HL
                  PUSH HL
                  PUSH HL
                  LD   BC,255                        ; max size of sequence is 255 bytes.
                  CPIR
                  DEC  HL                            ; point at null-terminator
                  POP  BC
                  SBC  HL,BC
                  JR   NZ, string_contents
                  POP  HL
                  POP  HL
                  JR   exit_SendString               ; no data to be sent...
.string_contents
                  LD   B,H
                  LD   C,L                           ; BC = length of string

                  POP  HL                            ; HL >> start of sequence
                  PUSH BC

                  LD   B,C                           ; B is DJNZ loop counter...
.block_evaluate
                  BIT  BB_CharTrans,(IX + rtmflags)
                  JR   Z, check_serdbg               ; skip, translation not enabled..
                  LD   A,(HL)
                  CALL TranslateOutgoing             ; A contains translated byte
                  LD   (HL),A                        ; update block with translated byte
.check_serdbg     BIT  BB_SerportLog,(IX + rtmflags)
                  CALL NZ,Dump_serport_out_byte      ; dump to debugging serport output file (if enabled)

                  INC  HL
                  DJNZ block_evaluate

                  POP  BC                            ; length of sequence..
                  POP  DE                            ; DE points at sequence to send
if EAZYLINK_TESTPROTOCOL
                  ; TESTPROTOCOL: don't send anything to real serial port, just log
                  CP   A                             ; Fc = 0
else
                  LD   L,SI_PX
                  OZ   OS_Si                         ; send bytes to serial port
endif
                  JR   C,err_SendString
.exit_SendString
                  POP  AF
                  LD   B,A
                  SET  0,A
                  OR   A                             ; reset Z flag, FC = 0
                  LD   A,B                           ; restore original contents of A
                  POP  BC                            ; restore original contents of BC
                  POP  DE                            ; restore original contents of DE
                  POP  HL                            ; restore original contents of HL
                  RET
.err_SendString   EX   AF,AF'                        ; save contents of F register
                  POP  AF
                  LD   B,A
                  EX   AF,AF'                        ; restore F register
                  LD   A,B                           ; restore original contents of A
                  POP  BC                            ; restore original contents of BC
                  POP  DE                            ; restore original contents of DE
                  POP  HL                            ; restore original contents of HL
                  RET


; *********************************************************************
; Transmit file buffer contents
; Handles CRLF conversion, XON/XOFF & ESC byte sequences
;
; IN:
;     BC = Buffer length
;     HL = Start of of buffer
;
; OUT:
;     Fc = 1, general transmit error (timeout, etc)
;     Fc = 0, Buffer transmitted succesfully
;
.SendFileBuffer
                  CALL IsBinaryFile                  ; if file contents is binary, disable trans + CrLf conversion (then continue send)
                  LD   D,H
                  LD   E,L                           ; remember start of block
.scan_block
                  LD   A,(HL)                        ; fetch byte from buffer
                  CP   ESC                           ; ESC?
                  JR   NZ,test_XON
                  CALL SendSubBlock
                  RET  C
                  CALL Send_ESC                      ; send ESC B 1B sequense for ESC
                  RET  C
                  JR   no_chartrans
.test_XON         CP   DC1                           ; XON?
                  JR   NZ,test_XOFF
.tx_esc_b
                  CALL SendSubBlock
                  RET  C
                  CALL Send_ESC_B
                  RET  C
                  JR   no_chartrans
.test_XOFF        CP   DC3                           ; XOFF?
                  JR   Z,tx_esc_b
.test_CR          CP   CR                            ; CR?
                  JR   NZ,continue_scan
                  BIT  BB_LnfdConv,(IX + rtmflags)   ; extended to CRLF?
                  JR   Z,continue_scan               ; no!
                  CALL SendSubBlock
                  RET  C
                  CALL Send_CRLF
                  RET  C
                  JR   no_chartrans
.continue_scan
                  BIT  BB_CharTrans,(IX + rtmflags)  ; character translation?
                  CALL NZ,TranslateOutgoing
                  LD   (HL),A                        ; update block with translated byte (if enabled)
.no_chartrans
                  INC  HL
                  DEC  BC
                  LD   A,B
                  OR   C
                  JR   NZ,scan_block
.SendSubBlock
                  PUSH BC
                  PUSH AF
                  PUSH HL
                  PUSH HL
                  PUSH DE

                  SBC  HL,DE                         ; Fz = 1 (Fc = 0) if there is no block to send...

                  LD   B,H
                  LD   C,L                           ; BC = total bytes in sub-block
                  POP  HL                            ; HL = start of sub-block
                  CALL NZ,SendStringRaw              ; only transmit sub-block if BC > 0

                  POP  DE
                  INC  DE                            ; DE points at beginning of new sub-block (byte after terminator)

                  POP  HL
                  POP  BC
                  LD   A,B                           ; restore original A (the terminator)
                  POP  BC
                  RET


; ***********************************************************************
; Fetch sequense of bytes from terminal, ESC terminated.
; All bytes received will also be dumped to debugging serial port file, optionally.
; HL points to buffer that will be filled from remote, max BC bytes
;
; Returns Fz = 1, A = ESC terminator
; Returns BC = length of block fetched
; Received block is null-terminated.
;
; Registers changed after return:
;
;       ....DEHL/IXIY same
;       AFBC..../.... different
;
.FetchBlock       PUSH DE
                  PUSH HL
                  PUSH HL
                  EX   DE,HL                         ; DE = pointer to buffer
if EAZYLINK_TESTPROTOCOL
                  PUSH IX
                  LD   IX,(protctestfile_handle)     ; for testing, let OS_Si, SI_GXT read from protocol testing file
endif
                  LD   L,SI_GXT
                  OZ   OS_SI                         ; receive block of data, ESC terminated
if EAZYLINK_TESTPROTOCOL
                  POP  IX
endif

.exit_FetchBlock
                  POP  HL
                  PUSH AF                            ; preserve Fc/z status...
                  XOR  A                             ; null-terminate received block (always)
                  LD   (DE),A
                  EX   DE,HL
                  SBC  HL,DE
                  LD   B,H
                  LD   C,L                           ; BC = total bytes received in buffer
                  POP  AF
                  POP  HL                            ; return pointer to start of received Block
                  POP  DE                            ; restore original contents of DE
                  RET


; **************************************************************************
;
; IN:
;     BC = Buffer length
;     HL = Start of of buffer
;
; Registers changed after return:
;       AFBCDEHL/IXIY same
;       ......../.... different
;
.TranslateIncomingBlock
        push    af                              ; CRLF conversion or Translation enabled, check block for binary data
        push    bc
        push    hl
.trblk_loop
        ld      a,b
        or      c
        jr      z,exit_trblk                    ; end of buffer reached, no binary data discovered..

        ld      a,(hl)
        call    TranslateIncoming
        ld      (hl),a
        inc     hl
        dec     bc
        jr      trblk_loop
.exit_trblk
        pop     hl
        pop     bc
        pop     af
        ret


; ***********************************************************************
; Multi-fetch ESC sequence or just single byte, translated (if enabled).
;
; Returns:
;     Fc = 1, A = RC_xxx, serial port error (typically timeout)
;
;     When [Serial port] -> X (any byte, no ESC id):
;     A = X, Fz = 1, Fc = 0
;
;     When [Serial port] -> ESC ESC (just ESC byte but not ESC id):
;     A = ESC, Fz = 1, Fc = 0
;
;     When [Serial port] -> ESC B HH (receive 'binary' byte):
;     A = encoded byte, Fz = 1, Fc = 0
;
;     When [Serial port] -> ESC X (ESC id):
;     A = ESC Id, Fz = Fc = 0
;
; Registers changed after return:
;       ...CDEHL/IXIY same
;       AFB...../.... different
;
.FetchBytes       CALL Getbyte                       ; byte in A.
                  RET  C                             ; serial port error or timeout
                  CP   ESC
                  JR   Z,fetch_ESC
                  CP   A                             ; Indicate Fc = 0, Fz = 1 (normal byte)
                  RET
.fetch_ESC        CALL Getbyte                       ; byte in A.
                  RET  C                             ; serial port error or timeout
                  CP   ESC                           ; is it a ESC ESC sequense ?
                  RET  Z                             ; Yes, return as ESC (Fz = 1, normal byte)
                  CP   'B'                           ; ESC B HH sequence?
                  JR   Z, GetHexByte                 ; Yes, ESC B HH sequense...
                  OR   A                             ; Ensure Fc = 0 (no error) and Fz = 0
                  RET                                ; (an ESC Id is also never a 0 byte)
.GetHexByte
                  CALL RxByte                        ; Yes, ESC B HH sequense...
                  RET  C                             ; serial port error or timeout
                  CALL ConvAsciiHexHighNibble        ; convert Ascii hex nibble into integer
                  LD   B,A
                  CALL RxByte
                  RET  C                             ; serial port error or timeout
                  CALL ConvAsciiHexNibble            ; calculate second 4 bit nibble
                  OR   B                             ; 'binary' byte received.
                  CP   A                             ; return as normal byte (Fc = 0, Fz = 1)
                  RET


; ***********************************************************************
; Receive file stream via protocol level 9 to RAM file
; (CRLF conversion is handled by client, before CRC-block is sent)
; -----------------------------------------------------------------------
;
.FetchRamFileCrcBlock
                  SET  BB_Plvl9, (IX + RuntimeFlags-start_workspace) ; file reception is now enabled through Protocol level 9
.receive_crcblock_loop
                  LD   BC,256                        ; receive approx 128 bytes, but never more than 256 bytes...
                  LD   HL,file_buffer
                  CALL FetchBlock
                  JR   C, crcblock_aborted           ; error or timeout - communication stopped
                  JR   NZ, crcblock_aborted          ; block was not ESC terminated!

                  LD   (buflen),BC                   ; fetched an ESC id, should be ESC 'N'... (total size of block stored)
                  CALL BufCrc16                      ; CRC-16 of received block in DE

                  CP   'N'
                  JR   NZ,crcblock_aborted           ; ESC 'N' was expected - protocol error...

                  LD   HL,ScratchBuffer
                  LD   C,8
                  CALL FetchBlock                    ; fetch 4 bytes of Ascii CRC-16 to validate received block...
                  JR   C, crcblock_aborted           ; error or timeout - communication stopped
                  JR   NZ, crcblock_aborted          ; sub-block was not ESC terminated!

                  CP   ']'
                  JR   NZ, crcblock_aborted          ; ESC ']' was expected, protocol error

                  CALL ConvHexCrc16                  ; convert Ascii CRC-16 at (ScratchBuffer) to integer, return in HL
                  SBC  HL,DE                         ; CRC-16 match?
                  JR   Z, signal_received_block      ; Yes

                  DEC  (IX + CrcBlockRetries-start_workspace)  ; less max retries...
                  CALL Send_ESC_Z                    ; respond with ESC Z to terminal to re-send block
                  RET  NC                            ; A = ']'
                  JR   crcblock_aborted              ; timeout, terminal didn't fetch the acknowledge
.signal_received_block
                  LD   BC,(buflen)
                  LD   HL,file_buffer
                  BIT  BB_CharTrans,(IX + rtmflags)
                  CALL NZ,TranslateIncomingBlock     ; no need to check binary file validation, trans + crlf is already OFF...
                  CALL Flush_buffer                  ; flush successfully received block to RAM file...
                  JR   NC, rambuf_flushed
                  CALL Send_ESC_F                    ; signal I/O error / No Room to Terminal
                  JR   crcblock_aborted
.rambuf_flushed                                      ; CRC-block successfully flushed to RAM file
                  LD   (IX + CrcBlockRetries-start_workspace),128      ; reset to max CRC-block retries for new block
                  CALL Send_ESC_Y                    ; and respond with ESC Y to terminal to acknowledge
                  RET  NC
.crcblock_aborted
                  POP  BC                            ; ignore return address of this call to abort file reception
                  JR   comms_aborted


; ***********************************************************************
; Receive file stream from remote to RAM file
; -----------------------------------------------------------------------
; 1) receive file through byte I/O and buffer, or
; 2) block CRC-16 I/O (protocol level 9)
;
.ReceiveRamFile
                  CALL Reset_buffer_ptrs             ; initial buffer ready
                  RES  BB_Plvl9, (IX + RuntimeFlags-start_workspace)   ; file reception is by default old protocol
                  LD   (IX + CrcBlockRetries-start_workspace),128      ; CRC-block retries

; receive file through byte-fetch I/O, using buffer
.receive_file_loop
                  CALL FetchBytes
                  JR   C, comms_aborted              ; serial port error or timeout - communication stopped
                  JR   Z, byte_to_file               ; received byte to file
                  CP   '['
                  CALL Z,FetchRamFileCrcBlock        ; ESC '[', protocol level 9 CRC-16 data block is being sent...
                  CP   ']'
                  JR   Z,check_crcblock_tries        ; ESC ']', CRC-block completed, get another block or wait for EOF..
                  CP   'E'                           ; ESC 'E', EOF?
                  JR   Z, close_rcvd_file
                  JR   receive_file_loop
.check_crcblock_tries
                  XOR  A
                  OR   (IX + CrcBlockRetries-start_workspace)
                  JR   NZ,receive_file_loop
                  JR   comms_aborted
; byte in A to file...
.byte_to_file
                  BIT  BB_Plvl9, (IX + RuntimeFlags-start_workspace) ; file reception is by default std protocol
                  JR   NZ,receive_file_loop          ; we're in Protocol level 9, skip any noise because of Blink UART

                  CP   LF                            ; is it a line feed?
                  JR   NZ,no_linefeed                ; Yes, ignore LF (reverse CRLF) and fetch next byte...
                  BIT  BB_LnfdConv,(IX + rtmflags)
                  JR   NZ,receive_file_loop          ; LF conversion active - ignore LF (reverse CRLF) and fetch next byte...
.no_linefeed      CALL Write_buffer                  ; put byte into buffer
                  JR   C,no_memory                   ; write error - memory full
                  JR   receive_file_loop             ; fetch next byte from serial port

.no_memory        CALL Msg_No_Room
.comms_aborted    CALL Msg_file_aborted
                  JP   Abort_file
.close_rcvd_file                                     ; ESC 'E' received.
                  CALL Flush_buffer                  ; save contents of buffer...
                  JP   Close_file


; ***********************************************************************
; Generate CRC-16 of received CRC-block
;
; IN:
;  HL = point to buffer
;   C = size of buffer
;
; OUT:
;   DE = updated CRC-16
;
.BufCrc16
                  PUSH AF
                  LD   DE,$FFFF                      ; Reset to initial CRC-16 checksum
.crc16_loop
                  LD   A,(HL)
                  INC  HL
                  CALL UpdateCrc16                   ; iterate over buffer to update CRC-16 in DE...
                  DEC  C                             ;
                  JR   NZ, crc16_loop
                  POP  AF
                  RET


; **************************************************************************
; Update CRC-16 checksum
; Implemented by Dennis Gröning (part of XY-modem)
;
; Test cases:
;     DE = initial 0x0000, "123456789" generates 0x31C3 CRC-CCITT (XModem)
;     DE = initial 0xFFFF, "123456789" generates 0x29B1 CRC-CCITT (0xFFFF)
;
; IN:
;   A  = byte to update current CRC-16
;   DE = current CRC-16
; OUT:
;   DE = updated CRC-16
;
; Registers changed after return:
;       ..BC..HL/IXIY same
;       AF..DE../.... different
;
.UpdateCrc16
        xor     d
        ld      d,a
        ld      a,b
        ex      af,af'
        ld      b,8
.crc_next
        sla     d
        jr      c,ex_or
        sla     e
        jr      nc,no_ex_or
        set     0,d
.no_ex_or
        djnz    crc_next
        ex      af,af'
        ld      b,a
        ret
.ex_or
        ld      a,d
        xor     $10
        ld      d,a
        sla     e
        jr      nc,no_carry_to_h
        set     0,d
.no_carry_to_h
        ld      a,e
        xor     $21
        ld      e,a
        jr      no_ex_or


; ***********************************************************************
; Retrieve the 4 Ascii charecters of CRC-16 at (HL) and convert them to int
;
; IN:  HL = pointer to CRC16 (high-byte, low-byte order)
; OUT: HL = CRC-16 (16bit integer)
; -----------------------------------------------------------------------
.ConvHexCrc16
        push    af
        push    bc
        call    ConvAsciiHexByte                ; Hex Nibbles of High byte in A
        ld      b,a
        call    ConvAsciiHexByte                ; Hex Nibbles of low byte in A
        ld      h,b
        ld      l,a                             ; return HL = CRC-16
        pop     bc
        pop     af
        ret

; ***********************************************************************
; Convert Ascii Hex byte at (HL), high byte - low byte order, to int in A
.ConvAsciiHexByte
        ld      a,(hl)
        inc     hl
        call    ConvAsciiHexHighNibble          ; convert Ascii hex nibble into integer
        ld      c,a
        ld      a,(hl)
        inc     hl
        call    ConvAsciiHexNibble              ; calculate second 4 bit nibble
        or      c
        ret

; ***********************************************************************
.ConvAsciiHexHighNibble
        call    ConvAsciiHexNibble              ; convert Ascii hex nibble into integer
        rlca
        rlca
        rlca
        rlca                                    ; first hex nibble * 16
        ret

; ***********************************************************************
.ConvAsciiHexNibble
        cp      $3a                             ; digit >= "a"?
        jr      nc,hex_alpha                    ; digit is in interval "a" - "f"
        sub     $30                             ; digit is in interval "0" - "9"
        ret
.hex_alpha
        sub     $37
        ret


; **************************************************************************
; if file buffer contents is binary, disable trans + CrLf conversion
; (if translation is already turned off, this routine exits immediately)
;
;
; IN:
;     BC = Buffer length
;     HL = Start of of buffer
;
; OUT:
;     BB_LnfdConv,(IX + rtmflags) = 0, if file (buffer) contents is binary
;     BB_CharTrans,(IX + rtmflags) = 0, if file (buffer) contents is binary
;
; Registers changed after return:
;       A.BCDEHL/IXIY same
;       .F....../.... different
;
.IsBinaryFile
        bit     BB_LnfdConv,(IX + rtmflags)
        jr      nz, init_validate_buf
        bit     BB_CharTrans,(IX + rtmflags)
        ret     z                               ; no need to check binary file validation, trans + crlf is already OFF...

.init_validate_buf
        push    af                              ; CRLF conversion or Translation enabled, check block for binary data
        push    bc
        push    hl
.validate_buf
        ld      a,b
        or      c
        jr      z,exit_isbf                     ; end of buffer reached, no binary data discovered..

        ld      a,(hl)
        inc     hl
        dec     bc
        cp      32
        jr      nc,validate_buf                 ; buf[i] >= 32 (ascii)
        cp      CR
        jr      z,validate_buf                  ; buf[i] = CR (ascii)
        jr      nc,disable_conv                 ; 13 < buf[i] < 32 (binary)
        cp      LF
        jr      z,validate_buf                  ; buf[i] = LF (ascii)
        cp      9
        jr      z,validate_buf                  ; buf[i] = TAB (ascii)

.disable_conv
        res     BB_LnfdConv,(IX + rtmflags)     ; everything else is regarded as binary data, disable conversion + translation
        res     BB_CharTrans,(IX + rtmflags)
.exit_isbf
        pop     hl
        pop     bc
        pop     af
        ret