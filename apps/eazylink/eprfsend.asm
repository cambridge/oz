; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2014
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************

        module FileEprSendFile

        xdef FileEprSendFile
        xref SendFileBuffer
        xref FileEprTransferBlockSize

        include "eprom.def"
        include "error.def"
        include "memory.def"


; ************************************************************************
;
; Transmit file (image) from File Eprom Card, identified by File Entry at BHL
; (B=00h-FFh embedded slot mask, HL=0000h-3FFFh bank offset) to serial port.
;
; IN:
;    BHL = pointer to Eprom File Entry (bits 7,6 of B is the slot mask)
;
; OUT:
;    Fc = 0, Fz = 0
;         File Image transferred successfully to serial port.
;
;    Fc = 1,
;         A = RC_ONF, File Eprom or File Entry was not found in slot
;         A = RC_xxx, I/O error during saving process.
;    Fz = 1, Time out on serial port
;
; Registers changed after return:
;    ..BCDEHL/IXIY same
;    AF....../.... different
;
; ------------------------------------------------------------------------
; Design & programming (based on original FileEprFetchFile):
;       Gunther Strube, March 2011
; ------------------------------------------------------------------------
;
.FileEprSendFile    PUSH BC
                    PUSH DE
                    PUSH HL

                    LD   A,EP_Size
                    OZ   OS_Epr                   ; get size of file in CDE, of entry BHL
                    JR   C, no_entry              ; there was no File Eprom Entry at BHL!

                    PUSH BC
                    PUSH DE                       ; preserve size of file (in CDE)
                    LD   A,EP_Image
                    OZ   OS_Epr                   ; get pointer to file image in BHL
                    EXX
                    POP  DE
                    POP  BC                       ; file size in CDE'
                    EXX
                    JR   C, no_entry
.write_loop
                    EXX                           ; file size = 0 ?
                    LD   A,D
                    OR   E
                    EXX
                    JR   NZ, get_block            ; No, bytes still left to transfer to RAM...
                    EXX
                    XOR  A
                    OR   C
                    EXX
                    JR   Z, fetch_completed       ; Yes, completed Eprom Image transfer to serial port!

.get_block          CALL FileEprTransferBlockSize ; get size of block to transfer in HL'
                    CALL TransferFileBlock        ; then transfer block at BHL to serial port...
                    JR   C,exit_fetch             ; Serial port I/O error occurred, abort...
                    JR   write_loop
.fetch_completed
                    SCF
                    CCF
                    SET 0,A
                    OR  A
                    JR  exit_fetch                ; Fc = 0, Fz = 0, indeicate successful file image transfer..
.no_entry
                    SCF
                    LD   A, RC_ONF                ; return Fc = 1, error code "Object not found"
.exit_fetch
                    POP  HL
                    POP  DE
                    POP  BC
                    RET


; ************************************************************************
;
; Transfer File Block, at (BHL), size hl to serial port
;
; IN:
;    BHL = pointer to file block
;    hl' = size of block to transfer
;
; OUT:
;    Fc = ?, file I/O status
;    A = RC_xxx, if I/O error occurred
;    BHL = updated to point at first byte of next block to save (or EOF)
;
.TransferFileBlock
                    PUSH BC                       ; preserve bank number of ext. address

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1
                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment

                    PUSH BC                       ; preserve original bank binding of segment

                    EXX
                    PUSH BC
                    PUSH DE                       ; preserve remaining file size...
                    PUSH HL
                    EXX                           ; HL = source (inside current bank B)
                    POP  BC                       ; BC = length of block to transfer to serial port
                    CALL SendFileBuffer
                    EXX
                    POP  DE
                    POP  BC                       ; restore remaining file size...
                    EXX

                    POP  BC
                    RST  OZ_MPB                   ; restore previous segment bank binding...
                    JR   C, err_TransferFileBlock ; transmitting current block failed, exit...

                    POP  BC
                    LD   A,H
                    AND  @00111111
                    LD   H,A
                    OR   L
                    RET  NZ                       ; we're still inside the current bank after block save...
                    INC  B                        ; set offset at start of new bank
                    RET                           ; always returns Fc = 0, Fz = 0
.err_TransferFileBlock
                    POP  BC
                    RET
