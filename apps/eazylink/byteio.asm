; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2014
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************

    MODULE Serialport_IO

    XREF ErrHandler
    XREF System_Error
    XREF Write_Message, Message2
    XREF SendString

    XDEF Check_synch, Send_Synch, SendString_ackn
    XDEF Send_ESC_byte_ackn
    XDEF GetByte, GetByte_ackn, PutByte, PutByte_ackn
    XDEF TranslateIncoming, TranslateOutgoing
    XDEF Getbyte_raw_ackn, RxByte
    XDEF Dump_serport_in_byte, Dump_serport_out_byte

    INCLUDE "rtmvars.def"
    INCLUDE "fileio.def"
    INCLUDE "error.def"
    INCLUDE "serintfc.def"


; ***********************************************************************
; H = <startsynch>, L = <end_synch> ID
;
.Check_synch
.Synch_loop       CALL RxByte                        ; byte in A.
                  RET  C                             ; system error or timeout
                  CP   L                             ; check end synch byte
                  RET  Z                             ; eof synch...
                  CP   H                             ; receive next synch byte?
                  JR   Z, Synch_loop                 ; synch byte, continue...
                  OR   A                             ; exit broken synch sequnce..
                  RET


; ***********************************************************************
.Send_synch       LD   B,2
.Send_synch_loop  LD   A,H                           ; start, body of synch...
                  CALL SxByte                        ; B = length of synch...
                  RET  C                             ; return if ESC pressed or timeout
                  DJNZ,Send_synch_loop
                  LD   A,L                           ; end of synch...
                  JR   SxByte


; ***********************************************************************
.Getbyte
                  CALL RxByte                        ; receive a byte from serial port via OZ
                  RET  C
                  BIT  BB_CharTrans,(IX + rtmflags)
                  RET  Z                             ; skip, translation not enabled..

; ***********************************************************************
.TranslateIncoming
                  PUSH HL
                  LD   HL,TraTableOut
                  CALL TranslateByte                 ; A = byte received & translated
                  POP  HL
                  RET


; ***********************************************************************
.TranslateOutgoing
                  PUSH HL
                  LD   HL,TraTableIn
                  CALL TranslateByte                 ; A = byte received & translated
                  POP  HL
                  RET


; ***********************************************************************
; byte in A.
;
.TranslateByte    PUSH BC                            ; HL >> Translation Table
                  LD   B,0
                  LD   C,A
                  ADD  HL,BC                         ; Offset calculated to translated byte... (Fc = 0 always)
                  LD   A,(HL)                        ; get translated byte...
                  POP  BC
                  RET


; ***********************************************************************
.RxByte           PUSH BC
                  PUSH HL
if EAZYLINK_TESTPROTOCOL
                  PUSH IX
                  LD   IX,(protctestfile_handle)
                  OZ   Os_Gb                         ; get byte from simulated serial port input stream
                  JR   NC,got_serportbyte
                  LD   A,RC_Time                     ; RC_Eof: signal RC_Time when last byte is fetched from input stream
.got_serportbyte
                  POP  IX
else
                  LD   BC,EazyLink_DefaultTimeout
                  LD   L, Si_Gbt
                  OZ   Os_Si                         ; get a byte from serial port, using OZ standard interface
endif
                  JR   C,io_check
                  BIT  BB_SerportLog,(IX + rtmflags)
                  CALL NZ,Dump_serport_in_byte       ; dump to debugging serport input file (if enabled)
.io_check         CALL C,Check_timeout
                  POP  HL
                  POP  BC
                  RET


; ***********************************************************************
.Putbyte
                  BIT  BB_CharTrans,(IX + rtmflags)
                  CALL NZ,TranslateOutgoing          ; Translate byte before sending...

; ***********************************************************************
.SxByte           PUSH BC
                  PUSH HL
if EAZYLINK_TESTPROTOCOL
                  ; TESTPROTOCOL: don't send anything to real serial port, just log
else
                  LD   BC,EazyLink_DefaultTimeout
                  LD   L, Si_Pbt
                  OZ   Os_Si
                  JR   C,io_check
endif
                  BIT  BB_SerportLog,(IX + rtmflags)
                  CALL NZ,Dump_serport_out_byte      ; dump to debugging serport output file (if enabled)
                  JR   io_check


; ***********************************************************************
.Check_timeout    CALL ErrHandler
                  CP   RC_Time                       ; timeout?
                  CALL Z,timeout_flag
                  SCF
                  RET                                ; other errors.
.timeout_flag     LD   HL,message2                   ; 'Waiting...'
                  JP   Write_message


; ***********************************************************************
; PCLINK II protocol
; Get byte from serial port and acknowledge
;
.Getbyte_ackn
                  CALL RxByte                        ; receive a byte from serial port (OZ or hardware interface)
                  RET  C
                  PUSH DE
                  LD   D,A
                  XOR  A                             ; acknowledge byte...
                  CALL SxByte
                  LD   A,D
                  POP  DE
                  RET  C                             ; system error, e.g. ESC pressed...
                  BIT  BB_CharTrans,(IX + rtmflags)
                  RET  Z                             ; Translation not enabled
                  JR   TranslateIncoming


; ***********************************************************************
; PCLINK II protocol
; Get byte and acknowledge (no translation)...
;
.Getbyte_raw_ackn
                  CALL RxByte                        ; receive a byte from serial port via OZ
                  RET  C
                  PUSH DE
                  LD   D,A
                  XOR  A                             ; acknowledge byte...
                  CALL SxByte
                  LD   A,D
                  POP  DE
                  RET


; ***********************************************************************
; PCLINK II protocol
; Send byte to serial port without translation
;
.Putbyte_raw_ackn
                  CALL SxByte
                  RET  C
                  JR   RxByte                        ; receive a byte from serial port via OZ


; ***********************************************************************
; PCLINK II protocol
; Send byte to serial port with translation
;
.Putbyte_ackn
                  BIT  BB_CharTrans,(IX + rtmflags)
                  CALL NZ,TranslateOutgoing          ; Translate byte before sending...
                  CALL SxByte
                  RET  C
                  JR   RxByte                        ; receive acknowledge byte from serial port via OZ


; ***********************************************************************
; If Serial port input stream copy is enabled,
;     BIT  BB_SerportLog,(IX + rtmflags) = 1
; then dump recently fetched byte into INPUT dump file
;
.Dump_serport_in_byte
                  PUSH IX
                  PUSH AF

                  LD   IX,(serfile_in_handle)
                  OZ   Os_Pb                          ; and dump a copy into debug file

                  POP  AF
                  POP  IX
                  RET


; ***********************************************************************
; If Serial port output stream copy is enabled,
;     BIT  BB_SerportLog,(IX + rtmflags) = 1
; then dump output byte to serial into OUTPUT dump file
;
.Dump_serport_out_byte
                  PUSH IX
                  PUSH AF

                  LD   IX,(serfile_out_handle)
                  OZ   Os_Pb                         ; and dump a copy into debug file

.exit_Dump_serport_out_byte
                  POP  AF
                  POP  IX
                  RET


; ***********************************************************************
; PCLINK II protocol
; Send sequense of bytes, untranslated.
;
.SendString_ackn  PUSH BC                            ; HL >> string, 0 terminated...
                  PUSH AF
.SendLoop_ackn    LD   A,(HL)
                  CP   0                             ; check for terminator
                  JR   Z,end_SendString_ackn
                  CALL Putbyte_ackn                  ; put byte to serial port
                  JR   C,err_SendString_ackn         ; error - stop transmit...
                  INC  HL                            ; byte sent , continue
                  JR   SendLoop_ackn
.end_SendString_ackn
                  POP  AF
                  LD   B,A
                  SET  0,A
                  OR   A                             ; reset Z flag.
                  LD   A,B                           ; restore original contents of A
                  POP  BC                            ; restore original contents of BC
                  RET
.err_SendString_ackn
                  EX   AF,AF'                        ; save contents of F register
                  POP  AF
                  LD   B,A
                  EX   AF,AF'                        ; restore F register
                  LD   A,B                           ; restore original contents of A
                  POP  BC                            ; restore original contents of BC
                  RET


; ***********************************************************************
; PCLINK II protocol
; Send ESC B HH sequence of byte in A, acknowledged.
;
.Send_ESC_byte_ackn
                  PUSH BC
                  PUSH HL
                  LD   B,A

                  LD   A,$1B
                  CALL Putbyte_raw_ackn        ; send ESC
                  JR   C,exit_Send_ESC_byte_ackn
                  LD   A,'B'
                  CALL Putbyte_raw_ackn        ; send 'B'
                  JR   C,exit_Send_ESC_byte_ackn

                  LD   A,B
                  AND  $F0
                  RRCA
                  RRCA
                  RRCA
                  RRCA
                  CALL HexNibble
                  CALL Putbyte_raw_ackn        ; send high nibble in HEX of byte
                  JR   C,exit_Send_ESC_byte_ackn

                  LD   A,B
                  AND  $0F
                  CALL HexNibble
                  CALL Putbyte_raw_ackn        ; send low nibble in HEX of byte

.exit_Send_ESC_byte_ackn
                  POP  HL
                  POP  BC
                  RET

.HexNibble        CP   $0A
                  JR   NC, HexNibble_16
                  ADD  A,$30
                  RET
.HexNibble_16     ADD  A,$37
                  RET

