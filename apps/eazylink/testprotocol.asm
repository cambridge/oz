; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2015
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************

    MODULE ProtocolTests


; *************************************************************************************
; Protocol test constants (outcommented for copy/paste use)
; *************************************************************************************

DEFC ESC = 27, CR = 13, LF = 10

; Terminators
;
; DEFB ESC,'Z'
; DEFB ESC,'F'
; DEFB ESC,'E'
; DEFB ESC,'N'
; DEFB ESC,'Y'
; DEFB ESC,'B','F','F'               ; send FFh
; DEFB ESC, ESC
; DEFB ESC, CR, LF


; EazyLink commands identifiers
;
; DEFB 5,5,6,ESC,'A'                 ; PCLINK  II 'Hello'
; DEFB 5,5,6,ESC,'H'                 ; PCLINK  II Devices
; DEFB 5,5,6,ESC,'D'                 ; PCLINK  II Directories
; DEFB 5,5,6,ESC,'N'                 ; PCLINK  II Files
; DEFB 5,5,6,ESC,'S'                 ; PCLINK  II Send file (from Z88)
; DEFB 5,5,6,ESC,'G'                 ; PCLINK  II Receive f. (from term.)
; DEFB 5,5,6,ESC,'Q'                 ; PCLINK  II Quit
; DEFB 1,1,2,ESC,'a'                 ; EasyLink 'Hello'
; DEFB 1,1,2,ESC,'h'                 ; EasyLink Devices
; DEFB 1,1,2,ESC,'d'                 ; EasyLink Directories
; DEFB 1,1,2,ESC,'n'                 ; EasyLink Files
; DEFB 1,1,2,ESC,'s'                 ; EasyLink Send files (ImpExp)
; DEFB 1,1,2,ESC,'b'                 ; EasyLink Receive files (ImpExp)
; DEFB 1,1,2,ESC,'k'                 ; EasyLink Backup files
; DEFB 1,1,2,ESC,'q'                 ; EasyLink Quit
; DEFB 1,1,2,ESC,'t'                 ; EasyLink Translation   ON
; DEFB 1,1,2,ESC,'T'                 ; EasyLink Translation   OFF
; DEFB 1,1,2,ESC,'c'                 ; EasyLink CRLF translation   ON
; DEFB 1,1,2,ESC,'C'                 ; EasyLink CRLF translation   OFF
; DEFB 1,1,2,ESC,'v'                 ; EasyLink Application & Protocol version ("X.X-pp")
; DEFB 1,1,2,ESC,'x'                 ; EasyLink File Size
; DEFB 1,1,2,ESC,'u'                 ; EasyLink File Update Date Stamp
; DEFB 1,1,2,ESC,'U'                 ; EasyLink Set File Date Stamp
; DEFB 1,1,2,ESC,'f'                 ; EasyLink File Exist query
; DEFB 1,1,2,ESC,'z'                 ; EasyLink Install translation table
; DEFB 1,1,2,ESC,'r'                 ; EasyLink Delete file on Z88
; DEFB 1,1,2,ESC,'y'                 ; EazyLink Create directory on Z88
; DEFB 1,1,2,ESC,'w'                 ; EazyLink Rename Filename on Z88
; DEFB 1,1,2,ESC,'g'                 ; EazyLink Get default Device/Directory
; DEFB 1,1,2,ESC,'m'                 ; EazyLink Get Estimated Free Memory
; DEFB 1,1,2,ESC,'p'                 ; EazyLink Set System Clock
; DEFB 1,1,2,ESC,'e'                 ; EazyLink Get System Clock
; DEFB 1,1,2,ESC,'M'                 ; EazyLink Get Explicit Free Memory (for RAM device)
; DEFB 1,1,2,ESC,'i'                 ; EazyLink File CRC-32
; DEFB 1,1,2,ESC,'O'                 ; EazyLink Device InfO
; DEFB 1,1,2,ESC,'L'                 ; EazyLink Change to Light Speed
; DEFB 1,1,2,ESC,'X'                 ; EazyLink Switch to Xon/Xoff RS232 protocol (software handshaking)
; DEFB 1,1,2,ESC,'Y'                 ; EazyLink Switch to RTS/CTS RS232 protocol (hardware handshaking)



; *************************************************************************************
; Protocol tests. Examine results in :RAM.0/rs232rsv.log and :RAM.0/rs232snd.log
; *************************************************************************************

; some white noise to fix problems of Z88 file I/O when appending bytes to a file
DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

; Test #1: send a simple 'Hello' to EazyLink
;DEFB 1,1,2,ESC,'a'


; Test #2, send devices
;DEFB 1,1,2,ESC,'h'


; Test #3, let EazyLink receive a file
;DEFB 1,1,2,ESC,'b'                          ; Batch Receive
;DEFM ESC,'N',":RAM.0/afile.txt",ESC,'F'     ; Filename
;DEFM "some file contents...",CR
;DEFB ESC,'E'                                ; EOF
;DEFB ESC,'Z'                                ; end Batch Receive


; Test #4, let EazyLink receive a file, protocol level 8 or lower
; Enable CRLF conversion
;DEFB 1,1,2,ESC,'c'                          ; EasyLink CRLF translation ON
;DEFB 1,1,2,ESC,'b'                          ; Batch Receive
;DEFM ESC,'N',":RAM.0/afile.txt",ESC,'F'     ; Filename
;DEFM "some file contents...",CR,LF
;DEFM "first line...",CR,LF
;DEFM "second line...",CR,LF
;DEFM "third line...",CR,LF
;DEFB ESC,'E'                                ; EOF
;DEFB ESC,'Z'                                ; end Batch Receive
; the prev. received file got CRLF converted to CR
; when sent back to terminal, the file will have CRLF again..
;DEFB 1,1,2,ESC,'s'                          ; EasyLink Send files to Terminal
;DEFM ":RAM.0/afile.txt",ESC,'F'             ; see file contents in :RAM.0/rs232snd.log


; Test #5, let EazyLink receive a file using protocol level 9 to :EPR.1 File Card
; (this test will report error if no file area exists in slot 1)
;DEFB 1,1,2,ESC,'b'                              ; Batch Receive
;DEFM ESC,'N',":EPR.1/afile.txt",ESC,'F'         ; Filename
;DEFM ESC,'[',"123456789",ESC,'N',"29B1",ESC,']' ; Here, EazyLink will send ESC Y to terminal...
;DEFB ESC,'E'                                    ; EOF
;DEFB ESC,'Z'                                    ; end Batch Receive


; Test #6, let EazyLink receive a binary file using protocol level 9 to RAM file
; (64 bytes, ASCI 00 - 63)
;DEFB 1,1,2,ESC,'b'                          		; Batch Receive
;DEFM ESC,'N',":RAM.0/afile.bin",ESC,'F'     		; Filename
;DEFB ESC,'['
;DEFM 0,1,2,3,4,5,6,7,8,9,10
;DEFM 11,12,13,14,15,16,17,18,19,20
;DEFB 21,22,23,24,25,26,ESC,ESC,28,29,30             ; ESC is sent as ESC ESC
;DEFB 31,32,33,34,35,36,37,38,39,40
;DEFB 41,42,43,44,45,46,47,48,49,50
;DEFB 51,52,53,54,55,56,57,58,59,60
;DEFB 61,62,63
;DEFM ESC,'N',"FD2F",ESC,']'
;DEFB ESC,'E'                                		; EOF
;DEFB ESC,'Z'                                		; end Batch Receive

; Test #7, let EazyLink receive a file using protocol level 9 to :EPR.1 File Card, including
; simulation of CRC-block failures, acknowledge to re-transmit.
; (this test will report error if no file area exists in slot 1)
DEFB 1,1,2,ESC,'b'                              ; Batch Receive
DEFM ESC,'N',":EPR.1/afile.txt",ESC,'F'         ; Filename
DEFM ESC,'[',"123.56789",ESC,'N',"29B1",ESC,']' ; Here, EazyLink will send ESC Z to terminal (CRC error)...
DEFM ESC,'[',"1234.6789",ESC,'N',"29B1",ESC,']' ; Here, EazyLink will send ESC Z to terminal (CRC error)...
DEFM ESC,'[',"123456789",ESC,'N',"29B1",ESC,']' ; Here, EazyLink will send ESC Y to terminal...
DEFB ESC,'E'                                    ; EOF
DEFB ESC,'Z'                                    ; end Batch Receive
