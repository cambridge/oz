; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2014
;
; 32bit Cyclic Redundancy Checksum Management for EazyLink
; CRC algorithm from UnZip, by Garry Lancaster, Copyright 1999, released as GPL.
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************


     module Crc32File

     include "fileio.def"
     include "crc32.def"
     include "integer.def"
     include "memory.def"
     include "eprom.def"
     include "rtmvars.def"

     xdef ESC_I_cmd, HexNibble

     xref SendString, Send_ESC_N, Send_ESC_Z, Debug_message, Message37
     xref CheckEprName, Set_TraFlag, Restore_TraFlag, Fetch_pathname
     xref Get_file_handle, Close_file, CheckFileAreaOfSlot, Msg_Command_aborted


; ************************************************************
; Get CRC-32 of file (RAM or File Card)
;
; Client:      ESC "i" <Filename> ESC "Z"
;
; Server:      ESC "N" <CRC32> ESC "Z"  (File found)
;              ESC "Z"                  (File not found)
;
.ESC_I_cmd     CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JR   C,esc_i_aborted

               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JR   Z, get_eprfilecrc32           ; Yes, try to get CRC-32 of file in File Area...

               LD   A, OP_IN                      ; open file for transfer...
               LD   D,H
               LD   E,L                           ; (explicit filename overwrite original fname)
               CALL Get_file_handle               ; open file
               JR   C, file_not_found             ; ups, file not available
               CALL CrcRamFile
               CALL Close_file                    ; close file
.crc_result
               LD   (File_ptr),BC                 ; low word of CRC-32
               LD   (File_ptr+2),DE               ; high word of CRC-32

.send_filecrc32
               LD   HL, File_ptr                  ; convert 32bit integer
               LD   DE, filename_buffer           ; to an ASCII string
               CALL Int32Hex

               CALL Send_ESC_N
               JR   C, esc_i_aborted

               LD   HL,filename_buffer            ; write File CRC-32 as ASCII string to Client
               CALL SendString
               JR   C, esc_i_aborted
.file_not_found
               CALL Send_ESC_Z
               JR   C, esc_i_aborted
               JR   end_ESC_I_cmd

.esc_i_aborted CALL Msg_Command_aborted
.end_ESC_I_cmd
               XOR A
               RET
.get_eprfilecrc32                                 ; Get CRC-32 of File in File Area
               call    CheckFileAreaOfSlot
               jr      c,file_not_found           ; this slot had no file area (no card)...
               jr      nz,file_not_found          ; this slot had no file area (card, but no file area)

               ld      de,filename_buffer+6       ; search for filename beginning at "/" in filea area of slot C
               ld      a, EP_Find
               oz      OS_Epr                     ; search for filename on file eprom...
               jr      c,file_not_found           ; this slot had no file area (no card)...
               jr      nz,file_not_found          ; File Entry was not found...

               call    FileEprFileCrc32           ; return CRC in DEBC
               jr      crc_result


; *************************************************************************************
;
; Perform a CRC-32 of RAM file, already opened by caller, from current file pointer until EOF.
; If the complete file is to be CRC'ed then it is vital that the current file pointer
; is at the beginning of the file (use FA_PTR / OS_FWM to reset file pointer) before
; executing this routine.
;
; In:
;    None.
;
; Out:
;    Fc = 0,
;    DEBC = CRC
;
; Registers changed after return:
;    ......../IXIY same
;    AFBCDEHL/.... different
;
.CrcRamFile
               push ix
               ld   ix,(file_handle)
               ld   a, CRC_FILE
               ld   hl,File_buffer                ; use buffer
               ld   bc,FileBufferSize
               ld   de,0                          ; Use FFFFFFFFh as initial Crc-32
               oz   GN_Crc                        ; get CRC-32 of Open file (with buffer)
               pop  ix
               ret
; *************************************************************************************


; ***************************************************************************************************
;
; Standard Z88 File Eprom Format.
;
; Generate CRC-32 of file entry in file area.
;
; IN:
;         BHL = pointer to file entry to be CRC-32 scanned
; OUT:
;         Fc = 0,
;              DEBC = CRC-32
;         Fc = 1,
;              File Entry at BHL was not found.
;                   A = RC_Onf
;
; Registers changed on return:
;    ......../..IY ........ same
;    AFBCDEHL/IX.. afbcdehl different
;
; -------------------------------------------------------------------------
; Design & Programming by Gunther Strube, June 2012
; -------------------------------------------------------------------------
;
.FileEprFileCrc32
               ld   A, CRC_EPFILE
               ld   de,0                          ; Use FFFFFFFFh as initial Crc-32
               oz   GN_Crc                        ; get CRC-32 of File Entry
               ret


; ***************************************************************************************************
;
; Convert 32bit integer at (HL) to hexadecimal string at (DE)
; Integer is in low byte, high byte order
.Int32Hex
                    push hl
                    ex   de,hl
                    ld   bc,8
                    add  hl,bc
                    ex   de,hl
                    pop  hl
                    xor  a
                    ld   (de),a              ; DE points a null-terminator
                    dec  de                  ; ready for first nibble of lowest byte of 32bit integer

                    ld   b,4                 ; 32bit integer is 4 bytes
.conv_hexloop
                    ld   a,(hl)
                    and  $0f
                    call hexnibble
                    ld   (de),a
                    dec  de

                    ld   a,(hl)
                    and  $f0
                    rrca
                    rrca
                    rrca
                    rrca
                    call hexnibble
                    ld   (de),a
                    dec  de
                    inc  hl                  ; get ready for next byte of 32bit integer..
                    djnz conv_hexloop
                    ret

.HexNibble          cp   $0a
                    jr   nc, hexnibble_16
                    add  a,$30
                    ret
.hexnibble_16       add  a,$37
                    ret
