
EasyLink compilation notes

To compile the EasyLink applications, execute the following:

1) Select the directory holding the EasyLink files as the current directory.
2) Execute:
                make.eazylink.bat (DOS/Windows command line script)
                make.eazylink.sh (UNIX/LINUX shell script)

   This will create the executable files "easylink.bin".
