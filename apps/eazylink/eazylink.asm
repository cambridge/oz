; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2014
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************

    MODULE EasyLink_main

; ******************************************************************************
;
;  Converted to QL Z80 Cross Assembler format, 08.03.91             ** V2
;  Improved and converted to Z88 ROM application format, 18.12.91

;  V0.241, 10.11.90 - 28.06.91                                      ** V2
;  V0.25   10.07.91                                                 ** V2
;  V0.26   15.07.91                                                 ** V2
;  V0.27   01.08.91                                                 ** V2
;  V0.271  ????????     transmitting ESC B 00 ...                 ** V2
;                       calling PCLINK II 'Hello' will also install
;                       Auto Translation and Auto CR > CRLF conversion.

;  V0.272  01.10.91     bug in Fetch_RAM_devices: 'File not found'    ** V2
;                       error when current directory for application
;                       isn't the root directory. Wrong device specifier.

;  V0.273  05.10.91     program crash if Search_filer receives timeout  ** V2
;                       on transmitting files. IX handle for serial
;                       port wasn't re-installed on timeout error.


; V3 updates and improvements:

;  V1.0    03.05.92     only 512 bytes in segment 0 are used as working buffer.
;                       system information are no longer put on a stack and then
;                       transmitted, but sent immediately on each found name.

;                       esc_h1_cmd (fetch z88 devices):
;                       wildcard is in segment 0 due to a bug in OZ.
;                       A '*' is appended to remote wildcard specifies.
;                       (request of file names and directories)

;                       a 'soft reset' of the serial port is now issued to install
;                       the defined communication parameters in the Panel.
;                       in V2 the serial port wasn't 'soft reset' which caused
;                       protocol errors since the panel parameters wasn't installed
;                       in the serial port driver (the Panel popdown does this
;                       automatically when ENTER is pressed).
;                       PC-link II don't issue a soft reset either which means
;                       that if the serial port previously had been installed with
;                       'Xon/Xoff Yes', this would cause protocol errors with the
;                       IBM PC LINK II program.
;
; V1.1                  Extended protocol with additional commands included (Multilink V2)
; 09.06.93              Split up into modules for new Z80 Cross Assembler

; 4.2                   Serial port protokol algorithms improved.
; V4.3, 03.03.95        Automatical directory creation on receiving files from outside
; V4.4                  Extended protokol fetch filename & directories commands changed:
;                       The wildcard hmust be explicitly spercified, e.g. ":ram.1/dir/*"

; V4.5, 05.09.96        ESC "v" command added: Client gets version of EasyLink Server
;                            and file protocol level
;                       ESC "x" command added: Client queries for file size.
;                       ESC "u" command added: Client queries for file Update Date Stamp
;                            ("dd-mm-yyyy hh:mm:ss" format)
;                       ESC "f" command added: Client queries for existence of file.
; 08.09.96              ESC "U" command added: Client sets (Create/Update) date
;                            stamp of file in Z88

; V4.6, 22.08.97        ESC "r" command added: Client sets EasyLink Server to delete file on Z88
;                       ESC "z" command added: Client queries EasyLink Server to
;                            update (re-load) the translation table
;                       Server release "4.6", protocol version updated to "02"

; V4.7, 07.10.97        ESC "y" command added: Client requests for Directory Create
;                       ESC "w" command added: Client requests for File Rename
;                       ESC "U" command extended: Update Date Stamp added...
; 09.10.97              ESC "g" command added: Client requests for default Device/Directory
; 18.10.97              ESC "m" command added: Client requests for estimated free memory
;
; V4.8, 27.10.97        ESC "u" command extended: Create Date Stamp added
;                       ESC "p" command added: Set System Clock
; V4.8, 17.11.97        ESC "e" command added: Get System Clock
;                       Server release "4.8", protocol version updated to "04"
;
; V4.9, 18.12.97        EazyLink appears now as a single popdown, accessed by #L.
;                       Command menu implemented in separate window which allowes:
;                            1. Toggle translation ON/OFF
;                            2. Toggle Line Feed Conversion ON/OFF
;                            3. Use std. ISO/LATIN 1 translations
;                            4. Load translations
;                            5. Quit EazyLink.
;
; V5.0, 20.12.97        Several messages were not placed in log window. Now fixed.
;                       OZ internal keyboard queue purged while using hardware scanning
;                       of keybard.
;
;                        ESC "M" command added: Get explicit free memory
;                        Protocol version updated to "05"
;
; V5.0.4, 07.06.2005     Serial port logging to "/serdump.in" and "/serdump.out".
;                        (Implemented as MTH commands)
;                        Auto Software handshaking used for PCLINK II protocol: Xon/Xoff Yes
;                        Auto Hardware handshaking used for EazyLink protocol (Xon/Xoff No)
;                        (Software handshaking is default when EazyLink is started)
;                        "PCLINK II" blinking message in topic window during PCLINK II protocol
;

    ; Various constant & operating system definitions...

     INCLUDE "stdio.def"
     INCLUDE "error.def"
     INCLUDE "integer.def"
     INCLUDE "time.def"
     INCLUDE "fileio.def"
     INCLUDE "eprom.def"
     INCLUDE "dor.def"
     INCLUDE "syspar.def"
     INCLUDE "serintfc.def"
     INCLUDE "director.def"
     INCLUDE "screen.def"
     INCLUDE "sysapps.def"
     INCLUDE "oz.def"

     INCLUDE "rtmvars.def"

     ORG EAZ_ORG

     XREF CreateDirectory
     XREF LoadTranslations
     XREF menu_banner, Command_banner
     XREF extended_synch, pclink_Synch, EscCommands, Subroutines
     XREF msg_serdmpfile, msg_serdmpfile_enable, msg_serdmpfile_disable
     XREF Message1, Message2, Message10, Message11, Message12, Message13
     XREF Message26, Message27, Message28
     XREF Message29, Message32, Message35, Message36, Message37, Message38, message39
     XREF Error_Message0, Error_Message1, Error_Message2, Error_Message3
     XREF Error_Message4, Error_Message5, Error_Message6
     XREF BaudRate9600, BaudRate38400, No_parameter, Yes_Parameter, EasyLinkVersion
     XREF Check_Synch, Send_Synch, RxByte, GetByte, PutByte, Getbyte_ackn
     XREF FetchBytes, FetchBlock
     XREF Get_file_handle, Get_File_Size, Close_file
     XREF Send_ESC_Y, Send_ESC_N, Send_ESC_Z
     XREF SendString
     XREF Fetch_pathname
     XREF rw_date
     XREF Use_StdTranslations
     XREF Dump_serport_in_byte, serdmpfile_in, serdmpfile_out
     XREF SafeSegmentMask, CheckEprName, CheckRamName, CheckFileAreaOfSlot
     XREF CreateFilename

     XDEF crctable
     XDEF ErrHandler
     XDEF Write_message
     XDEF Msg_File_Open_error, Msg_Protocol_error, Msg_file_aborted, Msg_No_Room
     XDEF Msg_file_received, Msg_file_sent, Msg_Command_aborted
     XDEF Set_Traflag, Restore_Traflag
     XDEF System_Error
     XDEF Open_serialport, Close_serialport
     XDEF ESC_T_cmd1, ESC_T_cmd2, ESC_C_cmd1, ESC_C_cmd2
     XDEF ESC_V_cmd, ESC_X_cmd, ESC_U_cmd, ESC_U_cmd2, ESC_F_cmd
     XDEF ESC_Z_cmd, ESC_R_cmd, ESC_Y_cmd, ESC_W_cmd, ESC_O_cmd
     XDEF ESC_G_cmd2, ESC_M_cmd, ESC_P_cmd, ESC_E_cmd, ESC_M_cmd2, ESC_L_cmd
     XDEF Switch_XonXoff_cmd, Switch_RtsCts_cmd
     XDEF UseHardwareHandshaking, UseSoftwareHandshaking
     XDEF KillPopdown
     XDEF Get_Time


; ***************************************************************************************************
; We are at beginning of segment 2, the entry point
;
               JP   app_main
               SCF
               RET
.app_main
               LD   A,(IX+$02)                    ; IX points at information block
               CP   $20+EAZ_BAD                   ; get end page+1 of contiguous RAM
               JR   Z, continue_ez                ; end page OK, RAM allocated...

               LD   A,RC_ROOM                     ; No Room for EazyLink, return to Index
               CALL_OZ(Os_Bye)                    ; EazyLink suicide...
.continue_ez
               ld   a,OZVERSION                   ; OZ V4.5 and later implements OS_Nq, NQ_Voz (verify OZ)
               ld   bc,NQ_Voz
               oz   OS_Nq                         ; This application only runs on latest ROM by principle
               jr   nc, init_eazylink
.not_compatible
               xor  a
               oz   OS_Bye                        ; return to Index in current running OZ... (not the OZ of these sources)

.init_eazylink
               LD   A,SC_Ena                      ; Enable escape detection
               CALL_OZ(Os_Esc)

               XOR  A
               LD   (MenuBarPosn),A               ; Display Menu Bar at top line the first time...
               LD   (UserToggles),A               ; Linefeed conversion OFF, Translations OFF

               CALL Appl_Main                     ; Call the code...
               JR   KillPopdown



; *************************************************************************************
;
.Errhandler    PUSH AF
               CP   RC_susp
               JR   Z, exit_errh
               CP   RC_esc
               JR   Z, akn_esc
               CP   RC_quit
               JR   Z, KillPopdown
.exit_errh     POP  AF
               RET
.akn_esc
               LD   A,SC_ACK                      ; acknowledge ESC detection
               CALL_OZ(OS_Esc)
.KillPopdown
               CALL Close_file                    ; close any opened/created file
if EAZYLINK_TESTPROTOCOL
               LD   IX,(protctestfile_handle)
               CALL_OZ (Gn_Cl)

               OZ   OS_Pout
               DEFM 1, "2H3","Test completed",13,10,0
               LD   BC,300
               OZ   OS_Dly                        ; pause for two seconds..
endif
               CALL Close_serportdump_filehandles ; close streams to serial port dump files (if open)
               CALL Restore_PanelSettings         ; restore original Panel Settings
               CALL_OZ(OS_pur)                    ; purge keyboard buffer
               LD   A,SC_SET                      ; ESC pressed.
               CALL_OZ(OS_Esc)
               XOR  A                             ; no error messages on quit
               CALL_OZ(OS_Bye)                    ; perform suicide, focus to Index...


; ***************************************************************************************************
.Appl_Main
               CALL LogWindow
               CALL CommandWindow
               CALL InitTraTable                  ; load translations file or install
               CALL InitHandles                   ; reset all I/O handles to zero
               CALL LoadTranslations              ; standard IBM - Z88 translation table.
               CALL ESC_T_cmd2                    ; No Auto translation
               CALL ESC_C_cmd2                    ; No CR conversion
               CALL Init_PanelSettings            ; set Transmit & Receive baud rates and store original values temporarily.

               LD   IX,start_workspace            ; point at base of workspace variable area
               LD   (IX + rtmflags),0             ; all runtime flags OFF

               LD   HL,message1                   ; 'Running'
               CALL Write_message
               CALL DisplayEazyLinkVersion        ; Display EazyLink version & protocol in bottom of topic window
               CALL DisplMenuBar                  ; Display initial menubar

if EAZYLINK_TESTPROTOCOL
               CALL EnableSerportLogging
               OZ   OS_Pout
               DEFM 1, "2H3","Start Testing...",13,10,0
               CALL OpenTestProtocolFile
               JR   NC, endless                   ; nothing to test, exit popdown...
               OZ   OS_Pout
               DEFM "Test file not found.",13,10,0
               JP   KillPopdown
endif

.endless
               CALL Fetch_synch                   ; 111112 & 555556 synch and ESC cmds
               JR   endless


; ***********************************************************************
;
; Display Command Window - use window "2"
;
.CommandWindow
               LD   B,0
               LD   HL, CmdWinDef
               OZ   GN_Win
               LD   HL, cmds
               CALL_OZ(Gn_Sop)
               RET
.cmds          DEFM 1, "2H2", 1, SD_DTS, 1, "2+T"
               DEFM " TOGGLE TRANSLATION MODE", 13, 10
               DEFM " TOGGLE LINEFEED MODE", 13, 10
               DEFM " USE ISO TRANSLATIONS", 13, 10
               DEFM " LOAD TRANSLATIONS", 13, 10
               DEFM " QUIT EAZYLINK", 13, 10
               DEFM 1, "2-T", 0


; ***********************************************************************
;
; Display Log Window - use window "3"
;
.LogWindow
               LD   B,0
               LD   HL, LogWinDef
               OZ   GN_Win
               RET


; *************************************************************************************
;
.DisplMenuBar  PUSH AF
               PUSH HL
               LD   HL,SelectMenuWindow
               CALL_OZ(Gn_Sop)
               LD   HL, xypos                     ; (old menu bar will be overwritten)
               CALL_OZ(Gn_Sop)
               LD   A,32                          ; display menu bar at (0,Y)
               CALL_OZ(Os_out)
               LD   A,(MenuBarPosn)               ; get Y position of menu bar
               ADD  A,32                          ; VDU...
               CALL_OZ(Os_out)
               LD   HL,MenuBarOn                  ; now display menu bar at cursor
               CALL_OZ(Gn_Sop)
               POP  HL
               POP  AF
               RET
.xypos         DEFM 1, "3@", 0
.SelectMenuWindow
               DEFM 1, "2H2", 1, "2-C", 0         ; activate menu window, no Cursor...
.MenuBarOn     DEFM 1, "2+R"                      ; set reverse video
               DEFM 1, "2A", 32+$1A, 0            ; XOR 'display' menu bar (20 chars wide)


; *************************************************************************************
;
.RemoveMenuBar PUSH AF
               PUSH HL
               LD   HL,SelectMenuWindow
               CALL_OZ(Gn_Sop)
               LD   HL, xypos                     ; (old menu bar will be overwritten)
               CALL_OZ(Gn_Sop)
               LD   A,32                          ; display menu bar at (0,Y)
               CALL_OZ(Os_out)
               LD   A,(MenuBarPosn)               ; get Y position of menu bar
               ADD  A,32                          ; VDU...
               CALL_OZ(Os_out)
               LD   HL,MenuBarOff                 ; now display menu bar at cursor
               CALL_OZ(Gn_Sop)
               POP  HL
               POP  AF
               RET
.MenuBarOff    DEFM 1, "2-R"                      ; set reverse video
               DEFM 1, "2A", 32+$1A, 0            ; apply 'display' menu bar (20 chars wide)


; *************************************************************************************
;
.Poll
.main_loop
               CALL PollInput                     ; Serial port data or key-press?
               LD   HL, MenuBarPosn
               CP   IN_ENT                        ; no shortcut cmd, ENTER ?
               JR   Z, get_command
               CP   IN_DWN                        ; Cursor Down ?
               JR   Z, MVbar_down
               CP   IN_UP                         ; Cursor Up ?
               JR   Z, MVbar_up

               CP   EazyLink_CC_dbgOn
               CALL Z,EnableSerportLogging        ; <>D, Enable Serial port logging
               CP   EazyLink_CC_dbgOff
               CALL Z,DisableSerportLogging       ; <>Z, Disable Serial port logging

               JR   main_loop                     ; ignore keypress, get another...

.MVbar_down
               CALL RemoveMenuBar
               LD   A,(HL)                        ; get Y position of menu bar
               CP   4                             ; has m.bar already reached bottom?
               JR   Z,Mbar_topwrap
               INC  A
               LD   (HL),A                        ; update new m.bar position
               CALL DisplMenuBar
               JR   main_loop                     ; display new m.bar position

.Mbar_topwrap
               LD   (HL),0
               CALL DisplMenuBar
               JR   main_loop

.MVbar_up
               CALL RemoveMenuBar
               LD   A,(HL)                        ; get Y position of menu bar
               CP   0                             ; has m.bar already reached top?
               JR   Z,Mbar_botwrap
               DEC  A
               LD   (HL),A                        ; update new m.bar position
               CALL DisplMenuBar
               JR   main_loop

.Mbar_botwrap  LD   A,4
               LD   (HL),A
               CALL DisplMenuBar
               JR   main_loop

.get_command   PUSH HL
               LD   A,(HL)                        ; use menu bar position as index to command
               CALL ActivateCommand               ; then execute...
               POP  HL
               JR   main_loop


; ******************************************************************************************
.PollInput
               LD   HL, -1
               LD   (PopDownTimeout),HL           ; reset timeout to approx 10 minutes
.pollinp_loop
if EAZYLINK_TESTPROTOCOL
               CALL RxByte                        ; get byte from simulated serial port input stream
               JP   C,KillPopdown                 ; RC_Time: No more bytes available in serial port input stream
else
               LD   BC,10                         ; keyboard polled, now check for serial port in 1/10 sec...
               LD   L, Si_Gbt
               OZ   Os_Si                         ; get a byte from serial port, using OZ standard interface
endif
               CALL C, CheckHandshakeMode         ; probably timeout from serial port, check for handshake signal
               CALL C, Errhandler                 ; no byte available (or ESC pressed)...
               JR   NC, evaluate_byte             ; then let it be processed by main loop.

               CALL C,ScanKeyboard                ; scan the keyboard, if no byte from serport
               CP   0
               CALL NZ,DisplayEazyLinkVersion     ; when using MTH, command window gets reset, so re-display when KD is used.
               RET  NZ                            ; Fc = 0, Fz = 0, return Key Code in A...
               JR   pollinp_loop
.evaluate_byte                                    ; byte available from serial port
               BIT  BB_SerportLog,(IX + rtmflags)
               CALL NZ,Dump_serport_in_byte       ; dump to debugging serport input file (if enabled)

               POP  HL                            ; remove this RETurn address
               POP  HL                            ; get RETurn address to Fetch_synch
               JP   (HL)


; ******************************************************************************************
; When there is a pause in the byte stream (a timeout occurred), check if a serial port
; handshake change was signaled in the previous main Fetch synch loop.
; If a change was signaled and the current serial port handshake settings are
; different to the signaled handshake mode, then re-configure the serial port while
; there is no communication activity from the client.
;
.CheckHandshakeMode
               PUSH AF
               PUSH BC

               LD   A,(PollHandshakeCounter)      ; get timeout counter for handshake check
               INC  A
               CP   5
               JR   NZ, exit_handshake_change     ; we're not allowing a handshake check until 0.5 second has passed

               LD   A,(CurrentSerportMode)
               LD   B,A                           ; get current serial handshake mode
               LD   A,(SignalSerportMode)
               OR   A
               JR   Z, no_handshake_change        ; no handshake change was signaled
               CP   B
               JR   Z, no_handshake_change        ; synch loop signaled current handshake
               CP   SerportXonXoffMode
               CALL Z, UseSoftwareHandshaking     ; change from Hardware Handshake to Xon/Xoff Handshake
               CALL NZ,UseHardwareHandshaking     ; change from Xon/Xoff Handshake to Hardware Handshake
.no_handshake_change
               XOR  A
               LD   (SignalSerportMode),A         ; clear handshake signal change flag
.exit_handshake_change
               LD   (PollHandshakeCounter),A      ; update timeout counter for next call to this routine
               POP  BC
               POP  AF
               RET


; ******************************************************************************************
;
; Scan keyboard, and return key codes for <ENTER>, <UP> and <DOWN> in A register.
; Return 0, if other keys are pressed, or keyboard timeout.
;
.ScanKeyboard  LD   BC,1
               CALL_OZ(OS_Tin)
               JR   C, CheckKeybTimeout
               CP   0
               JR   Z, ScanKeyboard
               CP   IN_DWN
               RET  Z
               CP   IN_UP
               RET  Z
               CP   IN_ENT
               RET  Z
               CP   EazyLink_CC_dbgOn   ; <>D
               RET  Z
               CP   EazyLink_CC_dbgOff  ; <>Z
               RET  Z
               XOR  A
               RET

.CheckKeybTimeout
               CP   RC_Time
               JR   Z, UpdateTimeout
               SCF
               CALL Errhandler
               RET                      ; return error to caller of ScanKeyboard
.UpdateTimeout
               LD   HL,(PopDownTimeout)
               LD   BC,5
               SBC  HL,BC
               LD   (PopDownTimeout),HL
               CALL C, ShutDown         ; 10 minute timeout reached, switch off
               XOR  A                   ; return 0, no valid key pressed
               RET
.ShutDown      CALL_OZ(OS_Off)
               RET


; ******************************************************************************************
;
; Activate command defined by index in A
;
.ActivateCommand
               RLCA                               ; index word boundary...
               LD   B,0
               LD   C,A
               LD   HL,Command_lookup             ; base of table
               ADD  HL,BC                         ; point at command index
               LD   E,(HL)                        ; get pointer to subroutine
               INC  HL
               LD   D,(HL)
               EX   DE,HL                         ; HL points at command subroutine
               JP   (HL)                          ; activate

.Command_lookup
               DEFW User_ToggleTranslation        ; toggle File Translation
               DEFW User_ToggleLineFeed           ; toggle File Linefeed Conversion
               DEFW User_StdTranslations          ; Use explicitly the ISO/IBM translations
               DEFW User_LoadTranslations         ; re-load "translate.dat" translations
               DEFW KillPopdown                   ; Terminate EazyLink
               DEFW disp_running


; ******************************************************************************************
;
.User_ToggleTranslation
               PUSH HL
               LD   HL, UserToggles
               LD   A,2
               XOR  (HL)
               LD   (HL),A
               BIT  1,(HL)
               PUSH AF
               CALL Z,ESC_T_cmd2                  ; File Translation OFF
               POP  AF
               CALL NZ,ESC_T_cmd1                 ; File Translation ON
               POP  HL
               JR   disp_running


; ******************************************************************************************
;
.User_ToggleLineFeed
               PUSH HL
               LD   HL, UserToggles
               LD   A,1
               XOR  (HL)
               LD   (HL),A
               BIT  0,(HL)
               PUSH AF
               CALL Z,ESC_C_cmd2                  ; CR conversion OFF
               POP  AF
               CALL NZ,ESC_C_cmd1                 ; CR conversion ON
               POP  HL
               JR   disp_running


; ******************************************************************************************
;
.User_StdTranslations
               CALL Use_StdTranslations           ; Use ISO/IBM translations...
               JR   disp_running


; ******************************************************************************************
;
.User_LoadTranslations
               CALL LoadTranslations              ; Load external "translate.dat" file...
.disp_running  LD   HL,message1                   ; 'Running'
               JP   Write_message


; ***********************************************************************
.Fetch_synch
.fetch_synch_loop
               CALL Poll                          ; get byte from serial port (and manage menu)
               CP   $01                           ; extended command protocol synch?
               JR   NZ,check_pclink_synch

               LD   IY,extended_synch
               LD   H,(IY+0)                      ; start and body of sequense...
               LD   L,(IY+1)                      ; end of sequense...
               CALL Check_synch                   ; is it really a synch?
               JR   C,Fetch_synch_loop            ; timeout or bad synch...

               XOR  A
               LD   (PollHandshakeCounter),A      ; reset timeout counter for handshake check

               CALL Send_synch                    ; acknowledge synch to terminal
               JR   C,fetch_synch_loop            ; return on system error or timeout - communication stopped
               CALL Extended_ESC_commands         ; synch sent - wait for command
               JR   fetch_synch_loop              ; wait for new synch & command

.check_pclink_synch
               CP   $05
               JR   NZ,fetch_synch_loop           ; ignore byte...
               LD   IY,pclink_synch
               LD   H,(IY+0)                      ; start and body of sequense...
               LD   L,(IY+1)                      ; end of sequense...
               CALL Check_synch                   ; is it really a synch?
               JR   C,Fetch_synch_loop            ; return on system error or timeout - communication stopped

               XOR  A
               LD   (PollHandshakeCounter),A      ; reset timeout counter for handshake check
               LD   A,SerportXonXoffMode          ; Pclink II protocol...
               LD   (SignalSerportMode),A         ; signal that Xon/Xoff handshake is needed

               CALL Send_synch                    ; acknowledge - B = length of synch
               JR   C,Fetch_synch_loop            ; return on system error or timeout - communication stopped
               CALL DisplayPclinkIIText           ; display a flashing 'PCLINK II' text in topic window
               CALL Pclink_ESC_commands           ; synch sent - wait for commands
               CALL C,RemovePclinkIIText
               JR   fetch_synch_loop              ; wait for new synch & command


; ***********************************************************************
.Extended_ESC_commands                            ; synch sent - wait for commands
               CALL FetchBytes
               RET  C                             ; serial port error or timeout - communication stopped
               JP   Z, Msg_Protocol_error         ; no ESC id (normal byte), protocol error...
               JR   EscCommand                    ; Find command, and execute


.PClink_ESC_commands                              ; synch sent - wait for commands
               CALL Getbyte_ackn
               RET  C
               CP   ESC
               JP   NZ, Msg_Protocol_error        ; no ESC id, protocol error...
               CALL Getbyte_ackn
               RET  C


.EscCommand    LD   HL,EscCommands                ; Find command, and execute
               LD   B, TotalOfCmds                ; total of different commands
               LD   C,0                           ; offset counter
.find_command_loop
               CP   (HL)
               JR   Z,found_command
               INC  C
               INC  HL
               DJNZ,find_command_loop
               RET

.found_command
               LD   B,0                           ; communication commences...
               SLA  C                             ; offset * 2 (find word boundary)
               LD   HL,subroutines
               ADD  HL,BC                         ; offset to address
               LD   E,(HL)                        ; subroutine returns to calling
               INC  HL
               LD   D,(HL)                        ; program by issuing a RET.
               EX   DE,HL
               JP   (HL)                          ; - go to subroutine...

.GreyCommandWindow
               PUSH HL
               LD   HL, greycmdwin
               CALL_OZ(GN_Sop)
               POP  HL
               RET
.greycmdwin    DEFM 1, "2H2", 1, "2G+", 0


; ***********************************************************************
.UseSoftwareHandshaking
               PUSH AF
               PUSH BC
               PUSH HL
               LD   A,SerportXonXoffMode
               LD   (CurrentSerportMode),A        ; remember new setting
               XOR  A
               LD   (SignalSerportMode),A         ; change done

               LD   HL, Message35
               Call Write_Message                 ; "Switching to Xon/Xoff serial port handshake"

               CALL SetPanel_SoftwareHandshaking  ; For PCLINK II protocol, use 9600 Tx/Rx, Parity No, Xon/Xoff Yes

               POP  HL
               POP  BC
               POP  AF
               RET

; ***********************************************************************
.UseHardwareHandshaking
               PUSH AF
               PUSH BC
               PUSH HL
               LD   A,SerportHardwareMode
               LD   (CurrentSerportMode),A        ; remember new setting
               XOR  A
               LD   (SignalSerportMode),A         ; change done

               LD   HL, Message36
               Call Write_Message                 ; "Switching to Hardware serial port handshake"

               CALL SetPanel_HardwareHandshaking  ; For EazyLink protocol, use 9600 Tx/Rx, Parity No, Xon/Xoff No

               POP  HL
               POP  BC
               POP  AF
               RET


; ***********************************************************************
; Display Current Transfer Baud rate, EazyLink version & protocol level
; in bottom of topic window.
;
.DisplayEazyLinkVersion
               PUSH AF
               PUSH BC
               PUSH DE
               PUSH HL

               LD    A, 2                         ; max 2 bytes (word)
               LD   BC, PA_Txb                    ; Transmit Baud rate
               LD   DE, ScratchBuffer             ; buffer for baud rate
               CALL Fetch_Parameter
               LD   HL,0
               LD   (ScratchBuffer+2),HL          ; 32 bit integer

               LD   HL, ScratchBuffer             ; convert 32bit integer
               LD   DE, filename_buffer           ; to an ASCII string
               LD   A, 1                          ; disable zero blanking
               CALL_OZ(GN_Pdn)
               XOR  A
               LD   (DE),A                        ; null-terminate string

               OZ   OS_Pout
               DEFM 1, "2H7", 1, '3', '@', 32+0, 32+6, 0
               LD   HL, filename_buffer
               CALL_OZ(GN_Sop)
               OZ   OS_Pout
               DEFM " BPS", 13, 10, 0

               LD   HL, EasyLinkVersion
               CALL_OZ(GN_Sop)
               OZ   OS_Pout
               DEFM " OZ",0

               POP  HL
               POP  DE
               POP  BC
               POP  AF
               RET


; ***********************************************************************
; Display a flashing 'PCLINK II' text in bottom of topic window.
; (while a PCLINK II command is being executed)
;
.DisplayPclinkIIText
               PUSH AF
               OZ   OS_Pout
.pclink2txt    DEFM 1, "2H7", 1, '3', '@', 32+0, 32+7, 1, "2+F", 1, "2-G", "PCLINK II", 1, "2-F", 0
               POP  AF
               RET


; ***********************************************************************
; Remove the flashing 'PCLINK II' text in bottom of topic window.
; (a PCLINK II command was completed)
;
.RemovePclinkIIText
               PUSH AF
               OZ   OS_Pout
.removepcl2txt DEFM 1, "2H7", 1, '3', '@', 32+0, 32+7, 1, '2', 'C', 253, 0
               POP  AF
               RET


; ***********************************************************************
; Client wants to switch to Xon/Xoff software handshaking
;
.Switch_XonXoff_cmd
               CALL Send_ESC_Y                    ; respond with 'Yes'
               JP   C, Msg_Command_aborted

               LD   A,SerportXonXoffMode
               LD   (SignalSerportMode),A         ; signal Xon/Xoff serial port handshaking mode
               RET                                ; to be switched in 0.5 seconds (when no activity)


; ***********************************************************************
; Client wants to switch to Rts/Cts hardware handshaking
;
.Switch_RtsCts_cmd
               CALL Send_ESC_Y                    ; respond with 'Yes'
               JP   C, Msg_Command_aborted

               LD   A,SerportHardwareMode
               LD   (SignalSerportMode),A         ; signal Hardwware serial port handshaking mode
               RET                                ; to be switched in 0.5 seconds (when no activity)


; ************************************************************
; Get EasyLink Version and file protocol level
;
; Client:      ESC "v"
;
; Server:      ESC "N" <Version> ESC "Z"
;
.ESC_V_cmd
               CALL Send_ESC_N
               JP   C, Msg_Command_aborted
               LD   HL,EasyLinkVersion
               CALL SendString
               JP   C, Msg_Command_aborted
               JP   Send_ESC_Z



; ************************************************************
; Get size of file
;
; Client:      ESC "x" <Filename> ESC "Z"
;
; Server:      ESC "N" <Size> ESC "Z"   (File found)
;              ESC "Z"                  (File not found)
;
.ESC_X_cmd     CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C, Msg_Command_aborted

               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JR   Z, get_fa_filesize            ; Yes, try to get size of file in File Area...

               LD   A, op_in                      ; open file for transfer...
               LD   D,H
               LD   E,L                           ; (explicit filename overwrite original fname)
               CALL Get_file_handle               ; open file
               JR   C, file_not_found             ; ups, file not available
               CALL Get_File_Size
               CALL Close_file                    ; close file
.send_filelength
               LD   HL, File_ptr                  ; convert 32bit integer
               LD   DE, filename_buffer           ; to an ASCII string
               LD   A, 1                          ; disable zero blanking
               CALL_OZ(GN_Pdn)
               XOR  A
               LD   (DE),A                        ; null-terminate string

               CALL Send_ESC_N
               JP   C, Msg_Command_aborted

               LD   HL,filename_buffer            ; write File length as ASCII string to Client
               CALL SendString
               JP   C, Msg_Command_aborted
.file_not_found
               JP   Send_ESC_Z

.get_fa_filesize                                  ; Get size of File in File Area
               call    CheckFileAreaOfSlot
               jr      c,file_not_found           ; this slot had no file area (no card)...
               jr      nz,file_not_found          ; this slot had no file area (card, but no file area)

               ld      de,filename_buffer+6       ; search for filename beginning at "/" in filea area of slot C
               ld      a,EP_Find
               oz      OS_Epr                     ; search for filename on file eprom...
               jr      c,file_not_found           ; this slot had no file area (no card)...
               jr      nz,file_not_found          ; File Entry was not found...

               ld      a,EP_Size
               oz      OS_Epr                     ; get 24bit file size in CDE (C = high byte)
               LD      (File_ptr),de
               ld      b,0
               LD      (File_ptr+2),bc            ; CDE -> (File_ptr)
               jr      send_filelength


; ************************************************************
; Get <Create> and <Update> Date stamp of file
;
; Client:      ESC "u" <Filename> ESC "Z"
;
; Server:      ESC "N" <Create Date Stamp>        (File found)
;              ESC "N" <Update Date Stamp
;              ESC "Z"                            or
;
;              ESC "Z"                            (File not found)
;
.ESC_U_cmd     CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C,Msg_Command_aborted

               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JP   Z, fileentry_fakesdates       ; Yes, send dummy dates to satisfy clients

               LD   DE,creation_date
               LD   H,dr_rd
               LD   L,'C'                         ; get Create Date Stamp
               CALL rw_date                       ; name of file in <filename_buffer>
               JR   C, file_not_avail             ; ups, file not available

               LD   A, @10110001                  ; Century output, European format, Leading zeroes
               LD   C, '/'                        ; use C as interfield delimeter
               LD   B, 0                          ; Numeric month
               LD   HL, Creation_date+3           ; pointer to internal date
               LD   DE, file_buffer               ; pointer to ASCII date, DD-MM-YYYY
               CALL_OZ(Gn_Pdt)
               LD   HL, Creation_date             ; pointer to internal time, DE pointer to ASCII time
               LD   A,@00100011                   ; begin with a space, Leading zeroes, display seconds
               CALL_OZ(Gn_Ptm)                    ; convert internal time to ASCII...
               XOR  A
               LD   (DE),A                        ; null-terminate string

               CALL Send_ESC_N
               JP   C,Msg_Command_aborted

               LD   HL,file_buffer                ; write Date Stamp ASCII string to Client
               CALL SendString
               JP   C,Msg_Command_aborted

               LD   DE,update_date
               LD   H,dr_rd
               LD   L,'U'                         ; get Update Date Stamp
               CALL rw_date                       ; name of file in <filename_buffer>
               JR   C, file_not_avail             ; ups, file not available

               LD   A, @10110001                  ; Century output, European format, Leading zeroes
               LD   C, '/'                        ; use C as interfield delimeter
               LD   B, 0                          ; Numeric month
               LD   HL, Update_date+3             ; pointer to internal date
               LD   DE, file_buffer               ; pointer to ASCII date, DD-MM-YYYY
               CALL_OZ(Gn_Pdt)
               LD   HL, Update_date               ; pointer to internal time, DE pointer to ASCII time
               LD   A,@00100011                   ; begin with a space, Leading zeroes, display seconds
               CALL_OZ(Gn_Ptm)                    ; convert internal time to ASCII...
               XOR  A
               LD   (DE),A                        ; null-terminate string

               CALL Send_ESC_N
               JP   C,Msg_Command_aborted

               LD   HL,file_buffer                ; write Date Stamp ASCII string to Client
               CALL SendString
               JP   C,Msg_Command_aborted
.file_not_avail
               JP   Send_ESC_Z

.zero_date     DEFM "00/00/0000 00:00:00",0

.fileentry_fakesdates
               call    CheckFileAreaOfSlot
               jr      c,file_not_avail           ; this slot had no file area (no card)...
               jr      nz,file_not_avail          ; this slot had no file area (card, but no file area)

               ld      de,filename_buffer+6       ; search for filename beginning at "/" in filea area of slot C
               ld      a,EP_Find
               oz      OS_Epr                     ; search for filename on file eprom...
               jr      c,file_not_avail           ; this slot had no file area (no card)...
               jr      nz,file_not_avail          ; File Entry was not found...

               CALL Send_ESC_N
               JP   C,Msg_Command_aborted
               LD   HL,zero_date                  ; dummy create stamp...
               CALL SendString
               JP   C,Msg_Command_aborted
               CALL Send_ESC_N
               JP   C,Msg_Command_aborted
               LD   HL,zero_date                  ; dummy update stamp...
               CALL SendString
               JP   C,Msg_Command_aborted
               JR   file_not_avail                ; terminate with ESC Z, according to protocol



; ************************************************************
; Set File Create & Update Date stamp of file
; Format of Date Stamp: "dd/mm/yyyy hh:nn:ss"
;
; Client:      ESC "U" <Filename>
;              ESC "N" <CreateDateStamp>
;              ESC "N" <UpdateDateStamp>
;              ESC "Z"
;
; Server:      ESC "Y"                            (Date stamp executed)
;              ESC "Z"                            (File not found)
;
.ESC_U_cmd2    CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C,Msg_Command_aborted

               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JP   Z, fileentry_updfakedate      ; Yes, pretend to update a file date..

               LD   BC,31
               LD   HL,file_buffer
               CALL FetchBlock
               JP   C,Msg_Command_aborted

               LD   BC,31
               LD   HL,ScratchBuffer              ; get update date stamp
               CALL FetchBlock
               JP   C,Msg_Command_aborted

               ld   hl, file_buffer               ; convert ASCII date Stamp to internal format
               ld   de, Creation_date+3           ; result at (de)
               ld   b, 10                         ; read max. 10 characters
               ld   c, '/'                        ; delimeter
               ld   a, @00110000                  ; european format, '/' delimted
               call_oz(Gn_Gdt)                    ; convert ASCII date to internal format

               ld   hl, file_buffer+11            ; point at ASCII time stamp
               ld   de, Creation_date
               call_oz(Gn_Gtm)                    ; convert ASCII time to internal format

               ld   hl, ScratchBuffer             ; convert ASCII date Stamp to internal format
               ld   de, Update_date+3             ; result at (de)
               ld   b, 10                         ; read max. 10 characters
               ld   c, '/'                        ; delimeter
               ld   a, @00110000                  ; european format, '/' delimted
               call_oz(Gn_Gdt)                    ; convert ASCII date to internal format

               ld   hl, ScratchBuffer+11          ; point at ASCII time stamp
               ld   de, Update_date
               call_oz(Gn_Gtm)                    ; convert ASCII time to internal format

               LD   DE, Creation_date
               LD   H, dr_wr
               LD   L, 'C'                        ; Set Create Date Stamp
               CALL rw_date                       ; name of file in <filename_buffer>
               JP   C, file_not_avail             ; ups, file not available

               LD   DE, Update_date
               LD   H, dr_wr
               LD   L, 'U'                        ; Set Update Date Stamp
               CALL rw_date                       ; name of file in <filename_buffer>
.dateupd_completed
               JP   Send_ESC_Y                    ; Signal "Date Stamp executed"
.comma         DEFM ", ", 0

.fileentry_updfakedate
               call    CheckFileAreaOfSlot
               jp      c,file_not_avail           ; this slot had no file area (no card)...
               jp      nz,file_not_avail          ; this slot had no file area (card, but no file area)

               ld      de,filename_buffer+6       ; search for filename beginning at "/" in filea area of slot C
               ld      a,EP_Find
               oz      OS_Epr                     ; search for filename on file eprom...
               jp      c,file_not_avail           ; this slot had no file area (no card)...
               jp      nz,file_not_avail          ; File Entry was not found...
               jr      dateupd_completed          ; pretend that date stamps were updated to satisfy Client..



; ************************************************************
; Set System Clock
;
; Client:      ESC "p" <System Date>              "dd/mm/yyyy"
;              ESC "N" <System Time>              "hh:nn:ss"
;              ESC "Z"
;
; Server:      ESC "Y"                            (System Time is set)
;              ESC "Z"                            (Illegal parameters)
;
.ESC_p_cmd     LD   HL, Message32
               CALL Write_message                 ; "Set System Clock."

               CALL Fetch_pathname                ; Get Ascii Date in filename_buffer
               JP   C,Msg_Command_aborted

               ld   de, Creation_date+3           ; convert ASCII date Stamp to internal format, result at (de)
               ld   b, 10                         ; read max. 10 characters
               ld   c, '/'                        ; delimeter
               ld   a, @00110000                  ; european format, '/' delimted
               call_oz(Gn_Gdt)                    ; convert ASCII date to internal format

               CALL Fetch_pathname                ; Get Ascii Time
               JP   C,Msg_Command_aborted

               ld   de, Creation_date             ; point at ASCII time stamp
               call_oz(Gn_Gtm)                    ; convert ASCII time to internal format

               LD   HL, Creation_Date+3
               CALL_OZ(Gn_Pmd)
               LD   E,(HL)                        ; least significant byte of date
               LD   HL, Creation_Date
               CALL_OZ(Gn_Pmt)

               ; Setting the time affects serial port UART
               CALL InstallPanelSettings          ; re-install panel settings, soft reset serial port
               CALL EazyLinkSerPortTimeout        ; and re-apply the Eazylink default serial port timeout

               JP   Send_ESC_Y                    ; Signal "System Clock has been set"



; ************************************************************
; Get Z88 System Clock
;
; Client:      ESC "e"
;
; Server:      ESC "N" <System Clock Date>
;              ESC "N" <System Clock Time>
;              ESC "Z"
;
.ESC_E_cmd
               LD   DE, creation_date
               CALL_OZ(Gn_Gmd)                    ; store machine date at (DE)

               LD   A, @10110001                  ; Century output, European format, Leading zeroes
               LD   C, '/'                        ; use C as interfield delimeter
               LD   B, 0                          ; Numeric month
               LD   HL, Creation_date             ; pointer to internal date
               LD   DE, file_buffer               ; pointer to ASCII date, DD-MM-YYYY
               CALL_OZ(Gn_Pdt)
               XOR  A
               LD   (DE),A                        ; null-terminate ASCII

               CALL Send_ESC_N
               JP   C, Msg_Command_aborted

               LD   HL, file_buffer
               CALL SendString                    ; ESC "N" <System Date>
               JP   C, Msg_Command_aborted

               LD   DE, creation_date
               CALL_OZ(Gn_Gmt)                    ; store machine time at (DE)

               LD   HL, Creation_date             ; pointer to internal time
               LD   DE, file_buffer               ; pointer to write Ascii version...
               LD   A,@00100001                   ; Leading zeroes, display seconds
               CALL_OZ(Gn_Ptm)                    ; convert internal time to ASCII...
               XOR  A
               LD   (DE),A                        ; null-terminate string

               CALL Send_ESC_N
               JP   C, Msg_Command_aborted

               LD   HL,file_buffer                ; ESC "N" <System Time>
               CALL SendString
               JP   C, Msg_Command_aborted

               JP   Send_ESC_Z                    ; ESC "Z" - end of Date/time strings



; ************************************************************
; Check for existence of file
;
; Client:      ESC "f" <Filename> ESC "Z"
;
; Server:      ESC "Y"                  (File found)
;              ESC "Z"                  (File not found)
;
.ESC_F_cmd
               CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C, Msg_Command_aborted

               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JR   Z, file_entry_avail           ; Yes, check if file is found in File area of slot X.

               LD   HL,filename_buffer
               LD   A, op_in                      ; open file for transfer...
               LD   D,H
               LD   E,L                           ; (explicit filename overwrite original fname)
               CALL Get_file_handle               ; open file
               JP   C, Send_ESC_Z                 ; ups, file not available
               CALL Close_file                    ; close file
               JP   Send_ESC_Y                    ; Signal "File exist!"

.file_entry_avail                                 ; Check if File exists in File Area
               call CheckFileAreaOfSlot
               jp   c, Send_ESC_Z                 ; this slot had no file area (no card)...
               jp   nz, Send_ESC_Z                ; this slot had no file area (card, but no file area)

               ld   de,filename_buffer+6          ; search for filename beginning at "/" in filea area of slot C
               ld   a,EP_Find
               oz   OS_Epr                        ; search for filename on file eprom...
               jp   c, Send_ESC_Z                 ; this slot had no file area (no card)...
               jp   nz,Send_ESC_Z                 ; File Entry was not found...
               jp   Send_ESC_Y                    ; Signal "File exist!"


; ************************************************************
; Rename Z88 filename
;
; Client:      ESC "w" <OrigFilename>
;              ESC "N" <NewFilename>
;              ESC "Z"
;
; Server:      ESC "Y"                  (File renamed)
;              ESC "Z"                  (Filename invalid or I/O error)
;
.ESC_W_cmd     LD   HL, Message29
               CALL Write_message                 ; "Rename file "
               CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C, Msg_Command_aborted

               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JP   Z, Send_ESC_Z                 ; Yes, but Rename is not supported file File Entries..

               CALL_OZ(Gn_Sop)                    ; display original filename (incl. path)
               OZ   OS_Pout
               DEFM " to ", 0

               CALL Set_TraFlag
               LD   BC,255                        ; upper limit of bytes of filename path to receive (probably never happens)
               LD   HL,file_buffer                ; get local filename
               CALL FetchBlock
               JP   C, Msg_Command_aborted
               CALL Restore_TraFlag

               LD   HL,file_buffer
               CALL Write_message                 ; display local filename

               LD   HL, filename_buffer           ; point at original filename
               LD   DE, file_buffer               ; point at new, local filename
               CALL_OZ(Gn_Ren)
               JP   C, Send_ESC_Z                 ; not renamed
               JP   Send_ESC_Y                    ; Signal "File renamed"



; ************************************************************
; Create Z88 directory
;
; Client:      ESC "y" <DirectoryPath> ESC "Z"
;
; Server:      ESC "Y"                  (Directory created)
;              ESC "Z"                  (Directory path invalid or in use)
;
.ESC_Y_cmd     LD   HL, Message28
               CALL Write_message                 ; "Delete file/dir "

               CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C,Msg_Command_aborted

               CALL Write_message                 ; display filename...
               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JP   Z, Send_ESC_Z                 ; Yes, but directories is not supported File Area..

               LD   B, 0
               CALL CreateDirectory
               JP   C, Send_ESC_Z
               JP   Send_ESC_Y                    ; Signal "Directory created"



; ************************************************************
; Delete Z88 file or directory
;
; Client:      ESC "r" <Filename> ESC "Z"
;
; Server:      ESC "Y"                  (File found and deleted)
;              ESC "Z"                  (File not found)
;
.ESC_R_cmd     LD   HL, Message27
               CALL Write_message                 ; "Delete file/dir "

               CALL Set_TraFlag
               CALL Fetch_pathname                ; load filename into filename_buffer
               CALL Restore_TraFlag
               JP   C,Msg_Command_aborted

               CALL Write_message                 ; display filename...
               CALL CheckEprName                  ; Path begins with ":EPR.x"?
               JR   Z, delete_fileentry           ; Yes, try to delete file entry

               LD   B,0
               LD   HL, filename_buffer
               CALL_OZ(Gn_Del)                    ;
               JP   C, Send_ESC_Z                 ; ups, file not available or in use
               JP   Send_ESC_Y                    ; Signal "File/Directory deleted"

.delete_fileentry
               call    CheckFileAreaOfSlot        ; File area in slot A?
               jp      c,Send_ESC_Z               ; this slot had no file area (no card)...
               jp      nz,Send_ESC_Z              ; this slot had no file area (card, but no file area)

               ld      de,filename_buffer+6       ; search for filename beginning at "/" in filea area of slot C
               ld      a,EP_Find
               oz      OS_Epr                     ; search for filename on file eprom...
               jp      c,Send_ESC_Z               ; this slot had no file area (no card)...
               jp      nz,Send_ESC_Z              ; File Entry was not found...

               ld      a,EP_Delete
               oz      OS_Epr                     ; mark file entry as deleted, if possible...
               jp      c, Send_ESC_Z
               jp      Send_ESC_Y


; ************************************************************
; Get RAM defaults
;
; Client:      ESC "g"
;
; Server:      ESC "N" <Default Device>      (Panel Default Device)
;              ESC "N" <Default Directory>   (Panel Default Directory)
;              ESC "Z"
;
.ESC_G_cmd2
               LD    A, 255
               LD   BC, PA_Dev                    ; Read default device
               LD   DE, file_buffer               ; buffer for device name
               CALL Fetch_Parameter
               LD   B,0
               LD   C,A
               EX   DE,HL
               ADD  HL,BC
               LD   (HL),0                        ; null-terminate device name

               CALL Send_ESC_N
               JP   C, Msg_Command_aborted

               LD   HL,file_buffer                ; Send default device to Client
               CALL SendString
               JP   C, Msg_Command_aborted

               LD    A, 255
               LD   BC, PA_Dir                    ; Read default directory
               LD   DE, file_buffer               ; buffer for device name
               CALL Fetch_Parameter
               LD   B,0
               LD   C,A
               EX   DE,HL
               ADD  HL,BC
               LD   (HL),0                        ; null-terminate device name

               CALL Send_ESC_N
               JP   C, Msg_Command_aborted

               LD   HL,file_buffer                ; Send default directory to Client
               CALL SendString
               JP   C, Msg_Command_aborted
               JP   Send_ESC_Z                    ; Default strings transmitted


; ************************************************************
; Get estimated free RAM memory
;
; Client:      ESC "m"
;
; Server:      ESC "N" <FreeMemory>          (Estimated free memory in bytes)
;              ESC "Z"
;
.ESC_M_cmd
.global_free_space
               CALL TotalFreeSpace                ; return DE = free pages in system
.send_free_space_de
               LD   B,E
               LD   C,0
               LD   E,D
               LD   D,0                           ; DEBC = (DE = free pages) * 256
.send_free_space_debc
               CALL send_integer_string
               JP   C, Msg_Command_aborted
               JP   Send_ESC_Z                    ; String transmitted


; ************************************************************
; IN: DEBC = Integer to transmit as ESC N <Ascii string>
.send_integer_string
               LD   (File_ptr),BC
               LD   (File_ptr+2),DE               ; low byte, high byte sequense

               LD   HL, File_ptr                  ; convert 32bit integer
               LD   DE, filename_buffer           ; to an ASCII string
               LD   A, 1                          ; disable zero blanking
               CALL_OZ(GN_Pdn)
               XOR  A
               LD   (DE),A                        ; null-terminate string

               CALL Send_ESC_N                    ; String transmitted
               RET  C

               LD   HL,filename_buffer            ; Send string to Client
               JP   SendString


; ************************************************************
; Get explicit free memory in specified RAM/EPR card slot.
;
; (Unfortunately, this call is ambiguous if there is both
; RAM and EPR in the same slot - using a Rakewell Hybrid card -
; RAM is always picked first)
;
; <RamDeviceNumber> = "0", "1", "2", "3" or "-" (all)
;
; Client:      ESC "M" <RamDeviceNumber> ESC "Z"
;
; Server:      ESC "N" <FreeMemory>          (RAM device found, free memory in bytes)
;              ESC "Z"
;                                  or
;              ESC "Z"                       (RAM device not found)
;
.ESC_M_cmd2
               CALL Fetch_pathname           ; fetch RAM device number
               JP   C, Msg_Command_aborted

               LD   A,(filename_buffer)      ; get RAM device number
               CP   '-'
               JR   Z, global_free_space     ; send global free space string

               SUB  48                       ; A = slot no of RAM card
               push bc
               ld   bc,Nq_Mfp
               oz   OS_Nq
               pop  bc
               JR   NC,send_free_space_de    ; RAM was found... return free space in DE = 256 byte pages..

               ld   a,(filename_buffer)      ; get device number for possible EPR device in slot
               call CheckFileAreaOfSlot      ; File area in slot A?
               jp   c,Send_ESC_Z              ; this slot had no file area (no card)...
               jp   nz,Send_ESC_Z

               ld   a,EP_FreSp
               oz   OS_Epr                   ; return Free space of File Area in DEBC
               jr   send_free_space_debc


; ************************************************************
; Get Device Info of specified device name
; Using :RAM.- total free memory & total RAM size is returned.
;
; <Devicename> = ":RAM.x", ":RAM.-" or ":EPR.x" (slot 0 - 3, or :RAM.-)
;
; Client:      ESC "O" <Devicename> ESC "Z"
;
; Server:      ESC "N" <FreeMemory>          (Device found, free memory in bytes)
;              ESC "N" <Device size>         (Device found, size of device in K)
;              ESC "Z"
;                                  or
;              ESC "Z"                       (Device not found)
;
.ESC_O_cmd
               CALL Fetch_pathname           ; fetch Device name
               JP   C, Msg_Command_aborted

               CALL CheckEprName             ; Device is ":EPR.x"?
               JR   Z, check_filearea        ; Yes, check if file area is available..

               CALL CheckRamName             ; Device is ":RAM.x"?
               JP   nz,Send_ESC_Z            ; Fz = 0 -> not a RAM card either...
               CP   '-'
               CALL Z, TotalFreeSpace        ; return A = total RAM in 16K banks, DE = total free 256 byte pages
               JR   Z,transmit_free_space
               push bc
               ld   bc,Nq_Mfp
               oz   OS_Nq
               pop  bc                       ; return A = RAM.x card in 16K banks, DE = free 256 byte pages
.transmit_free_space
               PUSH AF
               LD   B,E
               LD   C,0
               LD   E,D
               LD   D,0                      ; DEBC = RAM <free pages> * 256
               call send_integer_string      ; send ESC N <devsize>
               POP  HL
               JP   C, Msg_Command_aborted

               ld   l,h
               ld   h,0                      ; A -> HL
.send_card16k_size
               call m16                      ; HL = size of device in 16K banks
               ld   de,0
               ld   b,h
               ld   c,l                      ; DEBC = banks * 16K = size of card in K
               call send_integer_string      ; send ESC N <devsize>
               JP   C, Msg_Command_aborted
               jp   Send_ESC_Z

.check_filearea
               call CheckFileAreaOfSlot      ; File area in slot A?
               jp   c,Send_ESC_Z             ; this slot had no file area (no card)...
               jp   nz,Send_ESC_Z            ; this slot had no file area (card, but no file area)
               push de                       ; (preserve size of EPR card in 16K banks)
               ld   a,EP_FreSp
               oz   OS_Epr                   ; return Free space of File Area in DEBC
               call send_integer_string
               pop  hl
               JP   C, Msg_Command_aborted
               JR   send_card16k_size


; ************************************************************
; Change to light speed serial port transfer rate (38400bps)
; Protocol level 09
;
; Client:
;              ESC "L"
; Server:
;              ESC "Y"
;
.ESC_L_cmd
               CALL Send_ESC_Y
               JP   C, Msg_Command_aborted

               LD   HL,Message38
               CALL Write_message

               LD   A, 2                          ; word size
               LD   BC, PA_Txb                    ; new transmit baud rate, 38400
               LD   HL, BaudRate38400
               CALL_OZ (Os_Sp)
               LD   BC, PA_Rxb                    ; new receive baud rate, 38400
               CALL_OZ (Os_Sp)
               LD   BC, PA_Par                    ; Parity No
               LD   HL, No_Parameter
               CALL_OZ (Os_Sp)

               CALL SetPanel_HardwareHandshaking  ; Set hardware handshaking and install settings
               CALL DisplayEazyLinkVersion        ; Update left hand side window with 38400 BPS
               JP   disp_running                  ; display "Running" after having set new baud rate


; ************************************************************
;
; Scan all slots for RAM Cards, and get total amount of free
; space.
;
; IN:
;    -
; OUT:
;     F(out) = F(in)
;     A = total size of RAM in 16K banks
;    DE = total free 256 bytes pages in system.
;
.TotalFreeSpace
               PUSH BC
               PUSH AF
               PUSH HL

               LD   BC,$0300                      ; scan all 4 slots, 3 -> 0 ...
               LD   HL,0
.scan_ram_loop
               LD   A,B
               push bc
               ld   bc,Nq_Mfp
               oz   OS_Nq
               pop  bc
               PUSH AF
               DEC  B
               POP  AF
               JR   C, scan_ram_loop
               ADD  HL,DE                         ; add pages to sum of all pages
               ADD  A,C
               LD   C,A                           ; C = sum of total RAM in 16K banks

               LD   A,B
               CP   $FF
               JR   NZ,scan_ram_loop
               EX   DE,HL

               POP  HL
               POP  AF
               LD   A,C                           ; return A = total RAM in 16K banks
               POP  BC                            ; return DE = total free 256 bytes pages in system.
               RET


; ************************************************************
;
; Update translation table (re-load from "/translate.dat")
;
; Client:      ESC "Z"
;
.ESC_Z_cmd     LD   HL, Message26
               CALL Write_message                 ; "Update translations"
               JP   LoadTranslations              ; fetch "translate.dat" from filing system,


; ************************************************************
.ESC_C_cmd1    LD   HL,message12
               CALL Write_message

               SET  BB_LnfdConv,(IX + rtmflags)     ; Remote activation of auto CRLF conversion
               RET


; ************************************************************
.ESC_C_cmd2    LD   HL,message13
               CALL Write_message

               RES  BB_LnfdConv,(IX + rtmflags)     ; Remote de-activation of auto CRLF conversion
               RET


; ************************************************************
.ESC_T_cmd1    LD   HL,message10
               CALL Write_message

               SET  BB_CharTrans,(IX + rtmflags)    ; Remote activation of auto translation
               SET  BB_CharTransCpy,(IX + rtmflags)
               RET


; ************************************************************
.ESC_T_cmd2    LD   HL,message11
               CALL Write_message

               RES  BB_CharTrans,(IX + rtmflags)    ; Remote de-activation of auto translation
               RES  BB_CharTransCpy,(IX + rtmflags)
               RET


; ***********************************************************************
.EnableSerportLogging
               PUSH HL
               CALL Create_serportdump_files
               LD   HL, msg_serdmpfile
               OZ   GN_Sop
               LD   HL, msg_serdmpfile_enable
               CALL Write_message
               POP  HL
               RET


; ***********************************************************************
.DisableSerportLogging
               PUSH HL
               CALL Close_serportdump_filehandles
               LD   HL, msg_serdmpfile
               OZ   GN_Sop
               LD   HL, msg_serdmpfile_disable
               CALL Write_message
               POP  HL
               RET


; ***********************************************************************
.Close_serportdump_filehandles
               PUSH AF
               PUSH IX
               PUSH HL

               LD   HL,(serfile_in_handle)
               LD   A,H
               OR   L
               JR   Z, close_out_handle                     ; serport dump IN file not open
               PUSH HL
               POP  IX
               CALL_OZ(Gn_Cl)

.close_out_handle
               LD   HL,(serfile_out_handle)
               LD   A,H
               OR   L
               JR   Z, exit_Close_serportdump_filehandles   ; serport dump OUT file not open
               PUSH HL
               POP  IX
               CALL_OZ(Gn_Cl)

.exit_Close_serportdump_filehandles
               LD   HL,0
               LD   (serfile_out_handle),HL
               LD   (serfile_in_handle),HL

               POP  HL
               POP  IX

               RES  BB_SerportLog,(IX + rtmflags)      ; indicate disabled serial port logging

               POP  AF
               RET

if EAZYLINK_TESTPROTOCOL
; ***********************************************************************
.OpenTestProtocolFile
               push bc
               push hl
               push ix
               ld   b,0                      ; local filename
               ld   hl,TestProtocolFileName
               ld   d,h                      ; HL points at start of filename
               ld   e,l                      ; DE points at scratch buffer
               ld   a, OP_IN
               oz   GN_Opf                   ; try to open protocol test file
               jr   c, exit_OpenTestProtocolFile  ; an error occurred (most likely not found)
               ld   (protctestfile_handle),ix
.exit_OpenTestProtocolFile
               pop  ix
               pop  hl
               pop  bc
               ret
.TestProtocolFileName
               defm ":RAM.0/testprotocol.bin",0
endif

; ***********************************************************************
.Create_serportdump_files
               PUSH AF
               PUSH BC
               PUSH HL
               PUSH IX

               ; close current dump files, if they're open
               LD   HL,(serfile_in_handle)
               LD   A,H
               OR   L
               CALL NZ,Close_serportdump_filehandles

               ; then flush old files and create new ones...
               LD   B,0                   ; filename is local...
               LD   HL,serdmpfile_in
               CALL createfilename
               CALL C,Close_serportdump_filehandles
               JR   C,exit_Create_serportdump_files
               LD   IX,(file_handle)
               LD   HL,0
               LD   (file_handle),HL
               LD   (serfile_in_handle),IX

               LD   HL,serdmpfile_out
               CALL createfilename
               CALL C,Close_serportdump_filehandles
               JR   C,exit_Create_serportdump_files
               LD   IX,(file_handle)
               LD   HL,0
               LD   (file_handle),HL
               LD   (serfile_out_handle),IX

.exit_Create_serportdump_files
               POP  IX

               SET  BB_SerportLog,(IX + rtmflags)      ; indicate enabled serial port logging

               POP  HL
               POP  BC
               POP  AF
               RET


; ************************************************************
.Set_TraFlag   EX   AF,AF'
               BIT  BB_CharTrans,(IX + rtmflags)
               JR   Z,tra_notenabled
               SET  BB_CharTransCpy,(IX + rtmflags)
               JR   exit_Set_TraFlag
.tra_notenabled
               RES  BB_CharTransCpy,(IX + rtmflags)
               SET  BB_CharTrans,(IX + rtmflags)
.exit_Set_TraFlag
               EX   AF,AF'
               RET


; ************************************************************
.Restore_Traflag
               EX   AF,AF'
               BIT  BB_CharTransCpy,(IX + rtmflags)
               JR   NZ,tra_wasenabled
               RES  BB_CharTrans,(IX + rtmflags)
               JR   exit_Set_TraFlag
.tra_wasenabled
               SET  BB_CharTrans,(IX + rtmflags)
               EX   AF,AF'
               RET


; ***********************************************************************
.Init_PanelSettings                               ; Copy original parameters in buffers:
               CALL Fetch_PanelSettings

               ; Relevant parameters are now copied. Install now (temporarily) the new
               ; serial port parameters (9600 baud,  Parity No, Xon/Xoff Yes):

               CALL Define_PanelSettings

.EazyLinkSerPortTimeout
               LD   BC,EazyLink_DefaultTimeout
               LD   L, SI_TMO
               OZ   Os_Si                         ; set default timeout for OS_Si Block I/O
               RET


; ***********************************************************************
.Fetch_PanelSettings
               LD    A, 2                         ; max 2 bytes (word)
               LD   BC, PA_Txb                    ; Transmit Baud rate
               LD   DE, Cpy_Pa_Txb+1              ; buffer for baud rate
               CALL Fetch_Parameter
               JP   C, System_error
               DEC  DE
               LD   (DE), A                       ; remember length of PA_Txb
               LD    A, 2
               LD   BC, PA_Rxb
               LD   DE, Cpy_PA_Rxb+1
               CALL Fetch_Parameter
               JP   C, System_error
               DEC  DE
               LD   (DE), A                       ; remember length of PA_Rxb
               LD   A, 1
               LD   BC, PA_Xon
               LD   DE, Cpy_PA_Xon
               CALL Fetch_Parameter
               JP   C, System_error
               LD   A, 1
               LD   BC, PA_Par
               LD   DE, Cpy_PA_Par
               CALL Fetch_Parameter
               JP   C, System_error
               RET


; ***********************************************************************
; Default Serial port parameters: 9600 baud, Xon/Xoff No, Parity No
; (Hardware handshaking is default when OZ/EazyLink starts up)
;
.Define_PanelSettings

               LD   A, 2                          ; word size
               LD   BC, PA_Txb                    ; new transmit baud rate, 9600
               LD   HL, BaudRate9600
               CALL_OZ (Os_Sp)
               LD   BC, PA_Rxb                    ; new receive baud rate, 9600
               CALL_OZ (Os_Sp)
               LD   BC, PA_Par                    ; Parity No
               LD   HL, No_Parameter
               CALL_OZ (Os_Sp)
               JP   UseHardwareHandshaking

.SetPanel_HardwareHandshaking
               LD   A, 1
               LD   BC, PA_Xon                    ; Xon/Xoff No (hardware handshaking)
               LD   HL, No_Parameter
               CALL_OZ (Os_Sp)
               JR   InstallPanelSettings
.SetPanel_SoftwareHandshaking
               LD   A, 1
               LD   BC, PA_Xon                    ; Xon/Xoff Yes
               LD   HL, Yes_Parameter
               CALL_OZ (Os_Sp)
.InstallPanelSettings
               XOR  A
               LD   BC, PA_Gfi                    ; install new parameters
               CALL_OZ (Os_Sp)
               RET


; ***********************************************************************
; Restore Panel settings to original values that was present before
; MultiLink was called.
.Restore_PanelSettings
               LD   A, (Cpy_PA_Txb)                ; fetch length of parameter (Transmit baud rate)
               LD   BC, PA_Txb
               LD   HL, Cpy_PA_Txb+1               ; address of parameter
               CALL_OZ (Os_Sp)
               JP   C, System_error
               LD   A, (Cpy_PA_Rxb)                ; fetch length of parameter (Transmit baud rate)
               LD   BC, PA_Rxb
               LD   HL, Cpy_PA_Rxb+1               ; address of parameter
               CALL_OZ (Os_Sp)
               JP   C, System_error
               LD   A, 1
               LD   BC, PA_Xon                     ; Xon/Xoff
               LD   HL, Cpy_PA_Xon
               CALL_OZ (Os_Sp)
               LD   A, 1
               LD   BC, PA_Par                     ; Parity
               LD   HL, Cpy_PA_Par
               CALL_OZ (Os_Sp)
               XOR  A
               LD   BC, PA_Gfi                     ; install original parameters to Panel & reset serial port
               CALL_OZ (Os_Sp)
               RET


; ***********************************************************************
; Fetch a system parameter:
; BC = Parameter offset,   A = number of bytes to be read,
; DE = Buffer to put bytes
; - Returns A = bytes actually read
.Fetch_Parameter
               PUSH DE                              ; save pointer to buffer
               CALL_OZ (Os_Nq)
               POP  DE
               RET


; ***********************************************************************
; Reset all I/O handles to zero.
;
.InitHandles   PUSH HL
               LD   HL,0
               LD   (file_handle),HL
               LD   (serfile_in_handle),HL
               LD   (serfile_out_handle),HL
if EAZYLINK_TESTPROTOCOL
               LD   (protctestfile_handle),HL
endif
               LD   (wildcard_handle),HL
               POP  HL
               RET


; *************************************************************************************
;
; Multiply HL * 16, result in HL.
;
.m16
               PUSH BC
               LD   B,4
.multiply_loop      ADD  HL,HL
               DJNZ multiply_loop
               POP  BC
               RET


; ***********************************************************************
; Reset both translation tables to identical lookup values ( 0 - 255 )
;
.InitTraTable
               PUSH AF
               PUSH BC
               PUSH DE
               PUSH HL
               XOR  A
               LD   B,0                           ; 256 bytes to initiate
               LD   HL,TraTableIn
               LD   DE,TraTableOut
.initTraTable_loop
               LD   (HL),A                        ;  FOR a = 0 TO 255
               LD   (DE),A                        ;    TraTable_in(b) = a
               INC  HL                            ;    TraTable_out(b) = a
               INC  DE                            ;  END FOR a
               INC  A
               DJNZ,initTraTable_loop
               POP  HL
               POP  DE
               POP  BC
               POP  AF
               RET


; ***********************************************************************
.System_error  PUSH AF
               CALL_OZ (Gn_Err)                   ; display system error
               POP  AF
               RET


; ***********************************************************************
.Write_message PUSH AF
               PUSH HL
               OZ   OS_Pout
.vdulog        DEFM 1, "2H3", 0                   ; use log window for message
               OZ   Gn_Sop
               OZ   Gn_Nln
               POP  HL
               POP  AF
               RET

; ***********************************************************************
;
.Get_Time
               PUSH BC
               PUSH DE
               PUSH HL
               LD   C,0
               LD   DE,2
               CALL_OZ(GN_Gmt)                    ; current internal machine time
               LD   (timestamp),BC
               LD   (timestamp+2),A               ; current machine time
               POP  HL
               POP  DE
               POP  BC
               RET


; ***********************************************************************
;
.Display_Elapsedtime
               PUSH AF
               PUSH IX
               LD   C,0
               LD   DE,2
               OZ   GN_Gmt                        ; current internal machine time
               LD   H,B
               LD   L,C
               LD   BC,(timestamp)
               SBC  HL,BC
               LD   D,A
               LD   A,(timestamp+2)               ; elapsed time = current - previous
               LD   E,A
               LD   A,D
               SBC  A,E
               LD   (timestamp),HL
               LD   (timestamp+2),A               ; AHL = elapsed time in centiseconds

               OZ   OS_Pout
.time1_msg     DEFM " in ", 0

               LD   BC, NQ_OHN
               OZ   Os_Nq                         ; get handle in IX for standard output for GN_Ptm

               LD   DE,0                          ; write time to #5 window...
               LD   HL, timestamp                 ; pointer to internal time
               LD   A, @00110101
               OZ   Gn_Ptm                        ; display elapsed time...
               OZ   Gn_Nln
               POP  IX
               POP  AF
               RET


; ***********************************************************************
.Msg_File_open_error
               LD   HL, error_message1
               JR   Write_message

; ***********************************************************************
.Msg_Protocol_error
               LD   HL, error_message3
               JR   Write_message

; ***********************************************************************
.Msg_file_aborted
               LD   HL, error_message4
               JR   Write_message

; ***********************************************************************
.Msg_No_Room   LD   HL, error_message5
               JR   Write_message

; ***********************************************************************
.Msg_Command_aborted
               LD   HL,error_message2
               JR   Write_message

; ***********************************************************************
.Msg_file_received
               PUSH AF
               OZ   OS_Pout
               DEFM 1, "2H3", 0                   ; use log window for message
               LD   HL, message37
               OZ   Gn_Sop
               CALL Display_Elapsedtime
               POP  AF
               RET

; ***********************************************************************
.Msg_file_sent
               PUSH AF
               OZ   OS_Pout
               DEFM 1, "2H3", 0                   ; use log window for message
               LD   HL, message39
               OZ   Gn_Sop
               CALL Display_Elapsedtime
               POP  AF
               RET

.CmdWinDef
               DEFB 2 | 192
               DEFW $0000
               DEFW $081A
               DEFW command_banner
.LogWinDef
               DEFB 3 | 128
               DEFW $001C
               DEFW $083E
               DEFW menu_banner
