; *************************************************************************************
; EazyLink - Fast Client/Server File Management, including support for PCLINK II protocol
; (C) Gunther Strube (gstrube@gmail.com) 1990-2014
;
; EazyLink is free software; you can redistribute it and/or modify it under the terms of the
; GNU General Public License as published by the Free Software Foundation;
; either version 2, or (at your option) any later version.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; *************************************************************************************

    MODULE MultiLink4_commands

    XREF ToUpper
    XREF createfilename
    XREF ESC_F, ESC_E, Current_Dir, Parent_Dir
    XREF Eprdev, ramdev_wildcard
    XREF TranslateByte
    XREF cli_filename
    XREF FetchBlock, SendString, SendFileBuffer, PutByte, GetByte
    XREF Send_ESC, Send_ESC_B, Send_CRLF, Send_ESC_Y, Send_ESC_E, Send_ESC_F, Send_ESC_N, Send_ESC_Z
    XREF Get_wcard_handle, Find_Next_Match, Close_wcard_handler
    XREF Abort_file, Get_file_handle, Close_file
    XREF Write_buffer, Load_Buffer
    XREF Write_Message, Msg_Command_aborted, Msg_Protocol_error, Msg_File_aborted
    XREF Msg_file_received, Msg_file_sent
    XREF Msg_No_Room, Msg_file_open_error, System_Error
    XREF Message3, Message7, Message14, Message15, Message16
    XREF Message17, Message18
    XREF Error_message6
    XREF Set_Traflag, Restore_Traflag, Def_RamDev_wildc, SearchFileSystem, Get_directories
    XREF ReceiveRamFile, FileEprSendFile, FileEprSaveFile, SlotWriteSupport
    XREF KillPopdown
    XREF FetchBytes, Get_time

    XDEF SetEprDevName,CheckEprName,CheckRamName,CheckFileAreaOfSlot
    XDEF Transfer_filename, Transfer_RamfileImage
    XDEF ESC_A_cmd2, ESC_H_cmd2, ESC_D_cmd2, ESC_N_cmd2, ESC_Q_cmd2
    XDEF ImpExp_Send, ImpExp_Receive, ImpExp_Backup
    XDEF Transfer_file
    XDEF Fetch_pathname


    INCLUDE "rtmvars.def"
    INCLUDE "error.def"
    INCLUDE "fileio.def"
    include "eprom.def"
    INCLUDE "dor.def"
    INCLUDE "stdio.def"


; ***********************************************************************
; Hello
;
.ESC_A_cmd2       CALL Send_ESC_Y
                  JP   C, Msg_Command_aborted
                  LD   HL,message3
                  JP   Write_message


; ***********************************************************************
.ESC_Q_cmd2
                  CALL Send_ESC_Y                    ; Multilink 'Quit'
                  JP   C, Msg_Command_aborted
                  JP   KillPopdown                   ; Harakiri...


; ***********************************************************************
; Send Z88 Devices, extended protocol
;
.ESC_H_cmd2
                  LD   A, Dm_Dev
                  LD   (file_type),A
                  LD   A, 0                          ; wildcard search specifier...
                  CALL Def_RamDev_wildc

                  CALL Send_found_names              ; internal & external RAM cards...
                  JP   C, Msg_Command_aborted

                  CALL Send_Epr_devices              ; Send "EPR.X", if any file area is found...
                  JP   C, Msg_Command_aborted
                  JP   Send_ESC_Z                    ; no more names


; ***********************************************************************
; Send directory names, extended protocol
;
.ESC_D_cmd2
                  CALL Set_Traflag                   ; translation ON temporarily
                  CALL Fetch_pathname                ; fetch pathname to filename_buffer
                  JR   C, esc_d2_aborted

                  PUSH HL
                  LD   HL, Current_dir               ; Send "."
                  CALL SendString
                  POP  HL
                  JR   C, esc_d2_aborted

                  CALL CheckEprName                  ; Path begins with ":EPR.x"?
                  JR   Z, end_of_dirnames            ; Yes, indicate no directories in a file area...

                  CALL_OZ (Gn_Prs)                   ; parse pathname
                  LD   A,B                           ; B = no. of segments in path name
                  SUB  1                             ; without wildcard specifier '*'...
                  CP   1                             ; only 1 filename segment?
                  JR   Z,no_parent_dir               ; root directory...
                  LD   HL, Parent_dir                ; Send ".."
                  CALL SendString
                  JR   C, esc_d2_aborted
.no_parent_dir
                  LD   A,dn_dir
                  LD   (file_type),A                 ; find directories
                  LD   A, 1                          ; wildcard search specifier
                  LD   HL, filename_buffer
                  CALL Send_found_names
                  JR   C, esc_d2_aborted
.end_of_dirnames
                  CALL Send_ESC_Z                    ; no more names
                  JR   C, esc_d2_aborted
                  JR   end_esc_d2
.esc_d2_aborted
                  CALL Msg_Command_aborted           ; write message and set Fz
.end_esc_d2
                  JP   Restore_Traflag


; ***********************************************************************
.ESC_N_cmd2                                          ; send file names
                  CALL Set_Traflag
                  CALL Fetch_pathname                ; load pathname into filename_buffer
                  JR   C, esc_n2_aborted

                  CALL CheckEprName                  ; Path begins with ":EPR.x"?
                  JR   Z, get_fa_filenames           ; Yes, try to fetch filenames from File Area...

                  LD   A,dn_fil
                  LD   (file_type),A                 ; signal filenames to be found
                  LD   A, 1                          ; wildcard search specifier
                  LD   HL, filename_buffer
                  CALL Send_found_names
                  JR   C, esc_n2_aborted
.no_more_names
                  CALL Send_ESC_Z                      ; no more names
                  JR   C, esc_n2_aborted
                  JR   end_esc_n2
.esc_n2_aborted
                  CALL Msg_Command_aborted           ; write message and set Fz
.end_esc_n2
                  JP   Restore_Traflag

.get_fa_filenames
                  call    CheckFileAreaOfSlot        ; Check if there is a file area in A = "0", "1", "2" or "3"
                  jr      c,no_more_names            ; this slot had no file area (no card)...
                  jr      nz,no_more_names           ; this slot had no file area (card, but no file area)

                  ld      a,c
                  or      $30
                  call    SetEprDevName              ; Begin filename with device name, ":EPR.x"

                  call    GetFirstEprFile            ; slot C => BHL of first entry in file area..
.send_fa_names    jr      c, no_more_names           ; no more entries in file area...

                  ld      c,0
                  ld      de, filename_buffer+6      ; append filename at after ":EPR.x", null-terminated
                  ld      a,EP_Name
                  oz      OS_Epr                     ; copy filename from current file entry to local memory at (DE)
                  jr      c, no_more_names           ; no more entries in file area...

                  CALL    Send_ESC_N
                  JR      C, esc_n2_aborted          ; abort if serial port timed out...

                  push    bc
                  push    hl
                  LD      HL, filename_buffer        ; pointer to start of EPR.x name and File area filename
                  CALL    SendString
                  pop     hl
                  pop     bc
                  JR      C, esc_n2_aborted

                  call    GetNextEprFile             ; get pointer to next File Entry in BHL...
                  jr      send_fa_names


; ***********************************************************************
.ImpExp_Receive
                  LD   HL,message14                  ; send files to Z88, using ImpExp protocol.
                  CALL Write_message                 ; 'ImpExp Receive files...'

.Batch_Receive_loop
                  CALL FetchBytes
                  JR   C, abort_batch_receive            ; serial port error or timeout
                  JR   Z, err_protocol                   ; no ESC id, protocol error...
                  CP   'N'                               ; is it ESC "N" ?
                  JR   Z, fetch_flnm
                  CP   'Z'                               ; is it ESC 'Z' (End-batch)?
                  JR   NZ, err_protocol                  ; no, protocol error...
                  RET                                    ; yes, End Of Batch Receive, return to main...

.err_protocol     CALL Msg_Protocol_error                ; write error messages on screen, and
                  JP   Msg_Command_aborted
.Abort_receiving_file
                  CALL Abort_file
.abort_batch_receive
                  JP   Msg_Command_aborted               ; write 'Command aborted' message

.fetch_flnm       CALL Set_TraFlag
                  CALL Fetch_pathname                    ; load filename into filename_buffer
                  CALL Restore_TraFlag
                  JR   C,abort_batch_receive             ; error or timeout - communication stopped

                  CALL Write_message                     ; write filename to screen
                  CALL CheckEprName                      ; Path begins with ":EPR.x"?
                  JR   Z, verify_write_filearea          ; Yes, validate if it is possible to receive file into File Area?

                  CALL createfilename
                  JP   C, RamFile_create_error
.file_created
                  CALL Get_Time                          ; read system time, to know elapsed time of received file
                  CALL ReceiveRamFile
                  JR   C,Abort_receiving_file            ; error or timeout - communication stopped
                  CALL Msg_file_received
                  JR   Batch_Receive_loop                ; file received and closed, new file coming?

.void_file_loop                                      ; when an error occurred, receive the rest of the
                  CALL FetchBytes                    ; file until ESC E is received.... then back to main loop
                  JR   C,abort_batch_receive
                  JR   Z, void_file_loop             ; still receiving bytes from file, which is ignored...
                  CP   'E'                           ; is it ESC 'E' ?
                  JR   NZ, void_file_loop
                  JP   Batch_Receive_loop            ; yes, EOF received, wait more files in Batch Receive (or receice End of Batch)..
.RamFile_create_error
                  CALL Msg_file_aborted              ; write 'File aborted' message
                  jr   void_file_loop                ; wait for end of file marker, then back to main loop

.verify_write_filearea
                  call CheckFileAreaOfSlot           ; file area in slot A = "0", "1", "2" or "3" => C = slot number
                  jr   c,filecard_error              ; this slot had no file area (no card)...
                  jr   nz,filecard_error             ; this slot had no file area (card, but no file area)
                  call SlotWriteSupport              ; slot C writeable (ie. file area in UV Eprom / Intel flash in slot 1)?
                  jr   c,filecard_error              ; writing not possible... abort

                  ld   de,filename_buffer+6          ; filename begins with "/" (skip ":EPR.x" device name)
                  ld   a,EP_Find
                  oz   OS_Epr                        ; filename already exists on file eprom?
                  push af                            ; remember found status... Fz = 1, if found
                  push bc
                  push hl

                  ld   a,c                           ; slot C
                  ld   de,File_buffer
                  ld   c,164                         ; keep serial port buffer small for responsiveness (and space enough for protocol level 9)
                  ld   hl,filename_buffer+6          ; filename begins with "/" (skip ":EPR.x" device name)
                  call FileEprSaveFile               ; blow file data stream into file card in slot C
                  jr   c, FileEntry_create_error
                  pop  hl                            ; file blown successfully to File Area
                  pop  bc
                  pop  af
                  jp   nz,Batch_Receive_loop         ; mark old file as deleted?
                  ld   a,EP_Delete
                  oz   OS_Epr                        ; (BHL = old file Entry) Yes, so new "version" from serial port becomes the active one..
                  jp   Batch_Receive_loop
.FileEntry_create_error
                  pop  hl
                  pop  bc
                  pop  af
.filecard_error
                  LD   HL, Error_message6            ; "File Card Write Error."
                  CALL Write_message
                  jr   void_file_loop                ; wait for end of file marker, then back to main loop


; ***********************************************************************
.ImpExp_Send      LD   HL,message15                  ; send files to terminal, using ImpExp protocol.
                  CALL Write_message

                  CALL Set_Traflag
                  CALL Fetch_pathname                ; fetch pathname into filename_buffer
                  CALL Restore_Traflag
                  JR   C, err_batch_send

                  CALL Write_message                 ; display pathname / wildcard on screen...
                  CALL CheckEprName                  ; Path begins with ":EPR.x"?
                  JR   Z, send_fa_entry              ; Yes, lookup entry, and transmit file image to serial port...

                  XOR  A                             ; wildcard search specifier - files before directories
                  CALL Get_wcard_handle              ; get handle in IX for pathname in (HL)
                  RET  C
.find_files_loop  CALL Find_next_match
                  JR   C, batch_send_end             ; no more files found in wildcard
                  CP   dn_fil
                  JR   NZ,find_files_loop            ; not a file - get next

                  LD   HL, message17
                  CALL_OZ (gn_sop)

                  LD   HL, filename_buffer
                  CALL Write_message

                  CALL Get_Time                      ; read system time, to know elapsed time of sent file
                  CALL Transfer_filename
                  JR   C, batch_send_aborted         ; transmission error...

                  CALL Transfer_RamfileImage
                  JR   C, batch_send_aborted         ; transmission error...
                  CALL Msg_file_sent                 ; display "file sent in xxx" elapsed time
                  JR   find_files_loop
.batch_send_aborted
                  CALL Close_wcard_handler
.err_batch_send   JP   Msg_Command_aborted

.batch_send_end   CALL Close_wcard_handler
.batch_send_escz  JP   Send_ESC_Z                    ; end of files...

; transmitting a single File Entry (File Area system doesnt support wild card search...)
.send_fa_entry
                  call    CheckFileAreaOfSlot        ; Check if there is a file area in A = "0", "1", "2" or "3"
                  jr      c,batch_send_escz          ; this slot had no file area (no card)...
                  jr      nz,batch_send_escz         ; this slot had no file area (card, but no file area)

                  ld      de,filename_buffer+6       ; search for filename beginning at "/" in filea area of slot C
                  ld      a,EP_Find
                  oz      OS_Epr                     ; search for filename on file eprom...
                  jr      c,batch_send_escz          ; this slot had no file area (no card)...
                  jr      nz,batch_send_escz         ; File Entry was not found...

                  push    bc
                  push    hl                         ; preserve pointer to File Entry...

                  ld      hl, message17
                  oz      GN_sop
                  ld      hl, filename_buffer
                  call    write_message

                  call    get_time                   ; read system time, to know elapsed time of sent file
                  call    Transfer_filename          ; First transmit filename
                  pop     hl
                  pop     bc
                  jr      c,err_batch_send           ; transmission error...

                  call    Send_Esc_F
                  jr      c,err_batch_send           ; transmission error...

                  call    FileEprSendFile            ; transmit single File Entry image to serial port
                  jr      c,err_batch_send           ; transmission error...

                  call    Msg_file_sent              ; display "file sent in xxx" elapsed time

                  call    Send_Esc_E
                  jr      c,err_batch_send           ; transmission error...
                  jr      batch_send_escz            ; completed batch, send ESC Z


; ****************************************************'
; HL points at filename to be sent...
.Transfer_filename
                  CALL Send_ESC_N
                  RET  C
                  PUSH HL
                  CALL Set_Traflag
                  CALL SendString
                  CALL Restore_Traflag
                  POP  HL
                  RET


.Transfer_RamfileImage
                  CALL Send_Esc_F
                  RET  C

                  LD   A, op_in                      ; open file for transfer...
                  CALL Get_file_handle
                  CALL C, System_error
                  RET  C
.trnsf_file_loop  CALL Load_buffer                   ; load new block into buffer...
                  JR   Z, End_transfer_file          ; EOF reached...

                  LD   BC,(buflen)
                  LD   HL,file_buffer                ; start of buffer
                  CALL SendFileBuffer
                  JP   C, Close_file
                  JR   trnsf_file_loop
.end_transfer_file
                  CALL Close_file
                  JP   Send_Esc_E                    ; send EOF file marker to terminal



; ***********************************************************************
.ImpExp_Backup    LD   HL,message16                  ; Backup files to terminal, using ImpExp protocol.
                  CALL Write_message

.Backup_files     CALL Set_Traflag
                  CALL Fetch_pathname                ; without ACKN protocol...
                  CALL Restore_Traflag
                  JP   C, Msg_Command_aborted

                  CALL Write_message                 ; display pathname on screen...
                  CALL SearchFilesystem              ; backup files...
                  RET  C                             ; system error or timeout on serial port
                  LD   HL, message18
                  CALL Write_message                 ; "Searching file system..."
                  CALL Get_Directories               ; for directories in all RAM devices
                  LD   HL, cli_filename

                  CALL Transfer_filename             ; send file with found directories
                  JP   C, Msg_Command_aborted
                  CALL Transfer_RamfileImage
                  JP   C, Msg_Command_aborted
                  JP   Send_ESC_Z                    ; end of files...



; ***********************************************************************
; HL points at string, A = wildcard search specifier
;
.Send_found_names CALL Get_Wcard_handle
                  CALL C,System_error
                  RET  C
                  LD   DE,0                          ; reset counters for found names
                  LD   HL,file_type
.read_names_loop  CALL Find_Next_Match               ; read names matching wildcard
                  JR   NC, fetch_name
                  CCF                                ; Fc = 0, signal completed all names..
                  JR   end_fetch_names
.fetch_name       CP   (HL)                          ; found the wanted file type?
                  JR   NZ, read_names_loop           ; no, get next filename match...
                  EX   DE,HL
                  CALL Send_ESC_N
                  JR   C, end_Fetch_names
                  LD   HL, filename_buffer           ; pointer to start name
                  CALL SendString
                  JR   C, end_Fetch_names
                  EX   DE,HL
                  JR   read_names_loop
.end_fetch_names
                  PUSH AF
                  CALL Close_Wcard_handler
                  POP  AF
                  RET


; ***********************************************************************
.Fetch_pathname   LD   BC,256                        ; upper limit of bytes of filename path to receive (probably never happens)
                  LD   HL,filename_buffer
                  JP   FetchBlock                    ; receive block, ESC terminated 'Z','F' or 'N' byte


; ***********************************************************************
; Scan all slots for File Areas, and send ":EPR.x" accordingly.
.Send_Epr_devices
                  ld      bc,0
.poll_eprdev_loop ld      a,c
                  cp      4
                  jr      z,completed_Send_Epr_device ; all slots polled for Epr file area...
                  push    bc
                  ld      a,EP_Req
                  oz      OS_Epr
                  pop     bc
                  ld      a,c
                  inc     bc                         ; ready for next slot
                  jr      c,poll_eprdev_loop         ; this slot had no file area (no card)... try next
                  jr      nz,poll_eprdev_loop        ; this slot had no file area (car, but no file area)... try next

                  push    bc                         ; preserve slot poll number
                  or      $30                        ; current slot as '0', '1', '2' or '3'
                  call    EprDev_name

                  CALL    Send_ESC_N
                  JR      C, end_Send_Epr_device     ; abort if serial port timed out...
                  LD      HL, filename_buffer        ; pointer to start of EPR.x name
                  CALL    SendString
                  JR      C, end_Send_Epr_device
                  pop     bc
                  jr      poll_eprdev_loop
.completed_Send_Epr_device
                  or      a                          ; return Fc = 0, Fz = 0...
                  ret                                ; ESC Z has to be sent
.end_Send_Epr_device
                  pop     bc
.exit_Send_Epr_device
                  ret
.EprDev_name
                  ld      bc,5
                  ld      de,filename_buffer
                  push    de
                  ld      hl,eprdev
                  ldir
                  ld      (de),a                        ; append slot number
                  inc     de
                  xor     a
                  ld      (de),a                        ; null-terminate
                  pop     de
                  ret


; ***********************************************************************
; Check if there is a File Area in slot A = "0", "1", "2" or "3"
;
; returns Fz = 1, Fc = 0, if file area found, DE = size of cards in 16K banks
; otherwise Fc 1 or Fz = 0
;
.CheckFileAreaOfSlot
                  and     3                          ; strip to raw slot number number (0 - 3)
                  ld      c,a
                  push    bc
                  ld      a,EP_Req
                  oz      OS_Epr
                  ld      d,0
                  ld      e,c                        ; C -> DE = size of card in 16K banks
                  pop     bc
                  ret


; ***********************************************************************
; Path begins with ":RAM." ?
;
; HL = pointer to path containing device / dir names
;
; returns A = slot number ('0', '1', '2' or '3' or '-'), if :RAM.x device recognized (Fz = 1)
;
.CheckRamName
                  push    bc
                  push    de
                  push    hl

                  ld      de,ramdev_wildcard
                  call    CheckDevName

                  pop     hl
                  pop     de
                  pop     bc
                  ret


; ***********************************************************************
; Path begins with ":EPR." ?
;
; HL = pointer to path containing device / dir names
;
; returns A = slot number ('0', '1', '2' or '3'), if :EPR.x device recognized (Fz = 1)
;
.CheckEprName
                  push    bc
                  push    de
                  push    hl

                  ld      de,eprdev
                  call    CheckDevName

                  pop     hl
                  pop     de
                  pop     bc
                  ret


; ***********************************************************************
; Path begins with ":XXX."
;
; HL = pointer to path containing device / dir names
; DE = pointer to device name pattern
;
; returns A = slot number ('0', '1', '2', '3' or '-'), if device is recognized
;
.CheckDevName
                  ex      de,hl
                  ld      b,5
.cmp_loop
                  ld      a,(de)
                  call    ToUpper
                  cp      (hl)
                  ret     nz                         ; Fz = 0, no match
                  inc     hl
                  inc     de
                  djnz    cmp_loop
                  cp      a                          ; Fz = 1, match!
                  ld      a,(de)                     ; get slot number of file area
                  ret


; ***********************************************************************
; Set ":EPR.x" device name at (filename_buffer)
;
; IN: A = slot number ('0', '1', '2' or '3')
;
.SetEprDevName
                  push    bc
                  push    de
                  push    hl

                  ld      bc,5
                  ld      hl,eprdev
                  ld      de,filename_buffer
                  ldir
                  ld      (de),a
                  inc     de
                  xor     a
                  ld      (de),a

                  pop     hl
                  pop     de
                  pop     bc
                  ret


; *************************************************************************************
; Get pointer to first file entry. If only active files are currently displayed,
; deleted file entries are skipped until an active file entry is found.
;
; IN:
;    C = slot number of File area
;
; OUT:
;    Fc = 1, No file area or no file entries in file area.
;    Fc = 0
;         BHL = pointer to first file entry
;
.GetFirstEprFile
                    ld   a,EP_First
                    oz   OS_Epr
                    ret  c
                    ret  nz                          ; an active file entry was found...
                                                     ; Fz = 1, skip deleted file(s) until active file is found
; *************************************************************************************


; *************************************************************************************
; Get pointer to next file entry. If only active files are currently displayed,
; deleted file entries are skipped until an active file entry is found.
;
; IN:
;    BHL = current file entry pointer
;
; OUT:
;    Fc = 1, End of list reached
;    Fc = 0
;         BHL = pointer to next file entry
;
.GetNextEprFile
.fetch_next_fe_loop
                    ld   a,EP_Next
                    oz   OS_Epr
                    ret  c
                    ret  nz                          ; an active file entry was found...
                    jr   fetch_next_fe_loop          ; only active files, scan forward until active file..
; *************************************************************************************
