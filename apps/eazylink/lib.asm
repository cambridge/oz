; **************************************************************************************************
; Library functionality used by EazyLink (migrated from stdlib and optimized for OZ)
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; ***************************************************************************************************

        module LibraryFunctions

        xdef SetBlinkScreen, SetBlinkScreenOn
        xdef FileEprTransferBlockSize
        xdef ApplEprTyper
        xdef MemReadByte, MemReadLong, MemWriteByte, MemWriteWord
        xdef AddPointerDistance, ConvPtrToAddr, ConvAddrToPtr
        xdef divu8, ToUpper, CreateFilename, CreateDirectory

        include "error.def"
        include "memory.def"
        include "blink.def"
        include "fileio.def"
        include "dor.def"
        include "rtmvars.def"


; ***************************************************************************************************
; Switch Z88 LCD Screen On or Off.
;
; In:
;    Fz = 1, Turn LCD screen On
;    Fz = 0, Turn LCD screen Off
;
; Out:
;    None
;
; Registers changed after return:
;    AFBCDEHL/IXIY same
;    ......../.... different
;
; ------------------------------------------------------------------------
; Design & programming by Gunther Strube, Aug 2006
; ------------------------------------------------------------------------
;
.SetBlinkScreen     PUSH AF
                    PUSH BC
                    LD   BC,BLSC_COM         ; Address of soft copy of COM register
                    LD   A,(BC)
                    RES  BB_COMLCDON,A       ; Screen LCD Off by default
                    CALL Z, lcdon            ; but caller wants to switch On...
                    LD   (BC),A
                    OUT  (C),A               ; signal to Blink COM register to do the LCD...
                    POP  BC
                    POP  AF
                    RET
.lcdon              SET  BB_COMLCDON,A       ; Screen LCD On
                    RET

.SetBlinkScreenOn                            ; Public routine to enable LCD without specifying parameters.
                    PUSH AF
                    CP   A
                    CALL SetBlinkScreen
                    POP  AF
                    RET


; ***************************************************************************************************
;
; Read byte at base record pointer in BHL, offset A.
; Byte is returned in A.
;
; If B<>0, the byte is read from extended address.
; If B=0, the byte is read from local address space.
;
;    Register affected on return:
;         ..BCDEHL/IXIY same
;         AF....../.... different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, 1997, Sep 2004, Oct 2005, Aug 2012
; ----------------------------------------------------------------------
;
.MemReadByte        PUSH HL
                    PUSH DE
                    PUSH BC

                    LD   D,0
                    LD   E,A
                    ADD  HL,DE                    ; add offset to pointer

                    LD   C,MS_S1
                    RES  7,H
                    SET  6,H                      ; pointer adjusted for reading bank in segment 1

                    RST  OZ_MPB                   ; page in bank at C = Safe MS_S1 segment
                    LD   A,(HL)                   ; read byte at extended or local address
                    RST  OZ_MPB                   ; restore previous bank binding

                    POP  BC
                    POP  DE
                    POP  HL
                    RET


; **************************************************************************
;
; Add distance CDE (24bit integer) to current pointer address BHL
;
; A new pointer is returned in BHL, preserving original
; slot mask and segment mask.
;
; This routine is primarily used for File Eprom management.
;
; Registers changed after return:
;    AF.CDE../IXIY same
;    ..B...HL/.... different
;
; --------------------------------------------------------------------------
; Design & programming by Gunther Strube, InterLogic, Dec 1997
; --------------------------------------------------------------------------
;
.AddPointerDistance
                    PUSH AF
                    PUSH DE

                    LD   A,C
                    PUSH AF                  ; preserve C register

                    LD   A,H
                    AND  @11000000
                    PUSH AF                  ; preserve segment mask
                    RES  7,H
                    RES  6,H

                    LD   A,B
                    AND  @11000000
                    PUSH AF                  ; preserve slot mask
                    RES  7,B
                    RES  6,B

                    LD   A,C
                    PUSH DE                  ; preserve distance in ADE

                    CALL ConvPtrToAddr       ; BHL -> DEBC address

                    POP  HL
                    ADD  HL,BC
                    LD   B,H
                    LD   C,L
                    ADC  A,E                 ; distance added to DEBC,
                    LD   E,A                 ; result in DEBC, new abs. address

                    CALL ConvAddrToPtr       ; new abs. address to BHL logic...

                    POP  AF
                    OR   B
                    LD   B,A                 ; slot mask restored in bank number

                    POP  AF
                    OR   H
                    LD   H,A                 ; segment mask restored in offset

                    POP  AF
                    LD   C,A                 ; C register restored

                    POP  DE
                    POP  AF
                    RET


; ***************************************************************************
;
; Convert 1MB 20bit address to relative pointer in BHL
; (B = 00h - 3Fh, HL = 0000h - 3FFFh).
;
; This routine is primarily used File Eprom management
;
; IN:
;    EBC = 24bit integer (actually 20bit 1MB address)
;
; OUT:
;    BHL = pointer
;
; Registers changed after return:
;    AF.CDE../IXIY same
;    ..B...HL/.... different
;
; --------------------------------------------------------------------------
; Design & programming by Gunther Strube, InterLogic, Dec 1997
; --------------------------------------------------------------------------
;
.ConvAddrToPtr
                    PUSH AF
                    LD   A,B
                    AND  @11000000
                    LD   H,B
                    RES  7,H
                    RES  6,H
                    LD   L,C                 ; OFFSET READY...

                    LD   B,E                 ; now divide top 6 address bit with 16K
                    SLA  A                   ; and place it into B (bank) register
                    RL   B
                    SLA  A
                    RL   B
                    POP  AF
                    RET                      ; BHL now (relative) ext. address


; ***************************************************************************
;
; Convert relative pointer BHL (B = 00h - 3Fh, HL = 0000h - 3FFFh)
; to absolute 20bit 1MB address.
;
; This routine primarily used for File Eprom Management.
;
; IN:
;    BHL = pointer
;
; OUT:
;    DEBC = 32bit integer (actually 24bit)
;
; Registers changed after return:
;    AF....HL/IXIY same
;    ..BCDE../.... different
;
; ------------------------------------------------------------------------
; Design & programming by Gunther Strube, InterLogic, Dec 1997
; ------------------------------------------------------------------------
;
.ConvPtrToAddr      PUSH AF
                    PUSH HL
                    LD   D,0
                    LD   E,B
                    LD   BC,0
                    SRA  E
                    RR   B
                    SRA  E
                    RR   B
                    ADD  HL,BC               ; DEBC = <BankNumber> * 16K + offset
                    LD   B,H
                    LD   C,L                 ; DEBC = BHL changed to absolute address in Eprom
                    POP  HL
                    POP  AF
                    RET


; ***************************************************************************************************
; File Entry Management:
;       Internal support library routine for FileEprFetchFile & FlashEprCopyFileEntry.
;
; Define a block size to transfer, which is from the current bank offset and within 16K bank boundary,
; considering the remaining file size to copy.
;
; IN:
;    BHL = current pointer to file block data
;    cde' = current file size
;
; OUT:
;    hl' = size of block in File Area to transfer
;    cde' = updated according to adjusted block size (that can be transferred within the bank boundary)
;
; Registers changed after return:
;    ..BCDEHL/IXIY ........ same
;    AF....../.... afbcdehl different
;
.FileEprTransferBlockSize
                    PUSH BC
                    PUSH DE
                    PUSH HL

                    EX   DE,HL
                    LD   HL,$4000                 ; 16K bank boundary...
                    CP   A                        ; Fc = 0
                    SBC  HL,DE                    ; HL = <BankSpace>

                    EXX
                    PUSH DE
                    PUSH BC                       ; get a copy of current file size (CDE)
                    PUSH DE
                    PUSH BC                       ; and preserve a copy...
                    EXX
                    POP  BC
                    POP  DE                       ; divisor in CDE (current size of file)
                    LD   B,0                      ; dividend in BHL (remaining bytes of bank)
                    call Divu24                   ; <blocksize> = <FileSize> MOD <BankSpace>
                    EXX
                    POP  BC
                    POP  DE                       ; (restore current file size)
                    EXX

                    LD   A,H
                    OR   L
                    JR   Z, last_block
                    EXX
                    PUSH DE
                    EXX
                    POP  DE
                    CALL fsize_larger             ; copy remaining bank space from file inside current bank
                    JR   exit_TransferBlockSize
.last_block
                    LD   A,D
                    OR   E                        ; <blocksize> = 0 ?
                    CALL NZ, fsize_larger         ; no, FileSize > BankSpace
                    CALL Z, fsize_smaller         ; Yes, FileSize <= BankSpace
.exit_TransferBlockSize
                    POP  HL
                    POP  DE
                    POP  BC
                    RET
.fsize_smaller
                    EXX                           ; remaining file image to be copied is
                    EX   DE,HL                    ; smaller than <BankSpace>, therefore
                    LD   DE,0                     ; the last image block is resident in the
                    EXX                           ; current bank...
                    RET                           ; HL' = FileSize (max. 16K)
.fsize_larger
                    PUSH AF                       ; size of remaining file image crosses current
                    PUSH DE                       ; bank boundary...
                    EXX                           ; define block size only of <BankSpace> size.
                    POP  HL
                    PUSH HL
                    EX   DE,HL
                    SBC  HL,DE
                    LD   D,H
                    LD   E,L
                    LD   A,C
                    SBC  A,0
                    LD   C,A                      ; FileSize = FileSize - BankSpace
                    POP  HL                       ; HL' = BankSpace ...
                    EXX
                    POP  AF
                    RET

;       BHL/CDE -> BHL=quotient, CDE=remainder
.Divu24
                    push    hl
                    xor     a
                    ld      hl, 0
                    exx                                     ;       alt
                    pop     hl
                    ld      b, 24
.d24_2
                    rl      l
                    rl      h
                    exx                                     ;       main
                    rl      b
                    rl      l
                    rl      h
                    rl      a
                    push    af
                    push    hl
                    sbc     hl, de
                    sbc     a, c
                    ccf
                    jr      c, d24_3
                    pop     hl
                    pop     af
                    or      a
                    jr      d24_4
.d24_3
                    inc     sp
                    inc     sp
                    inc     sp
                    inc     sp
.d24_4
                    exx                                     ;       alt
                    djnz    d24_2

                    rl      l
                    rl      h
                    push    hl
                    exx                                     ;       main
                    rl      b
                    ex      de, hl
                    ld      c, a
                    pop     hl
                    or      a
.d24_5
                    ret


; ******************************************************************************
;
; ToUpper - convert character to upper case if possible.
;
;  IN:    A = ASCII byte
; OUT:    A = converted ASCII byte
;
; Registers changed after return:
;
;    ..BCDEHL/IXIY  same
;    AF....../....  different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, Copyright (C) 1995
; ----------------------------------------------------------------------
;
.ToUpper            CP   '{'       ; if A <= 'z'  &&
                    JR   NC, check_latin1
                    CP   'a'       ; if A >= 'a'
                    RET  C
                    SUB  32        ; then A = A - 32
                    RET
.check_latin1       CP   $FF       ; if A <= $FE  &&
                    RET  Z
                    CP   $E1       ; if A >= $E1
                    RET  C
                    SUB  32        ; then A = A - 32
                    RET


; ******************************************************************************
;
; Create filename and directory path.
;
; Try to create the specified filename using OP_OUT. If a directory path is
; specified, it will automatically be created if not defined in the filing
; system.
;
; The filename must not contain wildcards (standard convention). However if a
; RAM device is not specified, it will automatically be included (the current)
; into the filename.
;
; The buffer of the filename must have space to get itself expanded with a
; device name (additional 6 bytes).
;
; The filename pointer may not point in segment 2, since GN_FEX is unreliable
; in that segment.
;
; The routine deletes an existing file without warning.
;
; in:     bhl = pointer to null-terminated filename, (b = 0, means local)
;
; out, if successful:
;         file handle stored in (file_handle)
;         fc = 0, file (and directory) successfully created
;         (hl) = filename may have been expanded
;
; out, if failed:
;         fc = 1, unable to create file (directory)
;         a = error code:
;              RC_IVF, Invalid filename
;              RC_USE, File already in use
;
;    registers changed after return:
;         ..bcdehl/ixiy  same
;         af....../....  different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, Copyright (C) 1995
; ----------------------------------------------------------------------
;
.CreateFilename     push bc                       ; preserve original BC
                    push de                       ; preserve original DE
                    push hl                       ; preserve original HL
                    push iy                       ; preserve original IY
                    push ix

                    push hl
                    pop  iy                       ; IY points at start of filename
                    inc  b
                    dec  b
                    jr   z, local_filename        ; pointer to filename is local

                         ld   a,h
                         and  @11000000
                         rlca
                         rlca
                         ld   c,a                      ; MS_Sx
                         rst  OZ_MPB                   ; page filename into segment

                         push bc                       ; preserve old bank binding
                         call createfile
                         pop  bc
                         push af                       ; preserve error status...
                         rst  OZ_MPB                   ; restore previous bank binding

                         pop  af
                         jr   exit_createflnm

.local_filename          call createfile

.exit_createflnm    pop  ix
                    pop  iy
                    pop  hl
                    pop  de
                    pop  bc
                    ret


; ******************************************************************************
;
;    Create the filename
;
;    IN:  HL = pointer to filename
;
.createfile         ld   d,h
                    ld   e,l
                    xor  a
                    ld   bc,255
                    cpir                          ; find null-terminator
                    dec  hl
                    ex   de,hl
                    sbc  hl,de                    ; length = end - start
                    ld   c,6
                    add  hl,bc
                    ld   c,l                      ; length of buffer + 6 (max. length 255 bytes)

.openfile           push iy                       ; c = length of buffer
                    pop  hl
                    ld   d,h                      ; hl = pointer to filename
                    ld   e,l                      ; de = pointer to output
                    ld   a, OP_OUT
                    oz   GN_Opf
                    jr   c, check_onf
                    ld   (file_handle),ix
                    ret                           ; file created successfully, return...
.check_onf                    
                    cp   RC_ONF
                    jr   z, createfiledir         ; if error != RC_ONF
                         scf                      ;     return error
                         ret

.createfiledir      oz   GN_Fex                   ; first expand filename
                    ret  c                        ; invalid filename
                    ld   l,a
                    ld   a, RC_IVF                ; pre-load Fc = 1 & A = RC_IVF, if errors
                    scf
                    bit  7,l                      ; wildcards were used...
                    ret  nz
                    bit  1,l
                    ret  z                        ; filename not specified...

                    cp   a                        ; fc = 0
                    dec  b                        ; deduct filename segment
                    dec  b                        ; deduct device name segment
                    ex   de,hl                    ; hl points at null-terminator
                    call createfilepath           ; create directory path...
                    jr   nc, openfile             ; then try to create file again...
                    ret                           ; couldn't create directory, return...


; ******************************************************************************
;
; create directory sub-paths, recursively.
;
; in:     hl = pointer to null-terminator
;         b = number of directory path segments (levels) to create
;         c = length of filename
;
; out:    fc = 0, directory path created successfully, otherwise fc = 1
;
.createfilepath     call find_separator           ; directory separator at (hl)
                    ld   (hl),0                   ; null-terminate directory path segment (level)
                    dec  b
                    push hl
                    call nz, createfilepath       ; if (pathlevel != 0) createpath(pathlevel, sep)
                    pop  hl
                    ret  c                        ; sub-path couldn't be created...
                    call mkdir
                    ld   (hl),'/'                 ; restore directory separator
                    ret

.mkdir              push bc                       ; preserve current level in B
                    push hl                       ; preserve length of scratch buffer in C
                    push iy                       ; preserve pointer to current separator
                    pop  hl
                    ld   b,0                      ; local pointer (filename always paged in)
                    ld   d,h                      ; HL points at start of filename
                    ld   e,l                      ; DE points at scratch buffer
                    ld   a, OP_DIR
                    oz   GN_Opf                   ; mkdir pathname (returns DOR handle)
                    jr   nc, exit_mkdir
                    cp   RC_USE
                    jr   z, quit_mkdir            ; in use (it exists)...
                    cp   RC_EXIS
                    jr   z, quit_mkdir            ; already created...
                    scf
                    jr   quit_mkdir               ; other error occurred
.exit_mkdir         ld   a, dr_fre
                    oz   OS_Dor                   ; free dor handle
.quit_mkdir         pop  hl
                    pop  bc
                    ret



; ******************************************************************************
;
; Create Directory Path.
;
; The directory name must not contain wildcards (standard convention).
; However if a RAM device is not specified, it will automatically be included
; (the current) into the directory name.
;
; The buffer of the directory name must have space enough to get itself expanded
; with a device name (additional 6 bytes).
;
; The filename pointer may not point in segment 2, since GN_FEX is unreliable
; in that segment.
;
; in:     bhl = pointer to null-terminated directory path name, (b = 0, means local)
;
; out, if successful:
;         fc = 0, directory successfully created
;         (hl) = filename may have been expanded
;
; out, if failed:
;         fc = 1, unable to create directory
;         a = error code:
;              RC_IVF, Invalid directory path name
;              RC_USE, Directory in use
;              RC_EXIS, Directory already exists
;
;    registers changed after return:
;         ..bcdehl/..iy  same
;         af....../ix..  different
;
; ----------------------------------------------------------------------
; Design & programming by Gunther Strube, Copyright (C) 1997
; ----------------------------------------------------------------------
;
.CreateDirectory    push bc                       ; preserve original BC
                    push de                       ; preserve original DE
                    push hl                       ; preserve original HL
                    push ix
                    push iy                       ; preserve original IY                    

                    push hl
                    pop  iy                       ; IY points at start of filename
                    inc  b
                    dec  b
                    jr   z, local_dir             ; pointer to filename is local

                         ld   a,h
                         and  @11000000
                         rlca
                         rlca
                         ld   c,a                      ; MS_Sx
                         rst  OZ_MPB                   ; page filename into segment
                         push bc                       ; preserve old bank binding
                         call createdir
                         pop  bc
                         push af                       ; preserve error status...
                         rst  OZ_MPB                   ; restore previous bank binding
                         pop  af
                         jr   exit_createdir

.local_dir               call createdir

.exit_createdir     pop  iy
                    pop  ix
                    pop  hl
                    pop  de
                    pop  bc
                    ret


; ******************************************************************************
;
;    Create the filename
;
;    IN:  HL = pointer to directory path
;
.createdir          ld   b,0                      ; filename available in address space
                    ld   d,h                      ; HL points at start of filename
                    ld   e,l                      ; DE points at scratch buffer
                    ld   a, OP_DOR
                    oz   GN_Opf                   ; try to open directory path (DOR record)...
                    jr   c, try_create            ; an error occurred (presumably not found)
                         ld   a, dr_fre                ; directory was opened!
                         oz   OS_Dor                   ; free dor handle
                         ld   a, RC_EXIS
                         scf                           ; return "directory already created!"
                         ret
.try_create         push hl
                    xor  a
                    ld   bc,255
                    cpir                          ; find null-terminator
                    ld   a,255
                    inc  c
                    sub  c
                    add  a,6
                    ld   c,a                      ; length of buffer + 6 (max. length 255 bytes)
                    pop  hl

                    ld   d,h                      ; B = 0, C = length of buffer
                    ld   e,l                      ; DE points at output buffer...
                    oz   GN_Fex                   ; first expand filename
                    ret  c                        ; invalid filename

                                                  ; name expanded successfully,
                                                  ; DE points at null-terminator
                    ld   l,a
                    ld   a, RC_IVF                ; pre-load Fc = 1 & A = RC_IVF, if errors
                    scf
                    bit  7,l                      ; wildcards were used...
                    ret  nz
                    bit  1,l
                    ret  z                        ; filename not specified...

                    cp   a                        ; fc = 0
                    ex   de,hl                    ; hl points at null-terminator
                                                  ; create directory path...


; ******************************************************************************
;
;    create directory sub-paths, recursively.
;
; in:     hl = pointer to null-terminator
;         b = number of directory path segments (levels) to create
;         c = length of filename
;
; out:    fc = 0, directory path created successfully, otherwise fc = 1
;

.createpath         push hl
                    ld   d,(hl)                   ; preserve separator/null
                    ld   (hl),0                   ; this segment is en of path
                    push de
                    dec  b
                    ld   a,1                      ; only dev & root directory left..
                    cp   b
                    call nz, find_separator       ; more than one sub directory
                    call nz, createpath
                    call mkdir                    ; now create this sub directory
                    pop  de
                    pop  hl
                    ld   (hl),d                   ; restore separator/null
                    ret

.find_separator     push af                       ; scan for end of previous segment
.find_sep_loop      ld   a, '/'
                    cp   (hl)
                    jr   z, found_separator       ; directory separator found at (hl)
                    dec  hl
                    jr   find_sep_loop
.found_separator    pop  af                       ; HL points at segment separator
                    ret
