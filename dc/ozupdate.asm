; **************************************************************************************************
; DC_Upd, OZ Update functionality, based on RomUpdate implementation, optimized for OZ integration.
;
; This file is part of the Z88 operating system, OZ.     0000000000000000      ZZZZZZZZZZZZZZZZZZZ
;                                                       000000000000000000   ZZZZZZZZZZZZZZZZZZZ
; OZ is free software; you can redistribute it and/    0000            0000              ZZZZZ
; or modify it under the terms of the GNU General      0000            0000            ZZZZZ
; Public License as published by the Free Software     0000            0000          ZZZZZ
; Foundation; either version 2, or (at your option)    0000            0000        ZZZZZ
; any later version. OZ is distributed in the hope     0000            0000      ZZZZZ
; that it will be useful, but WITHOUT ANY WARRANTY;    0000            0000    ZZZZZ
; without even the implied warranty of MERCHANTA-       000000000000000000   ZZZZZZZZZZZZZZZZZZZZ
; BILITY or FITNESS FOR A PARTICULAR PURPOSE. See        0000000000000000  ZZZZZZZZZZZZZZZZZZZZ
; the GNU General Public License for more details.
; You should have received a copy of the GNU General Public License along with OZ; see the file
; COPYING. If not, write to:
;                                  Free Software Foundation, Inc.
;                                  59 Temple Place-Suite 330,
;                                  Boston, MA 02111-1307, USA.
;
; (C) Gunther Strube (gstrube@gmail.com), 2016
; (C) Thierry Peycru (pek@users.sf.net), 2016
;
; ***************************************************************************************************

Module OzUpdate

xdef DCUpd
xref SetOsfError

include "memory.def"
include "flashepr.def"
include "blink.def"
include "sysvar.def"
include "oz.def"
include "syspar.def"
include "error.def"
include "../os/lowram/lowram.def"



; ***************************************************************************************************
;
; Update OZ in slot 0/1, defined via "ozupdate.cfg" file and OZ ROM bank files stored in :RAM.*
;
; The config file has been parsed for OZ ROM target and bank files by Index command. This system call
; erases the AMD 512K flash chip, blows the bank files from RAM into the slot, completed by
; a soft reset.
;
; There's no fall-back if a partial update fails; the end-user must recover OZ manually.
;
; IN:
;         B = total of OZ ROM banks to flash
;         C = slot 0/1 to blow OZ ROM
;         IX = points to array space of max. 64 OZ ROM banks (1024K). Each entry has the following format:
;               -----------------------------------------------------------------------------------
;               byte 0:                        byte 1:                        byte 2:
;               [first 64 byte sector of file] [bank of first sector]         destination bank in slot
; OUT:
;         Success:
;              Z88 soft rest and return to Index in new, booted OZ.
;         Failure:
;              Fc = 1
;              A = RC_Btl, Batt Low condition
;              A = RC_NFE, flash card not found in slot
;              A = RC_Wp, Intel Flash is "write-protected" in slot 0/1
;
; ---------------------------------------------------------------
; Design & programming by:
;    Gunther Strube,
;       (RomUpdate) Dec 1997-Apr 1998, Aug 2004, Aug 2006
;       (OZ backport) Aug 2016
; OZ optimisations:
;    Thierry Peycru, Aug 2016
; ---------------------------------------------------------------
;
.DCUpd
; ----------------------------------------------------------------------------------------------------------------------
; first, flash chip presence in slot and check batlow condition
; ----------------------------------------------------------------------------------------------------------------------
        ld      a, FEP_CDID
        oz      OS_Fep                          ; check for flash chip in slot C
        jr      nc, flashcard_found
        ld      a,RC_NFE
        jp      SetOsfError                     ; card not found in slot...
.flashcard_found
        cp      FE_28F                          ; A = flash type is FE_29F or FE_28F, is this an Intel flash chip?
        jr      nz, continue_DCUpd              ; No, we found an AMD/STM/AMIC Flash chip (erase/write allowed in slot 0/1, Fc=0, Fz=1)
        ld      a,RC_Wp                         ; indicate that Intel Flash is "write-protected"..
        jp      SetOsfError                     ; no erase/write support in slot 0,1 or 2 with Intel Flash...

.continue_DCUpd
        ld      (iy+OSFrame_D),b                ; OS_Fep, A = FEP_CDID returned B = size of chip in 16K banks

        ld      bc, NQ_Btl
        oz      OS_Nq
        ld      a, RC_Btl
        jp      z,SetOsfError

; ----------------------------------------------------------------------------------------------------------------------
; before erasing slot X (wiping out the current OZ ROM code) and programming the bank files to slot X, disable INT and
; patch the NMI vectors in lower 8K RAM to return immediately (executing no functionality).
; This prevents any accidental interrupt being executed into a non-existing OZ ROM - or even worse - Flash memory
; that is not in Read Array Mode!
; ----------------------------------------------------------------------------------------------------------------------
        di                                      ; Disable Z80 CPU maskable int's
        ld      a, $C9                          ; ret ($C9) instruction replacing jump ($C3)
        ld      (OZ_NMI), a                     ; ret will avoid NMI in case of power failure, flap open, card insertion

; ----------------------------------------------------------------------------------------------------------------------
; Switch Z88 LCD Screen Off, don't let Blink interfere with font bitmap reading, when flash is in command mode
        ld      bc,BLSC_COM                     ; address of soft copy of com register
        ld      a,(bc)
        res     BB_COMLCDON,a                   ; screen lcd off
        ld      (bc),a
        out     (c),a                           ; signal to blink com register to do the lcd...

; ----------------------------------------------------------------------------------------------------------------------
; Disable Interrupts getting out of Blink, don't let any interrupts out of Blink while OZ is flashed to slot 0/1...
        ld      bc,BLSC_INT
        ld      a,(bc)
        res     BB_INTGINT,a                    ; (update soft copy first)
        ld      (bc),a
        out     (c),a                           ; no interrupts get out of blink (GINT = 0)

; ----------------------------------------------------------------------------------------------------------------------
; Erase card, then install OZ banks in slot C, identified by ozbanks[] array. Routine is executed on bottom of LOWRAM Stack
; ----------------------------------------------------------------------------------------------------------------------
        ld      hl,STARTFLASHROUTINE
        ld      de,SV_STACK_RAM
        ld      bc, ENDFLASHROUTINE - STARTFLASHROUTINE + 1
        ldir                                    ; copied erase + flash routines to RAM area

        ld      c,(iy+OSFrame_C)                ; C = slot number
        ld      b,(iy+OSFrame_D)                ; B = card size (total banks) in slot C
        jp      SV_STACK_RAM                    ; Execute on bottom of LOWRAM Stack, then soft reset...

.STARTFLASHROUTINE
        rrc     b                               ; Erase the individual sectors, one at a time
        rrc     b                               ; total of 16K banks on card -> total of 64K sectors on card.
        dec     b                               ; sectors, from (total sectors-1) downwards and including 0

.erase_2xF_card_blocks
        push    bc

        ld      a,b
        and     @00001111                       ; sector number range is only 0 - 15...
        add     a,a                             ; sector number * 4 (16K * 4 = 64K!)
        add     a,a                             ; (convert to first bank no of sector)
        ld      d,a

        ld      a,c
        and     @00000011                       ; only slots 0, 1, 2 or 3 possible
        jr      z, calc_bankno                  ; we're in slot 0, so flash chip can only be in lower 512K of slot 0
        set     5,d                             ; then use upper 512K address lines (to be compatible with hybrid card)
.calc_bankno
        rrca
        rrca                                    ; Converted to Slot mask $40, $80 or $C0
        or      d                               ; the absolute bank which is the bottom of the sector
        ld      d,a                             ; preserve a copy of bank number in D
.erase_sector
        ld      b,a                             ; bind sector to
        ld      c, MS_S1                        ; segment 1 (segment 2 & 3 contains OZ kernel banks)
        ld      hl,MM_S1 << 8                   ; HL points into segment
        rst     OZ_MPB

; ----------------------------------------------------------------------------------------------------------------------
; Erase block function in LOWRAM, for AMD 29Fxxxx Flash Memory, which is bound
; into segment x that HL points into.
;
;    HL = points into bound Flash Memory sector
;
; Registers changed after return:
;    ......../IXIY same
;    AFBCDEHL/.... different
;
        call    SV_STACK_RAM + (AM29Fx_PrepCmdMode-STARTFLASHROUTINE)
        call    AM29Fx_EraseSector              ; Erase sector in LOWRAM (ignore AF status of erased sector - we can't go back now),

        pop     bc                              ; total sectors left to erase...
        dec     b
        ld      a,b
        cp      -1
        jr      nz, erase_2xF_card_blocks

; -----------------------------------------------------------------------------------
; card in slot erased, get ready to blow bank stuff...
; -----------------------------------------------------------------------------------

        ld      b,(iy+OSFrame_B)                ; total of banks to update to slot X...
.update_ozrom_loop
        push    bc

        ld      (iy+OSFrame_D),MM_S2            ; start of bank (in S2) for destination in slot
        ld      (iy+OSFrame_E),0                ; preserve destination pointer to current card bank

; -----------------------------------------------------------------------------------
; Flash the RAM bank file contents directly to destination bank in slot.
; This routine is CALL'ed by InstallOZ when the OZ images reside in a RAM filing system.
;
; IX points to a three byte data block that contains pointers to start of the file and slot bank:
; -----------------------------------------------------------------------------------
; byte 0:                        byte 1:                        byte 2:
; [first 64 byte sector of file] [bank of first sector]         destination bank in slot

        ld      a,(ix+0)                        ; get first sector number
        ld      b,(ix+1)                        ; bank (of sector) to bind into segment...
.blowbank_loop
        ld      c,MS_S1                         ; Use segment 1 to bind in bank of sector
        rst     OZ_MPB                          ; current sector bound into segment...

; -----------------------------------------------------------------------------------
; Convert [File Sector Number, Bank] to extended memory pointer.
; Bank number = 0 evaluation (the last sector in the file) is not handled.
;
; IN:
;    A = Sector number (64 byte file sector)
;
; OUT:
;    HL = pointer to start of 64 byte sector (HL = applied to segment 1)
;
        ld      h, a
        ld      l, 0
        srl     h
        rr      l
        rr      h                               ; HL = 00eeeeee 00000000
        rr      l                               ;   -> 0000eeee ee000000
        set     6,h                             ; HL points to start of sector in segment 1
        res     7,h
; -----------------------------------------------------------------------------------

        ld      c,(hl)                          ; get next file sector number
        inc     hl
        ld      a,(hl)                          ; get bank of next file sector number
        inc     hl                              ; point at start of current file sector contents
        or      a
        ld      b,a
        push    bc                              ; preserve sector for next iteration of blow bank file
        jr      z, blowsector                   ; if bank number = 0: then this is the last sector (c = length of sector)
        ld      c,62                            ; if bank number <> 0, then (complete) sector contents is 62 bytes...
.blowsector

        ld      b,(ix+2)                        ; sector to be blown to bank no. in slot X
        res     7,b
        res     6,b                             ; remove slot mask of original destination bank number for bank file
        ld      a,(iy+OSFrame_C)                ; slot number
        rrca
        rrca                                    ; slot number -> slot mask
        or      b
        ld      b,a                             ; bank number in ozupdate.cfg converted to specified slot "OZ.x" number

        ld      a,c                             ; preserve size of sector in A (temporarily)
        ld      c,MS_S2
        rst     OZ_MPB                          ; segment 2 is now bound with bank of slot to be blown
        ld      c,a

        ld      d,(iy+OSFrame_D)                ; DE = current destination offset into bank (bound in S2)
        ld      e,(iy+OSFrame_E)

.blowsector_loop                                ; HL = pointer to first byte of sector to blow (bound in S1), C = size of sector
        push    bc
        push    de
        push    de
        exx
        pop     hl                              ; address of byte to be flashed in 'hl
        exx
        push    hl

        ld      a,(hl)                          ; get byte from sector
        ex      de,hl                           ; HL points at address to flash byte
        call    SV_STACK_RAM + (AM29Fx_PrepCmdMode-STARTFLASHROUTINE)     ; prepare the AMD command mode sequence
        call    AM29Fx_BlowByte                                           ; and blow the byte to the flash chip

        pop     hl
        inc     hl
        pop     de
        inc     de
        pop     bc
        dec     c
        jr      nz, blowsector_loop             ; blow next byte of sector...
        ld      (iy+OSFrame_D),d
        ld      (iy+OSFrame_E),e                ; sector has been flashed, preserve pointer to next byte in bank to be blown for next sector

        pop     bc
        xor     a
        or      b                               ; (bank of) last sector was 0 (EOF)?
        jr      z, nextozbank                   ; all sectors flashed of 16K RAM bank file in slot, get next RAM bank file...

        ld      a,c                             ; still more sectors to flash; A = next sector number, B = bank of next sector
        jr      blowbank_loop

.nextozbank
        inc     ix
        inc     ix
        inc     ix                              ; point at next bank data entry
        pop     bc
        djnz    update_ozrom_loop               ; blow next bank to slot...

        ld      a, $FF
        ld      (ubUpdated), a                  ; update successful

; ----------------------------------------------------------------------------------------------------------------------
; OZ ROM banks have been programmed to slot 0 or 1.
; Finally, it's time to issue a soft reset with the updated OZ ROM via LOWRAM RST 00H
; ----------------------------------------------------------------------------------------------------------------------
        ld      bc,BLSC_INT
        ld      a,(bc)
        set     BB_INTGINT,a                    ; enable interrupts from blink
        ld      (bc),a
        out     (c),a                           ; interrupts get out of blink (GINT = 1)
        ld      a, $C3                          ; jump instruction
        ld      (OZ_NMI), a                     ; enable NMI

        ld      a, BM_INTTIME | BM_INTGINT      ; ensure that RTC int's can fire again
        out     (BL_ACK), a
        ld      a,BM_TACKMIN | BM_TACKSEC | BM_TACKTICK ; acknowledge any RTC interrupts that were signalled during update
        out     (BL_TACK), a
        ei                                      ; enable INT
        rst     00h                             ; then soft reset to boot into new OZ ROM code

; ----------------------------------------------------------------------------------------------------------------------
; Prepare AMD Command Mode sequense addresses.
; HL points into bound bank of Flash Memory
; Out:
;       BC = $aa55
;       DE = address $x2AA  (derived from HL)
;       HL = address $x555  (derived from HL)
.AM29Fx_PrepCmdMode
        push    af
        ld      bc,$aa55                        ; B = Unlock cycle #1 code, C = Unlock cycle #2 code
        ld      a,h
        and     @11000000
        ld      d,a
        or      $05
        ld      h,a
        ld      l,c                             ; HL = address $x555
        set     1,d
        ld      e,b                             ; DE = address $x2AA
        pop     af
        ret
.ENDFLASHROUTINE
